namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.PublishSetting", new[] { "PublishSettingID" });
            DropIndex("dbo.ResultSetting", new[] { "ResultSettingID" });
            DropPrimaryKey("dbo.PublishSetting");
            DropPrimaryKey("dbo.ResultSetting");
            AlterColumn("dbo.PublishSetting", "PublishSettingID", c => c.Int(nullable: false));
            AlterColumn("dbo.ResultSetting", "ResultSettingID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.PublishSetting", "PublishSettingID");
            AddPrimaryKey("dbo.ResultSetting", "ResultSettingID");
            CreateIndex("dbo.PublishSetting", "PublishSettingID");
            CreateIndex("dbo.ResultSetting", "ResultSettingID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ResultSetting", new[] { "ResultSettingID" });
            DropIndex("dbo.PublishSetting", new[] { "PublishSettingID" });
            DropPrimaryKey("dbo.ResultSetting");
            DropPrimaryKey("dbo.PublishSetting");
            AlterColumn("dbo.ResultSetting", "ResultSettingID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.PublishSetting", "PublishSettingID", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ResultSetting", "ResultSettingID");
            AddPrimaryKey("dbo.PublishSetting", "PublishSettingID");
            CreateIndex("dbo.ResultSetting", "ResultSettingID");
            CreateIndex("dbo.PublishSetting", "PublishSettingID");
        }
    }
}
