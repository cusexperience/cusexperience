namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewMigration1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PublishSetting", "PublishSettingID", "dbo.Survey");
            DropForeignKey("dbo.ResultSetting", "ResultSettingID", "dbo.Survey");
            DropIndex("dbo.PublishSetting", new[] { "PublishSettingID" });
            DropIndex("dbo.ResultSetting", new[] { "ResultSettingID" });
            DropTable("dbo.PublishSetting");
            DropTable("dbo.ResultSetting");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ResultSetting",
                c => new
                    {
                        ResultSettingID = c.Int(nullable: false),
                        IsAllowViewSocial = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ResultSettingID);
            
            CreateTable(
                "dbo.PublishSetting",
                c => new
                    {
                        PublishSettingID = c.Int(nullable: false),
                        IsAllowViewResult = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.PublishSettingID);
            
            CreateIndex("dbo.ResultSetting", "ResultSettingID");
            CreateIndex("dbo.PublishSetting", "PublishSettingID");
            AddForeignKey("dbo.ResultSetting", "ResultSettingID", "dbo.Survey", "SurveyID");
            AddForeignKey("dbo.PublishSetting", "PublishSettingID", "dbo.Survey", "SurveyID");
        }
    }
}
