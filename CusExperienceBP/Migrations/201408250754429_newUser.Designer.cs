// <auto-generated />
namespace CusExperience.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class newUser : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(newUser));
        
        string IMigrationMetadata.Id
        {
            get { return "201408250754429_newUser"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
