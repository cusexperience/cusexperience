namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HierarchyMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SurveyConfig", "SurveyURL", c => c.String());
            AddColumn("dbo.SurveyConfig", "TouchpointURL", c => c.String());
            AddColumn("dbo.SurveyConfig", "Status", c => c.String());
            AddColumn("dbo.SurveyConfig", "OrgSub_OrgSubID", c => c.Long());
            AddColumn("dbo.SurveyConfig", "Touchpoint_TouchpointID", c => c.Long());
            CreateIndex("dbo.SurveyConfig", "OrgSub_OrgSubID");
            CreateIndex("dbo.SurveyConfig", "Touchpoint_TouchpointID");
            AddForeignKey("dbo.SurveyConfig", "OrgSub_OrgSubID", "dbo.OrgSub", "OrgSubID");
            AddForeignKey("dbo.SurveyConfig", "Touchpoint_TouchpointID", "dbo.Touchpoint", "TouchpointID");
            DropColumn("dbo.SurveyConfig", "ConfigURL");
            DropColumn("dbo.SurveyConfig", "TouchpointName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SurveyConfig", "TouchpointName", c => c.String());
            AddColumn("dbo.SurveyConfig", "ConfigURL", c => c.String());
            DropForeignKey("dbo.SurveyConfig", "Touchpoint_TouchpointID", "dbo.Touchpoint");
            DropForeignKey("dbo.SurveyConfig", "OrgSub_OrgSubID", "dbo.OrgSub");
            DropIndex("dbo.SurveyConfig", new[] { "Touchpoint_TouchpointID" });
            DropIndex("dbo.SurveyConfig", new[] { "OrgSub_OrgSubID" });
            DropColumn("dbo.SurveyConfig", "Touchpoint_TouchpointID");
            DropColumn("dbo.SurveyConfig", "OrgSub_OrgSubID");
            DropColumn("dbo.SurveyConfig", "Status");
            DropColumn("dbo.SurveyConfig", "TouchpointURL");
            DropColumn("dbo.SurveyConfig", "SurveyURL");
        }
    }
}
