namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExpertComments1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ExpertComment",
                c => new
                    {
                        ExpertCommentID = c.Long(nullable: false, identity: true),
                        Comment = c.String(),
                        CommentDate = c.DateTime(),
                        Status = c.String(),
                        module_ModuleID = c.Long(),
                        SurveyResponse_SurveyResponseID = c.Long(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ExpertCommentID)
                .ForeignKey("dbo.Module", t => t.module_ModuleID)
                .ForeignKey("dbo.SurveyResponse", t => t.SurveyResponse_SurveyResponseID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.module_ModuleID)
                .Index(t => t.SurveyResponse_SurveyResponseID)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ExpertComment", "User_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.ExpertComment", "SurveyResponse_SurveyResponseID", "dbo.SurveyResponse");
            DropForeignKey("dbo.ExpertComment", "module_ModuleID", "dbo.Module");
            DropIndex("dbo.ExpertComment", new[] { "User_Id" });
            DropIndex("dbo.ExpertComment", new[] { "SurveyResponse_SurveyResponseID" });
            DropIndex("dbo.ExpertComment", new[] { "module_ModuleID" });
            DropTable("dbo.ExpertComment");
        }
    }
}
