namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class yeekai : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.OrgSub", new[] { "ParentOrgSub_OrgSubID" });
            CreateIndex("dbo.OrgSub", "parentOrgSub_OrgSubID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.OrgSub", new[] { "parentOrgSub_OrgSubID" });
            CreateIndex("dbo.OrgSub", "ParentOrgSub_OrgSubID");
        }
    }
}
