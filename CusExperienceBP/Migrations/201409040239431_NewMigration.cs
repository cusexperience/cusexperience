namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Survey", "CreatedBy_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Survey", "CreatedBy_Id");
            AddForeignKey("dbo.Survey", "CreatedBy_Id", "dbo.AspNetUsers", "Id");
            DropColumn("dbo.Survey", "CreatedBy");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Survey", "CreatedBy", c => c.String());
            DropForeignKey("dbo.Survey", "CreatedBy_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Survey", new[] { "CreatedBy_Id" });
            DropColumn("dbo.Survey", "CreatedBy_Id");
        }
    }
}
