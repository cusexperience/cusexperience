namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SocialView : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Survey", "IsAllowViewSocial", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Survey", "IsAllowViewSocial");
        }
    }
}
