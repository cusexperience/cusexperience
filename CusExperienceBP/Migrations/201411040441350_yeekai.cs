namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class yeekai : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Touchpoint", "OrgSub_OrgSubID", "dbo.OrgSub");
            DropIndex("dbo.Touchpoint", new[] { "OrgSub_OrgSubID" });
            AddColumn("dbo.OrgSub", "Status", c => c.String());
            AddColumn("dbo.Touchpoint", "RegisteredName", c => c.String());
            AddColumn("dbo.Touchpoint", "Address", c => c.String());
            AddColumn("dbo.Touchpoint", "BrandWebsite", c => c.String());
            AddColumn("dbo.Touchpoint", "PhoneNumber", c => c.String());
            AddColumn("dbo.Touchpoint", "EmailAddress", c => c.String());
            AlterColumn("dbo.Touchpoint", "OrgSub_OrgSubID", c => c.Long(nullable: false));
            CreateIndex("dbo.Touchpoint", "OrgSub_OrgSubID");
            AddForeignKey("dbo.Touchpoint", "OrgSub_OrgSubID", "dbo.OrgSub", "OrgSubID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Touchpoint", "OrgSub_OrgSubID", "dbo.OrgSub");
            DropIndex("dbo.Touchpoint", new[] { "OrgSub_OrgSubID" });
            AlterColumn("dbo.Touchpoint", "OrgSub_OrgSubID", c => c.Long());
            DropColumn("dbo.Touchpoint", "EmailAddress");
            DropColumn("dbo.Touchpoint", "PhoneNumber");
            DropColumn("dbo.Touchpoint", "BrandWebsite");
            DropColumn("dbo.Touchpoint", "Address");
            DropColumn("dbo.Touchpoint", "RegisteredName");
            DropColumn("dbo.OrgSub", "Status");
            CreateIndex("dbo.Touchpoint", "OrgSub_OrgSubID");
            AddForeignKey("dbo.Touchpoint", "OrgSub_OrgSubID", "dbo.OrgSub", "OrgSubID");
        }
    }
}
