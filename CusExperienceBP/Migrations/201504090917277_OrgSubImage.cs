namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrgSubImage : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Touchpoint", "RegisteredName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Touchpoint", "RegisteredName", c => c.String());
        }
    }
}
