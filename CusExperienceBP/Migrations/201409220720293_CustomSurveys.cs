namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomSurveys : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Survey", "PublishPage", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Survey", "PublishPage");
        }
    }
}
