namespace CusExperience.Migrations
{
    using CusExperience.DAL;
    using CusExperience.Entities;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Security;
    using WebMatrix.WebData;


    public class Configuration : DbMigrationsConfiguration<CusExperience.DAL.CusExpContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;

        }

        protected override void Seed(CusExpContext context)
        {
             context.Configuration.LazyLoadingEnabled = true;
             //CreateRoles(context);
             //AddAdministratorUsers(context);
            // AddAdministratorRoles(context);
            // AddOrganisationUsers(context);
            // AddOrganisationRoles(context);
           //  AddSurvey(context);
        }

        public void CreateRoles(CusExpContext context)
        {
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            var role1 = new IdentityRole("Administrator");
            RoleManager.Create(role1);

            var role2 = new IdentityRole("CusXPProfessional");
            RoleManager.Create(role2);

            var role3 = new IdentityRole("Executive");
            RoleManager.Create(role3);

            var role4 = new IdentityRole("Manager");
            RoleManager.Create(role4);

            var role5 = new IdentityRole("Frontliner");
            RoleManager.Create(role5);

            var role6 = new IdentityRole("SuperAdministrator");
            RoleManager.Create(role6);

            context.SaveChanges();

        }

        public void AddAdministratorUsers(CusExpContext context)
        {

                var UserManager = new CusExpUserManager(new UserStore<CusExpUser>(context));
                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

                string password = "p@ssword1";
                var user = new CusExpUser
                {
                    FirstName = "Sathish Kumar",
                    LastName = "Meganathan",
                    Title = "Mr",
                    UserName = "sathishm",
                    Email = "s@cusjo.com",
                    PhoneNumber = "+6586212697",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now
                };
                UserManager.Create(user, password);

                var userB = new CusExpUser
                {
                    FirstName = "Yee Kai",
                    LastName = "Boo",
                    Title = "Mr",
                    UserName = "yeekai",
                    Email = "b@cusjo.com",
                    PhoneNumber = "+6591133910",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now
                };
                UserManager.Create(userB, password);

                var userM = new CusExpUser
                {
                    FirstName = "Manoj",
                    LastName = "Sharma",
                    Title = "Mr",
                    UserName = "manojsharma",
                    Email = "b@cusjo.com",
                    PhoneNumber = "+6591133910",
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now
                };
                UserManager.Create(userM, password);
                context.SaveChanges();
           
        }

        public void AddAdministratorRoles(CusExpContext context)
        {

            var UserManager = new CusExpUserManager(new UserStore<CusExpUser>(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            CusExpUser adminUser1 = context.Users.Where(u => u.UserName == "sathishm").FirstOrDefault();
            CusExpUser adminUser2 = context.Users.Where(u => u.UserName == "yeekai").FirstOrDefault();
            CusExpUser adminUser3 = context.Users.Where(u => u.UserName == "manojsharma").FirstOrDefault();
            /* Change the User ID of the Created Users */
            UserManager.AddToRole(adminUser1.Id, "SuperAdministrator");
            UserManager.AddToRole(adminUser2.Id, "SuperAdministrator");
            context.SaveChanges();
        }

        public void AddOrganisationUsers(CusExpContext context)
        {
            try
            {
            var UserManager = new CusExpUserManager(new UserStore<CusExpUser>(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            OrgSub cusXPOrg = new OrgSub
            {
                RegisteredName = "CusXP PTE Limited",
                RegisteredNumber = "NA",
                BrandName = "CusXP",
                BrandWebsite = "http://www.cusexperience.com",
                City = "Singapore",
                Address = "Block 79, Ayer Rajah Avenue",
                PhoneNumber = "+65 86212697",
                EmailAddress = "hi@cusjo.com",
                Industry = "Technology",
                Country = "Singapore",
                State = "Singapore",
                SurveyURL = "CusXP"
            };
            context.OrgSubs.Add(cusXPOrg);
            context.SaveChanges();

            OrgSub custOrg = new OrgSub
            {
                RegisteredName = "Great Eastern Life",
                RegisteredNumber = "NA",
                BrandName = "SKM",
                BrandWebsite = "http://kindness.sg/",
                City = "Singapore",
                Address = "1 Pickering Street, Singapore",
                PhoneNumber = "+65 6248 28888",
                EmailAddress = "sg@greateasternlife.com",
                Industry = "Insurance",
                Country = "Singapore",
                State = "Singapore",
                SurveyURL = "GEL"
            };
            context.OrgSubs.Add(custOrg);
            context.SaveChanges();


            string passwordCE1 = "p@ssword1";
            var userCE1 = new CusExpUser
            {
                FirstName = "Sathish Kumar",
                LastName = "Meganathan",
                Title = "Mr.",
                UserName = "CusXPAdmin1",
                Email = "s@cusjo.com",
                PhoneNumber = "+6586212697",
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
            };
            UserManager.Create(userCE1, passwordCE1);
            context.SaveChanges();

            string passwordCE2 = "p@ssword1";
            var userCE2 = new CusExpUser
            {
                FirstName = "Yee Kai",
                LastName = "Boo",
                Title = "Mr.",
                UserName = "CusXPAdmin2",
                Email = "b@cusjo.com",
                PhoneNumber = "+6591133910",
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
            };
            UserManager.Create(userCE2, passwordCE2);
            context.SaveChanges();

            string passwordC = "P@ssword123";
            var userC = new CusExpUser
            {
                FirstName = "Great Eastern",
                LastName = "Life",
                Title = "Mr",
                UserName = "CusAdmin",
                Email = "gel@cusjo.com",
                PhoneNumber = "+6588888888",
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now,
            };
            UserManager.Create(userC, passwordC);
            context.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                        validationErrors.Entry.Entity.GetType().FullName,
                        validationError.PropertyName,
                        validationError.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public void AddOrganisationRoles(CusExpContext context)
        {

            var UserManager = new CusExpUserManager(new UserStore<CusExpUser>(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            CusExpUser adminUser1 = context.Users.Where(u => u.UserName == "CusXPAdmin1").FirstOrDefault();
            CusExpUser adminUser2 = context.Users.Where(u => u.UserName == "CusXPAdmin2").FirstOrDefault();
            CusExpUser cusUser = context.Users.Where(u => u.UserName == "CusAdmin").FirstOrDefault();
            /* Change the User ID of the Created Users */
            UserManager.AddToRole(adminUser1.Id, "Administrator");
            UserManager.AddToRole(adminUser2.Id, "Administrator");
            UserManager.AddToRole(cusUser.Id, "Administrator");
            context.SaveChanges();
        }

        protected void AddSurvey(CusExpContext context)
        {
            Module m1 = new Module
            {
                ModuleData = "{\"WelcomeMessage\": null,\"FrontImage\": null,\"LogoImage\": null}",
                ModuleType = "Welcome",
                Position = 1
            };

            Module m2 = new Module
            {
                ModuleData = "{\"ThanksText\": null,\"DescriptionText\": null,\"SubmitText\": null}",
                ModuleType = "Submit",
                Position = 2
            };

            var modules = new List<Module> {
                m1,
                m2
            };

            Survey s1 = new Survey { SurveyTitle = "Use Sample Survey", CreatedDate = DateTime.Now, CreatedBy = null, IsTemplate = true, IsPredefined = true, TemplateName = "Sample", Modules = modules };

            context.Surveys.Add(s1);
            context.SaveChanges();
        }
    }
}
