namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class boo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ModuleResponse",
                c => new
                    {
                        ModuleResponseID = c.Long(nullable: false, identity: true),
                        ModuleData = c.String(),
                        ModuleType = c.String(),
                        Module_ModuleID = c.Long(nullable: false),
                        SurveyResponse_SurveyResponseID = c.Long(),
                    })
                .PrimaryKey(t => t.ModuleResponseID)
                .ForeignKey("dbo.Module", t => t.Module_ModuleID, cascadeDelete: true)
                .ForeignKey("dbo.SurveyResponse", t => t.SurveyResponse_SurveyResponseID)
                .Index(t => t.Module_ModuleID)
                .Index(t => t.SurveyResponse_SurveyResponseID);
            
            CreateTable(
                "dbo.Module",
                c => new
                    {
                        ModuleID = c.Long(nullable: false, identity: true),
                        ModuleData = c.String(),
                        ModuleType = c.String(),
                        Page = c.Int(nullable: false),
                        Position = c.Int(nullable: false),
                        Survey_SurveyID = c.Int(),
                    })
                .PrimaryKey(t => t.ModuleID)
                .ForeignKey("dbo.Survey", t => t.Survey_SurveyID)
                .Index(t => t.Survey_SurveyID);
            
            CreateTable(
                "dbo.OrgSub",
                c => new
                    {
                        OrgSubID = c.Long(nullable: false, identity: true),
                        RegisteredName = c.String(nullable: false),
                        RegisteredNumber = c.String(nullable: false),
                        BrandName = c.String(nullable: false),
                        BrandWebsite = c.String(nullable: false),
                        Industry = c.String(nullable: false),
                        Address = c.String(),
                        GoogleMapsAddress = c.String(),
                        Country = c.String(nullable: false),
                        State = c.String(nullable: false),
                        City = c.String(nullable: false),
                        FrontImage = c.String(),
                        LogoImage = c.String(),
                        PhoneNumber = c.String(),
                        EmailAddress = c.String(),
                        SurveyURL = c.String(),
                        Status = c.String(),
                        parentOrgSub_OrgSubID = c.Long(),
                    })
                .PrimaryKey(t => t.OrgSubID)
                .ForeignKey("dbo.OrgSub", t => t.parentOrgSub_OrgSubID)
                .Index(t => t.parentOrgSub_OrgSubID);
            
            CreateTable(
                "dbo.Touchpoint",
                c => new
                    {
                        TouchpointID = c.Long(nullable: false, identity: true),
                        TouchpointName = c.String(),
                        RegisteredName = c.String(),
                        Address = c.String(),
                        BrandWebsite = c.String(),
                        PhoneNumber = c.String(),
                        EmailAddress = c.String(),
                        FrontImage = c.String(),
                        LeftPos = c.String(),
                        TopPos = c.String(),
                        SliderValue = c.String(),
                        LogoImage = c.String(),
                        Position = c.Int(nullable: false),
                        OrgSub_OrgSubID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.TouchpointID)
                .ForeignKey("dbo.OrgSub", t => t.OrgSub_OrgSubID, cascadeDelete: true)
                .Index(t => t.OrgSub_OrgSubID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.SurveyConfig",
                c => new
                    {
                        SurveyConfigID = c.Int(nullable: false, identity: true),
                        ConfigURL = c.String(),
                        TouchpointName = c.String(),
                        Survey_SurveyID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SurveyConfigID)
                .ForeignKey("dbo.Survey", t => t.Survey_SurveyID)
                .Index(t => t.Survey_SurveyID);
            
            CreateTable(
                "dbo.Survey",
                c => new
                    {
                        SurveyID = c.Int(nullable: false, identity: true),
                        SurveyTitle = c.String(),
                        SurveyData = c.String(),
                        CreatedDate = c.DateTime(),
                        LastUpdatedDate = c.DateTime(),
                        Status = c.String(),
                        IsTemplate = c.Boolean(nullable: false),
                        IsPredefined = c.Boolean(nullable: false),
                        TemplateName = c.String(),
                        IsPublished = c.Boolean(nullable: false),
                        PublishedDate = c.DateTime(),
                        PublishedURL = c.String(),
                        PublishedID = c.String(),
                        IsAllowViewResults = c.Boolean(nullable: false),
                        IsAllowViewSocial = c.Boolean(nullable: false),
                        PublishPage = c.String(),
                        CreatedBy_Id = c.String(maxLength: 128),
                        LastUpdatedBy_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SurveyID)
                .ForeignKey("dbo.AspNetUsers", t => t.CreatedBy_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.LastUpdatedBy_Id)
                .Index(t => t.CreatedBy_Id)
                .Index(t => t.LastUpdatedBy_Id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        Title = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(),
                        Country = c.String(),
                        Status = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        OrgSub_OrgSubID = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrgSub", t => t.OrgSub_OrgSubID)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.OrgSub_OrgSubID);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SurveyResponse",
                c => new
                    {
                        SurveyResponseID = c.Long(nullable: false, identity: true),
                        ResponseDate = c.DateTime(),
                        Survey_SurveyID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SurveyResponseID)
                .ForeignKey("dbo.Survey", t => t.Survey_SurveyID, cascadeDelete: true)
                .Index(t => t.Survey_SurveyID);
            
            CreateTable(
                "dbo.SurveyResponseSurveyConfig",
                c => new
                    {
                        SurveyResponseID = c.Long(nullable: false),
                        SurveyConfigID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SurveyResponseID, t.SurveyConfigID })
                .ForeignKey("dbo.SurveyResponse", t => t.SurveyResponseID, cascadeDelete: true)
                .ForeignKey("dbo.SurveyConfig", t => t.SurveyConfigID, cascadeDelete: true)
                .Index(t => t.SurveyResponseID)
                .Index(t => t.SurveyConfigID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SurveyResponseSurveyConfig", "SurveyConfigID", "dbo.SurveyConfig");
            DropForeignKey("dbo.SurveyResponseSurveyConfig", "SurveyResponseID", "dbo.SurveyResponse");
            DropForeignKey("dbo.SurveyResponse", "Survey_SurveyID", "dbo.Survey");
            DropForeignKey("dbo.ModuleResponse", "SurveyResponse_SurveyResponseID", "dbo.SurveyResponse");
            DropForeignKey("dbo.SurveyConfig", "Survey_SurveyID", "dbo.Survey");
            DropForeignKey("dbo.Module", "Survey_SurveyID", "dbo.Survey");
            DropForeignKey("dbo.Survey", "LastUpdatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Survey", "CreatedBy_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "OrgSub_OrgSubID", "dbo.OrgSub");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Touchpoint", "OrgSub_OrgSubID", "dbo.OrgSub");
            DropForeignKey("dbo.OrgSub", "parentOrgSub_OrgSubID", "dbo.OrgSub");
            DropForeignKey("dbo.ModuleResponse", "Module_ModuleID", "dbo.Module");
            DropIndex("dbo.SurveyResponseSurveyConfig", new[] { "SurveyConfigID" });
            DropIndex("dbo.SurveyResponseSurveyConfig", new[] { "SurveyResponseID" });
            DropIndex("dbo.SurveyResponse", new[] { "Survey_SurveyID" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "OrgSub_OrgSubID" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Survey", new[] { "LastUpdatedBy_Id" });
            DropIndex("dbo.Survey", new[] { "CreatedBy_Id" });
            DropIndex("dbo.SurveyConfig", new[] { "Survey_SurveyID" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Touchpoint", new[] { "OrgSub_OrgSubID" });
            DropIndex("dbo.OrgSub", new[] { "parentOrgSub_OrgSubID" });
            DropIndex("dbo.Module", new[] { "Survey_SurveyID" });
            DropIndex("dbo.ModuleResponse", new[] { "SurveyResponse_SurveyResponseID" });
            DropIndex("dbo.ModuleResponse", new[] { "Module_ModuleID" });
            DropTable("dbo.SurveyResponseSurveyConfig");
            DropTable("dbo.SurveyResponse");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Survey");
            DropTable("dbo.SurveyConfig");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Touchpoint");
            DropTable("dbo.OrgSub");
            DropTable("dbo.Module");
            DropTable("dbo.ModuleResponse");
        }
    }
}
