namespace CusExperience.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TouchPointMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SurveyConfig",
                c => new
                    {
                        SurveyConfigID = c.Int(nullable: false, identity: true),
                        ConfigURL = c.String(),
                        TouchPointName = c.String(),
                        Survey_SurveyID = c.Int(),
                        Module_ModuleID = c.Long(),
                    })
                .PrimaryKey(t => t.SurveyConfigID)
                .ForeignKey("dbo.Survey", t => t.Survey_SurveyID)
                .ForeignKey("dbo.Module", t => t.Module_ModuleID, cascadeDelete: true)
                .Index(t => t.Survey_SurveyID)
                .Index(t => t.Module_ModuleID);
            
            CreateTable(
                "dbo.SurveyResponseSurveyConfig",
                c => new
                    {
                        SurveyResponse_SurveyResponseID = c.Long(nullable: false),
                        SurveyConfig_SurveyConfigID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.SurveyResponse_SurveyResponseID, t.SurveyConfig_SurveyConfigID })
                .ForeignKey("dbo.SurveyResponse", t => t.SurveyResponse_SurveyResponseID, cascadeDelete: true)
                .ForeignKey("dbo.SurveyConfig", t => t.SurveyConfig_SurveyConfigID, cascadeDelete: true)
                .Index(t => t.SurveyResponse_SurveyResponseID)
                .Index(t => t.SurveyConfig_SurveyConfigID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SurveyConfig", "Module_ModuleID", "dbo.Module");
            DropForeignKey("dbo.SurveyResponseSurveyConfig", "SurveyConfig_SurveyConfigID", "dbo.SurveyConfig");
            DropForeignKey("dbo.SurveyResponseSurveyConfig", "SurveyResponse_SurveyResponseID", "dbo.SurveyResponse");
            DropForeignKey("dbo.SurveyConfig", "Survey_SurveyID", "dbo.Survey");
            DropIndex("dbo.SurveyResponseSurveyConfig", new[] { "SurveyConfig_SurveyConfigID" });
            DropIndex("dbo.SurveyResponseSurveyConfig", new[] { "SurveyResponse_SurveyResponseID" });
            DropIndex("dbo.SurveyConfig", new[] { "Module_ModuleID" });
            DropIndex("dbo.SurveyConfig", new[] { "Survey_SurveyID" });
            DropTable("dbo.SurveyResponseSurveyConfig");
            DropTable("dbo.SurveyConfig");
        }
    }
}
