﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Quartz;
using Quartz.Impl;
using System.Net.Mail;
using System.Configuration;
using CusExperienceContract.Entities.SurveyData;
using Quartz.Impl.Matchers;
using CusExperience.Entities;
using CusExperience.Repositories;
using System.Collections.Specialized;

namespace CusExperience.Utilities
{
    public class QuartzScheduler
    {
        public static QuartzScheduler instance;

        IScheduler scheduler = null;

        private QuartzScheduler()
        {
            try
            {
                Common.Logging.LogManager.Adapter = new Common.Logging.Simple.ConsoleOutLoggerFactoryAdapter { Level = Common.Logging.LogLevel.Info };
                NameValueCollection config = (NameValueCollection)ConfigurationManager.GetSection("quartz");
                ISchedulerFactory factory = new StdSchedulerFactory(config);
                scheduler = factory.GetScheduler();
                scheduler.Start();
            }
            catch (Exception se)
            {
                Console.WriteLine(se);
            }
        }

        public static QuartzScheduler getInstance()
        {
            return instance;
        }

        public void DeleteExistingAlerts(Survey survey)
        {
            Quartz.Collection.ISet<JobKey> keys = scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupContains(survey.SurveyID.ToString()));
            foreach (JobKey jk in keys)
            {
                scheduler.DeleteJob(jk);
            }
        }


        public void AddNewEmailAlert(Survey survey, Alert alert, string userId)
        {
            string concatString = survey.SurveyID.ToString() + "|" + alert.AlertDay + "|" + alert.AlertTime + "|" + alert.SendToRole;

            Quartz.Collection.ISet<JobKey> keys = scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupContains(survey.SurveyID.ToString()));
            if (!keys.Any(j => j.Name.Equals(concatString )))
            {

            IJobDetail job = JobBuilder.Create<EmailJob>()
                                        .WithIdentity(concatString, survey.SurveyID.ToString())
                                        .Build();

            JobDataMap jm = new JobDataMap();

            job.JobDataMap.Add("Alert", alert);
            job.JobDataMap.Add("SurveyID", survey.SurveyID);
            job.JobDataMap.Add("UserId", userId);

            string[] timeSplit = alert.AlertTime.Split(':');
            int hours = Convert.ToInt32(timeSplit[0]);
            int min = Convert.ToInt32(timeSplit[1]);

            DayOfWeek d = DayOfWeek.Monday;

            switch (alert.AlertDay)
            {
                case "Monday":
                    d = DayOfWeek.Monday;
                    break;
                case "Tuesday":
                    d = DayOfWeek.Tuesday;
                    break;
                case "Wednesday":
                    d = DayOfWeek.Wednesday;
                    break;
                case "Thursday":
                    d = DayOfWeek.Thursday;
                    break;
                case "Friday":
                    d = DayOfWeek.Friday;
                    break;
                case "Saturday":
                    d = DayOfWeek.Saturday;
                    break;
                case "Sunday":
                    d = DayOfWeek.Sunday;
                    break;
            }
           
            // Trigger the job to run now, and then repeat every 10 seconds
            ITrigger trigger = TriggerBuilder.Create()
                                                .WithIdentity("trigger" + concatString , survey.SurveyID.ToString())
                                                .WithSchedule(CronScheduleBuilder.WeeklyOnDayAndHourAndMinute(d,hours,min))
                                                .Build();

            // Tell quartz to schedule the job using our trigger
            scheduler.ScheduleJob(job, trigger);
           }
        }

        public static void StartInstance()
        {
            instance = new QuartzScheduler();
        }
    }

    public class EmailJob : IJob
    {
        SurveyRepository sRep = new SurveyRepository();
        ResponseRepository rRep = new ResponseRepository();
        UserRepository uRep = new UserRepository();

        public void Execute(IJobExecutionContext context)
        {
            int surveyID = (int)context.MergedJobDataMap.Get("SurveyID");
            Alert alert = (Alert)context.MergedJobDataMap.Get("Alert");
            string userId = (string) context.MergedJobDataMap.Get("UserId");
            
            Survey survey = sRep.GetSurvey(surveyID, userId);
            int totalResponses = 0, lastWeekResponses = 0;
            rRep.GetResponseCount(survey.SurveyID, userId, alert, ref totalResponses, ref lastWeekResponses);
            List<CusExpUser> Users = uRep.GetAllUsersByRole(survey.OrgSub.OrgSubID, alert.SendToRole, userId);
            foreach (CusExpUser user in Users) {
                MailMessage newMessage = new MailMessage();
                newMessage.Subject = "Alert : " + survey.SurveyTitle;
                if (!string.IsNullOrEmpty(user.Email)) {
                    string alertMessage = string.Format("Hi  {0} , <br/><br/>  You have {1} new responses since last {2} and a total of {3} responses as of the survey launch date, {5}. <br/> <br/> Please login <a href='www.cusexperience.com'> www.CusExperience.com </a> to view the Survey Analytics. <br/><br/>Warmly,<br/>CusExperience<br/><a href='mailto:Hi@CusJo.com'>Hi@CusJo.com</a>", user.FirstName,  lastWeekResponses, alert.AlertDay, totalResponses, survey.PublishedDate);
                    newMessage.Body = alertMessage;
                    newMessage.IsBodyHtml = true;
                    newMessage.To.Add(user.Email);
                    SMTPEmailService.Send(newMessage);
                }
            }
        }
    }
}
