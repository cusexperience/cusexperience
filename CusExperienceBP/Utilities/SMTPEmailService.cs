﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CusExperience.Utilities
{
    public class SMTPEmailService
    {
        public static async Task SendAsync(MailMessage message)
        {
            SmtpClient client = new SmtpClient();
            client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MailSMTPPort"]);
            client.Host = ConfigurationManager.AppSettings["MailSMTPDomain"];
            client.EnableSsl = true;
            client.Timeout = 10000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MailFrom"], WebConfigurationManager.AppSettings["MailFromPwd"]);
            message.IsBodyHtml = true;
            message.BodyEncoding = UTF8Encoding.UTF8;
            message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            await client.SendMailAsync(message);
        }

        public static void Send(MailMessage message)
        {
            try
            { 
                SmtpClient client = new SmtpClient();
                client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["MailSMTPPort"]);
                client.Host = ConfigurationManager.AppSettings["MailSMTPDomain"];
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["MailFrom"], WebConfigurationManager.AppSettings["MailFromPwd"]);
                message.IsBodyHtml = true;
                message.From = new MailAddress(ConfigurationManager.AppSettings["MailFrom"]);
                message.BodyEncoding = UTF8Encoding.UTF8;
                message.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                client.SendMailAsync(message);
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
            }
        }
    }
}
