﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.Utilities
{
    public static class QUESTIONTYPES
    {
        public const string RATE = "Rate";
        public const string TAG = "Tag";
        public const string TEXT = "Text";
        public const string CHOICE = "Choice";
        public const string RANK = "Rank";
        public const string SLIDE = "Slide";
        public const string PICTURE = "Picture";
        public const string MULTIPLE = "Multiple";
        public const string ATTACH = "Attach";
        public const string PSYCHOMETRIC = "Psychometric";
        public const string LOCATION = "Location";
        public const string SEGMENT = "Segment";
        public const string SOCIAL = "Social";
        public const string STAFF = "Staff";
        public const string DEMOGRAPHIC = "Demographic";
    };

    public static class ROLES
    {
        public const string SUPERADMINISTRATOR = "SuperAdministrator";
        public const string ADMINISTRATOR = "Administrator";
        public const string CUSXPPROFESSIONAL = "CusXP Professional";
        public const string EXECUTIVE = "Executive";
        public const string MANAGER = "Manager";
        public const string FRONTLINER = "FrontLiner";
        public const string ANONYMOUS = "Anonymous";
    }

    public static class REPOSITORY
    {
        public const string ACCOUNT = "ACCOUNT";
        public const string ANALYTICS = "ANALYTICS";
        public const string USER = "USER";
        public const string ORGSUB = "ORGSUB";
        public const string STAFF = "STAFF";
        public const string RESPONSE = "RESPONSE";
        public const string SURVEY = "SURVEY";
    }

    public static class EXCEPTIONMSG
    {
        public const string EX_USERIDBLANK = "Username is blank";
        public const string EX_USERIDINVALID = "Username is Invalid";
        public const string EX_INVALIDARGS = "Arguments are Invalid";
        public const string EX_CODEBLANK = "Code is blank";
        public const string EX_PWDBLANK = "Password or Confirm Password is blank";
        public const string EX_PWDMISMATCH = "Password and Confirm Password are not same";
        public const string EX_RESETPWDNULL = "Reset Password Information Not Available";
        public const string EX_CHGPWDNULL = "Change Password Information Not Available";
        public const string EX_ONCPWDBLANK = "Current Password or New Password is blank";
        public const string EX_CPWDINVALID = "Current Password is incorrect";
        public const string EX_UPBLANK = "Username or Password is blank";
        public const string EX_USERALREADYEXISTS = "UserName already exists";
        public static string EX_INVALIDORG = "Organisation does not exists";
        public static string EX_INVALIDSURVEY = "Survey does not exists";
        public static string EX_FILEINVALID = "Error in saving file. File Invalid";
        public static string EX_FILENOTFOUND = "File Not Found";
        public static string EX_CUPASSWORD = "Your last three passwords cannot be used";
        public static string EX_USERNOTEXISTS = "User not exists";
        public static string EX_USERORPWDINVALID = "UserId or Password is Invalid";
        public static string EX_PUBLISHIDEXISTS = "PublishID Already Exists";
    }

    public static class EMAILTEMPLATES
    {
        public static string FORGETPASSWORD = "We received a request to reset the password associated with this e-mail address. If you made this request, please follow the instructions below. <br/><br/>Click the link below to reset your password: <br/><br/>" +
               "<a href=\"{0}/Account/PasswordReset?userId={1}&code={2}\">{2}</a>" + "<br/><br/>If you did not request to have your password reset you can safely ignore this email. Rest assured your account is safe.<br/><br/>If clicking the link doesn't seem to work, you can copy and paste the link into your browser's address window, or retype it there. Once you have returned to CusExperience.com, we will give instructions for resetting your password. ";
        public static string CONTACT = @"<table> 
                                           <td>Given Names</td>
                                           <td>{0}</td>
                                           </tr>
                                           <tr>
                                           <td>Surname</td>
                                           <td>{1}</td>
                                           </tr>
                                           <tr>
                                           <td>Organisation</td>
                                           <td>{2}</td>
                                           </tr>
                                           <tr>
                                           <tr>
                                           <td>Country</td>
                                           <td>{3}</td>
                                           </tr>
                                           <tr>
                                           <td>Mobile</td>
                                           <td>{4}</td>
                                           </tr>
                                           <tr>
                                           <td>Email</td>
                                           <td>{5}</td>
                                           </tr> 
                                           <tr>
                                           <td>Email</td>
                                           <td>{6}</td>
                                           </tr>
                                           </table>";

        public static string SUPPORT = @"<table> 
                                       <tr>
                                       <td>Title</td>
                                       <td>{0}</td>
                                       </tr>
                                       <tr>
                                       <td>Description</td>
                                       <td>{1}</td>
                                       </tr>
                                       <tr>
                                       <td>Given Names</td>
                                       <td>{2}</td>
                                       </tr>
                                       <tr>
                                       <td>Surname</td>
                                       <td>{3}</td>
                                       </tr>
                                       <tr>
                                       <td>Country</td>
                                       <td>{4}</td>
                                       </tr>
                                       <tr>
                                       <td>Mobile</td>
                                       <td>{5}</td>
                                       </tr>
                                       <tr>
                                       <td>Email</td>
                                       <td>{6}</td>
                                       </tr>";
        public static string ATTACHMENTSUPPORT = @"<tr>
                                                    <td>Attachment</td>
                                                    <td><a href = ""{0}"">{0}</a></td>
                                                    </tr>
                                                    <tr>
                                                    <td>Comment</td>
                                                    <td><a href = ""{1}"">{1}</a></td>
                                                    </tr>";
    }

    public static class FILEEXTENSIONS
    {
        public static readonly List<string> IMAGE = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
        public static readonly List<string> VIDEO = new List<string> { ".MPEG", ".MPG", ".MOV", ".OGG", ".FLV", ".MP4" };
        public static readonly List<string> PDF = new List<string> { ".PDF"};
        public static readonly List<string> OFFICE = new List<string> { ".XLS",".XLSX",".TXT",".DOC",".DOCX",".CSV",".PPT",".PPTX",".RTF" };
    }

    public static class SURVEYSTATUS
    {
        public const string PUBLISHED = "PUBLISHED";
        public const string UNPUBLISHED = "UNPUBLISHED";
        public const string DELETED = "DELETED";
    }

    public static class CONSTANTS {
        public static string[] EXCLUDELIST = new[] { "123", "456", "789", "<", ">", "+", "=", "_", "/", "\\", ":", ";", "(", ")", "&", "\'", "\"", "?", "able", "about", "above", "abst", "accordance", "according", "accordingly", "across", "act", "actually", "added", "adj", "affected", "affecting", "affects", "after", "afterwards", "again", "against", "ah", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "an", "and", "announce", "another", "any", "anybody", "anyhow", "anymore", "anyone", "anything", "anyway", "anyways", "anywhere", "apparently", "approximately", "are", "aren", "arent", "arise", "around", "as", "aside", "ask", "asking", "at", "auth", "available", "away", "awfully", "b", "back", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "begin", "beginning", "beginnings", "begins", "behind", "being", "believe", "below", "beside", "besides", "between", "beyond", "biol", "both", "brief", "briefly", "but", "by", "c", "ca", "came", "can", "cannot", "cant", "can't", "cause", "causes", "certain", "certainly", "co", "com", "come", "comes", "comment", "comments", "contain", "containing", "contains", "could", "couldnt", "d", "date", "did", "didn't", "different", "do", "does", "doesn't", "doing", "done", "don't", "dont", "didnt", "doesnt", "down", "downwards", "due", "during", "e", "each", "ed", "edu", "effect", "eg", "eight", "eighty", "either", "else", "elsewhere", "end", "ending", "enough", "especially", "et", "et-al", "etc", "even", "ever", "every", "everybody", "everyone", "everything", "everywhere", "ex", "except", "f", "far", "few", "ff", "fifth", "first", "five", "fix", "followed", "following", "follows", "for", "former", "formerly", "forth", "found", "four", "from", "further", "furthermore", "g", "gave", "get", "gets", "getting", "give", "given", "gives", "giving", "go", "goes", "gone", "got", "gotten", "h", "had", "happens", "hardly", "has", "hasn't", "have", "haven't", "having", "he", "hed", "hence", "her", "here", "hereafter", "hereby", "herein", "heres", "hereupon", "hers", "herself", "hes", "hi", "hid", "him", "himself", "his", "hither", "home", "how", "howbeit", "however", "hundred", "i", "id", "ie", "if", "i'll", "im", "immediate", "immediately", "importance", "important", "in", "inc", "indeed", "index", "information", "instead", "into", "invention", "inward", "is", "isn't", "it", "itd", "it'll", "its", "itself", "i've", "j", "just", "k", "keep", "keeps", "kept", "kg", "km", "know", "known", "knows", "l", "largely", "last", "lately", "later", "latter", "latterly", "least", "less", "lest", "let", "lets", "like", "liked", "likely", "line", "little", "'ll", "look", "looking", "looks", "ltd", "m", "made", "mainly", "make", "makes", "many", "may", "maybe", "me", "mean", "means", "meantime", "meanwhile", "merely", "mg", "might", "million", "miss", "ml", "more", "moreover", "most", "mostly", "mr", "mrs", "much", "mug", "must", "my", "myself", "n", "na", "name", "namely", "nay", "nd", "near", "nearly", "necessarily", "necessary", "need", "needs", "neither", "never", "nevertheless", "new", "next", "nil", "nine", "ninety", "no", "nobody", "non", "none", "nonetheless", "noone", "nor", "normally", "nos", "not", "noted", "nothing", "now", "nowhere", "o", "obtain", "obtained", "obviously", "of", "off", "often", "oh", "ok", "okay", "old", "omitted", "on", "once", "one", "ones", "only", "onto", "or", "ord", "other", "others", "otherwise", "ought", "our", "ours", "ourselves", "out", "outside", "over", "overall", "owing", "own", "p", "page", "pages", "part", "particular", "particularly", "past", "per", "perhaps", "placed", "please", "plus", "poorly", "possible", "possibly", "potentially", "pp", "predominantly", "present", "previously", "primarily", "probably", "promptly", "proud", "provides", "put", "q", "que", "quickly", "quite", "qv", "r", "ran", "rather", "rd", "re", "readily", "really", "recent", "recently", "ref", "refs", "regarding", "regardless", "regards", "related", "relatively", "research", "respectively", "resulted", "resulting", "results", "right", "run", "s", "said", "same", "saw", "say", "saying", "says", "sec", "section", "see", "seeing", "seem", "seemed", "seeming", "seems", "seen", "self", "selves", "sent", "seven", "several", "shall", "she", "shed", "she'll", "shes", "should", "shouldn't", "show", "showed", "shown", "showns", "shows", "significant", "significantly", "similar", "similarly", "since", "six", "slightly", "so", "some", "somebody", "somehow", "someone", "somethan", "something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specifically", "specified", "specify", "specifying", "still", "stop", "strongly", "sub", "substantially", "successfully", "such", "sufficiently", "suggest", "sup", "sure", "t", "take", "taken", "taking", "tell", "tends", "test", "testing", "th", "than", "thank", "thanks", "thanx", "that", "that'll", "thats", "that've", "the", "their", "theirs", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "thered", "therefore", "therein", "there'll", "thereof", "therere", "theres", "thereto", "thereupon", "there've", "these", "they", "theyd", "they'll", "theyre", "they've", "think", "this", "those", "thou", "though", "thoughh", "thousand", "throug", "through", "throughout", "thru", "thus", "til", "tip", "to", "together", "too", "took", "toward", "towards", "tried", "tries", "truly", "try", "trying", "ts", "twice", "two", "u", "un", "under", "unfortunately", "unless", "unlike", "unlikely", "until", "unto", "up", "upon", "ups", "us", "use", "used", "useful", "usefully", "usefulness", "uses", "using", "usually", "v", "value", "various", "'ve", "very", "via", "viz", "vol", "vols", "vs", "w", "want", "wants", "was", "wasn't", "way", "we", "wed", "welcome", "we'll", "went", "were", "weren't", "we've", "what", "whatever", "what'll", "whats", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "wheres", "whereupon", "wherever", "whether", "which", "while", "whim", "whither", "who", "whod", "whoever", "whole", "who'll", "whom", "whomever", "whos", "whose", "why", "widely", "willing", "wish", "with", "within", "without", "won't", "words", "world", "would", "wouldn't", "www", "x", "y", "yes", "yet", "you", "youd", "you'll", "your", "youre", "yours", "yourself", "yourselves", "you've", "z", "zero", "will" };
    }
     
}
