﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.Utilities
{
    public static class StringExtensions
    {
        public static string ToSQLString(this string sqlString)
        {
            if (!String.IsNullOrEmpty(sqlString))
                return sqlString.Replace("'", "''");
            else return null;
        }

        public static string GetExtensionFromFilename(string filename)
        {
            if (filename != null && filename.IndexOf(".", System.StringComparison.Ordinal) == -1)
                return string.Empty;

            return filename.Substring(filename.LastIndexOf(".", System.StringComparison.Ordinal), filename.Length - filename.LastIndexOf("."));
        }

        public static string RemovePunctutions(this string str)
        {
            var punctuations = new[] { '.', ',', '/', '\\', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '{', '}', '[', ']', ';', ':', '\'', '"', '`', '~', '-', '_', '+', '=', '?' };
            var retVal = string.Join(string.Empty, str.Where(a => !punctuations.Contains(a)).ToArray());

            return retVal;
        }
    }
}