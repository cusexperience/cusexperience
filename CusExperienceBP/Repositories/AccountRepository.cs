﻿using CusExperience.DAL;
using CusExperience.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using System.Web;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.Owin.Security;
using CusExperience.Exceptions;
using CusExperience.Utilities;
using System.Net.Mail;
using System.Web.Configuration;
using Microsoft.Owin.Security.DataProtection;
using System.Security.Claims;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Net;
using CusExperience.Repositories;
using System.Security.Principal;
using System.Transactions;


namespace CusExperience.Repositories
{
    public class AccountRepository
    {
        public CusExpContext dbContext = new CusExpContext();
        public CusExpUserManager UserManager { get; private set; }
        public UserStore<CusExpUser> cUserStore;
        public UserRepository uRep = new UserRepository();
        private const int PASSWORD_HISTORY_LIMIT = 3;

        public AccountRepository()
        {
            cUserStore = new UserStore<CusExpUser>(dbContext);
            UserManager = new CusExpUserManager(cUserStore);
        }

        public ClaimsIdentity LoginUser(string loginObj)
        {
            JObject jsonUserCredential = JObject.Parse(loginObj.ToString());

            if (jsonUserCredential["UserName"] == null || jsonUserCredential["Password"] == null)
                throw new CusExpException(EXCEPTIONMSG.EX_INVALIDARGS);

            string username = jsonUserCredential["UserName"].ToString();
            string password = jsonUserCredential["Password"].ToString();

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                throw new CusExpException(EXCEPTIONMSG.EX_UPBLANK);

            CusExpUser user = UserManager.Find(username, password);

            if (user != null && user.Status != "DELETED")
            {
                return UserManager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            }
            else
            {
                throw new CusExpException(EXCEPTIONMSG.EX_USERORPWDINVALID);
            }
        }

        public void ForgotPassword(string forgorPwdObj)
        {
            JObject jsonForgetPwdObj = JObject.Parse(forgorPwdObj.ToString());

            if (jsonForgetPwdObj["UserId"] == null || jsonForgetPwdObj["baseURL"] == null)
                throw new CusExpException(EXCEPTIONMSG.EX_USERIDBLANK);

            string userId = jsonForgetPwdObj["UserId"].ToString();
            string id = dbContext.Users.Where(u => u.UserName == userId).Select(s => s.Id).FirstOrDefault();
            if (id == null)
                throw new CusExpException(EXCEPTIONMSG.EX_USERIDINVALID);

            string homeURL = jsonForgetPwdObj["baseURL"].ToString();
            string code = UserManager.GeneratePasswordResetToken(id);
            string emailMessage = String.Format(EMAILTEMPLATES.FORGETPASSWORD, homeURL, userId, code);
            UserManager.SendEmail(id, "CusExperience.com Password Assistance", emailMessage);
        }

        public void ResetPassword(string resetPasswordObj)
        {

            if (resetPasswordObj == null)
                throw new CusExpException(EXCEPTIONMSG.EX_RESETPWDNULL);

            JObject jsonForgetPwdObj = JObject.Parse(resetPasswordObj.ToString());
            if (jsonForgetPwdObj["UserId"] == null)
                throw new CusExpException(EXCEPTIONMSG.EX_USERIDBLANK);

            string userId = jsonForgetPwdObj["UserId"].ToString();
            string id = dbContext.Users.Where(u => u.UserName == userId).Select(s => s.Id).FirstOrDefault();
            if (id == null)
                throw new CusExpException(EXCEPTIONMSG.EX_USERIDINVALID);

            if (jsonForgetPwdObj["Code"] == null || jsonForgetPwdObj["Password"] == null)
                throw new CusExpException(EXCEPTIONMSG.EX_INVALIDARGS);

            string code = jsonForgetPwdObj["Code"].ToString();
            code = code.Replace(' ', '+');

            if (string.IsNullOrEmpty(code))
                throw new CusExpException(EXCEPTIONMSG.EX_CODEBLANK);

            string password = jsonForgetPwdObj["Password"].ToString();
            /*string confirmPassword = jsonForgetPwdObj["ConfirmPassword"].ToString();*/

            if (string.IsNullOrEmpty(password))
                throw new CusExpException(EXCEPTIONMSG.EX_PWDBLANK);

           /* if (!password.Equals(confirmPassword))
                throw new CusExpException(CusExpException.EX_PWDMISMATCH); */

            CusExpUser cUser = dbContext.Users.Include(u => u.PreviousUserPasswords).Where(u => u.UserName == userId).FirstOrDefault();

            if (cUser == null)
                throw new CusExpException(EXCEPTIONMSG.EX_USERIDINVALID);

            if (IsPreviousPassword(cUser, password))
                throw new CusExpException(EXCEPTIONMSG.EX_CUPASSWORD);

            var result = UserManager.ResetPassword(id, code, password);

            if (!result.Succeeded)
                throw new CusExpException(result.Errors.First());

            AddToPreviousPasswords(cUser, cUser.PasswordHash);

            string emailMessage = String.Format("It seems that you recently changed your password. If you didn't do so please contact our administrator for assistance.");
            UserManager.SendEmail(cUser.Id, "CusExperience.com Password Assistance", emailMessage);
        }

        public void ChangePassword(string changePasswordObj, string userId)
        {
            if (changePasswordObj == null)
                throw new CusExpException(EXCEPTIONMSG.EX_CHGPWDNULL);

            JObject jsonChangePassword = JObject.Parse(changePasswordObj.ToString());

            if (string.IsNullOrEmpty(userId))
                throw new CusExpException(EXCEPTIONMSG.EX_USERIDBLANK);

            if (jsonChangePassword["OldPassword"] == null || jsonChangePassword["NewPassword"] == null)
                throw new CusExpException(EXCEPTIONMSG.EX_INVALIDARGS);

            string oldpassword = jsonChangePassword["OldPassword"].ToString();
            string newpassword = jsonChangePassword["NewPassword"].ToString();
            /* string confirmPassword = jsonChangePassword["ConfirmPassword"].ToString(); */

            if (string.IsNullOrEmpty(oldpassword) || string.IsNullOrEmpty(newpassword)) // || string.IsNullOrEmpty(confirmPassword))
                throw new CusExpException(EXCEPTIONMSG.EX_ONCPWDBLANK);

            /*if (!newpassword.Equals(confirmPassword))
                throw new CusExpException(CusExpException.EX_PWDMISMATCH);*/

            CusExpUser cUser = dbContext.Users.Include(u=> u.PreviousUserPasswords).Where(u => u.Id == userId).FirstOrDefault();

            if(cUser == null)
                throw new CusExpException(EXCEPTIONMSG.EX_USERIDINVALID);

            if (IsPreviousPassword(cUser, newpassword))
                throw new CusExpException(EXCEPTIONMSG.EX_CUPASSWORD);

            var result = UserManager.ChangePassword(cUser.Id, oldpassword, newpassword);
            if (!result.Succeeded) {
                if (result.Errors.First().Equals("Incorrect Password"))
                {
                    throw new CusExpException(EXCEPTIONMSG.EX_CPWDINVALID);
                }
                    throw new CusExpException(result.Errors.First());
            }

            AddToPreviousPasswords(cUser, cUser.PasswordHash);

            string emailMessage = String.Format("It seems that you recently changed your password. If you didn't do so please contact our administrator for assistance.");
            UserManager.SendEmail(cUser.Id, "CusExperience.com Password Assistance", emailMessage);
        }

        public void AddToPreviousPasswords(CusExpUser user, string password)
        {
            dbContext.PreviousPasswords.Add(new PreviousPassword() { UserId = user.Id, PasswordHash = password });
            dbContext.SaveChanges();
        }

        public Task SubmitContactForm(string contactFormObj)
        {
            JObject contractformJSONObj = JObject.Parse(contactFormObj);

            SMTPEmailService mailService = new SMTPEmailService();
            string Givennames = contractformJSONObj["GivenNames"].ToString();
            string Surname = contractformJSONObj["Surname"].ToString();
            string Organisation = contractformJSONObj["Organisation"].ToString();
            string Country = contractformJSONObj["Country"].ToString();
            string Mobile = contractformJSONObj["Mobile"].ToString();
            string Email = contractformJSONObj["EmailAddress"].ToString();
            string Comment = contractformJSONObj["Comment"].ToString();
            string emailMessage = String.Format(EMAILTEMPLATES.SUPPORT, Givennames, Surname, Organisation, Country, Mobile, Email, Comment);
            MailMessage newMessage = new MailMessage();
            newMessage.Subject = "Contact Us";
            newMessage.From = new MailAddress(WebConfigurationManager.AppSettings["MailFrom"]);
            newMessage.Body = emailMessage;
            newMessage.To.Add(System.Web.Configuration.WebConfigurationManager.AppSettings["ContactMailTo"]);
            return SMTPEmailService.SendAsync(newMessage);
        }

        public Task SubmitSupportForm(string supportObj)
        {
            JObject supportJSONObj = JObject.Parse(supportObj);
            SMTPEmailService mailService = new SMTPEmailService();
            string Title = supportJSONObj["Title"] != null ? supportJSONObj["Title"].ToString() : "";
            string Description = supportJSONObj["Description"] != null ? supportJSONObj["Description"].ToString() : "";
            string Givennames = supportJSONObj["GivenNames"] != null ? supportJSONObj["GivenNames"].ToString() : "";
            string Surname = supportJSONObj["Surname"] != null ? supportJSONObj["Surname"].ToString() : "";
            string Country = supportJSONObj["Country"] != null ? supportJSONObj["Country"].ToString() : "";
            string Mobile = supportJSONObj["Mobile"] != null ? supportJSONObj["Mobile"].ToString() : "";
            string Email = supportJSONObj["EmailAddress"] != null ? supportJSONObj["EmailAddress"].ToString() : "";
            JArray Attachments = supportJSONObj["Attachments"] != null ? (JArray)supportJSONObj["Attachments"] : new JArray();
            string emailMessage = String.Format(EMAILTEMPLATES.SUPPORT, Title, Description, Givennames, Surname, Country, Mobile, Email);
            foreach (JObject Attachment in Attachments)    
            {
                if (Attachment["FilePath"] != null)
                {
                    string Comment = Attachment["Comment"] != null ? Attachment["Comment"].ToString() : "";
                    emailMessage += String.Format(EMAILTEMPLATES.ATTACHMENTSUPPORT, Attachment["FilePath"].ToString(), Comment);
                }
            }
            emailMessage += "</table>";
            MailMessage newMessage = new MailMessage();
            newMessage.Subject = Title;
            newMessage.From = new MailAddress(WebConfigurationManager.AppSettings["MailFrom"]);
            newMessage.Body = emailMessage;
            newMessage.To.Add(System.Web.Configuration.WebConfigurationManager.AppSettings["SupportMailTo"]);
            return SMTPEmailService.SendAsync(newMessage);
        }

        /* Private Methods */
        private bool IsPreviousPassword(CusExpUser user, string newPassword)
        {
            PasswordHasher PasswordHasher = new PasswordHasher();

            if (user.PreviousUserPasswords == null) return false;

            if (user.PreviousUserPasswords.OrderByDescending(x => x.CreateDate).Select(x => x.PasswordHash).Take(PASSWORD_HISTORY_LIMIT).Where(x => PasswordHasher.VerifyHashedPassword(x, newPassword) != PasswordVerificationResult.Failed).Any())
            {
                return true;
            }
            return false;
        }
    }
}
