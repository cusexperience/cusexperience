﻿using CusExperience.DAL;
using CusExperience.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using CusExperience.Repositories;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.Utilities;
using System.Web;
using System.Security.Principal;
using System.Data.Entity.Core;

namespace CusExperience.Repositories
{
    public class OrgSubRepository
    {
        CusExpContext dbContext = new CusExpContext();
        public UserRepository uRep = new UserRepository();
        public UserManager<CusExpUser> UserManager { get; private set; }
        private string hierarchyLevel = "";
        private string hierarchyURL = "";
        private string baseURL = "";
        private string baseOrgSubName = "";

        public OrgSubRepository()
        {
            UserManager = new CusExpUserManager(new UserStore<CusExpUser>(dbContext));
        }

        public List<OrgSub> GetOrgSubs(string userId)
        {
            if (uRep.isSuperAdministrator(userId))
            {
                List<OrgSub> OrgSubs = dbContext.OrgSubs.Include(o => o.Touchpoints).Include(o => o.Subsidiaries).Where(o => o.Status != "DELETED" && o.ParentOrgSub == null).ToList();
                for (int i = 0; i < OrgSubs.Count; i++)
                {
                    int level = 1;
                    hierarchyLevel = (i + 1).ToString() + ".";
                    OrgSubs[i].RegisteredName = hierarchyLevel + "  " + OrgSubs[i].RegisteredName;
                    OrgSubs[i].HierarchyLevel = level;
                    hierarchyURL = OrgSubs[i].SurveyURL == null ? "" : OrgSubs[i].SurveyURL;
                    List<OrgSub> subsidiaries = OrgSubs[i].Subsidiaries;
                    AddSubsidiaries(ref subsidiaries, level + 1);
                }
                return OrgSubs;
            }
            throw new UnauthorizedAccessException();
        }

        public OrgSub GetNewOrgSub(long parentOrgSubID, string userId)
        {
            OrgSub parentOrgSub = GetOrgSub(parentOrgSubID, userId);
            OrgSub orgSub = new OrgSub()
            {
                Industry = parentOrgSub.Industry,
                BrandName = parentOrgSub.BrandName,
                BrandWebsite = parentOrgSub.BrandWebsite,
                Address = parentOrgSub.Address,
                Country = parentOrgSub.Country,
                PhoneNumber = parentOrgSub.PhoneNumber,
                EmailAddress = parentOrgSub.EmailAddress,
                ParentOrgSubID = parentOrgSubID,
                ParentOrgSubName = parentOrgSub.RegisteredName,
                HierarchyURL = !string.IsNullOrEmpty(parentOrgSub.HierarchyURL) ? parentOrgSub.HierarchyURL + "/" + parentOrgSub.SurveyURL : !string.IsNullOrEmpty(parentOrgSub.SurveyURL) ? parentOrgSub.SurveyURL : null
            };
            return orgSub;
        }

        private void AddSubsidiaries(ref List<OrgSub> OrgSubs, int level)
        {
            if (OrgSubs.Count <= 0)
            {
                removeLastHierarchyLevel();
                return;
            }
            for (int i = 0; i < OrgSubs.Count; i++)
            {
                long orgSubId = OrgSubs[i].OrgSubID;
                OrgSubs[i] = dbContext.OrgSubs.Include(o => o.Subsidiaries)
                    .Include(o => o.Touchpoints).Where(o => o.Status != "DELETED" && o.OrgSubID == orgSubId).FirstOrDefault();
                if (OrgSubs[i] != null)
                {
                    hierarchyLevel += (i + 1).ToString() + ".";
                    OrgSubs[i].HierarchyURL = hierarchyURL;
                    OrgSubs[i].HierarchyLevel = level;
                    hierarchyURL += "/" + OrgSubs[i].SurveyURL;
                    OrgSubs[i].RegisteredName = hierarchyLevel + "  " + OrgSubs[i].RegisteredName;
                    OrgSubs[i].BaseURL = baseURL;
                    List<OrgSub> subsidiaries = OrgSubs[i].Subsidiaries;
                    AddSubsidiaries(ref subsidiaries, level + 1);
                }
                else
                {
                    OrgSubs.RemoveAt(i);
                    i--;
                }
            }
            removeLastHierarchyLevel();
        }

        private void removeLastHierarchyLevel()
        {
            var n = hierarchyLevel.LastIndexOf('.');
            if (n != -1) hierarchyLevel = hierarchyLevel.Substring(0, n);
            n = hierarchyLevel.LastIndexOf('.');
            if (n != -1) hierarchyLevel = hierarchyLevel.Substring(0, n + 1);
            else hierarchyLevel = "";
            var n1 = hierarchyURL.LastIndexOf('/');
            if (n1 != -1) hierarchyURL = hierarchyURL.Substring(0, n1);
        }

        public OrgSub GetOrgSub(long orgSubID, string userId)
        {
            int level = 1;
            if (uRep.isAnonymousUser(userId))
            {
                OrgSub orgSub = dbContext.OrgSubs.Include(o => o.Subsidiaries).Include(o => o.ParentOrgSub)
                       .Include(o => o.Touchpoints).Where(o => o.Status != "DELETED" && o.OrgSubID == orgSubID).FirstOrDefault();
                List<OrgSub> subsidiaries = orgSub.Subsidiaries;
                baseURL = orgSub.SurveyURL;
                GetParentHierarchyURL(orgSub.ParentOrgSub);
                orgSub.BaseURL = baseURL;
                orgSub.HierarchyURL = hierarchyURL;
                orgSub.HierarchyLevel = level;
                orgSub.ParentOrgSubName = baseOrgSubName;
                if (!string.IsNullOrEmpty(hierarchyURL))
                    hierarchyURL = orgSub.SurveyURL;
                AddSubsidiaries(ref subsidiaries,level + 1);
                return orgSub;
            }
            else if (uRep.isSuperAdministrator(userId))
            {
                OrgSub orgSub = dbContext.OrgSubs.Include(o => o.Subsidiaries).Include(o => o.ParentOrgSub)
                    .Include(o => o.Touchpoints).Where(o => o.Status != "DELETED" && o.OrgSubID == orgSubID).FirstOrDefault();
                List<OrgSub> subsidiaries = orgSub.Subsidiaries;
                baseURL = orgSub.SurveyURL;
                GetParentHierarchyURL(orgSub.ParentOrgSub);
                orgSub.BaseURL = baseURL;
                orgSub.HierarchyURL = hierarchyURL;
                orgSub.HierarchyLevel = level;
                orgSub.ParentOrgSubName = baseOrgSubName;
                if (!string.IsNullOrEmpty(hierarchyURL))
                    hierarchyURL = orgSub.SurveyURL;
                AddSubsidiaries(ref subsidiaries, level);
                return orgSub;
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId) || uRep.isManager(userId))
            {
                if (orgSubID == uRep.GetCurrentUser(userId).OrgSub.OrgSubID)
                {
                    OrgSub orgSub = dbContext.OrgSubs.Include(o => o.Subsidiaries).Include(o => o.ParentOrgSub)
                        .Include(o => o.Touchpoints).Where(o => o.Status != "DELETED" && o.OrgSubID == orgSubID).FirstOrDefault();
                    List<OrgSub> subsidiaries = orgSub.Subsidiaries;
                    baseURL = orgSub.SurveyURL;
                    GetParentHierarchyURL(orgSub.ParentOrgSub);
                    orgSub.BaseURL = baseURL;
                    orgSub.HierarchyURL = hierarchyURL;
                    orgSub.HierarchyLevel = level;
                    orgSub.ParentOrgSubName = baseOrgSubName;
                    if (!string.IsNullOrEmpty(hierarchyURL))
                        hierarchyURL = orgSub.SurveyURL;
                    AddSubsidiaries(ref subsidiaries, level);
                    return orgSub;
                }
                throw new UnauthorizedAccessException();
            }
            throw new UnauthorizedAccessException();
        }


        public void GetParentHierarchyURL(OrgSub cOrgSub)
        {
            if(cOrgSub == null)
                return;
            OrgSub orgSub = dbContext.OrgSubs.Include(o => o.ParentOrgSub).Where(o => o.OrgSubID == cOrgSub.OrgSubID).FirstOrDefault();
            OrgSub parentSub = orgSub.ParentOrgSub;
            if (parentSub == null)
            {
                baseURL = cOrgSub.SurveyURL;
                baseOrgSubName = baseOrgSubName == "" ? orgSub.RegisteredName : orgSub.RegisteredName + " > " + baseOrgSubName;
                return;
            }
            hierarchyURL = hierarchyURL == "" ? orgSub.SurveyURL : orgSub.SurveyURL + "/" + hierarchyURL;
            baseOrgSubName = baseOrgSubName == "" ? orgSub.RegisteredName : orgSub.RegisteredName + " > " + baseOrgSubName;
            GetParentHierarchyURL(parentSub);
        }

        private void AddAnalyticsSubsidiaries(ref List<OrgSub> OrgSubs, ref Dictionary<long, string> orgSubDictionary, ref Dictionary<long, string> tpDictionary)
        {
            if (OrgSubs.Count <= 0)
            {
                removeLastHierarchyLevel();
                return;
            }
            for (int i = 0; i < OrgSubs.Count; i++)
            {
                long orgSubId = OrgSubs[i].OrgSubID;
                OrgSubs[i] = dbContext.OrgSubs.Include(o => o.Subsidiaries)
                    .Include(o => o.Touchpoints).Where(o => o.Status != "DELETED" && o.OrgSubID == orgSubId).FirstOrDefault();
                if (OrgSubs[i] != null)
                {
                    hierarchyLevel += (i + 1).ToString() + ".";
                    string registeredName = hierarchyLevel + "  " + OrgSubs[i].RegisteredName;
                    OrgSubs[i].RegisteredName = registeredName;
                    orgSubDictionary.Add(OrgSubs[i].OrgSubID, registeredName);
                    Dictionary<long, string> tpDc = OrgSubs[i].Touchpoints.Select(t => new { id = t.TouchpointID, value = t.TouchpointName + "[" + registeredName + "]" }).ToDictionary(t => t.id, t => t.value);
                    foreach (KeyValuePair<long, string> each in tpDc)
                    {
                        tpDictionary.Add(each.Key, each.Value);
                    }
                    List<OrgSub> subsidiaries = OrgSubs[i].Subsidiaries;
                    AddAnalyticsSubsidiaries(ref subsidiaries, ref orgSubDictionary, ref tpDictionary);
                }
                else
                {
                    OrgSubs.RemoveAt(i);
                    i--;
                }
            }
            removeLastHierarchyLevel();
        }

        public OrgSub GetAnalyticsOrgSub(long orgSubID, ref Dictionary<long, string> orgSubDictionary, ref Dictionary<long, string> tpDictionary, string userId)
        {
            if (uRep.isSuperAdministrator(userId))
            {
                OrgSub orgSub = dbContext.OrgSubs.Include(o => o.ParentOrgSub).Include(o => o.Subsidiaries)
                    .Include(o => o.Touchpoints).Where(o => o.Status != "DELETED" && o.OrgSubID == orgSubID).FirstOrDefault();
                List<OrgSub> subsidiaries = orgSub.Subsidiaries;
                orgSubDictionary.Add(orgSub.OrgSubID, orgSub.RegisteredName);
                Dictionary<long, string> tpDc = orgSub.Touchpoints.Select(t => new { id = t.TouchpointID, value = t.TouchpointName + "[" + orgSub.RegisteredName + "]" }).ToDictionary(t => t.id, t => t.value);
                foreach (KeyValuePair<long, string> each in tpDc)
                {
                    tpDictionary.Add(each.Key, each.Value);
                }
                AddAnalyticsSubsidiaries(ref subsidiaries, ref orgSubDictionary, ref tpDictionary);
                return orgSub;
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId) || uRep.isManager(userId))
            {
                if (orgSubID == uRep.GetCurrentUser(userId).OrgSub.OrgSubID)
                {
                    OrgSub orgSub = dbContext.OrgSubs.Include(o => o.ParentOrgSub).Include(o => o.Subsidiaries)
                        .Include(o => o.Touchpoints).Where(o => o.Status != "DELETED" && o.OrgSubID == orgSubID).FirstOrDefault();
                    List<OrgSub> subsidiaries = orgSub.Subsidiaries;
                    orgSubDictionary.Add(orgSub.OrgSubID, orgSub.RegisteredName);
                    Dictionary<long, string> tpDc = orgSub.Touchpoints.Select(t => new { id = t.TouchpointID, value = t.TouchpointName + " [" + orgSub.RegisteredName + "]" }).ToDictionary(t => t.id, t => t.value);
                    foreach (KeyValuePair<long, string> each in tpDc)
                    {
                        tpDictionary.Add(each.Key, each.Value);
                    }
                    AddAnalyticsSubsidiaries(ref subsidiaries, ref orgSubDictionary, ref tpDictionary);
                    return orgSub;
                }
            }
            throw new UnauthorizedAccessException();
        }

        public OrgSub GetOrgSubByUser(string userId)
        {
            CusExpUser User = dbContext.Users.Include(s => s.OrgSub.Touchpoints).Where(u => u.Id == userId).FirstOrDefault();
            if (User == null)
                return null;
            return User.OrgSub;
        }

        public void AddorUpdateOrgSub(OrgSub OrgSub, string userId)
        {
            if (uRep.isSuperAdministrator(userId))
            {
                if (OrgSub.OrgSubID == 0)
                    AddOrgSub(OrgSub);
                else
                    UpdateOrgSub(OrgSub);
                return;
            }
            else if (uRep.isAdministrator(userId))
            {
                if (uRep.GetCurrentUser(userId).OrgSub.OrgSubID == OrgSub.OrgSubID)
                {
                    UpdateOrgSub(OrgSub);
                    return;
                }
            }
            throw new UnauthorizedAccessException();
        }

        private void AddOrgSub(OrgSub OrgSub)
        {
            OrgSub parentOrgSub = dbContext.OrgSubs.Find(OrgSub.ParentOrgSubID);
            OrgSub.ParentOrgSub = parentOrgSub;
            dbContext.OrgSubs.Add(OrgSub);
            dbContext.SaveChanges();
        }

        private void UpdateOrgSub(OrgSub OrgSub)
        {
            var existingOrgSub = dbContext.OrgSubs.Find(OrgSub.OrgSubID);
            dbContext.Entry(existingOrgSub).State = EntityState.Detached;
            OrgSub.ParentOrgSub = existingOrgSub.ParentOrgSub;
            dbContext.OrgSubs.Attach(OrgSub);
            dbContext.Entry(OrgSub).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void SaveTouchpoints(string userId, long orgSubID, List<Touchpoint> Touchpoints)
        {
            if (uRep.isSuperAdministrator(userId))
            {
                AddOrUpdateTouchpoint(orgSubID, Touchpoints);
                return;
            }
            else if (uRep.isAdministrator(userId))
            {
                if (uRep.GetCurrentUser(userId).OrgSub.OrgSubID == orgSubID)
                {
                    AddOrUpdateTouchpoint(orgSubID, Touchpoints);
                    return;
                }
            }
            throw new UnauthorizedAccessException();
        }

        private void AddOrUpdateTouchpoint(long orgSubID, List<Touchpoint> Touchpoints)
        {
            long[] presentTouchpointIDs = Touchpoints.Where(q => q.TouchpointID != 0).Select(q => q.TouchpointID).ToArray();
            var removedTouchpoints = dbContext.Touchpoints.Where(m => !presentTouchpointIDs.Contains(m.TouchpointID) && m.OrgSub.OrgSubID == orgSubID);
            dbContext.Touchpoints.RemoveRange(removedTouchpoints);
            dbContext.SaveChanges();
            OrgSub dbOrgSub = dbContext.OrgSubs.Where(o => o.Status != "DELETED" && o.OrgSubID == orgSubID).FirstOrDefault();
            dbOrgSub.Touchpoints = Touchpoints;

            foreach (var touchpoint in dbOrgSub.Touchpoints)
            {
                touchpoint.OrgSub = dbOrgSub;

                if (touchpoint.TouchpointID == 0)
                    dbContext.Set<Touchpoint>().Add(touchpoint);
                else
                {
                    dbContext.Set<Touchpoint>().Attach(touchpoint);
                    dbContext.Entry(touchpoint).State = EntityState.Modified;
                }
            }
            dbContext.Set<OrgSub>().Attach(dbOrgSub);
            dbContext.Entry(dbOrgSub).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public List<Touchpoint> GetTouchpoints(long orgSubID, string userId)
        {
            if (uRep.isSuperAdministrator(userId))
            {
                List<Touchpoint> touchpoints = dbContext.OrgSubs.Include(o => o.Touchpoints).Where(o => o.OrgSubID == orgSubID && o.Status != "DELETED").FirstOrDefault().Touchpoints;
                return touchpoints;
            }
            else if (uRep.isAdministrator(userId))
            {
                if (orgSubID == uRep.GetCurrentUser(userId).OrgSub.OrgSubID)
                {
                    List<Touchpoint> touchpoints = dbContext.OrgSubs.Include(o => o.Touchpoints).Where(o => o.OrgSubID == orgSubID && o.Status != "DELETED").FirstOrDefault().Touchpoints;
                    return touchpoints;
                }
                throw new UnauthorizedAccessException();
            }
            throw new UnauthorizedAccessException();
        }

        public void DeleteOrgSub(long orgSubID, string userId)
        {
            if (uRep.isSuperAdministrator(userId))
            {
                var existingOrgSub = dbContext.OrgSubs.Include(o => o.Touchpoints).Where(o => o.Status != "DELETED" && o.OrgSubID == orgSubID).FirstOrDefault();
                if (existingOrgSub == null) throw new ObjectNotFoundException();

                existingOrgSub.Status = "DELETED";
                dbContext.Entry(existingOrgSub).State = EntityState.Modified;

                List<CusExpUser> orgUsers = dbContext.Users.Where(u => u.OrgSub.OrgSubID == orgSubID).ToList();
                foreach (CusExpUser cusExpUser in orgUsers)
                {
                    cusExpUser.Status = "DELETED";
                    dbContext.Entry(cusExpUser).State = EntityState.Modified;
                }
                dbContext.SaveChanges();
                return;
            }
            throw new UnauthorizedAccessException();
        }

        internal OrgSub findOrgSub(string baseURL, string hierarchyTpURL, ref OrgSub selOrgSub, ref Touchpoint selTouchpoint)
        {
            OrgSub baseOrgSub;

            baseOrgSub = dbContext.OrgSubs.Include(o => o.Subsidiaries)
                            .Include(o => o.Touchpoints).Where(o => o.Status != "DELETED" && baseURL.ToLower() == o.SurveyURL.ToLower()).FirstOrDefault();
            if (baseOrgSub == null) return null;

            if (baseOrgSub.SurveyURL.ToLower() == baseURL.ToLower()) selOrgSub = baseOrgSub;

            selTouchpoint = baseOrgSub.Touchpoints.Where(tp => tp.TouchpointURL != null && hierarchyTpURL.ToLower().Equals("/" + tp.TouchpointURL.ToLower())).FirstOrDefault();
            if (selTouchpoint != null) selOrgSub = baseOrgSub;

            List<OrgSub> subsidiaries = baseOrgSub.Subsidiaries;

            findSubsidiaries(ref subsidiaries, hierarchyTpURL, ref selOrgSub, ref selTouchpoint);

            return baseOrgSub;
        }

        private void findSubsidiaries(ref List<OrgSub> OrgSubs, string hierarchyTpURL, ref OrgSub selOrgSub, ref Touchpoint selTouchpoint)
        {

            if (OrgSubs.Count <= 0)
            {
                removeLastHierarchyLevel();
                return;
            }
            for (int i = 0; i < OrgSubs.Count; i++)
            {
                long orgSubId = OrgSubs[i].OrgSubID;
                OrgSubs[i] = dbContext.OrgSubs.Include(o => o.Subsidiaries)
                    .Include(o => o.Touchpoints).Where(o => o.Status != "DELETED" && o.OrgSubID == orgSubId).FirstOrDefault();
                if (OrgSubs[i] != null)
                {
                    hierarchyLevel += (i + 1).ToString() + ".";
                    hierarchyURL += "/" + OrgSubs[i].SurveyURL;

                    if (hierarchyURL.ToLower() == hierarchyTpURL.ToLower())
                        selOrgSub = OrgSubs[i];

                    var touchPoint = OrgSubs[i].Touchpoints.Where(tp => hierarchyTpURL.ToLower().Equals(hierarchyURL.ToLower() + "/" + tp.TouchpointURL.ToLower())).FirstOrDefault();
                    if (touchPoint != null)
                    {
                        selTouchpoint = touchPoint;
                        selOrgSub = OrgSubs[i];
                    }

                    List<OrgSub> subsidiaries = OrgSubs[i].Subsidiaries;
                    findSubsidiaries(ref subsidiaries, hierarchyTpURL, ref selOrgSub, ref selTouchpoint);
                }
                else
                {
                    OrgSubs.RemoveAt(i);
                    i--;
                }
            }
            removeLastHierarchyLevel();
        }
    }
}
