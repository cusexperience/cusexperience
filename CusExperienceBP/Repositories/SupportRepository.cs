﻿using CusExperience.DAL;
using CusExperience.Entities;
using CusExperience.Utilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace CusExperience.Repositories
{
    public class SupportRepository 
    {
        public CusExpContext dbContext = new CusExpContext();

        public Support GetSupport(string supportURL)
        {
            return dbContext.Supports.Where(s => s.SupportURL.ToLower().Equals(supportURL.ToLower())).FirstOrDefault();
        }

        public Task SendSupport(string id, string supportObj)
        {
            JObject supportJSONObj = JObject.Parse(supportObj);
            SMTPEmailService mailService = new SMTPEmailService();
            string Problem = supportJSONObj["Problem"] != null ? supportJSONObj["Problem"].ToString() : "";
            string Solution = supportJSONObj["Solution"] != null ? supportJSONObj["Solution"].ToString() : "";
            string Title = supportJSONObj["Title"] != null ? supportJSONObj["Title"].ToString() : "";
            string Description = supportJSONObj["Description"] != null ? supportJSONObj["Description"].ToString() : "";
            string Givennames = supportJSONObj["GivenNames"] != null ? supportJSONObj["GivenNames"].ToString() : "";
            string Surname = supportJSONObj["Surname"] != null ? supportJSONObj["Surname"].ToString() : "";
            string Country = supportJSONObj["Country"] != null ? supportJSONObj["Country"].ToString() : "";
            string Mobile = supportJSONObj["Mobile"] != null ? supportJSONObj["Mobile"].ToString() : "";
            string Email = supportJSONObj["EmailAddress"] != null ? supportJSONObj["EmailAddress"].ToString() : "";
            JArray Attachments = supportJSONObj["Attachments"] != null ? (JArray)supportJSONObj["Attachments"] : new JArray();
            string ProblemOrSolution = "";
            if (Problem.ToLower().Equals("true")) ProblemOrSolution = "Problem";
            if (Solution.ToLower().Equals("true"))
            {
                if (!string.IsNullOrEmpty(ProblemOrSolution)) ProblemOrSolution += ",";
                ProblemOrSolution += "Solution";
            }

            string emailMessage = String.Format(
             @"<table> 
               <tr>
               <td>Type</td>
               <td>{0}</td>
               </tr>
               <tr>
               <td>Title</td>
               <td>{1}</td>
               </tr>
               <tr>
               <td>Description</td>
               <td>{2}</td>
               </tr>
               <tr>
               <td>Given Names</td>
               <td>{3}</td>
               </tr>
               <tr>
               <td>Surname</td>
               <td>{4}</td>
               </tr>
               <tr>
               <td>Country</td>
               <td>{5}</td>
               </tr>
               <tr>
               <td>Mobile</td>
               <td>{6}</td>
               </tr>
               <tr>
               <td>Email</td>
               <td>{7}</td>
               </tr>       
            ", ProblemOrSolution, Title, Description, Givennames, Surname, Country, Mobile, Email);

            foreach (JObject Attachment in Attachments)
            {
                if (Attachment["FilePath"] != null)
                {
                    string AttachemenComment = Attachment["CommentText"] != null ? Attachment["CommentText"].ToString() : "";
                    emailMessage += String.Format(@"<tr>
                                                    <td>Attachment</td>
                                                    <td><a href = ""{0}"">{0}</a></td>
                                                    </tr>
                                                    <tr>
                                                    <td>Comment</td>
                                                    <td>{1}</td>
                                                    </tr>", Attachment["FilePath"].ToString(), AttachemenComment);
                }
            }
            Support s = GetSupport(id);

            MailMessage newMessage = new MailMessage();
            newMessage.Subject = Title;
            newMessage.From = new MailAddress(WebConfigurationManager.AppSettings["MailFrom"]);
            newMessage.Body = emailMessage;
            newMessage.To.Add(s.SupportEmailIDs);
            return SMTPEmailService.SendAsync(newMessage);
        }
    }
}
