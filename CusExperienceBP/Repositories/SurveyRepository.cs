﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using CusExperience.Entities;
using CusExperience.Repositories;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.DAL;
using CusExperience.Utilities;
using System.Web;
using System.Security.Principal;
using CusExperience.Repositories.AnalyticsHelpers;
using System.Data.Entity.Validation;
using System.Diagnostics;
using Newtonsoft.Json;
using CusExperience.Exceptions;
using System.Transactions;
using System.Threading.Tasks;
using CusExperienceContract.Entities.SurveyData;


namespace CusExperience.Repositories
{
    public class SurveyRepository
    {
        internal static string QUESTION = "Question";
        internal static string TIMEZONE = "Singapore Standard Time";

        CusExpContext dbContext = new CusExpContext();
        TimeZoneInfo SingaporeTimeZone = TimeZoneInfo.FindSystemTimeZoneById(TIMEZONE);

        private OrgSubRepository oRep = new OrgSubRepository();
        private UserRepository uRep = new UserRepository();

        public bool IsSurveyExists(int surveyId, string userId)
        {
            Survey survey = null;
            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Where(s => s.SurveyID == surveyId && s.Status != "DELETED").FirstOrDefault();
            }
            else if (uRep.isAdministrator(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                if (User == null) return false;
                survey = dbContext.Surveys.Where(s => s.SurveyID == surveyId && s.Status != "DELETED" && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).OrderBy(s => s.IsTemplate).FirstOrDefault();
            }
            if (survey != null) return true;
            return false;
        }

        public List<Survey> GetSurveysByOrgSub(long orgSubId, string userId)
        {
            if (uRep.isSuperAdministrator(userId))
            {
                List<Survey> Surveys = dbContext.Surveys.Include(o=> o.OrgSub.ParentOrgSub).Where(s => s.Status != "DELETED" && (s.OrgSub.OrgSubID == orgSubId || s.IsTemplate == true)).OrderByDescending(s => s.IsTemplate).ThenByDescending(s => s.CreatedDate).ToList();
                OrgSub orgSub = oRep.GetOrgSub(orgSubId, userId);

                foreach (Survey s in Surveys)
                {
                    OrgSub newOrgSub = new OrgSub();
                    newOrgSub.BaseURL = orgSub.BaseURL;
                    newOrgSub.HierarchyURL = orgSub.HierarchyURL;
                    newOrgSub.SurveyURL = orgSub.SurveyURL;
                    s.OrgSub = newOrgSub;
                }
                GetAllParentSurveys(ref Surveys, orgSub.ParentOrgSub, userId);
                return Surveys;
            }
            return null;
        }

        private void GetAllParentSurveys(ref List<Survey> Surveys, OrgSub cOrgSub, string userId)
        {
            if (cOrgSub == null)
                return;
            OrgSub orgSub = oRep.GetOrgSub(cOrgSub.OrgSubID,userId);
            List<Survey> parentSurveys = dbContext.Surveys.Include(o => o.OrgSub.ParentOrgSub).Where(s => s.Status != "DELETED" && s.OrgSub.OrgSubID == cOrgSub.OrgSubID).OrderByDescending(s => s.IsTemplate).ThenByDescending(s => s.CreatedDate).ToList();

            foreach (Survey s in parentSurveys)
            {
                OrgSub newOrgSub = new OrgSub();
                newOrgSub.BaseURL = orgSub.BaseURL;
                newOrgSub.HierarchyURL = orgSub.HierarchyURL;
                newOrgSub.SurveyURL = orgSub.SurveyURL;
                s.OrgSub = newOrgSub;
            }
            
            if(parentSurveys.Count > 0)
                Surveys.InsertRange(0, parentSurveys);

            GetAllParentSurveys(ref Surveys, orgSub.ParentOrgSub, userId);
        }

        public List<Survey> GetSurveys(string userId)
        {
            if (uRep.isSuperAdministrator(userId))
            {
                return dbContext.Surveys.Where(s => s.Status != "DELETED").OrderByDescending(s => s.IsTemplate).ThenByDescending(s => s.CreatedDate).ToList();
            }
            else if (uRep.isAdministrator(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                if (User == null) return null;
                return dbContext.Surveys.Include(o => o.OrgSub).Where(s => s.Status != "DELETED" && (s.OrgSub.OrgSubID == User.OrgSub.OrgSubID || s.IsTemplate == true)).OrderByDescending(s => s.IsTemplate).ThenByDescending(s => s.CreatedDate).ToList();
            }
            return null;
        }

        public Survey GetTemplateSurveyByOrgSub(long orgSubId, string templateName, string userId)
        {
            if (uRep.isSuperAdministrator(userId))
            {
                Survey survey = null;
                if (!string.IsNullOrEmpty(templateName))
                {
                    survey = dbContext.Surveys.Include(s => s.Modules).Where(s => s.TemplateName == templateName && s.Status != "DELETED").FirstOrDefault();
                    if (survey == null) return null;
                    survey.SurveyID = 0;
                    survey.OrgSub = oRep.GetOrgSub(orgSubId, userId);
                    survey.SurveyTitle = "(Add your organisation's name here) Customer Experience Analytics Survey";
                    //Sort based on the module position
                    survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();
                    foreach (Module module in survey.Modules)
                    {
                        module.ModuleID = 0;
                    }
                }
                else
                {
                    survey = new Survey();
                    survey.SurveyID = 0;
                    survey.SurveyTitle = "";
                    survey.Modules = new List<Module>();
                    survey.OrgSub = oRep.GetOrgSub(orgSubId, userId);
                }
                return survey;
            }
            throw new UnauthorizedAccessException();
        }

        public Survey GetTemplateSurvey(string templateName, string userId)
        {
            Survey survey = null;
            if (uRep.isAdministrator(userId))
            {

            if (!string.IsNullOrEmpty(templateName))
            {
                    survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.TemplateName == templateName && s.Status != "DELETED").FirstOrDefault();
                if (survey == null) return null;
                survey.SurveyID = 0;
                    survey.SurveyTitle = "(Add your organisation's name here) Customer Experience Analytics Survey";
                survey.OrgSub = uRep.GetCurrentUser(userId).OrgSub;

                //Sort based on the module position
                survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();
                foreach (Module module in survey.Modules)
                {
                    module.ModuleID = 0;
                }
            }
            else
            {
                survey = new Survey();
                survey.SurveyID = 0;
                survey.SurveyTitle = "";
                survey.Modules = new List<Module>();
                survey.OrgSub = uRep.GetCurrentUser(userId).OrgSub;
            }
            }
            return survey;
        }

        public Survey GetSurvey(int surveyId, string userId)
        {
            Survey survey = null;
            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub).Where(s => s.SurveyID == surveyId
                          && s.Status != "DELETED").FirstOrDefault();
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId) || uRep.isManager(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                if (User == null) return null;
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub).Where(s => s.SurveyID == surveyId
                           && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID && s.Status != "DELETED").FirstOrDefault();
            }

            if (survey == null) return null;
            survey.OrgSub = oRep.GetOrgSub(survey.OrgSub.OrgSubID, userId);
            survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();
            return survey;
        }

        public Survey GetPublishedSurvey(string url)
        {
            string publishedID = "", baseURL = "", heirarchyURL = "";

            OrgSub baseOrgSub, selOrgSub =  null;
            Touchpoint selTouchpoint = null;

            int firstIndex = url.IndexOf('/');
            int secondIndex = url.IndexOf('/', firstIndex + 1);

            if (firstIndex != -1)
            {
                baseURL = url.Substring(0, firstIndex);
                if (secondIndex != -1)
                { 
                    heirarchyURL = "/" + url.Substring(secondIndex + 1, url.Length - (secondIndex + 1));
                    publishedID = url.Substring(firstIndex + 1, secondIndex-(firstIndex + 1));
                }
                else
                {
                    publishedID = url.Substring(firstIndex + 1, url.Length - (firstIndex + 1));
                }
            }
            else
            {
                baseURL = url.ToLower();
            }

            baseOrgSub = oRep.findOrgSub(baseURL, heirarchyURL,ref selOrgSub, ref selTouchpoint);
            if (baseOrgSub == null || selOrgSub == null) return null;

            Survey survey = null;
            survey = dbContext.Surveys.Include(s => s.OrgSub).Include(s => s.Modules).Where(s => s.Status != "DELETED"
                         && s.Status == SURVEYSTATUS.PUBLISHED && s.PublishedID.ToLower() == publishedID.ToLower() && s.OrgSub.OrgSubID == baseOrgSub.OrgSubID).FirstOrDefault();

            if (survey == null) return null;

            survey.OrgSub = baseOrgSub;
            survey.SelectedSubsidiaries = new string[] { selOrgSub.OrgSubID.ToString() };
            survey.SelectedTouchpoints = selTouchpoint!= null?  new string[] { selTouchpoint.TouchpointID.ToString() } : selOrgSub.Touchpoints.Count > 0 ? new string[] { selOrgSub.Touchpoints.First().TouchpointID.ToString() } : null;
            survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();

            foreach (Module module in survey.Modules)
            {
                if (module.ModuleType == "Appreciation")
                {
                    string randomAppCode = "";
                    do
                    {
                        randomAppCode = RandomStringGenerator.RandomString(12);
                        randomAppCode = randomAppCode + TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone).ToString("ddMMyyyy");
                    } while (dbContext.SurveyResponses.Any(s => s.AppreciationCode.Equals(randomAppCode)));
                    survey.AppreciationCode = randomAppCode;
                }
            }
            return survey;
        }

        public void AddorUpdateSurvey(ref Survey survey, string userId)
        {
            if (IsSurveyExists(survey.SurveyID, userId))
                UpdateSurvey(ref survey, userId);
            else
                AddSurvey(ref survey, userId);
        }

        public void UpdatePublishID(int surveyId, string publishID, string userId)
        {
            if (IsSurveyExists(surveyId, userId))
            {
                Survey matchedSurvey = dbContext.Surveys.Include(s => s.OrgSub).Where(s => s.SurveyID == surveyId).FirstOrDefault();

                Survey exists = dbContext.Surveys.Where(s => s.SurveyID != surveyId && s.PublishedID == publishID && s.OrgSub.OrgSubID == matchedSurvey.OrgSub.OrgSubID).FirstOrDefault();
               
                if (exists != null)
                {
                    throw new CusExpException(EXCEPTIONMSG.EX_PUBLISHIDEXISTS);
                }
 
                matchedSurvey.PublishedID = publishID;
                dbContext.Entry(matchedSurvey).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
        }

        public void AddorUpdatePublishedSurvey(ref Survey survey, string userId)
        {
            if (!survey.IsPublished)
            {
                survey.IsPublished = true;
                survey.PublishedDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone);
                if (string.IsNullOrEmpty(survey.PublishedID))
                {
                    string publishedID = "";
                    do
                    {
                        publishedID = RandomStringGenerator.RandomString(8);
                    } while (dbContext.Surveys.Any(s => s.PublishedID.Equals(publishedID)));
                    survey.PublishedID = publishedID;
                }
            }
            survey.Status = SURVEYSTATUS.PUBLISHED;
            if (IsSurveyExists(survey.SurveyID, userId))
                UpdateSurvey(ref survey, userId);
            else
                AddSurvey(ref survey, userId);
        }

        internal void AddSurvey(ref Survey survey, string userId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                try
                {
                    if (string.IsNullOrEmpty(survey.SurveyTitle))
                    {
                        survey.SurveyTitle = "Untitled Survey";
                    }

                    //Created Date doesn't need to be changed.
                    survey.CreatedDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone);
                    survey.CreatedBy = dbContext.Users.Where(u => u.Id == userId).FirstOrDefault();
                    survey.LastUpdatedDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone);
                    survey.PublishedDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone);
                    OrgSub parentOrgSub = oRep.GetOrgSub(survey.OrgSub.OrgSubID, userId);
                    survey.OrgSub = parentOrgSub;

                    dbContext.OrgSubs.Attach(parentOrgSub);

                    foreach (var module in survey.Modules)
                    {
                        dbContext.Entry(module).State = EntityState.Added;
                    }
                    dbContext.Entry(survey).State = EntityState.Added;
                    dbContext.SaveChanges();
                    List<Touchpoint> Touchpoints = parentOrgSub.Touchpoints.ToList();
                    List<OrgSub> subsidiaries = parentOrgSub.Subsidiaries;
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Trace.TraceError("Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage);
                        }
                    }
                    throw;
                }
                catch (Exception ex)
                {
                    Trace.TraceError("Exception : " + ex.Message + "\nSource " + ex.Source);
                    throw;
                }
                scope.Complete();
            }
        }

        internal void UpdateSurvey(ref Survey survey, string userId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                int surveyId = survey.SurveyID;

                if (string.IsNullOrEmpty(survey.SurveyTitle))
                {
                    survey.SurveyTitle = "Untitled Survey";
                }
                Survey matchedSurvey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.CreatedBy).Where(s => s.SurveyID == surveyId).FirstOrDefault();

                //Created Date doesn't need to be changed.
                survey.CreatedDate = matchedSurvey.CreatedDate;
                survey.CreatedBy = matchedSurvey.CreatedBy;
                survey.LastUpdatedDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone);
                survey.LastUpdatedBy = dbContext.Users.Where(u => u.Id == userId).FirstOrDefault();
                if (matchedSurvey.PublishedDate == null && survey.PublishedDate == null) survey.PublishedDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone);
                else if (survey.PublishedDate == null) survey.PublishedDate = matchedSurvey.PublishedDate;

                long[] presentModuleIDs = survey.Modules.Where(q => q.ModuleID != 0).Select(q => q.ModuleID).ToArray();
                if (presentModuleIDs.Length > 0)
                {
                   int surveyID = survey.SurveyID;
                   List<Module> removedModules = dbContext.Modules.Where(m => !presentModuleIDs.Contains(m.ModuleID) && m.Survey.SurveyID == surveyID).ToList();
                   dbContext.Modules.RemoveRange(removedModules);
                   dbContext.SaveChanges();
                }   
                
                foreach (var module in survey.Modules)
                {
                    if (module.ModuleID == 0)
                    {
                        module.Survey = matchedSurvey;
                        dbContext.Modules.Add(module);
                    }
                    else
                    {
                        var existingModule = dbContext.Modules.Find(module.ModuleID);
                        module.Survey = matchedSurvey;
                        dbContext.Entry(existingModule).State = EntityState.Detached;
                        dbContext.Modules.Attach(module);
                        dbContext.Entry(module).State = EntityState.Modified;
                    }
                    dbContext.SaveChanges();
                }

                OrgSub parentOrgSub = oRep.GetOrgSub(survey.OrgSub.OrgSubID, userId);
                dbContext.OrgSubs.Attach(parentOrgSub);
                survey.OrgSub = parentOrgSub;
                survey.Modules = dbContext.Modules.Where(s => s.Survey.SurveyID == matchedSurvey.SurveyID).ToList();
                var existingSurvey = dbContext.Surveys.Find(survey.SurveyID);
                dbContext.Entry(existingSurvey).State = EntityState.Detached;
                dbContext.Surveys.Attach(survey);
                dbContext.Entry(survey).State = EntityState.Modified;
                dbContext.SaveChanges();
                Survey aSurvey = survey;
                Task.Run(() => AddOrRemoveAlerts(aSurvey, userId));
                scope.Complete();
            }
        }

        internal void AddOrRemoveAlerts(Survey s, string userId)
        {
          QuartzScheduler.getInstance().DeleteExistingAlerts(s);
          SurveyData sd = s.JsonSurveyData;
          foreach(Alert alert in sd.AlertSettings.Alerts){
              QuartzScheduler.getInstance().AddNewEmailAlert(s, alert, userId);
          }
        }

        public void DeleteSurvey(Survey survey, string userId)
        {
            var existingSurvey = GetSurvey(survey.SurveyID, userId);
            existingSurvey.PublishedDate = DateTime.Now;
            if (existingSurvey != null)
            {
                existingSurvey.Status = "DELETED";
                existingSurvey.SurveyTitle = existingSurvey.SurveyTitle + "_DELETED";
                dbContext.Entry(existingSurvey).State = EntityState.Modified;
                dbContext.SaveChanges();
                return;
            }
            throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);
        }

        public Survey DuplicateSurvey(int surveyId, string userId)
        {
            Survey survey = null;

            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Include(o => o.OrgSub).Where(s => s.SurveyID == surveyId
                          && s.Status != "DELETED").FirstOrDefault();
            }
            else if (uRep.isAdministrator(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                if (User == null)
                    return null;
                survey = dbContext.Surveys.Include(s => s.Modules).Include(o => o.OrgSub).Where(s => s.SurveyID == surveyId && s.Status != "DELETED"
                    && s.CreatedBy.OrgSub.OrgSubID == User.OrgSub.OrgSubID).OrderBy(s => s.IsTemplate).FirstOrDefault();
            }

            if (survey == null)
                return null;

            string SurveyTitle = survey.SurveyTitle;

            int appendCount = 1;

            while (true)
            {
                int checkExists = dbContext.Surveys.Where(s => s.SurveyTitle == SurveyTitle && s.Status != "DELETED").Count();
                if (checkExists <= 0) break;
                SurveyTitle = appendCount == 1 ? SurveyTitle + appendCount : SurveyTitle.Substring(0, SurveyTitle.Length - (appendCount - 1).ToString().Length) + appendCount;
                appendCount++;
            }

            Survey duplicateSurvey = new Survey()
            {
                SurveyTitle = SurveyTitle,
                SurveyData = survey.SurveyData,
                OrgSub = survey.OrgSub,
                IsPublished = false,
                Status = SURVEYSTATUS.UNPUBLISHED,
                CreatedDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone),
                LastUpdatedDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone),
                PublishedDate = null,
                CreatedBy = dbContext.Users.Where(u => u.Id == userId).FirstOrDefault(),
                Modules = new List<Module>()
            };

            foreach (Module module in survey.Modules)
            {
                Module dupModule = new Module()
                {
                    ModuleType = module.ModuleType,
                    ModuleData = module.ModuleData,
                    Position = module.Position
                };
                duplicateSurvey.Modules.Add(dupModule);
            }

            dbContext.Surveys.Add(duplicateSurvey);
            dbContext.SaveChanges();

            return duplicateSurvey;
        }

        public void UnPublishedSurvey(int surveyId, string userId)
        {
            Survey survey = null;

            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Where(s => s.SurveyID == surveyId
                          && s.Status != "DELETED").FirstOrDefault();
            }
            else if (uRep.isAdministrator(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                if (User == null) return;
                survey = dbContext.Surveys.Include(s => s.Modules).Where(s => s.SurveyID == surveyId && s.Status != "DELETED"
                    && s.CreatedBy.OrgSub.OrgSubID == User.OrgSub.OrgSubID).FirstOrDefault();
            }

            survey.Status = SURVEYSTATUS.UNPUBLISHED;
            dbContext.Entry(survey).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public List<string> GetAttachmentExtensions(int surveyId, long moduleId)
        {
            Module module = dbContext.Modules.Where(m => m.Survey.SurveyID == surveyId && m.ModuleID == moduleId).FirstOrDefault();
            if (module == null) return null;
            AttachmentAnalyticsStrategy atStrategy = new AttachmentAnalyticsStrategy();
            return atStrategy.GetAttachmentExtensions(module.ModuleData);
        }


    }
}