﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using CusExperience.Entities;
using CusExperience.Repositories;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.DAL;
using CusExperience.Utilities;
using System.Web;
using System.Security.Principal;
using CusExperience.Repositories.AnalyticsHelpers;
using Newtonsoft.Json;
using CusExperience.Exceptions;
using System.Data.Entity.Validation;

namespace CusExperience.Repositories
{
    public class AnalyticsRepository
    {
        public CusExpContext dbContext = new CusExpContext();
        internal static string QUESTION = "Question";
        UserRepository uRep = new UserRepository();
        ResponseRepository rRep = new ResponseRepository();
        OrgSubRepository oRep = new OrgSubRepository();

        internal static string TIMEZONE = "Singapore Standard Time";
        TimeZoneInfo SingaporeTimeZone = TimeZoneInfo.FindSystemTimeZoneById(TIMEZONE);

        Dictionary<long, AnalyticsStrategy> strategyDictionary = new Dictionary<long, AnalyticsStrategy>();

        Dictionary<OrgSub, List<long>> orgFStatistics = new Dictionary<OrgSub, List<long>>();
        Dictionary<OrgSub, double> orgEStatistics = new Dictionary<OrgSub, double>();

        Dictionary<Touchpoint, List<long>> tpFStatistics = new Dictionary<Touchpoint, List<long>>();
        Dictionary<Touchpoint, double> tpEStatistics = new Dictionary<Touchpoint, double>();

        Dictionary<long, string> orgSubDictionary = new Dictionary<long, string>();
        Dictionary<long, string> tpDictionary = new Dictionary<long, string>();

        public Survey GetAnalytics(int surveyId, long orgSubID, string userId)
        {
            Survey survey = null;
            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId).FirstOrDefault<Survey>();
                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);
                survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();
                survey.OrgSub = oRep.GetAnalyticsOrgSub(orgSubID, ref orgSubDictionary, ref tpDictionary, userId);
                var surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == surveyId && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID) && s.Status != "DELETED").ToList();
                return GetAnalyticsForResponses(survey, surveyResponses);
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId && s.Status != "DELETED" && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).FirstOrDefault();
                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);
                survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();
                survey.OrgSub = oRep.GetAnalyticsOrgSub(orgSubID, ref orgSubDictionary, ref tpDictionary, userId);
                var surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == surveyId && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID) && s.Status != "DELETED").ToList();
                return GetAnalyticsForResponses(survey, surveyResponses);
            }
            else if (uRep.isManager(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId && s.Status != "DELETED" && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).FirstOrDefault();
                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);
                var UserTouchpointNames = User.Touchpoints.Select(t => t.Touchpoint.TouchpointName).ToList();
                survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();
                survey.OrgSub = oRep.GetAnalyticsOrgSub(survey.OrgSub.OrgSubID, ref orgSubDictionary, ref tpDictionary, userId);
                var surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == surveyId && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID) && s.Status != "DELETED").ToList();
                return GetAnalyticsForResponses(survey, surveyResponses);
            }
            throw new UnauthorizedAccessException();
        }

        public Survey GetAnalyticsForResponses(Survey survey, List<SurveyResponse> surveyResponses)
        {
            strategyDictionary.Clear();

            survey.Modules = survey.Modules.Where(m => m.ModuleType == QUESTION).ToList();

            foreach (Module module in survey.Modules)
            {
                JObject moduleData = JObject.Parse(module.ModuleData);
                if (moduleData == null) continue;

                AnalyticsStrategy strategyObject = AnalyticsStrategyFactory.getAnalyticsStrategy(module.ModuleID, moduleData, null, null);
                if (strategyObject == null) continue;

                foreach (SurveyResponse surveyResponse in surveyResponses)
                {
                    surveyResponse.CusXPIndex = ResponseRepository.findCusXPIndex(surveyResponse.ModuleResponses.ToList());
                    strategyObject.collectAnalyticsData(surveyResponse);
                }

                strategyObject.getModuleDataWithAnalytics();
                strategyDictionary.Add(module.ModuleID, strategyObject);
                module.ModuleData = moduleData.ToString();
            }

            // AnalyticsCacheManager.CaptureAnalyticsData(survey);

            if (survey.OrgSub != null)
            {

                JObject jsonTouchpointAnalyticsData = new JObject();
                JObject jsonOrgSubAnalyticsData = new JObject();

                JArray jsonTouchpointStatistics = new JArray();
                JArray jsonOrgSubStatistics = new JArray();
                JArray jsonHierarchyStatistics = new JArray();

                int overAllCount = surveyResponses.Count;
                double overAllCusXPIndex = surveyResponses.Count > 0 ? surveyResponses.Select(sr => sr.CusXPIndex).Average() : 0.0;

                orgFStatistics = surveyResponses.Select(sr => new { OrgSub = sr.OrgSub, sr.SurveyResponseID }).Where(t => t.OrgSub != null).GroupBy(t => t.OrgSub).Select(p => new { p.Key, SRID = p.Select(x => x.SurveyResponseID).ToList() }).OrderByDescending(a => a.SRID.Count).Where(t => t.Key != null).ToDictionary(t => t.Key, t => t.SRID);
                orgEStatistics = surveyResponses.Select(sr => new { OrgSub = sr.OrgSub, sr.CusXPIndex }).Where(t => t.OrgSub != null).GroupBy(t => t.OrgSub).Where(t => t.Key != null).ToDictionary(group => group.Key, group => group.Average(g => g.CusXPIndex)).OrderByDescending(d => d.Value).ToDictionary(t => t.Key, t => t.Value);

                tpFStatistics = surveyResponses.Select(sr => new { Touchpoint = sr.Touchpoint, OrgSub = sr.OrgSub, sr.SurveyResponseID }).Where(t => t.OrgSub != null && t.Touchpoint != null).GroupBy(t => new { t.OrgSub, t.Touchpoint }).Select(p => new { p.Key, SRID = p.Select(x => x.SurveyResponseID).ToList() }).OrderByDescending(a => a.SRID.Count).ToDictionary(group => group.Key.Touchpoint, t => t.SRID);
                tpEStatistics = surveyResponses.Select(sr => new { Touchpoint = sr.Touchpoint, OrgSub = sr.OrgSub, sr.CusXPIndex }).Where(t => t.OrgSub != null && t.Touchpoint != null).GroupBy(t => new { t.OrgSub, t.Touchpoint }).ToDictionary(group => group.Key.Touchpoint, group => group.Average(g => g.CusXPIndex)).OrderByDescending(d => d.Value).ToDictionary(t => t.Key, t => t.Value);


                JObject jsonovItem = new JObject();
                jsonovItem["Text"] = "Overall Hierarchy";
                jsonovItem["Count"] = overAllCount;
                jsonovItem["Score"] = overAllCusXPIndex;
                jsonOrgSubStatistics.Add(jsonovItem);
                jsonHierarchyStatistics.Add(jsonovItem);

                foreach (KeyValuePair<OrgSub, double> eachItem in orgEStatistics)
                {
                    JObject jsoneachItem = new JObject();
                    jsoneachItem["Text"] = orgSubDictionary[eachItem.Key.OrgSubID];
                    jsoneachItem["Count"] = orgFStatistics[eachItem.Key].Count;
                    jsoneachItem["Score"] = eachItem.Value;
                    jsonOrgSubStatistics.Add(jsoneachItem);
                }

                foreach (KeyValuePair<long, string> eachItem in orgSubDictionary)
                {
                    if (!orgEStatistics.Select(s => s.Key.OrgSubID).ToList().Contains(eachItem.Key))
                    {
                        JObject jsoneachItem = new JObject();
                        jsoneachItem["Text"] = eachItem.Value;
                        jsoneachItem["Count"] = 0;
                        jsoneachItem["Score"] = 0.0;
                        jsonOrgSubStatistics.Add(jsoneachItem);
                    }
                }

                foreach (KeyValuePair<Touchpoint, double> eachItem in tpEStatistics)
                {
                    JObject jsoneachItem = new JObject();
                    jsoneachItem["Text"] = tpDictionary[eachItem.Key.TouchpointID];
                    jsoneachItem["Count"] = tpFStatistics[eachItem.Key].Count;
                    jsoneachItem["Score"] = eachItem.Value;
                    jsonTouchpointStatistics.Add(jsoneachItem);
                }

                foreach (KeyValuePair<long, string> eachItem in orgSubDictionary)
                {
                    OrgSub orgSub = orgEStatistics.Where(o => o.Key.OrgSubID == eachItem.Key).Select(o => o.Key).FirstOrDefault();

                    if (orgSub != null)
                    {
                        JObject jsoneachItem = new JObject();
                        jsoneachItem["Text"] = orgSubDictionary[orgSub.OrgSubID];
                        jsoneachItem["Count"] = orgFStatistics[orgSub].Count;
                        jsoneachItem["Score"] = orgEStatistics[orgSub];
                        jsonHierarchyStatistics.Add(jsoneachItem);
                    }
                    else
                    {
                        JObject jsoneachItem = new JObject();
                        jsoneachItem["Text"] = eachItem.Value;
                        jsoneachItem["Count"] = 0;
                        jsoneachItem["Score"] = 0.0;
                        jsonHierarchyStatistics.Add(jsoneachItem);
                    }
                }

                foreach (KeyValuePair<long, string> eachItem in tpDictionary)
                {
                    if (!tpEStatistics.Select(t => t.Key.TouchpointID).ToList().Contains(eachItem.Key))
                    {
                        JObject jsoneachItem = new JObject();
                        jsoneachItem["Text"] = eachItem.Value;
                        jsoneachItem["Count"] = 0;
                        jsoneachItem["Score"] = 0.0;
                        jsonTouchpointStatistics.Add(jsoneachItem);
                    }
                }

                jsonOrgSubAnalyticsData["HStatistics"] = jsonHierarchyStatistics;
                jsonOrgSubAnalyticsData["Statistics"] = jsonOrgSubStatistics;
                survey.OrgSubAnalyticsData = jsonOrgSubAnalyticsData.ToString();

                jsonTouchpointAnalyticsData["Statistics"] = jsonTouchpointStatistics;
                survey.TouchpointAnalyticsData = jsonTouchpointAnalyticsData.ToString();
            }
            survey.ExpertComments = GetExpertComments(survey.SurveyID);
            return survey;
        }

        public Survey GetSearchAnalytics(Survey searchSurvey, string userId)
        {
            Survey survey = null;
            DateTime? fromDateTime = DateTime.ParseExact(searchSurvey.FromDateTime, "dd/MM/yyyy @ HH:mm", null);
            DateTime? toDateTime = DateTime.ParseExact(searchSurvey.ToDateTime, "dd/MM/yyyy @ HH:mm", null);

            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == searchSurvey.SurveyID).FirstOrDefault<Survey>();
                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);

                survey.OrgSub = oRep.GetAnalyticsOrgSub(searchSurvey.OrgSub.OrgSubID, ref orgSubDictionary, ref tpDictionary, userId);

                List<SurveyResponse> surveyResponses = new List<SurveyResponse>();
                if (searchSurvey.SelectedTouchpoints == null) return null;

                if (searchSurvey.SelectedTouchpoints.Length > 0)
                {
                    if (searchSurvey.SelectedTouchpoints.Contains("All") && searchSurvey.SelectedTouchpoints.Length > 1)
                    {
                        surveyResponses.AddRange(dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(mr => mr.Module)).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList());
                    }
                    else
                    {
                        surveyResponses.AddRange(dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => searchSurvey.SelectedTouchpoints.Contains(s.Touchpoint.TouchpointID.ToString()) && s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList());
                    }
                }

                if (searchSurvey.SelectedSubsidiaries.Length > 0)
                {
                    if (searchSurvey.SelectedSubsidiaries.Contains("All") && searchSurvey.SelectedSubsidiaries.Length > 1)
                    {
                        surveyResponses.AddRange(dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(mr => mr.Module)).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == searchSurvey.SurveyID && s.Touchpoint == null && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList());
                    }
                    else
                    {
                        surveyResponses.AddRange(dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => searchSurvey.SelectedSubsidiaries.Contains(s.OrgSub.OrgSubID.ToString()) && s.Touchpoint == null && s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList());
                    }
                }

                if (searchSurvey.SelectedTouchpoints.Length <= 0 && searchSurvey.SelectedSubsidiaries.Length <= 0)
                {
                    surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(mr => mr.Module)).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList();
                }

                return GetSearchAnalyticsForResponses(searchSurvey, survey, surveyResponses);
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);

                survey.OrgSub = oRep.GetAnalyticsOrgSub(survey.OrgSub.OrgSubID, ref orgSubDictionary, ref tpDictionary, userId);

                survey = dbContext.Surveys.Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == searchSurvey.SurveyID && s.Status != "DELETED" && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).FirstOrDefault();

                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);

                survey.OrgSub = oRep.GetOrgSub(survey.OrgSub.OrgSubID, userId);

                List<SurveyResponse> surveyResponses = new List<SurveyResponse>();
                if (searchSurvey.SelectedTouchpoints == null) return null;

                if (searchSurvey.SelectedTouchpoints.Length > 0)
                {
                    if (searchSurvey.SelectedTouchpoints.Contains("All") && searchSurvey.SelectedSubsidiaries.Contains("All"))
                    {
                        surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(mr => mr.Module)).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList();
                    }
                    else
                    {
                        surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList();
                    }
                }
                else if (searchSurvey.SelectedTouchpoints.Length <= 0 && searchSurvey.SelectedSubsidiaries.Length > 0)
                {
                    if (searchSurvey.SelectedSubsidiaries.Contains("All"))
                    {
                        surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(mr => mr.Module)).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList();
                    }
                    else
                    {
                        surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList();
                    }
                }
                else
                {
                    surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(mr => mr.Module)).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList();
                }
                return GetSearchAnalyticsForResponses(searchSurvey, survey, surveyResponses);
            }
            else if (uRep.isManager(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);

                survey = dbContext.Surveys.Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == searchSurvey.SurveyID && s.Status != "DELETED" && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).FirstOrDefault();

                survey.OrgSub = oRep.GetAnalyticsOrgSub(survey.OrgSub.OrgSubID, ref orgSubDictionary, ref tpDictionary, userId);

                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);

                survey.OrgSub = oRep.GetOrgSub(survey.OrgSub.OrgSubID, userId);

                List<SurveyResponse> surveyResponses = new List<SurveyResponse>();
                if (searchSurvey.SelectedTouchpoints == null) return null;

                var UserTouchpointNames = User.Touchpoints.Select(t => t.Touchpoint.TouchpointName).ToList();

                if (searchSurvey.SelectedTouchpoints.Length > 0)
                {
                    if (searchSurvey.SelectedTouchpoints.Contains("All") && searchSurvey.SelectedSubsidiaries.Contains("All"))
                    {
                        surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(mr => mr.Module)).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => UserTouchpointNames.Contains(s.Touchpoint.TouchpointName) && s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList();
                    }
                    else
                    {
                        surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => searchSurvey.SelectedTouchpoints.Contains(s.Touchpoint.TouchpointName) && s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList();
                    }
                }
                else
                {
                    surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Where(s => s.Survey.SurveyID == searchSurvey.SurveyID && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime && s.Status != "DELETED" && orgSubDictionary.Keys.Contains(s.OrgSub.OrgSubID)).ToList();
                }
                return GetSearchAnalyticsForResponses(searchSurvey, survey, surveyResponses);
            }
            throw new UnauthorizedAccessException();
        }

        public Survey GetSearchAnalyticsForResponses(Survey searchSurvey, Survey survey, List<SurveyResponse> surveyResponses)
        {
            strategyDictionary.Clear();

            List<string> keywords = new List<string>();
            List<string> woKeywords = new List<string>();

            if (!string.IsNullOrEmpty(searchSurvey.Keywords))
                keywords = searchSurvey.Keywords.ToLower().Split(',').Select(s => s.Trim().ToLower()).Where(s => !string.IsNullOrEmpty(s)).ToList();

            if (!string.IsNullOrEmpty(searchSurvey.WOKeywords))
                woKeywords = searchSurvey.WOKeywords.ToLower().Split(',').Select(s => s.Trim().ToLower()).Where(s => !string.IsNullOrEmpty(s)).ToList();

            List<SurveyResponse> filteredResponses = new List<SurveyResponse>();
            searchSurvey.Modules = searchSurvey.Modules.Where(m => m.ModuleType == QUESTION).ToList();

            foreach (Module module in searchSurvey.Modules)
            {
                JObject moduleData = JObject.Parse(module.ModuleData);
                if (moduleData == null) continue;

                AnalyticsStrategy strategyObject = AnalyticsStrategyFactory.getAnalyticsStrategy(module.ModuleID, moduleData, keywords, woKeywords);
                if (strategyObject == null) continue;

                strategyDictionary.Add(module.ModuleID, strategyObject);
            }

            foreach (SurveyResponse surveyResponse in surveyResponses)
            {
                bool isAndQuestionNotFound = true;
                bool isOrQuestionNotFound = true;
                bool isAndFilteredResponse = true;
                bool isOrFilteredResponse = false;
                bool isKeywordFound = false;
                bool isWOKeywordFound = false;

                if (keywords == null && woKeywords == null) isKeywordFound = true;

                foreach (KeyValuePair<long, AnalyticsStrategy> eachStrategy in strategyDictionary)
                {
                    AnalyticsStrategy strategyObject = eachStrategy.Value;

                    ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == eachStrategy.Key).FirstOrDefault();

                    if (mr != null)
                    {
                        JObject responseData = JObject.Parse(mr.ModuleData);
                        if (strategyObject is RatingAnalyticsStrategy || strategyObject is MultipleAnalyticsStrategy || strategyObject is DGAnalyticsStrategy)
                        {
                            isAndQuestionNotFound = false;
                            if (!strategyObject.IsFilteredResponse(responseData))
                            {
                                isAndFilteredResponse = false;
                                break;
                            }
                        }
                        else
                        {
                            isOrQuestionNotFound = false;
                            if (strategyObject.IsFilteredResponse(responseData))
                            {
                                isOrFilteredResponse = true;
                            }
                        }

                        if (woKeywords.Count > 0 && strategyObject.IsKeywordFound(responseData, woKeywords.ToList()))
                        {
                            isWOKeywordFound = true;
                            break;
                        }

                        if (!isKeywordFound && keywords.Count > 0 && strategyObject.IsKeywordFound(responseData, keywords.ToList()))
                            isKeywordFound = true;
                    }
                }
                if ((isAndQuestionNotFound || isAndFilteredResponse) && (isOrQuestionNotFound || isOrFilteredResponse) && (keywords.Count <= 0 || isKeywordFound) && (woKeywords.Count <= 0 || !isWOKeywordFound)) filteredResponses.Add(surveyResponse);
            }

            foreach (Module module in searchSurvey.Modules)
            {
                AnalyticsStrategy strategyObject = strategyDictionary[module.ModuleID];
                foreach (SurveyResponse surveyResponse in filteredResponses)
                {
                    surveyResponse.CusXPIndex = ResponseRepository.findCusXPIndex(surveyResponse.ModuleResponses.ToList());
                    strategyObject.collectAnalyticsData(surveyResponse);
                }
                strategyObject.getModuleDataWithSearchAnalytics();
                module.ModuleData = strategyObject.ModuleData.ToString();
            }

            if (filteredResponses.Count <= 0) searchSurvey.NoResultsFound = true;
            else searchSurvey.NoResultsFound = false;

            if (survey.OrgSub != null)
            {
                JObject jsonTouchpointAnalyticsData = new JObject();
                JObject jsonOrgSubAnalyticsData = new JObject();

                JArray jsonTouchpointStatistics = new JArray();
                JArray jsonOrgSubStatistics = new JArray();
                JArray jsonHierarchyStatistics = new JArray();

                int overAllCount = filteredResponses.Count;
                double overAllCusXPIndex = filteredResponses.Count > 0 ? filteredResponses.Select(sr => sr.CusXPIndex).Average() : 0.0;

                orgFStatistics = filteredResponses.Select(sr => new { OrgSub = sr.OrgSub, sr.SurveyResponseID }).Where(t => t.OrgSub != null).GroupBy(t => t.OrgSub).Select(p => new { p.Key, SRID = p.Select(x => x.SurveyResponseID).ToList() }).OrderByDescending(a => a.SRID.Count).Where(t => t.Key != null).ToDictionary(t => t.Key, t => t.SRID);
                orgEStatistics = filteredResponses.Select(sr => new { OrgSub = sr.OrgSub, sr.CusXPIndex }).Where(t => t.OrgSub != null).GroupBy(t => t.OrgSub).Where(t => t.Key != null).ToDictionary(group => group.Key, group => group.Average(g => g.CusXPIndex)).OrderByDescending(d => d.Value).ToDictionary(t => t.Key, t => t.Value);

                tpFStatistics = filteredResponses.Select(sr => new { Touchpoint = sr.Touchpoint, OrgSub = sr.OrgSub, sr.SurveyResponseID }).Where(t => t.OrgSub != null && t.Touchpoint != null).GroupBy(t => new { t.OrgSub, t.Touchpoint }).Select(p => new { p.Key, SRID = p.Select(x => x.SurveyResponseID).ToList() }).OrderByDescending(a => a.SRID.Count).ToDictionary(group => group.Key.Touchpoint, t => t.SRID);
                tpEStatistics = filteredResponses.Select(sr => new { Touchpoint = sr.Touchpoint, OrgSub = sr.OrgSub, sr.CusXPIndex }).Where(t => t.OrgSub != null && t.Touchpoint != null).GroupBy(t => new { t.OrgSub, t.Touchpoint }).ToDictionary(group => group.Key.Touchpoint, group => group.Average(g => g.CusXPIndex)).OrderByDescending(d => d.Value).ToDictionary(t => t.Key, t => t.Value);

                JObject jsonovItem = new JObject();
                jsonovItem["Text"] = "Overall Hierarchy";
                jsonovItem["Count"] = overAllCount;
                jsonovItem["Score"] = overAllCusXPIndex;
                jsonOrgSubStatistics.Add(jsonovItem);
                jsonHierarchyStatistics.Add(jsonovItem);

                foreach (KeyValuePair<OrgSub, double> eachItem in orgEStatistics)
                {
                    JObject jsoneachItem = new JObject();
                    jsoneachItem["Text"] = orgSubDictionary[eachItem.Key.OrgSubID];
                    jsoneachItem["Count"] = orgFStatistics[eachItem.Key].Count;
                    jsoneachItem["Score"] = eachItem.Value;
                    jsonOrgSubStatistics.Add(jsoneachItem);
                }

                foreach (KeyValuePair<long, string> eachItem in orgSubDictionary)
                {
                    if (!orgEStatistics.Select(s => s.Key.OrgSubID).ToList().Contains(eachItem.Key))
                    {
                        JObject jsoneachItem = new JObject();
                        jsoneachItem["Text"] = eachItem.Value;
                        jsoneachItem["Count"] = 0;
                        jsoneachItem["Score"] = 0.0;
                        jsonOrgSubStatistics.Add(jsoneachItem);
                    }
                }

                foreach (KeyValuePair<long, string> eachItem in orgSubDictionary)
                {
                    OrgSub orgSub = orgEStatistics.Where(o => o.Key.OrgSubID == eachItem.Key).Select(o => o.Key).FirstOrDefault();

                    if (orgSub != null)
                    {
                        JObject jsoneachItem = new JObject();
                        jsoneachItem["Text"] = orgSubDictionary[orgSub.OrgSubID];
                        jsoneachItem["Count"] = orgFStatistics[orgSub].Count;
                        jsoneachItem["Score"] = orgEStatistics[orgSub];
                        jsonHierarchyStatistics.Add(jsoneachItem);
                        
                    }
                    else
                    {
                        JObject jsoneachItem = new JObject();
                        jsoneachItem["Text"] = eachItem.Value;
                        jsoneachItem["Count"] = 0;
                        jsoneachItem["Score"] = 0.0;
                        jsonHierarchyStatistics.Add(jsoneachItem);
                      
                    }
                }

                foreach (KeyValuePair<Touchpoint, double> eachItem in tpEStatistics)
                {
                    JObject jsoneachItem = new JObject();
                    jsoneachItem["Text"] = tpDictionary[eachItem.Key.TouchpointID];
                    jsoneachItem["Count"] = tpFStatistics[eachItem.Key].Count;
                    jsoneachItem["Score"] = eachItem.Value;
                    jsonTouchpointStatistics.Add(jsoneachItem);
                }

                foreach (KeyValuePair<long, string> eachItem in tpDictionary)
                {
                    if (!tpEStatistics.Select(t => t.Key.TouchpointID).ToList().Contains(eachItem.Key))
                    {
                        JObject jsoneachItem = new JObject();
                        jsoneachItem["Text"] = eachItem.Value;
                        jsoneachItem["Count"] = 0;
                        jsoneachItem["Score"] = 0.0;
                        jsonTouchpointStatistics.Add(jsoneachItem);
                    }
                }

                jsonOrgSubAnalyticsData["HStatistics"] = jsonHierarchyStatistics;
                jsonOrgSubAnalyticsData["Statistics"] = jsonOrgSubStatistics;

                searchSurvey.OrgSubAnalyticsData = jsonOrgSubAnalyticsData.ToString();

                jsonTouchpointAnalyticsData["Statistics"] = jsonTouchpointStatistics;
                searchSurvey.TouchpointAnalyticsData = jsonTouchpointAnalyticsData.ToString();
            }
            searchSurvey.ExpertComments = GetExpertComments(survey.SurveyID);
            return searchSurvey;
        }

        public Survey GetQuantitativeAnalytics(string url)
        {
            Survey survey = null;

            string[] urlParams = url.Split('/');

            if (urlParams.Length > 3) return null;

            if (urlParams.Length == 1)
            {
                var dbSurvey = dbContext.Surveys.Where(s => url.ToLower().Equals(s.PublishedID.ToLower())
                            && s.PublishedID.Equals("")
                            && s.Status != "DELETED"
                            && s.IsAllowViewResults == true
                            && s.IsPublished == true).Select(sv => new { sv, sv.OrgSub, TouchPoints = sv.OrgSub.Touchpoints, sv.Modules }).FirstOrDefault();

                if (dbSurvey == null) return null;

                survey = dbSurvey.sv;
                survey.Modules = dbSurvey.Modules;
                survey.OrgSub = dbSurvey.OrgSub;
                survey.OrgSub.Touchpoints = dbSurvey.TouchPoints;
            }
            else if (urlParams.Length == 2)
            {
                string publishURL = urlParams[0];
                string secondParam = urlParams[1];

                var dbSurvey = dbContext.Surveys.Where(s => (s.PublishedID.ToLower().Equals(publishURL.ToLower()) && s.PublishedID.ToLower().Equals(secondParam.ToLower()))
                             && s.Status != "DELETED"
                             && s.IsAllowViewResults == true
                             && s.IsPublished == true).Select(sv => new { sv, sv.OrgSub, TouchPoints = sv.OrgSub.Touchpoints, sv.Modules }).FirstOrDefault();

                if (dbSurvey != null)
                {
                    survey = dbSurvey.sv;
                    survey.OrgSub = dbSurvey.OrgSub;
                    survey.OrgSub.Touchpoints = dbSurvey.TouchPoints;
                    survey.CreatedBy = dbSurvey.sv.CreatedBy;
                    survey.Modules = dbSurvey.Modules;
                }
                else if (dbSurvey == null && !secondParam.Equals(""))
                {
                    dbSurvey = dbContext.Surveys.Where(s => (s.PublishedID.ToLower().Equals(publishURL) && s.PublishedID.Equals(""))
                             && s.Status != "DELETED"
                             && s.IsAllowViewResults == true
                             && s.IsPublished == true).Select(sv => new { sv, sv.OrgSub, TouchPoints = sv.OrgSub.Touchpoints, sv.Modules }).FirstOrDefault();

                    if (dbSurvey == null) return null;

                    survey = dbSurvey.sv;
                    survey.OrgSub = dbSurvey.OrgSub;
                    survey.OrgSub.Touchpoints = dbSurvey.TouchPoints;
                    survey.CreatedBy = dbSurvey.sv.CreatedBy;
                    survey.Modules = dbSurvey.Modules;
                }
                else return null;

            }
            else if (urlParams.Length == 3)
            {
                string baseURL = urlParams[0] + "/" + urlParams[1];
                string configURL = urlParams[2];

                var dbSurvey = dbContext.Surveys.Where(s => baseURL.ToLower().Equals(s.PublishedID.ToLower() + "/" + s.PublishedID.ToLower())
                              && s.Status != "DELETED"
                              && s.IsAllowViewResults == true
                              && s.IsPublished == true).Select(sv => new { sv, sv.OrgSub, TouchPoints = sv.OrgSub.Touchpoints, sv.Modules }).FirstOrDefault();

                if (dbSurvey == null) return null;

                survey = dbSurvey.sv;
                survey.CreatedBy = dbSurvey.sv.CreatedBy;
                survey.Modules = dbSurvey.Modules;
                survey.OrgSub = dbSurvey.OrgSub;
                survey.OrgSub.Touchpoints = dbSurvey.TouchPoints;
            }

            if (survey == null)
                return null;

            var surveyResponses = dbContext.SurveyResponses.Include("ModuleResponses").Where(s => s.Survey.SurveyID == survey.SurveyID && s.Status != "DELETED").ToList();

            //Sort based on the module position
            survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();

            return GetAnalyticsForResponses(survey, surveyResponses);
        }

        private Survey GetQuanitativeAnalyticsForResponses(Survey survey, List<SurveyResponse> surveyResponses)
        {
            survey.Modules = survey.Modules.Where(m => m.ModuleType == QUESTION).ToList();

            int moduleCount = survey.Modules.Count;

            survey.Modules = survey.Modules.Where(m => m.ModuleType == QUESTION).ToList();

            for (int indexCount = 0; indexCount < moduleCount; indexCount++)
            {
                Module module = survey.Modules.ElementAt(indexCount);

                JObject moduleData = JObject.Parse(module.ModuleData);
                if (moduleData == null) continue;

                string questionType = moduleData["QuestionType"].ToString();

                if (questionType == QUESTIONTYPES.MULTIPLE || questionType == QUESTIONTYPES.RATE || questionType == QUESTIONTYPES.CHOICE)
                {

                    AnalyticsStrategy strategyObject = AnalyticsStrategyFactory.getAnalyticsStrategy(module.ModuleID, moduleData, null, null);
                    if (strategyObject == null) continue;

                    foreach (SurveyResponse surveyResponse in surveyResponses)
                    {
                        ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == module.ModuleID).FirstOrDefault();
                        strategyObject.collectAnalyticsData(surveyResponse);
                    }

                    strategyObject.getModuleDataWithAnalytics();

                }
                else
                {
                    survey.Modules.Remove(survey.Modules.ElementAt(indexCount));
                    indexCount--;
                    moduleCount--;
                }
            }
            return survey;
        }

        public Dictionary<long, string> GetComments(string commentsObj, string userId)
        {
            JObject jsonCommentsObj = JObject.Parse(commentsObj);

            List<SurveyResponse> surveyResponses = new List<SurveyResponse>();
            Dictionary<long, string> Comments = new Dictionary<long, string>();

            if (jsonCommentsObj["keyWord"] == null || jsonCommentsObj["moduleID"] == null || jsonCommentsObj["searchSurvey"] == null) return null;
            string keyWord = jsonCommentsObj["keyWord"].ToString();

            Survey searchSurvey = JsonConvert.DeserializeObject<Survey>(jsonCommentsObj["searchSurvey"].ToString());
            GetSearchAnalytics(searchSurvey, userId);

            long moduleID = Convert.ToInt64(jsonCommentsObj["moduleID"]);
            AnalyticsStrategy strategy = strategyDictionary[moduleID];
            if (strategy is TagAnalyticsStrategy)
            {
                TagAnalyticsStrategy tagAnalytcicsStrategy = (TagAnalyticsStrategy)strategy;
                if (!tagAnalytcicsStrategy.WordFrequency.ContainsKey(keyWord)) return null;
                List<long> SRIDs = tagAnalytcicsStrategy.WordFrequency[keyWord];
                surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(m => m.Module)).Where(s => SRIDs.Contains(s.SurveyResponseID) && s.Status != "DELETED").ToList();
            }
            else if (strategy is TextAnalyticsStrategy)
            {
                TextAnalyticsStrategy textAnalyticsStrategy = (TextAnalyticsStrategy)strategy;
                if (!textAnalyticsStrategy.WordFrequency.ContainsKey(keyWord)) return null;
                List<long> SRIDs = textAnalyticsStrategy.WordFrequency[keyWord];
                surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(m => m.Module)).Where(s => SRIDs.Contains(s.SurveyResponseID) && s.Status != "DELETED").ToList();
            }
            else if (strategy is RatingAnalyticsStrategy)
            {
                RatingAnalyticsStrategy ratingAnalyticsStrategy = (RatingAnalyticsStrategy)strategy;
                if (!ratingAnalyticsStrategy.WordFrequency.ContainsKey(keyWord)) return null;
                List<long> SRIDs = ratingAnalyticsStrategy.WordFrequency[keyWord];
                surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(m => m.Module)).Where(s => SRIDs.Contains(s.SurveyResponseID) && s.Status != "DELETED").ToList();
            }

            foreach (SurveyResponse surveyResponse in surveyResponses)
            {
                ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
                if (mr != null)
                {
                    JObject responseData = JObject.Parse(mr.ModuleData);
                    if (responseData == null) continue;

                    JObject answerData = (JObject)responseData["AnswerData"];
                    if (answerData == null) continue;

                    string comment = answerData["CommentText"].ToString();
                    if (!string.IsNullOrEmpty(comment) && comment.ToLower().RemovePunctutions().Split(' ', '\n').Contains(keyWord))
                        Comments.Add(surveyResponse.SurveyResponseID, string.Join(" ", comment.Split(' ', '\n').Select(c => keyWord.Equals(c.RemovePunctutions().ToLower()) ? c.Replace(c, "<strong>" + c + "</strong>") : c)));
                }
            }
            return Comments;
        }

        public Dictionary<long, string> GetCommentsByTag(string tagsObj, string userId)
        {
            JObject jsonTagsObj = JObject.Parse(tagsObj);

            List<SurveyResponse> surveyResponses = new List<SurveyResponse>();

            if (jsonTagsObj["tagName"] == null || jsonTagsObj["moduleID"] == null || jsonTagsObj["searchSurvey"] == null) return null;

            Survey searchSurvey = JsonConvert.DeserializeObject<Survey>(jsonTagsObj["searchSurvey"].ToString());
            GetSearchAnalytics(searchSurvey, userId);

            string tagName = jsonTagsObj["tagName"].ToString();
            long moduleID = Convert.ToInt64(jsonTagsObj["moduleID"]);

            TagAnalyticsStrategy tagAnalytcicsStrategy = (TagAnalyticsStrategy)strategyDictionary[moduleID];

            Module module = dbContext.Modules.Where(m => m.ModuleID == moduleID).FirstOrDefault();
            Dictionary<string, int> TagListIndex = new Dictionary<string, int>();

            JObject moduleData = JObject.Parse(module.ModuleData);
            if (moduleData == null) return null;

            JObject questionData = (JObject)moduleData["QuestionData"];

            JArray Tags = new JArray();

            if (questionData["Tags"] != null)
                Tags = (JArray)questionData["Tags"];

            int indexValue = 0;
            foreach (JObject obj in Tags)
            {
                if (obj["TagName"] != null)
                {
                    TagListIndex.Add(obj["TagName"].ToString(), indexValue);
                    indexValue++;
                }
            }
            List<long> SRIDs = tagAnalytcicsStrategy.TagCount[tagName];
            surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses.Select(m => m.Module)).Where(s => SRIDs.Contains(s.SurveyResponseID) && s.Status != "DELETED").ToList();

            Dictionary<long, string> Comments = new Dictionary<long, string>();

            foreach (SurveyResponse surveyResponse in surveyResponses)
            {
                ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
                if (mr != null)
                {
                    JObject responseData = JObject.Parse(mr.ModuleData);
                    if (responseData == null) continue;

                    JObject answerData = (JObject)responseData["AnswerData"];
                    if (answerData == null) continue;

                    if (answerData["CommentText"] != null)
                    {
                        string comment = answerData["CommentText"].ToString();

                        if (answerData["SelectedTags"] != null)
                        {

                            JArray jsonSelectedTags = (JArray)answerData["SelectedTags"];

                            foreach (JValue jsonSelectedTag in jsonSelectedTags)
                            {
                                int value = Int32.Parse(jsonSelectedTag.ToString());
                                if (value == TagListIndex[tagName])
                                {
                                    Comments.Add(surveyResponse.SurveyResponseID, string.Join(" ", comment.Split(' ', '\n').Select(c => tagName == c.RemovePunctutions() ? c.Replace(c, "<strong>" + c + "</strong>") : c)));
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return Comments;
        }

        public Survey GetMatchedResponses(int surveyID, int skip, int pageSize, string orderByDirection, string filterKeyword, string cvData, string userId)
        {
            JObject cvJSONData = JObject.Parse(cvData);

            Survey survey = survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyID).FirstOrDefault<Survey>();
            survey.TotalResponses = 0;
            survey.SurveyResponses = new List<SurveyResponse>();

            if (cvJSONData["keyword"] == null || cvJSONData["moduleID"] == null || cvJSONData["searchSurvey"] == null) return survey;

            Survey searchSurvey = JsonConvert.DeserializeObject<Survey>(cvJSONData["searchSurvey"].ToString());
            GetSearchAnalytics(searchSurvey, userId);

            string keyWord = cvJSONData["keyword"].ToString();

            if (!string.IsNullOrEmpty(cvJSONData["moduleID"].ToString()))
            {
                string strModuleID = cvJSONData["moduleID"].ToString();
                int moduleID;
                if (!Int32.TryParse(strModuleID, out moduleID)) return survey;

                AnalyticsStrategy strategy = strategyDictionary[moduleID];

                if (strategy is StaffAnalyticsStrategy)
                {
                    StaffAnalyticsStrategy dirStrategy = (StaffAnalyticsStrategy)strategy;
                    if (!dirStrategy.StaffEntryResponses.ContainsKey(keyWord)) return survey;

                    return rRep.GetResponsesByResponseIds(surveyID, userId, skip, pageSize, orderByDirection, filterKeyword, dirStrategy.StaffEntryResponses[keyWord]);
                }
            }
            else if (cvJSONData["DGmoduleID"] != null && !string.IsNullOrEmpty(cvJSONData["DGmoduleID"].ToString()))
            {
                string strModuleID = cvJSONData["DGmoduleID"].ToString();
                int moduleID;
                if (!Int32.TryParse(strModuleID, out moduleID)) return null;

                if (cvJSONData["questionText"] == null) return null;
                string questionText = cvJSONData["questionText"].ToString();
                if (string.IsNullOrEmpty(questionText)) return null;

                AnalyticsStrategy strategy = strategyDictionary[moduleID];

                if (strategy is DGAnalyticsStrategy)
                {
                    DGAnalyticsStrategy dgStrategy = (DGAnalyticsStrategy)strategy;
                    if (!dgStrategy.DGStatistics.ContainsKey(questionText)) return survey;

                    List<Statistics> stats = dgStrategy.DGStatistics[questionText];
                    Statistics matchStat = stats.Where(s => s.Text.Equals(keyWord)).FirstOrDefault();
                    if (matchStat == null) return survey;
                    return rRep.GetResponsesByResponseIds(surveyID, userId, skip, pageSize, orderByDirection, filterKeyword, matchStat.SRIDs);
                }
            }
            else
            {
                if (tpDictionary.ContainsValue(keyWord))
                {
                    long touchpointID = tpDictionary.Where(k => k.Value == keyWord).Select(t => t.Key).FirstOrDefault();
                    List<long> responses = tpFStatistics.Where(t => t.Key.TouchpointID == touchpointID).Select(t => t.Value).FirstOrDefault();
                    if (responses == null) return survey;
                    return rRep.GetResponsesByResponseIds(surveyID, userId, skip, pageSize, orderByDirection, filterKeyword, responses);
                }

                if (orgSubDictionary.ContainsValue(keyWord))
                {
                    long orgSubId = orgSubDictionary.Where(k => k.Value == keyWord).Select(t => t.Key).FirstOrDefault();
                    List<long> responses = orgFStatistics.Where(t => t.Key.OrgSubID == orgSubId).Select(t => t.Value).FirstOrDefault();
                    if (responses == null) return survey;
                    return rRep.GetResponsesByResponseIds(surveyID, userId, skip, pageSize, orderByDirection, filterKeyword, responses);
                }
            }
            return survey;
        }

        public bool IsSurveyExists(int surveyId, string userId)
        {
            Survey survey = null;

            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Where(s => s.SurveyID == surveyId
                          && s.Status != "DELETED").FirstOrDefault();
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId) || uRep.isManager(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                if (User == null)
                    return false;
                survey = dbContext.Surveys.Where(s => s.SurveyID == surveyId && s.Status != "DELETED"
                    && s.CreatedBy.OrgSub.OrgSubID == User.OrgSub.OrgSubID).OrderBy(s => s.IsTemplate).FirstOrDefault();
            }
            if (survey != null)
                return true;
            return false;
        }

        public List<ExpertComment> DeleteExpertComment(long ExpertCommentID, string userId)
        {
            ExpertComment ec = dbContext.ExpertComments.Include(s => s.Survey).Where(e => e.ExpertCommentID == ExpertCommentID).FirstOrDefault();
            if (ec != null)
            {
                ec.Status = "DELETED";
                dbContext.Entry<ExpertComment>(ec).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
            return GetExpertComments(ec.Survey.SurveyID);
        }

        public List<ExpertComment> SaveExpertComment(string expertCommentData, string userId)
        {
            try
            {
                long surveyID = 0;
                long moduleID = 0;
                JObject jsonExpertComment = JObject.Parse(expertCommentData);
                if (jsonExpertComment["SurveyID"] != null && jsonExpertComment["ModuleID"] != null && jsonExpertComment["Comment"] != null && jsonExpertComment["ModuleInfo"] != null)
                {
                    if (Int64.TryParse(jsonExpertComment["SurveyID"].ToString(), out surveyID) && Int64.TryParse(jsonExpertComment["ModuleID"].ToString(), out moduleID))
                    {
                        ExpertComment ec = new ExpertComment();
                        ec.Survey = dbContext.Surveys.Where(s => s.SurveyID == surveyID).FirstOrDefault();
                        if (moduleID != 0) ec.Module = dbContext.Modules.Where(m => m.ModuleID == moduleID).FirstOrDefault();
                        ec.User = dbContext.Users.Where(u => u.Id == userId).FirstOrDefault();
                        ec.Comment = jsonExpertComment["Comment"].ToString();
                        ec.CommentDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone);
                        ec.ModuleInfo = jsonExpertComment["ModuleInfo"].ToString();
                        dbContext.ExpertComments.Add(ec);
                        dbContext.SaveChanges();
                    }
                    return GetExpertComments(surveyID);
                }
            }
            catch (DbEntityValidationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        private List<ExpertComment> GetExpertComments(long surveyID)
        {
            List<ExpertComment> comments = dbContext.ExpertComments.Include(s => s.Survey).Include(m => m.Module).Include(u => u.User).Where(ec => ec.Survey.SurveyID == surveyID && ec.Status != "DELETED").OrderByDescending(ec => ec.CommentDate).ToList();
            foreach(ExpertComment cm in comments){
                cm.UserName = cm.User.FullName;
                cm.ModuleID = cm.Module != null ? cm.Module.ModuleID : 0;
                cm.SurveyID = cm.Survey.SurveyID;
            }
            return comments;
        }
    }
}