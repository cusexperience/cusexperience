﻿using CusExperience.Entities;
using CusExperience.Utilities;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    class StaffAnalyticsStrategy : AnalyticsStrategy
    {
        public Dictionary<string, List<long>> StaffEntryResponses { get; set; }
        Dictionary<string, double> StaffEntryRating = new Dictionary<string, double>();

        List<string> searchEntryResponses = new List<string>();

        public StaffAnalyticsStrategy()
        {
            StaffEntryResponses = new Dictionary<string, List<long>>();
        }

        public StaffAnalyticsStrategy(long mID, JObject moduleData, List<string> keywords, List<string> woKeywords)
            : base(mID, moduleData, keywords, woKeywords)
        {
            StaffEntryResponses = new Dictionary<string, List<long>>();

            if (moduleData["SearchData"] != null)
            {
                JObject searchData = JObject.Parse(moduleData["SearchData"].ToString());

                JArray jsonSTAFFEntry = JArray.Parse(searchData["STAFFEntry"].ToString());

                foreach (JObject jsonDE in jsonSTAFFEntry)
                {
                    if (jsonDE["STAFFEntryText"] != null)
                    {
                        string STAFFEntryText = jsonDE["STAFFEntryText"].ToString();

                        if (!string.IsNullOrEmpty(STAFFEntryText))
                        {
                            searchEntryResponses.Add(STAFFEntryText);
                        }
                    }
                } 
                
            }

        }

        public override bool IsFilteredResponse(JObject responseData)
        {
            if (searchEntryResponses.Count <= 0) return true;

            if (responseData["AnswerData"] == null) return false;
            JObject answerData = JObject.Parse(responseData["AnswerData"].ToString());

            if (answerData["STAFFEntry"] == null) return false;

            JArray jsonSTAFFEntry = JArray.Parse(answerData["STAFFEntry"].ToString());

            foreach (JObject jsonDE in jsonSTAFFEntry)
            {
                if (jsonDE["STAFFEntryText"] != null)
                {
                    string STAFFEntryText = jsonDE["STAFFEntryText"].ToString();

                    if (!string.IsNullOrEmpty(STAFFEntryText))
                    { 
                        if (searchEntryResponses.Contains(STAFFEntryText)) 
                            return true;
                    }
                }
            } 
            return false;
        }

        public override bool IsKeywordFound(JObject responseData, List<string> filterKeywords)
        {
            if (filterKeywords.Count <= 0) return false;

            if (responseData["AnswerData"] == null) return false;
            JObject answerData = JObject.Parse(responseData["AnswerData"].ToString());

            if (answerData["STAFFEntry"] == null) return false;

            JArray jsonSTAFFEntry = JArray.Parse(answerData["STAFFEntry"].ToString());

            foreach (JObject jsonDE in jsonSTAFFEntry)
            {
                if (jsonDE["STAFFEntryText"] != null)
                {
                    string STAFFEntryText = jsonDE["STAFFEntryText"].ToString();

                    if (!string.IsNullOrEmpty(STAFFEntryText))
                    {
                        if (filterKeywords.Any(k => STAFFEntryText.ToLower().Contains(k.ToLower())))
                            return true;
                    }
                }
            } 
            return false;
        }

        public override void collectAnalyticsData(SurveyResponse surveyResponse)
        {
            ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
            if (mr == null) return;

            if (string.IsNullOrEmpty(mr.ModuleData)) return;
            JObject responseData = JObject.Parse(mr.ModuleData);

            if (responseData["AnswerData"] == null) return;
            JObject answerData = JObject.Parse(responseData["AnswerData"].ToString());

            if (answerData["STAFFEntry"] == null) return;
            JArray jsonSTAFFEntry = JArray.Parse(answerData["STAFFEntry"].ToString());

            foreach (JObject jsonDE in jsonSTAFFEntry)
            {
                if (jsonDE["STAFFEntryText"] != null)
                {
                    
                    string STAFFEntryText = jsonDE["STAFFEntryText"].ToString();
                    if (!string.IsNullOrEmpty(STAFFEntryText))
                    {
                       // if (Keywords.Count > 0 && Keywords.Any(k => !STAFFEntryText.ToLower().Contains(k.ToLower()))) continue;

                        if (StaffEntryResponses.ContainsKey(STAFFEntryText))
                        {
                            StaffEntryResponses[STAFFEntryText].Add(surveyResponse.SurveyResponseID);
                            StaffEntryRating[STAFFEntryText] = StaffEntryRating[STAFFEntryText] + surveyResponse.CusXPIndex;
                        }
                        else
                        {
                            List<long> SRList = new List<long>();
                            SRList.Add(surveyResponse.SurveyResponseID);
                            StaffEntryResponses.Add(STAFFEntryText, SRList);
                            StaffEntryRating.Add(STAFFEntryText, surveyResponse.CusXPIndex);
                        }
                    }
                }
            }
        }


        public override void getModuleDataWithAnalytics()
        {
            if (StaffEntryResponses.Count > 0) { 
            JArray STAFFResponse = new JArray();

            var sortedRating = from entry in StaffEntryRating orderby entry.Value / StaffEntryResponses[entry.Key].Count descending select entry;

            foreach (KeyValuePair<string, double> eachResponse in sortedRating)
            {
                JObject eachItem = new JObject();
                eachItem["Text"] = eachResponse.Key;
                eachItem["Count"] = StaffEntryResponses[eachResponse.Key].Count;
                eachItem["SRIDs"] = JArray.Parse("[" + string.Join(",", StaffEntryResponses[eachResponse.Key].Select(s => s.ToString())) + "]");
                eachItem["Score"] = eachResponse.Value / StaffEntryResponses[eachResponse.Key].Count;
                STAFFResponse.Add(eachItem);
            }
            analyticsData["Statistics"] = STAFFResponse;
            analyticsData["NotFound"] = false;
            }
            else
            {
                analyticsData["NotFound"] = true;
            }
        }


        public override void getModuleDataWithSearchAnalytics()
        {
            if (StaffEntryResponses.Count > 0)
            {
                JArray STAFFResponse = new JArray();

                var sortedRating = from entry in StaffEntryRating orderby entry.Value / StaffEntryResponses[entry.Key].Count descending select entry;

                foreach (KeyValuePair<string, double> eachResponse in sortedRating)
                {
                    JObject eachItem = new JObject();
                    eachItem["Text"] = eachResponse.Key;
                    eachItem["Count"] = StaffEntryResponses[eachResponse.Key].Count;
                    eachItem["Score"] = eachResponse.Value / StaffEntryResponses[eachResponse.Key].Count;
                    if (Keywords.Any(kw => eachResponse.Key.ToLower().Contains(kw))) 
                        eachItem["IsSearchKeyword"] = true;
                    else 
                        eachItem["IsSearchKeyword"] = false;
                    STAFFResponse.Add(eachItem);
                }
                analyticsData["Statistics"] = STAFFResponse;
                analyticsData["NotFound"] = false;
            }
            else
            {
                analyticsData["NotFound"] = true;
            }
        }

        internal static void writetoExcel(ref string columnName,ref uint rowIndex, WorksheetPart worksheetPart, long moduleID, JObject moduleData, List<SurveyResponse> surveyResponses)
        {
            Cell cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            cell.CellValue = new CellValue(StaffAnalyticsStrategy.GetQuestionText(moduleData));

            var beginColumn = columnName;
            ExcelUtility.GetNextRow(ref rowIndex);

            cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            cell.CellValue = new CellValue("Staff Names");

            foreach (SurveyResponse surveyResponse in surveyResponses)
            {
                columnName = beginColumn;

                ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
                if (mr == null) continue;

                JObject responseData = JObject.Parse(mr.ModuleData);

                cell = ExcelUtility.InsertCellInWorksheet(columnName, ExcelUtility.GetNextRow(ref rowIndex), worksheetPart);
                cell.CellValue = new CellValue(StaffAnalyticsStrategy.GetSTAFFText(responseData));
            }

            rowIndex = 1;
            columnName = ExcelUtility.GetNextColumn(ref columnName);
        }

        internal static string GetQuestionText(JObject moduleData)
        {
            string questionText = "";

            JObject questionData = (JObject)moduleData["QuestionData"];
            if (questionData == null) return questionText;

            if (questionData["QuestionText"] == null) return questionText;

            questionText = questionData["QuestionText"].ToString();
            questionText = Regex.Replace(questionText, @"<[^>]+>|;", string.Empty).Trim();
            return questionText;
        }

        internal static string GetSTAFFText(JObject responseData)
        {
            string STAFFText = "";
            List<string> enteredSTAFFText = new List<string>();

            if (responseData["AnswerData"] == null) return STAFFText;
            JObject answerData = JObject.Parse(responseData["AnswerData"].ToString());

            if (answerData["STAFFEntry"] == null) return STAFFText;

            JArray jsonSTAFFEntry = JArray.Parse(answerData["STAFFEntry"].ToString());

            foreach (JObject jsonDE in jsonSTAFFEntry)
            {
                if (jsonDE["STAFFEntryText"] != null && !string.IsNullOrEmpty(jsonDE["STAFFEntryText"].ToString())) 
                {
                    enteredSTAFFText.Add(jsonDE["STAFFEntryText"].ToString());
                }
            }
            STAFFText = string.Join(",", enteredSTAFFText);
            return STAFFText;
        }
    }
}
