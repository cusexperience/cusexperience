﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    public abstract class AnalyticsStrategy
    {
        protected long moduleID;
        protected JObject analyticsData = null;
        protected JObject questionData = null;


        protected JObject searchData = null;
        protected List<string> Keywords = new List<string>();
        protected List<string> WOKeywords = new List<string>();

        public JObject ModuleData { get; set; }

        public AnalyticsStrategy()
        {
        }

        public AnalyticsStrategy(long mID, JObject moduleData)
        {
            moduleID = mID;
            searchData = (JObject)moduleData["SearchData"];
            questionData = (JObject)moduleData["QuestionData"];
            analyticsData = new JObject();
            moduleData["AnalyticsData"] = analyticsData;
        }

        public AnalyticsStrategy(long mID, JObject moduleData, List<string> keywords, List<string> woKeywords)
        {
            moduleID = mID;
            ModuleData = moduleData;
            analyticsData = new JObject();
            searchData = (JObject)moduleData["SearchData"];
            questionData = (JObject)moduleData["QuestionData"];
            moduleData["AnalyticsData"] = analyticsData;
            Keywords = keywords;
            WOKeywords = woKeywords;
        }

        public abstract void collectAnalyticsData(SurveyResponse surveyResponse);

        public abstract void getModuleDataWithAnalytics();

        public abstract bool IsFilteredResponse(JObject responseData);

        public abstract bool IsKeywordFound(JObject responseData, List<string> filterKeywords);

        public virtual void getModuleDataWithAnalytics(int beginCount, int endCount, bool order)
        {
            getModuleDataWithAnalytics();
        }

        public virtual void getModuleDataWithSearchAnalytics()
        {
            getModuleDataWithAnalytics();
        }

    }
}