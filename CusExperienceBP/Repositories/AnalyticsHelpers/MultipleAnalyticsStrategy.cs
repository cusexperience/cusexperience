﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    public class MultipleAnalyticsStrategy : AnalyticsStrategy
    {
        //For Rating Question
        int[] multipleRating = null, responseCount = null;

        //For Choice Question
        Dictionary<string, int>[] multipleChoices = null;
        Dictionary<string, int> choices = new Dictionary<string, int>();
        List<string> choiceTextList = new List<string>();


        //Total Number of Responses
        int totalCount = 0;

        /* Search Criteria */
        int fromRating = 0;
        int toRating = 10;
        int numLimit = 10;
        List<int> SearchChoosenOptions = new List<int>();

        public MultipleAnalyticsStrategy()
        {

        }

        public static int GetFirstRating(JObject responseData)
        {
            JObject questionData = (JObject)responseData["QuestionData"];
            if (questionData == null) return 0;

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return 0;

            JArray answerItems = (JArray)answerData["AnswerItems"];
            if (answerItems == null) return 0;

            string multipleType = questionData["MultipleType"].ToString();

            if (!string.IsNullOrEmpty(multipleType) && (multipleType == "Number" || multipleType == "Heart" || multipleType == "Star"))
            {
                foreach (JObject answerItem in answerItems)
                {
                    if (answerItem["Rating"] != null && !string.IsNullOrEmpty(answerItem["Rating"].ToString()))
                    {
                        int rating = Int32.Parse(answerItem["Rating"].ToString());
                        return rating;
                    }
                }
            }
            return 0;
        }

        public MultipleAnalyticsStrategy(long mID, JObject moduleData, List<string> keywords, List<string> woKeywords)
            : base(mID, moduleData, keywords, woKeywords)
        {
            string multipleType = "";

            if (questionData["MultipleType"] != null) { 

                multipleType = questionData["MultipleType"].ToString();

                if (!string.IsNullOrEmpty(multipleType) && (multipleType == "Number" || multipleType == "Heart" || multipleType == "Star"))
                {
                    JArray questionItems = (JArray)questionData["QuestionItems"];
                    multipleRating = new int[questionItems.Count];
                    responseCount = new int[questionItems.Count];
                }
                else
                {
                    JArray questionItems = (JArray)questionData["QuestionItems"];
                    multipleChoices = new Dictionary<string, int>[questionItems.Count];
                    responseCount = new int[questionItems.Count];

                    JArray jsonChoices = (JArray)questionData["Choices"];
                    foreach (JObject jsonChoice in jsonChoices)
                    {
                        choiceTextList.Add(jsonChoice["ChoiceText"].ToString());
                    }
                }
            }

            if (questionData["NumLimit"] != null)
            {
                Int32.TryParse(questionData["NumLimit"].ToString(), out numLimit);
            }

            if (moduleData["SearchData"] != null)
            {
                JObject searchData = (JObject)moduleData["SearchData"];
                if(!string.IsNullOrEmpty(multipleType))
                {
                    if (multipleType == "Number" || multipleType == "Heart" || multipleType == "Star")
                    {
                        var somedata = searchData["FromRating"];
                        if (searchData["FromRating"] != null && !string.IsNullOrEmpty(searchData["FromRating"].ToString()))
                        {
                            fromRating = Int32.Parse(searchData["FromRating"].ToString());

                            if (searchData["ToRating"] != null && !string.IsNullOrEmpty(searchData["ToRating"].ToString()))
                                toRating = Int32.Parse(searchData["ToRating"].ToString());
                            else toRating = fromRating;
                        }
                    }
                    else
                    {
                        JArray jsonChoosenOptions = (JArray)searchData["ChoosenOptions"];
                        if (jsonChoosenOptions != null)
                        {
                            foreach (JValue jsonChoosenOption in jsonChoosenOptions)
                            {
                                int optionIndex = Int32.Parse(jsonChoosenOption.ToString());
                                SearchChoosenOptions.Add(optionIndex);
                            }
                        }
                    }
                }
            }
        }

        public override bool IsFilteredResponse(JObject responseData)
        {
            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return false;

            JArray answerItems = (JArray)answerData["AnswerItems"];
            if (answerItems == null) return false;

            string multipleType = questionData["MultipleType"].ToString();
            if (!string.IsNullOrEmpty(multipleType) && (multipleType == "Number" || multipleType == "Heart" || multipleType == "Star"))
            {
                if (answerItems.Count <= 0) return false;

                foreach (JObject answerItem in answerItems)
                {
                    if (answerItem["Rating"] != null && !string.IsNullOrEmpty(answerItem["Rating"].ToString()))
                    {
                        int rating = Int32.Parse(answerItem["Rating"].ToString());
                        if (rating < fromRating || rating > toRating)
                            return false;
                    }
                }
            }
            else
            {
                foreach (JObject answerItem in answerItems)
                {
                    if (answerItem["ChoosenOptions"] != null && !string.IsNullOrEmpty(answerItem["ChoosenOptions"].ToString()))
                    {
                        JArray jsonChoosenOptions = (JArray)answerItem["ChoosenOptions"];
                        if (jsonChoosenOptions == null) continue;

                        bool choiceFound = false;
                        int optionCount = jsonChoosenOptions.Count;

                        if (optionCount <= 0) return false;

                        for (int index = 0; index < optionCount; index++)
                        {
                            JValue jsonChoosenOption = (JValue)jsonChoosenOptions[index];
                            int optionIndex = Int32.Parse(jsonChoosenOption.ToString());
                            if (SearchChoosenOptions.Contains(optionIndex))
                                choiceFound = true;
                            else
                            {
                                jsonChoosenOptions.RemoveAt(index);
                                index = index - 1;
                                optionCount = optionCount - 1;
                            }
                        }
                        if (!choiceFound) return false;
                    }
                }
            }
            return true;
        }

        public override bool IsKeywordFound(JObject responseData, List<string> filterKeywords)
        {
            return false;
        }

        public override void collectAnalyticsData(SurveyResponse surveyResponse)
        {
            if (questionData == null) return;

            ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
            if (mr == null) return;

            if (string.IsNullOrEmpty(mr.ModuleData)) return;
            JObject responseData = JObject.Parse(mr.ModuleData);

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return;

            JArray answerItems = (JArray)answerData["AnswerItems"];
            if (answerItems == null) return;

            string multipleType = questionData["MultipleType"].ToString();

            if (!string.IsNullOrEmpty(multipleType) && (multipleType == "Number" || multipleType == "Heart" || multipleType == "Star"))
            {
                int indexCount = 0;
                foreach (JObject answerItem in answerItems)
                {
                    if (answerItem["Rating"] != null && !string.IsNullOrEmpty(answerItem["Rating"].ToString()))
                    {
                        try
                        {
                            multipleRating[indexCount] += Int32.Parse(answerItem["Rating"].ToString());
                            responseCount[indexCount] += 1;
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Trace.Write(ex.Message);
                        }
                    }
                    indexCount++;
                }
            }
            else if (!string.IsNullOrEmpty(multipleType) && (multipleType == "Single" || multipleType == "Multiple"))
            {
                int indexCount = 0;

                foreach (JObject answerItem in answerItems)
                {
                    if (answerItem["ChoosenOptions"] != null && !string.IsNullOrEmpty(answerItem["ChoosenOptions"].ToString()))
                    {
                        JArray jsonChoosenOptions = (JArray)answerItem["ChoosenOptions"];
                        if (jsonChoosenOptions == null) continue;

                        foreach (JValue jsonChoosenOption in jsonChoosenOptions)
                        {
                            int optionIndex = Int32.Parse(jsonChoosenOption.ToString());
                            if (multipleChoices[indexCount] == null)
                            {
                                multipleChoices[indexCount] = new Dictionary<string, int>();
                                foreach (string choiceText in choiceTextList)
                                {
                                    multipleChoices[indexCount].Add(choiceText, 0);
                                }
                            }
                            responseCount[indexCount] += 1;
                            multipleChoices[indexCount][choiceTextList[optionIndex]] = multipleChoices[indexCount][choiceTextList[optionIndex]] + 1;
                        }
                    }
                    indexCount++;
                }
            }
            totalCount += 1;
        }

        public override void getModuleDataWithAnalytics()
        {
            string multipleType = questionData["MultipleType"].ToString();

            if (!string.IsNullOrEmpty(multipleType) && (multipleType == "Number" || multipleType == "Heart" || multipleType == "Star"))
            {
                JArray analyticItems = new JArray();
                for (int indexCount = 0; indexCount < multipleRating.Length; indexCount++)
                {
                    if (responseCount[indexCount] > 0)
                    {
                        double overallRating = ((double)multipleRating[indexCount] / numLimit * 100) / responseCount[indexCount];
                        JObject ratingProperty = new JObject();
                        ratingProperty.Add("NotFound", false);
                        ratingProperty.Add("FromRating", fromRating);
                        ratingProperty.Add("ToRating", toRating);
                        ratingProperty.Add("OverallRating", overallRating);
                        ratingProperty.Add("ResponseCount", responseCount[indexCount]);
                        analyticItems.Add(ratingProperty);
                    }
                    else
                    {
                        JObject notFoundProperty = new JObject();
                        notFoundProperty.Add("NotFound", true);
                        analyticItems.Add(notFoundProperty);
                    }
                }
                analyticsData["AnalyticsItems"] = analyticItems;
            }
            else if (!string.IsNullOrEmpty(multipleType) && (multipleType == "Single" || multipleType == "Multiple"))
            {
                JArray multipleChoiceWCounts = new JArray();

                for (int indexCount = 0; indexCount < multipleChoices.Length; indexCount++)
                {
                    if (responseCount[indexCount] > 0)
                    {

                        if (multipleChoices[indexCount] == null) continue;

                        JArray choiceWCounts = new JArray();
                        foreach (var choice in multipleChoices[indexCount])
                        {
                            JObject choiceWCount = new JObject();
                            choiceWCount["ChoiceText"] = choice.Key;
                            choiceWCount["Count"] = choice.Value;
                            choiceWCount["Percentage"] = ((double)choice.Value * 100) / responseCount[indexCount];
                            choiceWCounts.Add(choiceWCount);
                        }
                        JObject choiceArray = new JObject();
                        choiceArray.Add("ChoiceWCounts", choiceWCounts);
                        choiceArray.Add("NotFound", false);
                        multipleChoiceWCounts.Add(choiceArray);
                    }
                    else
                    {
                        JObject notFoundProperty = new JObject();
                        notFoundProperty.Add("NotFound", true);
                        multipleChoiceWCounts.Add(notFoundProperty);
                    }
                    analyticsData["AnalyticsItems"] = multipleChoiceWCounts;
                    analyticsData["TotalResponses"] = totalCount;
                }
            }
        }
    }
}