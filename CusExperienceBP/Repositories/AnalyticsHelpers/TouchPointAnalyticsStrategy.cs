﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    public class TouchPointAnalyticsStrategy
    {
        public static List<String> GetTouchPointNames(JToken jsonModuleData)
        {
            List<String> tpNames = new List<String>();
            if(jsonModuleData==null) return tpNames;
 
            JObject moduleData = JObject.Parse(jsonModuleData.ToString());
            
            if(moduleData["Touchpoints"] == null) return tpNames;

            JArray touchPoints = (JArray)moduleData["Touchpoints"];
            foreach (JObject ob in touchPoints)
            {
                if( ob["TouchpointName"] !=null && !string.IsNullOrEmpty(ob["TouchpointName"].ToString())) {
                    tpNames.Add(ob["TouchpointName"].ToString());
                }
            }
            return tpNames;
        }
    }
}
