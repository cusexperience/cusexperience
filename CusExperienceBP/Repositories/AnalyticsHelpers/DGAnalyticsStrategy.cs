﻿using CusExperience.Entities;
using CusExperience.Utilities;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    public class Statistics: IComparable<Statistics>
    {
        public string Text { get; set; }
        public List<long> SRIDs { get; set; }
        public double sumScore { get; set; }

        public int CompareTo(Statistics compareObj)
        {
           return sumScore.CompareTo(compareObj.sumScore);
        }
    }

    public class DGAnalyticsStrategy : AnalyticsStrategy
    {

        public Dictionary<string, List<Statistics>> DGStatistics;

        public DGAnalyticsStrategy()
        {

        }

        public DGAnalyticsStrategy(long mID, JObject moduleData, List<string> keywords, List<string> woKeywords)
            : base(mID, moduleData, keywords, woKeywords)
        {
            DGStatistics = new Dictionary<string, List<Statistics>>(); 

            JObject jsonQuestionData = (JObject)moduleData["QuestionData"];
            if (jsonQuestionData == null) return;

            JArray jsonDGQuestionFields = (JArray)jsonQuestionData["DGQuestionFields"];

            foreach (JObject jsonDGQuestionField in jsonDGQuestionFields)
            {
                if (jsonDGQuestionField["FieldName"] == null)  continue; 
                string jsonFieldName = jsonDGQuestionField["FieldName"].ToString();

                if (jsonDGQuestionField["FieldType"] == null) continue; 
                string jsonFieldType = jsonDGQuestionField["FieldType"].ToString();

                if (moduleData["SearchData"] != null)
                {
                    if(jsonDGQuestionField["IsAnonymized"] != null && jsonDGQuestionField["IsAnonymized"].ToString().ToLower().Equals("false"))
                        if (jsonDGQuestionField["HideInSearch"] != null && jsonDGQuestionField["HideInSearch"].ToString().ToLower().Equals("false"))
                            DGStatistics.Add(jsonFieldName, new List<Statistics>());
                        else if (jsonDGQuestionField["IsAnonymized"] == null)
                            DGStatistics.Add(jsonFieldName, new List<Statistics>());
                }
                else
                {
                    if(jsonDGQuestionField["IsAnonymized"] != null && jsonDGQuestionField["IsAnonymized"].ToString().ToLower().Equals("false"))
                        DGStatistics.Add(jsonFieldName, new List<Statistics>());
                    else if (jsonDGQuestionField["IsAnonymized"] == null)
                        DGStatistics.Add(jsonFieldName, new List<Statistics>());
                }
            }
        }

        public static void searchDemographicsFieldsRequired(Dictionary<string, string[]> requiredValues, JObject responseData)
        {
            Dictionary<string, string[]> moduleDictionary = BuildDGDictionary(responseData);

            if (moduleDictionary == null) return;

            int count = requiredValues.Keys.Count;

            for (int i = 0; i < count; i++)
            {
                string[] value = new string[] { };
                if (moduleDictionary.TryGetValue(requiredValues.Keys.ElementAt(i), out value))
                {
                    requiredValues[requiredValues.Keys.ElementAt(i)] = value;
                }
            }
        }

        public static string findFirstEmailField(JObject moduleData)
        {
            JObject jsonQuestionData = (JObject)moduleData["QuestionData"];
            if (jsonQuestionData == null) return null;

            JObject jsonAnswerData = (JObject)moduleData["AnswerData"];
            if (jsonAnswerData == null) return null;

            JArray jsonDGQuestionFields = (JArray)jsonQuestionData["DGQuestionFields"];
            JArray jsonDGAnswerFields = (JArray)jsonAnswerData["DGAnswerFields"];

            if (jsonDGQuestionFields.Count != jsonDGAnswerFields.Count) return null;

            int indexCount = 0;

            foreach (JObject jsonDGQuestionField in jsonDGQuestionFields)
            {
                if (jsonDGQuestionField["Validation"] == null) { indexCount++; continue; }
                string jsonFieldValidation = jsonDGQuestionField["Validation"].ToString();

                if (jsonFieldValidation.Equals("Email"))
                {
                    string jsonEmail = null;
                    if (jsonDGAnswerFields[indexCount]["TextValue"] != null) {
                        jsonEmail = jsonDGAnswerFields[indexCount]["TextValue"].ToString();
                    }
                    return jsonEmail;
                }
                indexCount++;
            }
            return null;
        }

        internal static Dictionary<string, string[]> BuildDGDictionary(JObject moduleData)
        {
            Dictionary<string, string[]> dgDictionary = new Dictionary<string, string[]>(StringComparer.InvariantCultureIgnoreCase);

            JObject jsonQuestionData = (JObject)moduleData["QuestionData"];
            if (jsonQuestionData == null) return dgDictionary;

            JObject jsonAnswerData = (JObject)moduleData["AnswerData"];
            if (jsonAnswerData == null) return dgDictionary;

            JArray jsonDGQuestionFields = (JArray)jsonQuestionData["DGQuestionFields"];
            JArray jsonDGAnswerFields = (JArray)jsonAnswerData["DGAnswerFields"];

            if (jsonDGQuestionFields.Count != jsonDGAnswerFields.Count) return dgDictionary;

            int indexCount = 0;

            foreach (JObject jsonDGQuestionField in jsonDGQuestionFields)
            {

                if (jsonDGAnswerFields[indexCount].ToString().Equals("")) { indexCount++; continue; }

                if (jsonDGQuestionField["FieldName"] == null) { indexCount++; continue; }
                string jsonFieldName = jsonDGQuestionField["FieldName"].ToString();

                if (jsonDGQuestionField["FieldType"] == null) { indexCount++; continue; }
                string jsonFieldType = jsonDGQuestionField["FieldType"].ToString();

                if (jsonFieldType.Equals("Single Line Textbox") || jsonFieldType.Equals("Multiple Line Textbox"))
                {
                    if (jsonDGAnswerFields[indexCount]["TextValue"] == null) { 
                        dgDictionary.Add(jsonFieldName, new string[] { "" });
                        indexCount++; continue; 
                    }
                    string jsonTextValue = jsonDGAnswerFields[indexCount]["TextValue"].ToString();
                    dgDictionary.Add(jsonFieldName, new string[] { jsonTextValue });
                }
                else if (jsonFieldType.Equals("Single Option Dropdown") || jsonFieldType.Equals("Multiple Option Dropdown"))
                {
                    if (jsonDGAnswerFields[indexCount]["FieldValues"] == null)
                    {
                        dgDictionary.Add(jsonFieldName, new string[] { "" });
                        continue;
                    }
                    JArray valueArray = (JArray)jsonDGAnswerFields[indexCount]["FieldValues"];
                    string[] strFieldValues = valueArray.Select(value => value.ToString()).ToArray();
                    dgDictionary.Add(jsonFieldName, strFieldValues);
                }
                indexCount++;
            }
            return dgDictionary;
        }

        internal Dictionary<string, string[]> BuildSearchDGDictionary(JObject dgSearch)
        {
            Dictionary<string, string[]> dgDictionary = new Dictionary<string, string[]>();
            JArray jsonDGSearchFields = (JArray)dgSearch["DGSearchFields"];
            for (int i = 0; i < jsonDGSearchFields.Count; i++)
            {
                string convertedString = jsonDGSearchFields[i].ToString().Trim();
                if (convertedString.Equals("")) continue;

                JObject jsonDGSearchField = (JObject)jsonDGSearchFields[i];
                if (jsonDGSearchField == null) continue;

                if (jsonDGSearchField["FieldName"] == null) continue;
                if (jsonDGSearchField["FieldValues"] == null) continue;

                string strFieldName = jsonDGSearchField["FieldName"].ToString();
                JArray valueArray = (JArray)jsonDGSearchField["FieldValues"];
                string[] strFieldValues = valueArray.Select(value => value.ToString()).ToArray();

                if (strFieldValues.Length > 0)
                    dgDictionary.Add(strFieldName, strFieldValues);
            }

            return dgDictionary;
        }

        //Dummy Methods
        public override bool IsFilteredResponse(JObject responseData)
        {
            Dictionary<string, string[]> searchDictionary = BuildSearchDGDictionary(searchData);
            Dictionary<string, string[]> moduleDictionary = BuildDGDictionary(responseData);

            if(searchDictionary.Count <= 0) return true;

            foreach (KeyValuePair<string, string[]> searchItem in searchDictionary)
            {
                string[] value = new string[] { };
                if (!searchItem.Value.Contains("All"))
                {
                    if (moduleDictionary.TryGetValue(searchItem.Key, out value))
                    {
                        if (!searchItem.Value.All(s => value.Contains(s)))
                        {
                            return false;
                        }
                    }
                    if (value == null) return false;
                }
            }
            return true;
        }

        public override bool IsKeywordFound(JObject responseData, List<string> filterKeywords)
        {
            Dictionary<string, string[]> moduleDictionary = BuildDGDictionary(responseData);
            foreach (KeyValuePair<string, string[]> eachItem in moduleDictionary)
            {
                if (eachItem.Value.Any(s => filterKeywords.Any(f => s.ToLower().Contains(f))))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsFilterKeywordFound(JObject responseData, List<string> filterKeywords)
        {
            Dictionary<string, string[]> moduleDictionary = BuildDGDictionary(responseData);
            foreach (KeyValuePair<string, string[]> eachItem in moduleDictionary)
            {
                if (eachItem.Value.Any(s => filterKeywords.Any(f => s.ToLower().Contains(f))))
                {
                    return true;
                }
            }
            return false;
        }

        public override void collectAnalyticsData(SurveyResponse surveyResponse)
        {
                 ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
                 if (mr == null) return;

                 if (string.IsNullOrEmpty(mr.ModuleData)) return;
                 JObject responseData = JObject.Parse(mr.ModuleData);

                 JObject jsonAnswerData = (JObject)responseData["AnswerData"];
                 if (jsonAnswerData == null) return;

                 Dictionary<string, string[]> DemographicsData = BuildDGDictionary(responseData);

                 foreach (KeyValuePair<string,string[]> eachDGData in DemographicsData)
                 {
                     if(!DGStatistics.ContainsKey(eachDGData.Key)) continue;

                     List<Statistics> statArray = DGStatistics[eachDGData.Key];

                     foreach(string value in eachDGData.Value){

                         if(string.IsNullOrEmpty(value)) continue;

                          Statistics valueFound = statArray.Where(s => s.Text == value).FirstOrDefault();
                          if(valueFound != null){
                               valueFound.SRIDs.Add(surveyResponse.SurveyResponseID);
                               valueFound.sumScore = valueFound.sumScore + surveyResponse.CusXPIndex;
                          } else {
                              valueFound = new Statistics();
                              valueFound.Text = value;
                              valueFound.SRIDs = new List<long>();
                              valueFound.SRIDs.Add(surveyResponse.SurveyResponseID);
                              valueFound.sumScore = surveyResponse.CusXPIndex;
                              statArray.Add(valueFound);
                          }
                     }
                     DGStatistics[eachDGData.Key] = statArray;
             }
         }

        public override void getModuleDataWithAnalytics()
        {
            JArray dgStatistics = new JArray();

            foreach (KeyValuePair<string, List<Statistics>> eachField in DGStatistics)
            {
                JObject eachJSONField = new JObject();
                JArray dgJSONStatistics = new JArray();
                List<Statistics> Stats = eachField.Value;
                Stats.Sort();
                Stats.Reverse();
                foreach (Statistics eachStat in Stats)
                {
                    JObject eachJSONStat = new JObject();
                    eachJSONStat["Text"] = eachStat.Text;
                    eachJSONStat["Count"] = eachStat.SRIDs.Count;
                    eachJSONStat["Score"] = eachStat.sumScore / eachStat.SRIDs.Count;
                    dgJSONStatistics.Add(eachJSONStat);
                }
                eachJSONField["DGQuestionField"] = eachField.Key;
                eachJSONField["Statistics"] = dgJSONStatistics;
                dgStatistics.Add(eachJSONField);

            }
            if(DGStatistics.Count <= 0) analyticsData["NotFound"] = true;
            analyticsData["DGStatistics"] = dgStatistics;
        }

        internal static void writetoExcel(ref string columnName, ref uint rowIndex, WorksheetPart worksheetPart, long moduleID, JObject moduleData, List<SurveyResponse> surveyResponses)
        {
            List<string> dgQuestions = GetDGQuestions(moduleData);

            Cell cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            cell.CellValue = new CellValue(StaffAnalyticsStrategy.GetQuestionText(moduleData));

            var beginColumn = columnName;
            ExcelUtility.GetNextRow(ref rowIndex);

            var indexCount = 0;
            foreach (string dgQuestion in dgQuestions)
            {
                cell = ExcelUtility.InsertCellInWorksheet(columnName,  rowIndex, worksheetPart);
                cell.CellValue = new CellValue(dgQuestion);

                if(++indexCount < dgQuestions.Count)
                    ExcelUtility.GetNextColumn(ref columnName);
            }

            foreach (SurveyResponse surveyResponse in surveyResponses)
            {
                ExcelUtility.GetNextRow(ref rowIndex);
                columnName = beginColumn;

                ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
                if (mr == null) continue;

                JObject responseData = JObject.Parse(mr.ModuleData);

                Dictionary<string, string[]> responseDictionary = BuildDGDictionary(responseData);
                indexCount = 0;
                foreach (string dgQuestion in dgQuestions)
                {
                    string value = "";
                    if (responseDictionary.ContainsKey(dgQuestion))
                        value = string.Join(",", responseDictionary[dgQuestion]);

                    cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
                    cell.CellValue = new CellValue(value);

                    if (++indexCount < dgQuestions.Count)
                        ExcelUtility.GetNextColumn(ref columnName);
                }
            }
            rowIndex = 1;
            columnName = ExcelUtility.GetNextColumn(ref columnName);
        }

        internal static string GetQuestionText(JObject moduleData)
        {
            string questionText = "";

            JObject questionData = (JObject)moduleData["QuestionData"];
            if (questionData == null) return questionText;

            if (questionData["QuestionText"] == null) return questionText;

            questionText = questionData["QuestionText"].ToString();
            questionText = Regex.Replace(questionText, @"<[^>]+>|;", string.Empty).Trim();
            return questionText;
        }

        internal static List<string> GetDGQuestions(JObject moduleData)
        {
            List<string> DGQuestions = new List<string>();

            JObject jsonQuestionData = (JObject)moduleData["QuestionData"];
            if (jsonQuestionData == null) return DGQuestions;

            JArray jsonDGQuestionFields = (JArray)jsonQuestionData["DGQuestionFields"];

            foreach (JObject jsonDGQuestionField in jsonDGQuestionFields)
            {
                if (jsonDGQuestionField["FieldName"] == null) continue;
                DGQuestions.Add(jsonDGQuestionField["FieldName"].ToString());
            }
            return DGQuestions;
        }
    }
}