﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CusExperience.Utilities;
using System.Web;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    public class RatingAnalyticsStrategy : AnalyticsStrategy
    {
        double rating = 0;
        int responseCount = 0;
        int totalCount = 0;

        /* Search Criteria */
        int fromRating = 0;
        int toRating = 10;
        int numLimit = 10;

        string rateType = "";

        Dictionary<string, int> CCCRating = new Dictionary<string, int>();
        public Dictionary<string, List<long>> WordFrequency { get; set; }
        List<SRComment> Comments = new List<SRComment>();

        public RatingAnalyticsStrategy()
        {

        }

        public RatingAnalyticsStrategy(long mID, JObject moduleData, List<string> keywords, List<string> woKeywords)
            : base(mID, moduleData,keywords,woKeywords)
        {
            if (moduleData["QuestionData"] != null)
            {
                JObject questionData = JObject.Parse(moduleData["QuestionData"].ToString());
                rateType = questionData["RateType"].ToString();
                 if (rateType == "Comment")
                 {
                     CCCRating.Add("Comment",0);
                     CCCRating.Add("Compliment", 0);
                     CCCRating.Add("Complaint", 0);
                 }
                 if (questionData["NumLimit"] != null)
                 {
                     Int32.TryParse(questionData["NumLimit"].ToString(), out numLimit);
                 }
            }

            if (moduleData["SearchData"] != null)
            {
                JObject searchData = (JObject) moduleData["SearchData"];
                var somedata = searchData["FromRating"];
                if(searchData["FromRating"] != null && !string.IsNullOrEmpty(searchData["FromRating"].ToString())) {    
                    fromRating = Int32.Parse(searchData["FromRating"].ToString());

                    if(searchData["ToRating"] != null && !string.IsNullOrEmpty(searchData["ToRating"].ToString()))
                           toRating = Int32.Parse(searchData["ToRating"].ToString());
                    else   toRating = fromRating;
                }
            }
        }

        public static double GetFirstRating(JObject responseData) {
            double firstRating = 0;

            if (responseData["AnswerData"] != null && responseData["QuestionData"] != null)
            {
                JObject answerData = (JObject)responseData["AnswerData"];
                JObject questionData = (JObject)responseData["QuestionData"];

                if (answerData["Rating"] != null && questionData["NumLimit"] !=null && !string.IsNullOrEmpty(answerData["Rating"].ToString()))
                {
                    string rateType = questionData["RateType"].ToString();
                    if (rateType != "Comment") {
                        firstRating =  (Double.Parse(answerData["Rating"].ToString()) / Double.Parse(questionData["NumLimit"].ToString())) * 100;
                    }
                    else
                    {
                        string ratingAnswer = answerData["Rating"].ToString();
                        if (ratingAnswer.Equals("Compliment")) firstRating = 9;
                        else if (ratingAnswer.Equals("Comment")) firstRating = 6;
                        else firstRating = 3;
                    }
                }
            }
            return firstRating;
        }

        public override bool IsFilteredResponse(JObject responseData)
        {
            JObject answerData = (JObject)responseData["AnswerData"];

            if (answerData["Rating"] != null && !string.IsNullOrEmpty(answerData["Rating"].ToString()))
            {
                int rating = Int32.Parse(answerData["Rating"].ToString());
                if (rating >= fromRating && rating <= toRating)
                    return true;
                return false;
            }
            return false;
        }

        public override bool IsKeywordFound(JObject responseData, List<string> filterKeywords)
        {
            return false;
        }

        public override void collectAnalyticsData(SurveyResponse surveyResponse)
        {
            ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
            if (mr == null) return;

            if (string.IsNullOrEmpty(mr.ModuleData)) return;
            JObject responseData = JObject.Parse(mr.ModuleData);
            JObject answerData = (JObject)responseData["AnswerData"];

            if (answerData == null) return;

            if (answerData["Rating"] != null && !string.IsNullOrEmpty(answerData["Rating"].ToString()))
            {
                if (rateType != "Comment")
                {
                    rating += Int32.Parse(answerData["Rating"].ToString());  
                    string comment = answerData["CommentText"].ToString();
                    if (!string.IsNullOrEmpty(comment)) 
                    { 
                        SRComment c = new SRComment();
                        c.SurveyResponseID = surveyResponse.SurveyResponseID;
                        c.Comment = comment;
                        Comments.Add(c);
                    }
                }
                else
                {
                    string ratingText = answerData["Rating"].ToString();
                    CCCRating[ratingText] = ++CCCRating[ratingText];
                    string comment = answerData["CommentText"].ToString();
                    if (!string.IsNullOrEmpty(comment))
                    {
                        SRComment c = new SRComment();
                        c.SurveyResponseID = surveyResponse.SurveyResponseID;
                        c.Comment = comment;
                        Comments.Add(c);
                    }
                }
                responseCount++;
            }
            totalCount++;
        }

        public override void getModuleDataWithAnalytics()
        {
            
            if (responseCount != 0)
            {
                if (rateType != "Comment")
                {
                    JArray wordCloud = new JArray();

                    analyticsData["OverallRating"] = ((double)(rating / numLimit) * 100) / responseCount;
                    analyticsData["FromRating"] = fromRating;
                    analyticsData["ToRating"] = numLimit;
                    analyticsData["ResponseCount"] = responseCount;
                    analyticsData["NotFound"] = false;

                    WordFrequency = Comments.Select(a => new { a.SurveyResponseID, rValue = Regex.Replace(a.Comment, @"\s+", " ") })
                                                       .Where(a => !string.IsNullOrWhiteSpace(a.rValue))
                                                       .SelectMany(s => s.rValue.Split(' '), (s, word) => new { word, s.SurveyResponseID })
                                                       .Select(a => new { rWord = a.word.RemovePunctutions(), a.SurveyResponseID })
                                                       .Select(a => new { rWord = a.rWord.Trim().ToLower(), a.SurveyResponseID })
                                                       .Where(a => !string.IsNullOrWhiteSpace(a.rWord))
                                                       .Where(a => !CONSTANTS.EXCLUDELIST.Contains(a.rWord))
                                                       .GroupBy(word => word.rWord)
                                                       .Select(p => new { p.Key, SRID = p.Select(x => x.SurveyResponseID).ToList() })
                                                       .OrderByDescending(a => a.SRID.Count)
                                                       .Take(1000).ToDictionary(t => t.Key, t => t.SRID);

                    foreach (var word in WordFrequency)
                    {
                        JObject cloudTag = new JObject();
                        cloudTag["Word"] = word.Key;
                        cloudTag["Frequency"] = word.Value.Count;
                        wordCloud.Add(cloudTag);
                    }
                    analyticsData["WordCloud"] = wordCloud;
                }
                else
                {
                    JArray choiceWCounts = new JArray();
                    JArray wordCloud = new JArray();

                    foreach (KeyValuePair<string, int> comment in CCCRating)
                    {
                        JObject choiceWCount = new JObject();
                        choiceWCount["ChoiceText"] = comment.Key;
                        choiceWCount["Count"] = comment.Value;
                        choiceWCount["Percentage"] = ((double)comment.Value * 100) / totalCount;
                        choiceWCounts.Add(choiceWCount);
                    }
                    analyticsData["ChoiceWCounts"] = choiceWCounts;
                    analyticsData["TotalResponses"] = totalCount;
                    analyticsData["NotFound"] = false;

                    WordFrequency = Comments.Select(a => new { a.SurveyResponseID, rValue = Regex.Replace(a.Comment, @"\s+", " ") })
                                   .Where(a => !string.IsNullOrWhiteSpace(a.rValue))
                                   .SelectMany(s => s.rValue.Split(' '), (s, word) => new { word, s.SurveyResponseID })
                                   .Select(a => new { rWord = a.word.RemovePunctutions(), a.SurveyResponseID })
                                   .Select(a => new { rWord = a.rWord.Trim().ToLower(), a.SurveyResponseID })
                                   .Where(a => !string.IsNullOrWhiteSpace(a.rWord))
                                   .Where(a => !CONSTANTS.EXCLUDELIST.Contains(a.rWord))
                                   .GroupBy(word => word.rWord)
                                   .Select(p => new { p.Key, SRID = p.Select(x => x.SurveyResponseID).ToList() })
                                   .OrderByDescending(a => a.SRID.Count)
                                   .Take(1000).ToDictionary(t => t.Key, t => t.SRID);

                    foreach (var word in WordFrequency)
                    {
                        JObject cloudTag = new JObject();
                        cloudTag["Word"] = word.Key;
                        cloudTag["Frequency"] = word.Value.Count;
                        wordCloud.Add(cloudTag);
                    }
                    analyticsData["WordCloud"] = wordCloud;
                }
            }
            else
                analyticsData["NotFound"] = true;
        }

        internal static void writetoExcel(ref string columnName, ref uint rowIndex, WorksheetPart worksheetPart, long moduleID, JObject moduleData, List<SurveyResponse> surveyResponses)
        {
            Cell cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            cell.CellValue = new CellValue(RatingAnalyticsStrategy.GetQuestionText(moduleData));

            var beginColumn = columnName;
            ExcelUtility.GetNextRow(ref rowIndex);

            cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            cell.CellValue = new CellValue("Rating");

            foreach (SurveyResponse surveyResponse in surveyResponses)
            {
                columnName = beginColumn;

                ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
                if (mr == null) continue;

                JObject responseData = JObject.Parse(mr.ModuleData);

                cell = ExcelUtility.InsertCellInWorksheet(columnName, ExcelUtility.GetNextRow(ref rowIndex), worksheetPart);
                cell.DataType = CellValues.Number;
                cell.CellValue = new CellValue(RatingAnalyticsStrategy.GetRating(responseData));
            }

            rowIndex = 1;
            columnName = ExcelUtility.GetNextColumn(ref columnName);
        }

        internal static string GetQuestionText(JObject moduleData)
        {
            string questionText = "";

            JObject questionData = (JObject)moduleData["QuestionData"];
            if (questionData == null) return questionText;

            if (questionData["QuestionText"] == null) return questionText;

            questionText = questionData["QuestionText"].ToString();
            questionText = Regex.Replace(questionText, @"<[^>]+>|;", string.Empty).Trim();
            return questionText;
        }

        internal static string GetRating(JObject responseData)
        {
            string rating = "";
            JObject answerData = (JObject)responseData["AnswerData"];

            if (answerData["Rating"] != null && !string.IsNullOrEmpty(answerData["Rating"].ToString()))
            {
                rating = answerData["Rating"].ToString();
            }
            return rating;
        }
    }
}