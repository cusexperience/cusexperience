﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    public class SRComment
    {
        public long SurveyResponseID { get; set; }
        public string Comment { get; set; }
    }
}
