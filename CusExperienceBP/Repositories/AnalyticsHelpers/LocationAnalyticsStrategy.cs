﻿using CusExperience.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    public class LocationAnalyticsStrategy: AnalyticsStrategy
    {

        public Dictionary<long, double[]> locationDictionary = new Dictionary<long, double[]>();
        private List<string> keywords;
        private List<string> woKeywords;

        public LocationAnalyticsStrategy()
        {

        }

        public LocationAnalyticsStrategy(long moduleID, JObject moduleData, List<string> keywords, List<string> woKeywords)
            : base(moduleID, moduleData, keywords, woKeywords)
        {

        }

        public override void collectAnalyticsData(Entities.SurveyResponse surveyResponse)
        {
            if (questionData == null) return;

            ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
            if (mr == null) return;

            if (string.IsNullOrEmpty(mr.ModuleData)) return;
            JObject responseData = JObject.Parse(mr.ModuleData);

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return;

            double latitude, longitude;

            if (answerData["Latitude"] != null && answerData["Longitude"] != null && Double.TryParse(answerData["Latitude"].ToString(), out latitude) && Double.TryParse(answerData["Longitude"].ToString(), out longitude))
            {
                double[] location = new double[] { latitude, longitude };
                locationDictionary.Add(surveyResponse.SurveyResponseID, location);
            }

        }

        public override void getModuleDataWithAnalytics()
        {
            JObject locationInfo = new JObject();
             
            if (locationDictionary.Count > 0) { 
                analyticsData["LocationInfo"] = JsonConvert.SerializeObject(locationDictionary);
                analyticsData["TotalResponses"] = locationDictionary.Count;
                analyticsData["NotFound"] = false;
            }
            else
            {
                analyticsData["NotFound"] = true;
            }
        }

        public override bool IsFilteredResponse(Newtonsoft.Json.Linq.JObject responseData)
        {
            throw new NotImplementedException();
        }

        public override bool IsKeywordFound(Newtonsoft.Json.Linq.JObject responseData, List<string> filterKeywords)
        {
            throw new NotImplementedException();
        }
    }
}
