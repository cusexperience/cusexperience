﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    public class ChoiceAnalyticsStrategy : AnalyticsStrategy
    {
        Dictionary<int, int> Choices = new Dictionary<int, int>();
        List<string> ChoiceTextList = new List<string>();
        int totalCount = 0;
        int totalEntries = 0;

        List<int> SearchChoosenOptions = new List<int>();

        public ChoiceAnalyticsStrategy()
        {
        }

        public ChoiceAnalyticsStrategy(long mID, JObject moduleData, List<string> keywords, List<string> woKeywords)
            : base(mID, moduleData, keywords, woKeywords)
        {
            JArray jsonChoices = (JArray)questionData["Choices"];

            int indexValue = 0;

            foreach (JObject jsonChoice in jsonChoices)
            {
                if (jsonChoice["ChoiceText"] != null)
                {
                    Choices.Add(indexValue, 0);
                    ChoiceTextList.Add(jsonChoice["ChoiceText"].ToString());
                    indexValue++;
                }
            }


            if (moduleData["SearchData"] != null)
            {
                JObject searchData = (JObject)moduleData["SearchData"];
                JArray jsonChoosenOptions = (JArray)searchData["ChoosenOptions"];

                if (jsonChoosenOptions != null)
                {
                    foreach (JValue jsonChoosenOption in jsonChoosenOptions)
                    {
                        int optionIndex = Int32.Parse(jsonChoosenOption.ToString());
                        SearchChoosenOptions.Add(optionIndex);
                    }
                }
            }
        }

        public override bool IsFilteredResponse(JObject responseData)
        {
            if (questionData == null) return false;

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return false;

            JArray jsonChoosenOptions = (JArray)answerData["ChoosenOptions"];
            if (jsonChoosenOptions == null) return false;

            bool choiceFound = false;
            int optionCount = jsonChoosenOptions.Count;

            for (int index = 0; index < optionCount; index++)
            {
                JValue jsonChoosenOption = (JValue)jsonChoosenOptions[index];
                int optionIndex = Int32.Parse(jsonChoosenOption.ToString());
                if (SearchChoosenOptions.Contains(optionIndex))
                    choiceFound = true;
                else
                {
                    jsonChoosenOptions.RemoveAt(index);
                    index = index - 1;
                    optionCount = optionCount - 1;
                }
            }
            if (choiceFound)
                return true;
            return false;
        }

        public override bool IsKeywordFound(JObject responseData, List<string> filterKeywords)
        {
            return false;
        }

        public override void collectAnalyticsData(SurveyResponse surveyResponse)
        {
            if (questionData == null) return;

            ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
            if (mr == null) return;

            if (string.IsNullOrEmpty(mr.ModuleData)) return;
            JObject responseData = JObject.Parse(mr.ModuleData);

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return;

            JArray jsonChoosenOptions = (JArray)answerData["ChoosenOptions"];
            if (jsonChoosenOptions == null) return;

            foreach (JValue jsonChoosenOption in jsonChoosenOptions)
            {
                int optionIndex = Int32.Parse(jsonChoosenOption.ToString());
                Choices[optionIndex] = Choices[optionIndex] + 1;
                totalEntries += 1;
            }

            totalCount++;
        }

        public override void getModuleDataWithAnalytics()
        {
            if (totalEntries <= 0)
            {
                analyticsData["NotFound"] = true;
                return;
            }

            JArray choiceWCounts = new JArray();
            foreach (var choice in Choices)
            {
                JObject choiceWCount = new JObject();
                choiceWCount["ChoiceText"] = ChoiceTextList[choice.Key];
                choiceWCount["Count"] = choice.Value;
                choiceWCount["Percentage"] = ((double)choice.Value * 100) / totalEntries;
                choiceWCounts.Add(choiceWCount);
            }
            analyticsData["ChoiceWCounts"] = choiceWCounts;
            analyticsData["TotalResponses"] = totalCount;
        }

        public override void getModuleDataWithSearchAnalytics()
        {
            if (totalEntries <= 0)
            {
                analyticsData["NotFound"] = true;
                return;
            }
            JArray choiceWCounts = new JArray();
            foreach (var choice in Choices)
            {
                if (SearchChoosenOptions.Contains(choice.Key))
                {
                    JObject choiceWCount = new JObject();
                    choiceWCount["ChoiceText"] = ChoiceTextList[choice.Key];
                    choiceWCount["Count"] = choice.Value;
                    choiceWCount["Percentage"] = ((double)choice.Value * 100) / totalEntries;
                    choiceWCounts.Add(choiceWCount);
                }
            }
            analyticsData["ChoiceWCounts"] = choiceWCounts;
            analyticsData["TotalResponses"] = totalCount;
        }
    }
}