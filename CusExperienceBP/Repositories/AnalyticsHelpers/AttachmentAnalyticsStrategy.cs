﻿ using CusExperience.Entities;
using CusExperience.Utilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    class AttachmentAnalyticsStrategy : AnalyticsStrategy
    {
        JArray jsonAllAttachments= new JArray();

        public AttachmentAnalyticsStrategy()
        {
        }

        public AttachmentAnalyticsStrategy(long mID, JObject moduleData, List<string> keywords, List<string> woKeywords)
            : base(mID, moduleData, keywords, woKeywords)
        {

        }

        public override bool IsFilteredResponse(JObject responseData)
        {
            return false;
        }

        public override bool IsKeywordFound(JObject responseData, List<string> filterKeywords)
        {
            return false;
        }

        public override void collectAnalyticsData(SurveyResponse surveyResponse)
        {
            if (questionData == null) return;

            ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
            if (mr == null) return;

            if (string.IsNullOrEmpty(mr.ModuleData)) return;
            JObject responseData = JObject.Parse(mr.ModuleData);

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return;

            if (answerData["Attachments"] != null) {
                JArray jsonAttachments = JArray.Parse(answerData["Attachments"].ToString());
                foreach (JObject jsonAttachment in jsonAttachments)
                    jsonAllAttachments.Add(jsonAttachment);
            }
        }

        public override void getModuleDataWithAnalytics()
        {
            if(jsonAllAttachments.Count <= 0)
                analyticsData["NotFound"] = true;
            else
            {
                analyticsData["AllAttachments"] = jsonAllAttachments;
            }
        }


        internal List<string> GetAttachmentExtensions(string mData)
        {
            List<string> fileExtensions = new List<string>();
            JObject moduleData = JObject.Parse(mData);
            if (moduleData["QuestionData"] == null) return fileExtensions;

            JObject questionData = JObject.Parse(moduleData["QuestionData"].ToString());
            if (questionData["IsImageAllowed"] != null && questionData["IsImageAllowed"].ToString().Equals("True"))
                fileExtensions.AddRange(FILEEXTENSIONS.IMAGE);
            if (questionData["IsVideoAllowed"] != null && questionData["IsVideoAllowed"].ToString().Equals("True"))
                fileExtensions.AddRange(FILEEXTENSIONS.VIDEO);
            if (questionData["IsPdfAllowed"] != null && questionData["IsPdfAllowed"].ToString().Equals("True"))
                fileExtensions.AddRange(FILEEXTENSIONS.PDF);
            if (questionData["IsOffDocsAllowed"] != null && questionData["IsOffDocsAllowed"].ToString().Equals("True"))
                fileExtensions.AddRange(FILEEXTENSIONS.OFFICE);
            if (questionData["IsImageAllowed"] == null && questionData["IsVideoAllowed"] == null && questionData["IsPdfAllowed"] == null && questionData["IsOffDocsAllowed"] == null)
            {
                fileExtensions.AddRange(FILEEXTENSIONS.IMAGE);
                fileExtensions.AddRange(FILEEXTENSIONS.VIDEO);
                fileExtensions.AddRange(FILEEXTENSIONS.PDF);
                fileExtensions.AddRange(FILEEXTENSIONS.OFFICE);
            }
            return fileExtensions;
        }
    }
}
