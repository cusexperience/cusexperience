﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using CusExperience.Utilities;
using System.Text;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;


namespace CusExperience.Repositories.AnalyticsHelpers
{

    class TagAnalyticsStrategy : AnalyticsStrategy
    {
        //Analytics Variables
        public Dictionary<string, List<long>> WordFrequency { get; set; }
        public Dictionary<string, List<long>> TagCount { get; set; } 

        // Data Collection Variables
        List<SRComment> Comments = new List<SRComment>();
        Dictionary<int, string> TagIndex = new Dictionary<int, string>();


        // Search Variables
        List<string> searchTags = new List<string>();
        bool IsNoTagsSelected = true;

        public TagAnalyticsStrategy()
        {
            WordFrequency = new Dictionary<string, List<long>>();
            TagCount = new Dictionary<string, List<long>>();
        }

        public TagAnalyticsStrategy(long mID, JObject moduleData, List<string> keywords, List<string> woKeywords)
            : base(mID, moduleData, keywords, woKeywords)
        {
            WordFrequency = new Dictionary<string, List<long>>();
            TagCount = new Dictionary<string, List<long>>();

            JArray Tags = null;
            if (questionData["Tags"] != null)
                Tags = (JArray)questionData["Tags"];
            int indexValue = 0;

            foreach (JObject obj in Tags)
            {
                if (obj["TagName"] != null)
                {
                    string tagName = obj["TagName"].ToString();
                    TagIndex.Add(indexValue++, tagName);
                    TagCount.Add(tagName, new List<long>());
                }
            }

            if (moduleData["SearchData"] != null)
            {
                JArray jsonSelectedTags = new JArray();
                JObject searchData = (JObject)moduleData["SearchData"];

                if (searchData["SelectedTags"] != null)
                    jsonSelectedTags = (JArray)searchData["SelectedTags"];

                foreach (JValue jsonSelectedTag in jsonSelectedTags)
                {
                    int value = Int32.Parse(jsonSelectedTag.ToString());
                    if (!TagIndex.Keys.Contains(value)) continue;
                    searchTags.Add(TagIndex[value]);
                }

                if (searchData["IsNoTagsSelected"] != null)
                    IsNoTagsSelected = Convert.ToBoolean(searchData["IsNoTagsSelected"].ToString());
            }
        }

        public override bool IsFilteredResponse(JObject responseData)
        {
            string comment = null;
            JArray jsonSelectedTags = new JArray();
            List<int> selectedTags = new List<int>();
            Dictionary<int, string> ResponseTagIndex = new Dictionary<int, string>();

            JObject questionData = (JObject)responseData["QuestionData"];
            if (questionData == null) return false;

            JArray Tags = null;
            if (questionData["Tags"] != null)
                Tags = (JArray)questionData["Tags"];

            int indexValue = 0;

            foreach (JObject obj in Tags)
            {
                if (obj["TagName"] != null)
                {
                    string tagName = obj["TagName"].ToString();
                    ResponseTagIndex.Add(indexValue++, tagName);
                }
            }

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return false;

            if (answerData["CommentText"] == null) return false;

            comment = answerData["CommentText"].ToString();

            if (answerData["SelectedTags"] != null)
                jsonSelectedTags = (JArray)answerData["SelectedTags"];

            if (IsNoTagsSelected == true && jsonSelectedTags.Count <= 0)
                return true;

            if (searchTags.Count > 0 && jsonSelectedTags.Count <= 0)
                return false;

            foreach (JValue jsonSelectedTag in jsonSelectedTags)
            {
                int value = Int32.Parse(jsonSelectedTag.ToString());
                if (!ResponseTagIndex.Keys.Contains(value)) return false;

                string selTag = ResponseTagIndex[value];
                if (searchTags.Contains(selTag)) 
                    return true;
            }

            return false;
        }

        public override bool IsKeywordFound(JObject responseData, List<string> filterKeywords)
        {
            if (filterKeywords.Count <= 0) return false;

            string comment = null;

            if (questionData == null) return false;

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return false;

            if (answerData["CommentText"] == null) return false;

            comment = answerData["CommentText"].ToString();

            if (!string.IsNullOrEmpty(comment) && (filterKeywords.Count <= 0 || filterKeywords.Any(kw => comment.Split(' ', '\n').Any(w => w.RemovePunctutions().Trim().ToLower().Contains(kw)))))
                return true;

            return false;
        }

        public override void collectAnalyticsData(SurveyResponse surveyResponse)
        {
            Dictionary<int, string> ResponseTagIndex = new Dictionary<int, string>();

            ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
            if (mr == null) return;

            if (string.IsNullOrEmpty(mr.ModuleData)) return;
            JObject responseData = JObject.Parse(mr.ModuleData);

            string comment = null;
            JArray jsonSelectedTags = null;

            JObject questionData = (JObject)responseData["QuestionData"];
            if (questionData == null) return;

            JArray Tags = null;
            if (questionData["Tags"] != null)
                Tags = (JArray)questionData["Tags"];

            int indexValue = 0;

            foreach (JObject obj in Tags)
            {
                if (obj["TagName"] != null)
                {
                    string tagName = obj["TagName"].ToString();
                    ResponseTagIndex.Add(indexValue++, tagName);
                }
            }

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return;

            if (answerData["CommentText"] != null)
            {
                comment = answerData["CommentText"].ToString();

                if (answerData["SelectedTags"] != null)
                    jsonSelectedTags = (JArray)answerData["SelectedTags"];

                foreach (JValue jsonSelectedTag in jsonSelectedTags)
                {
                    int value = Int32.Parse(jsonSelectedTag.ToString());
                    if (!ResponseTagIndex.Keys.Contains(value)) return;
                    string selTag = ResponseTagIndex[value];
                    if (!TagCount.Keys.Contains(selTag)) return;
                    TagCount[selTag].Add(surveyResponse.SurveyResponseID);
                }
                
                 SRComment c = new SRComment();
                 c.SurveyResponseID = surveyResponse.SurveyResponseID;
                 c.Comment = comment;
                 Comments.Add(c);
            }
        }

        public override void getModuleDataWithAnalytics()
        {

            if (Comments.Count <= 0)
            {
                analyticsData["NotFound"] = true;
                return;
            }
            
            var rnd = new Random();
            JArray wordCloud = new JArray();
            WordFrequency = Comments.Select(a => new { a.SurveyResponseID, rValue = Regex.Replace(a.Comment, @"\s+", " ") })
                                               .Where(a => !string.IsNullOrWhiteSpace(a.rValue))
                                               .SelectMany(s => s.rValue.Split(' '), (s, word) => new { word, s.SurveyResponseID })
                                               .Select(a => new { rWord = a.word.RemovePunctutions(), a.SurveyResponseID })
                                               .Select(a => new { rWord = a.rWord.Trim().ToLower(), a.SurveyResponseID })
                                               .Where(a => !string.IsNullOrWhiteSpace(a.rWord))
                                               .Where(a => !CONSTANTS.EXCLUDELIST.Contains(a.rWord))
                                               .GroupBy(word => word.rWord)
                                               .Select(p => new { p.Key, SRID = p.Select(x => x.SurveyResponseID).ToList() })
                                               .OrderByDescending(a => a.SRID.Count)
                                               .Take(1000).ToDictionary(t => t.Key, t => t.SRID);

            foreach (var word in WordFrequency)
            {
                JObject cloudTag = new JObject();
                cloudTag["Word"] = word.Key;
                cloudTag["Frequency"] = word.Value.Count;
                wordCloud.Add(cloudTag);
            }

            JArray tagCount = new JArray();
            foreach (var element in TagCount)
            {
                JObject eachTag = new JObject();
                eachTag["TagName"] = element.Key;
                eachTag["Count"] = element.Value.Count;
                tagCount.Add(eachTag);
            }

            analyticsData["WordCloud"] = wordCloud;
            analyticsData["TagCount"] = tagCount;
        }


        public override void getModuleDataWithSearchAnalytics()
        {

            if (Comments.Count <= 0)
            {
                analyticsData["NotFound"] = true;
                return;
            }
            var excludeList = new[] { "a", "able", "about", "above", "abst", "accordance", "according", "accordingly", "across", "act", "actually", "added", "adj", "affected", "affecting", "affects", "after", "afterwards", "again", "against", "ah", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "an", "and", "announce", "another", "any", "anybody", "anyhow", "anymore", "anyone", "anything", "anyway", "anyways", "anywhere", "apparently", "approximately", "are", "aren", "arent", "arise", "around", "as", "aside", "ask", "asking", "at", "auth", "available", "away", "awfully", "b", "back", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "begin", "beginning", "beginnings", "begins", "behind", "being", "believe", "below", "beside", "besides", "between", "beyond", "biol", "both", "brief", "briefly", "but", "by", "c", "ca", "came", "can", "cannot", "can't", "cause", "causes", "certain", "certainly", "co", "com", "come", "comes", "contain", "containing", "contains", "could", "couldnt", "d", "date", "did", "didn't", "different", "do", "does", "doesn't", "doing", "done", "don't", "dont", "didnt", "doesnt", "down", "downwards", "due", "during", "e", "each", "ed", "edu", "effect", "eg", "eight", "eighty", "either", "else", "elsewhere", "end", "ending", "enough", "especially", "et", "et-al", "etc", "even", "ever", "every", "everybody", "everyone", "everything", "everywhere", "ex", "except", "f", "far", "few", "ff", "fifth", "first", "five", "fix", "followed", "following", "follows", "for", "former", "formerly", "forth", "found", "four", "from", "further", "furthermore", "g", "gave", "get", "gets", "getting", "give", "given", "gives", "giving", "go", "goes", "gone", "got", "gotten", "h", "had", "happens", "hardly", "has", "hasn't", "have", "haven't", "having", "he", "hed", "hence", "her", "here", "hereafter", "hereby", "herein", "heres", "hereupon", "hers", "herself", "hes", "hi", "hid", "him", "himself", "his", "hither", "home", "how", "howbeit", "however", "hundred", "i", "id", "ie", "if", "i'll", "im", "immediate", "immediately", "importance", "important", "in", "inc", "indeed", "index", "information", "instead", "into", "invention", "inward", "is", "isn't", "it", "itd", "it'll", "its", "itself", "i've", "j", "just", "k", "keep", "keeps", "kept", "kg", "km", "know", "known", "knows", "l", "largely", "last", "lately", "later", "latter", "latterly", "least", "less", "lest", "let", "lets", "like", "liked", "likely", "line", "little", "'ll", "look", "looking", "looks", "ltd", "m", "made", "mainly", "make", "makes", "many", "may", "maybe", "me", "mean", "means", "meantime", "meanwhile", "merely", "mg", "might", "million", "miss", "ml", "more", "moreover", "most", "mostly", "mr", "mrs", "much", "mug", "must", "my", "myself", "n", "na", "name", "namely", "nay", "nd", "near", "nearly", "necessarily", "necessary", "need", "needs", "neither", "never", "nevertheless", "new", "next", "nine", "ninety", "no", "nobody", "non", "none", "nonetheless", "noone", "nor", "normally", "nos", "not", "noted", "nothing", "now", "nowhere", "o", "obtain", "obtained", "obviously", "of", "off", "often", "oh", "ok", "okay", "old", "omitted", "on", "once", "one", "ones", "only", "onto", "or", "ord", "other", "others", "otherwise", "ought", "our", "ours", "ourselves", "out", "outside", "over", "overall", "owing", "own", "p", "page", "pages", "part", "particular", "particularly", "past", "per", "perhaps", "placed", "please", "plus", "poorly", "possible", "possibly", "potentially", "pp", "predominantly", "present", "previously", "primarily", "probably", "promptly", "proud", "provides", "put", "q", "que", "quickly", "quite", "qv", "r", "ran", "rather", "rd", "re", "readily", "really", "recent", "recently", "ref", "refs", "regarding", "regardless", "regards", "related", "relatively", "research", "respectively", "resulted", "resulting", "results", "right", "run", "s", "said", "same", "saw", "say", "saying", "says", "sec", "section", "see", "seeing", "seem", "seemed", "seeming", "seems", "seen", "self", "selves", "sent", "seven", "several", "shall", "she", "shed", "she'll", "shes", "should", "shouldn't", "show", "showed", "shown", "showns", "shows", "significant", "significantly", "similar", "similarly", "since", "six", "slightly", "so", "some", "somebody", "somehow", "someone", "somethan", "something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specifically", "specified", "specify", "specifying", "still", "stop", "strongly", "sub", "substantially", "successfully", "such", "sufficiently", "suggest", "sup", "sure", "t", "take", "taken", "taking", "tell", "tends", "th", "than", "thank", "thanks", "thanx", "that", "that'll", "thats", "that've", "the", "their", "theirs", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "thered", "therefore", "therein", "there'll", "thereof", "therere", "theres", "thereto", "thereupon", "there've", "these", "they", "theyd", "they'll", "theyre", "they've", "think", "this", "those", "thou", "though", "thoughh", "thousand", "throug", "through", "throughout", "thru", "thus", "til", "tip", "to", "together", "too", "took", "toward", "towards", "tried", "tries", "truly", "try", "trying", "ts", "twice", "two", "u", "un", "under", "unfortunately", "unless", "unlike", "unlikely", "until", "unto", "up", "upon", "ups", "us", "use", "used", "useful", "usefully", "usefulness", "uses", "using", "usually", "v", "value", "various", "'ve", "very", "via", "viz", "vol", "vols", "vs", "w", "want", "wants", "was", "wasn't", "way", "we", "wed", "welcome", "we'll", "went", "were", "weren't", "we've", "what", "whatever", "what'll", "whats", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "wheres", "whereupon", "wherever", "whether", "which", "while", "whim", "whither", "who", "whod", "whoever", "whole", "who'll", "whom", "whomever", "whos", "whose", "why", "widely", "willing", "wish", "with", "within", "without", "won't", "words", "world", "would", "wouldn't", "www", "x", "y", "yes", "yet", "you", "youd", "you'll", "your", "youre", "yours", "yourself", "yourselves", "you've", "z", "zero", "will" };
            JArray wordCloud = new JArray();

            WordFrequency = Comments.Select(a => new { a.SurveyResponseID, rValue = Regex.Replace(a.Comment, @"\s+", " ") })
                                   .Where(a => !string.IsNullOrWhiteSpace(a.rValue))
                                   .SelectMany(s => s.rValue.Split(' '), (s, word) => new { word, s.SurveyResponseID })
                                   .Select(a => new { rWord = a.word.RemovePunctutions(), a.SurveyResponseID })
                                   .Select(a => new { rWord = a.rWord.Trim().ToLower(), a.SurveyResponseID })
                                   .Where(a => !string.IsNullOrWhiteSpace(a.rWord))
                                   .Where(a => !CONSTANTS.EXCLUDELIST.Contains(a.rWord))
                                   .GroupBy(word => word.rWord)
                                   .Select(p => new { p.Key, SRID = p.Select(x => x.SurveyResponseID).ToList() })
                                   .OrderByDescending(a => a.SRID.Count)
                                   .Take(1000).ToDictionary(t => t.Key, t => t.SRID);

          /* if (wordFrequencyx.Count <= 0)
            {
                analyticsData["NotFound"] = true;
                return;
            } */

            foreach (var word in WordFrequency)
            {
                string wordHtml = "";
                if (Keywords.Any(kw => word.Key.Contains(kw)))
                    wordHtml = "<strong>" + word.Key + "<strong>";
                else wordHtml = word.Key;
                JObject cloudTag = new JObject();
                cloudTag["Word"] = wordHtml;
                cloudTag["Frequency"] = word.Value.Count;
                wordCloud.Add(cloudTag);
            }

            JArray tagCount = new JArray();
            foreach (var element in TagCount)
            {
                JObject eachTag = new JObject();
                eachTag["TagName"] = element.Key;
                eachTag["Count"] = element.Value.Count;
                tagCount.Add(eachTag);
            }

            analyticsData["WordCloud"] = wordCloud;
            analyticsData["TagCount"] = tagCount;
        }

        internal static void writetoExcel(ref string columnName, ref uint rowIndex, WorksheetPart worksheetPart, long moduleID, JObject moduleData, List<SurveyResponse> surveyResponses)
        {
            Cell cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            cell.CellValue = new CellValue(TagAnalyticsStrategy.GetQuestionText(moduleData));

            var beginColumn = columnName;

            ExcelUtility.GetNextRow(ref rowIndex);

            cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            cell.CellValue = new CellValue("Comment");

            cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
            cell.CellValue = new CellValue("Tags");

            foreach (SurveyResponse surveyResponse in surveyResponses)
            {
                columnName = beginColumn;

                ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
                if (mr == null) continue;

                JObject responseData = JObject.Parse(mr.ModuleData);

                cell = ExcelUtility.InsertCellInWorksheet(columnName, ExcelUtility.GetNextRow(ref rowIndex), worksheetPart);
                cell.CellValue = new CellValue(TagAnalyticsStrategy.GetComment(responseData));

                cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                cell.CellValue = new CellValue(TagAnalyticsStrategy.GetSelectedTags(responseData));
            }

            rowIndex = 1;
            columnName = ExcelUtility.GetNextColumn(ref columnName);
        }


        internal static string GetQuestionText(JObject moduleData)
        {
            string questionText = "";

            JObject questionData = (JObject)moduleData["QuestionData"];
            if (questionData == null) return questionText;

            if (questionData["QuestionText"] == null) return questionText;

            questionText = questionData["QuestionText"].ToString();
            questionText = Regex.Replace(questionText, @"<[^>]+>|;", string.Empty).Trim();
            return questionText;
        }

        internal static string GetComment(JObject responseData)
        {
            string comment = "";

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return comment;

            if (answerData["CommentText"] == null)
                return comment;

            comment = answerData["CommentText"].ToString();
            return comment;
        }

        internal static string GetSelectedTags(JObject responseData)
        {
            string tags = "";
            Dictionary<int, string> ResponseTagIndex = new Dictionary<int, string>();
            JArray jsonSelectedTags = new JArray();
            List<string> selectedTags = new List<string>();

            JObject questionData = (JObject)responseData["QuestionData"];
            if (questionData == null) return tags;

            JArray Tags = null;
            if (questionData["Tags"] != null)
                Tags = (JArray)questionData["Tags"];

            int indexValue = 0;

            foreach (JObject obj in Tags)
            {
                if (obj["TagName"] != null)
                {
                    string tagName = obj["TagName"].ToString();
                    ResponseTagIndex.Add(indexValue++, tagName);
                }
            }

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return tags;

            if (answerData["SelectedTags"] != null)
                jsonSelectedTags = (JArray)answerData["SelectedTags"];

            foreach (JValue jsonSelectedTag in jsonSelectedTags)
            {
                int value = Int32.Parse(jsonSelectedTag.ToString());
                if (!ResponseTagIndex.Keys.Contains(value)) continue;
                string selTag = ResponseTagIndex[value];
                selectedTags.Add(selTag);
            }
            tags = string.Join(",", selectedTags);
            return tags;
        }
    }
}
