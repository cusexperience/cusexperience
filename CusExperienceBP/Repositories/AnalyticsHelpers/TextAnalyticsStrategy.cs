﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using CusExperience.Utilities;
using System.Text;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    public class TextAnalyticsStrategy : AnalyticsStrategy
    {
        List<SRComment> Comments = new List<SRComment>();
        public Dictionary<string, List<long>> WordFrequency { get; set; }

        public TextAnalyticsStrategy()
        {

        }

        public TextAnalyticsStrategy(long mID, JObject moduleData, List<string> keywords, List<string> woKeywords)
            : base(mID, moduleData, keywords, woKeywords)
        {
            WordFrequency = new Dictionary<string, List<long>>();
        }

        public override bool IsFilteredResponse(JObject responseData)
        {
            return true;
        }

        public override bool IsKeywordFound(JObject responseData, List<string> filterKeywords)
        {
            if (filterKeywords.Count <= 0) return false;

            string comment = null;

            if (questionData == null) return false;

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return false;

            if (answerData["CommentText"] == null) return false;

            comment = answerData["CommentText"].ToString();

            if (!string.IsNullOrEmpty(comment) && (filterKeywords.Count <= 0 || filterKeywords.Any(kw => comment.Split(' ', '\n').Any(w => w.RemovePunctutions().Trim().ToLower().Contains(kw)))))
                return true;

            return false;
        }

        public override void collectAnalyticsData(SurveyResponse surveyResponse)
        {
            if (questionData == null) return;

            ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
            if (mr == null) return;

            if (string.IsNullOrEmpty(mr.ModuleData)) return;
            JObject responseData = JObject.Parse(mr.ModuleData);

            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return;

            string comment = answerData["CommentText"].ToString();
            if (string.IsNullOrEmpty(comment)) return;

            SRComment c = new SRComment();
            c.SurveyResponseID = surveyResponse.SurveyResponseID;
            c.Comment = comment;
            Comments.Add(c);
        }

        public static string GetComment(JObject responseData)
        {
            JObject answerData = (JObject)responseData["AnswerData"];
            if (answerData == null) return "";

            string comment = answerData["CommentText"].ToString();
            if (!string.IsNullOrEmpty(comment)) return comment;
            else return "";
        }

        public override void getModuleDataWithAnalytics()
        {
            if (Comments.Count <= 0)
            {
                analyticsData["NotFound"] = true;
                return;
            }

            if (Comments.Count <= 0)
            {
                analyticsData["NotFound"] = true;
                return;
            }

            JArray wordCloud = new JArray();

            WordFrequency = Comments.Select(a => new { a.SurveyResponseID, rValue = Regex.Replace(a.Comment, @"\s+", " ") })
                                              .Where(a => !string.IsNullOrWhiteSpace(a.rValue))
                                              .SelectMany(s => s.rValue.Split(' '), (s, word) => new { word, s.SurveyResponseID })
                                              .Select(a => new { rWord = a.word.RemovePunctutions(), a.SurveyResponseID })
                                              .Select(a => new { rWord = a.rWord.Trim().ToLower(), a.SurveyResponseID })
                                              .Where(a => !string.IsNullOrWhiteSpace(a.rWord))
                                              .Where(a => !CONSTANTS.EXCLUDELIST.Contains(a.rWord))
                                              .GroupBy(word => word.rWord)
                                              .Select(p => new { p.Key, SRID = p.Select(x => x.SurveyResponseID).ToList() })
                                              .OrderByDescending(a => a.SRID.Count)
                                              .Take(1000).ToDictionary(t => t.Key, t => t.SRID);

            foreach (var word in WordFrequency)
            {
                JObject cloudTag = new JObject();
                cloudTag["Word"] = word.Key;
                cloudTag["Frequency"] = word.Value.Count;
                wordCloud.Add(cloudTag);
            }
            analyticsData["WordCloud"] = wordCloud;
        }

        public override void getModuleDataWithSearchAnalytics()
        {

            if (Comments.Count <= 0)
            {
                analyticsData["NotFound"] = true;
                return;
            }

            JArray wordCloud = new JArray();

            WordFrequency = Comments.Select(a => new { a.SurveyResponseID, rValue = Regex.Replace(a.Comment, @"\s+", " ") })
                                               .Where(a => !string.IsNullOrWhiteSpace(a.rValue))
                                               .SelectMany(s => s.rValue.Split(' '), (s, word) => new { word, s.SurveyResponseID })
                                               .Select(a => new { rWord = a.word.RemovePunctutions(), a.SurveyResponseID })
                                               .Select(a => new { rWord = a.rWord.Trim().ToLower(), a.SurveyResponseID })
                                               .Where(a => !string.IsNullOrWhiteSpace(a.rWord))
                                               .Where(a => !CONSTANTS.EXCLUDELIST.Contains(a.rWord))
                                               .GroupBy(word => word.rWord)
                                               .Select(p => new { p.Key, SRID = p.Select(x => x.SurveyResponseID).ToList() })
                                               .OrderByDescending(a => a.SRID.Count)
                                               .Take(1000).ToDictionary(t => t.Key, t => t.SRID);

            if (WordFrequency.Count <= 0)
            {
                analyticsData["NotFound"] = true;
                return;
            }

            foreach (var word in WordFrequency)
            {
                string wordHtml = "";
                if (Keywords.Any(kw => word.Key.Contains(kw)))
                    wordHtml = "<strong>" + word.Key + "<strong>";
                else wordHtml = word.Key;
                JObject cloudTag = new JObject();
                cloudTag["Word"] = wordHtml;
                cloudTag["Frequency"] = word.Value.Count;
                wordCloud.Add(cloudTag);
            }

            analyticsData["WordCloud"] = wordCloud;
        }

        internal static string GetQuestionText(JObject moduleData)
        {
            string questionText = "";

            JObject questionData = (JObject)moduleData["QuestionData"];
            if (questionData == null) return questionText;

            if (questionData["QuestionText"] == null) return questionText;

            questionText = questionData["QuestionText"].ToString();
            questionText = Regex.Replace(questionText, @"<[^>]+>|;", string.Empty).Trim();
            return questionText;
        }



        internal static void writetoExcel(ref string columnName, ref uint rowIndex, WorksheetPart worksheetPart, long moduleID, JObject moduleData, List<SurveyResponse> surveyResponses)
        {
            Cell cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            cell.CellValue = new CellValue(TextAnalyticsStrategy.GetQuestionText(moduleData));

            var beginColumn = columnName;
            ExcelUtility.GetNextRow(ref rowIndex);

            cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
            cell.CellValue = new CellValue("Comment");

            foreach (SurveyResponse surveyResponse in surveyResponses)
            {
                columnName = beginColumn;

                ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();
                if (mr == null) continue;

                JObject responseData = JObject.Parse(mr.ModuleData);

                cell = ExcelUtility.InsertCellInWorksheet(columnName, ExcelUtility.GetNextRow(ref rowIndex), worksheetPart);
                cell.DataType = CellValues.Number;
                cell.CellValue = new CellValue(TextAnalyticsStrategy.GetComment(responseData));
            }

            rowIndex = 1;
            columnName = ExcelUtility.GetNextColumn(ref columnName);
        }
    }
}