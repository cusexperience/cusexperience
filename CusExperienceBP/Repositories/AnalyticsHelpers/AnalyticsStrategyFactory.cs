﻿using CusExperience.Entities;
using CusExperience.Utilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.Repositories.AnalyticsHelpers
{
    public class AnalyticsStrategyFactory
    {
        public static AnalyticsStrategy getAnalyticsStrategy(string questionType)
        {
            switch (questionType)
            {
                case QUESTIONTYPES.RATE:
                    return new RatingAnalyticsStrategy();

                case QUESTIONTYPES.MULTIPLE:
                    return new MultipleAnalyticsStrategy();

                case QUESTIONTYPES.TEXT:
                    return new TextAnalyticsStrategy();

                case QUESTIONTYPES.TAG:
                    return new TagAnalyticsStrategy();

                case QUESTIONTYPES.CHOICE:
                    return new ChoiceAnalyticsStrategy();

                case QUESTIONTYPES.ATTACH:
                    return new AttachmentAnalyticsStrategy();

                case QUESTIONTYPES.STAFF:
                    return new StaffAnalyticsStrategy();

                case QUESTIONTYPES.DEMOGRAPHIC:
                    return new DGAnalyticsStrategy();

                case QUESTIONTYPES.LOCATION:
                    return new LocationAnalyticsStrategy();

            }
            return null;
        }

        public static AnalyticsStrategy getAnalyticsStrategy(long moduleID, JObject moduleData, List<string> keywords, List<string> woKeywords)
        {

            string questionType = moduleData["QuestionType"].ToString();

            switch (questionType)
            {
                case QUESTIONTYPES.RATE:
                    return new RatingAnalyticsStrategy(moduleID, moduleData,keywords,woKeywords);

                case QUESTIONTYPES.MULTIPLE:
                    return new MultipleAnalyticsStrategy(moduleID, moduleData, keywords, woKeywords);

                case QUESTIONTYPES.TEXT:
                    return new TextAnalyticsStrategy(moduleID, moduleData, keywords, woKeywords);

                case QUESTIONTYPES.TAG:
                    return new TagAnalyticsStrategy(moduleID, moduleData, keywords, woKeywords);

                case QUESTIONTYPES.CHOICE:
                    return new ChoiceAnalyticsStrategy(moduleID, moduleData, keywords, woKeywords);

                case QUESTIONTYPES.ATTACH:
                    return new AttachmentAnalyticsStrategy(moduleID, moduleData, keywords, woKeywords);

                case QUESTIONTYPES.STAFF:
                    return new StaffAnalyticsStrategy(moduleID, moduleData, keywords, woKeywords);

                case QUESTIONTYPES.DEMOGRAPHIC:
                    return new DGAnalyticsStrategy(moduleID, moduleData, keywords, woKeywords);

                case QUESTIONTYPES.LOCATION:
                    return new LocationAnalyticsStrategy(moduleID, moduleData, keywords, woKeywords);

            }
            return null;
        }
    }
}