﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using CusExperience.Entities;
using CusExperience.Repositories;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.DAL;
using CusExperience.Utilities;
using CusExperience.Repositories.AnalyticsHelpers;
using System.Web.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using CusExperience.Exceptions;
using CusExperienceContract.Entities.SurveyData;
using System.Diagnostics;
using System.Data.Entity.Core.Objects;


namespace CusExperience.Repositories
{
    public class ResponseRepository
    {
        public CusExpContext dbContext = new CusExpContext();
        internal static string QUESTION = "Question";
        internal static string APPRECIATION = "Appreciation";
        internal static string TIMEZONE = "Singapore Standard Time";
        TimeZoneInfo SingaporeTimeZone = TimeZoneInfo.FindSystemTimeZoneById(TIMEZONE);
        OrgSubRepository oRep = new OrgSubRepository();
        UserRepository uRep = new UserRepository();

        Dictionary<long, string> orgSubDictionary = new Dictionary<long, string>();
        Dictionary<long, string> tpDictionary = new Dictionary<long, string>();

        public ResponseRepository()
        {
        }

        public List<Survey> GetPbSurveysWithRespCountByOrgSub(long orgSubId, string userId)
        {
            List<Survey> publishedSurveys = null;
            List<Survey> pbSurveys = new List<Survey>();


            if (uRep.isSuperAdministrator(userId))
            {
                publishedSurveys = dbContext.Surveys.Where(s => s.Status != "DELETED" && s.IsTemplate == false && s.IsPublished == true && s.OrgSub.OrgSubID == orgSubId).ToList();
                if (publishedSurveys == null)
                    return null;

                OrgSub orgSub = oRep.GetAnalyticsOrgSub(orgSubId, ref orgSubDictionary, ref tpDictionary, userId);

                Dictionary<int,int> surveyResponseCount = dbContext.SurveyResponses.Include(o => o.OrgSub).Include(s => s.Survey).Where(x => x.Status != "DELETED" && orgSubDictionary.Keys.Contains(x.OrgSub.OrgSubID)).GroupBy(x => x.Survey.SurveyID).Select(g => new{ g.Key, TotalCount = g.Count() }).ToDictionary(t=> t.Key, t=> t.TotalCount);

                foreach (Survey survey in publishedSurveys)
                {
                    Survey eachSurvey = new Survey();
                    eachSurvey.SurveyID = survey.SurveyID;
                    eachSurvey.SurveyTitle = survey.SurveyTitle;
                    if (surveyResponseCount.ContainsKey(survey.SurveyID))
                        eachSurvey.TotalResponses = surveyResponseCount[survey.SurveyID];
                    pbSurveys.Add(eachSurvey);
                }

                GetAllParentSurveys(ref pbSurveys, orgSub.ParentOrgSub, userId);

                return pbSurveys;
            }
            throw new UnauthorizedAccessException();
        }

        private void GetAllParentSurveys(ref List<Survey> Surveys, OrgSub cOrgSub, string userId)
        {
            if (cOrgSub == null)
                return;
            OrgSub orgSub = oRep.GetOrgSub(cOrgSub.OrgSubID, userId);

            List<Survey> parentSurveys = dbContext.Surveys.Include(o => o.OrgSub.ParentOrgSub).Where(s => s.Status != "DELETED" && s.OrgSub.OrgSubID == cOrgSub.OrgSubID).OrderBy(s => s.CreatedDate).ToList();

            Dictionary<int, int> surveyResponseCount = dbContext.SurveyResponses.Include(o => o.OrgSub).Include(s => s.Survey).Where(x => x.Status != "DELETED" && orgSubDictionary.Keys.Contains(x.OrgSub.OrgSubID)).GroupBy(x => x.Survey.SurveyID).Select(g => new { g.Key, TotalCount = g.Count() }).ToDictionary(t => t.Key, t => t.TotalCount);

            foreach (Survey survey in parentSurveys)
            {
                Survey eachSurvey = new Survey();
                eachSurvey.SurveyID = survey.SurveyID;
                eachSurvey.SurveyTitle = survey.SurveyTitle;
                if (surveyResponseCount.ContainsKey(survey.SurveyID))
                    eachSurvey.TotalResponses = surveyResponseCount[survey.SurveyID];
                Surveys.Insert(0, eachSurvey);
            }

            GetAllParentSurveys(ref Surveys, orgSub.ParentOrgSub, userId);
        }

        public List<Survey> GetPbSurveysWithRespCount(string userId)
        {
            List<Survey> publishedSurveys = new List<Survey>();
            List<Survey> pbSurveys = new List<Survey>();

            if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                publishedSurveys = dbContext.Surveys.Where(s => s.Status != "DELETED" && s.IsTemplate == false && s.IsPublished == true && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).ToList();

                Dictionary<int, int> surveyResponseCount = dbContext.SurveyResponses.Include(s => s.Survey).Where(x => x.Status != "DELETED").GroupBy(x => x.Survey.SurveyID).Select(g => new { g.Key, TotalCount = g.Count() }).ToDictionary(t => t.Key, t => t.TotalCount);
                
                foreach (Survey survey in publishedSurveys)
                {
                    Survey eachSurvey = new Survey();
                    eachSurvey.SurveyID = survey.SurveyID;
                    eachSurvey.SurveyTitle = survey.SurveyTitle;
                    if (surveyResponseCount.ContainsKey(survey.SurveyID))
                        eachSurvey.TotalResponses = surveyResponseCount[survey.SurveyID];
                    pbSurveys.Add(eachSurvey);
                }
                return pbSurveys;
            }
            else if (uRep.isManager(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                publishedSurveys = dbContext.Surveys.Where(s => s.Status != "DELETED" && s.IsTemplate == false && s.IsPublished == true && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).ToList();
                foreach (Survey survey in publishedSurveys)
                {
                    Survey eachSurvey = new Survey();
                    eachSurvey.SurveyID = survey.SurveyID;
                    eachSurvey.SurveyTitle = survey.SurveyTitle;
                    var UserTouchpointNames = User.Touchpoints.Select(t => t.Touchpoint.TouchpointName).ToList();
                    eachSurvey.TotalResponses = dbContext.SurveyResponses.Where(x => x.Survey.SurveyID == survey.SurveyID && x.Status != "DELETED").Count();
                    pbSurveys.Add(eachSurvey);
                }
                return pbSurveys;
            }
           throw new UnauthorizedAccessException();
        }

        public Survey GetSurveyForIV(long surveyId, long orgSubId, string userId)
        {
            Survey survey = null;

            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId
                 && s.Status != "DELETED" && s.IsPublished == true).FirstOrDefault();
                
                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);

                survey.OrgSub = oRep.GetOrgSub(orgSubId, userId);
                survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();

                DateTime? SurveyCreatedDate = survey.CreatedDate;
                survey.FromDateTime = SurveyCreatedDate.Value.ToString("dd/MM/yyyy @ hh:mm");

                return survey;
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId) || uRep.isManager(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId && s.Status != "DELETED"
                    && s.IsPublished == true && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).FirstOrDefault();

                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);

                survey.OrgSub = oRep.GetOrgSub(orgSubId, userId);
                survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();

                DateTime? SurveyCreatedDate = survey.CreatedDate;
                survey.FromDateTime = SurveyCreatedDate.Value.ToString("dd/MM/yyyy @ hh:mm");

                return survey;
            }
            throw new UnauthorizedAccessException();
        }

        public Survey GetResponsesBySurvey(int surveyId, long orgSubId, string userId, int skip, int pageSize, string orderByDirection, string filterKeyword)
        {
            List<SurveyResponse> surveyResponses = new List<SurveyResponse>();

            Survey survey = null;
            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId).FirstOrDefault<Survey>();
                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);

                OrgSub orgSub = oRep.GetAnalyticsOrgSub(orgSubId, ref orgSubDictionary, ref tpDictionary, userId);

                if (orderByDirection.ToUpper().Equals("ASCENDING"))
                    surveyResponses = dbContext.SurveyResponses.Include(o => o.OrgSub).Include(sr => sr.ModuleResponses).Include(sr => sr.Survey.OrgSub).Where(x => x.Survey.SurveyID == surveyId && x.Status != "DELETED" && orgSubDictionary.Keys.Contains(x.OrgSub.OrgSubID)).OrderBy(sr => sr.ResponseDate).ToList();
                else
                    surveyResponses = dbContext.SurveyResponses.Include(o => o.OrgSub).Include(sr => sr.ModuleResponses).Include(sr => sr.Survey.OrgSub).Where(x => x.Survey.SurveyID == surveyId && x.Status != "DELETED" && orgSubDictionary.Keys.Contains(x.OrgSub.OrgSubID)).OrderByDescending(sr => sr.ResponseDate).ToList();

                if (string.IsNullOrWhiteSpace(filterKeyword))
                {
                    foreach (var eachResponse in surveyResponses)
                    {

                        eachResponse.DemographicData = findDemographicFieldsRequired(eachResponse.ModuleResponses.ToList()).ToString();
                        eachResponse.CusXPIndex = findCusXPIndex(eachResponse.ModuleResponses.ToList());
                    }
                }
                else
                {
                    List<string> filteredKeywords = filterKeyword.ToLower().Split(',').ToList();
                    findFilteredResponses(survey, ref surveyResponses, filteredKeywords);
                }

                survey.TotalResponses = surveyResponses.Count;
                survey.SurveyResponses = surveyResponses.Skip(skip).Take(pageSize).ToList();
                return survey;
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);

                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId && s.Status != "DELETED" && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).FirstOrDefault();

                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);

                OrgSub orgSub = oRep.GetAnalyticsOrgSub(orgSubId, ref orgSubDictionary, ref tpDictionary, userId);

                if (orderByDirection.ToUpper().Equals("ASCENDING"))
                    surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.Survey.OrgSub).Where(x => x.Survey.SurveyID == surveyId && x.Status != "DELETED" && orgSubDictionary.Keys.Contains(x.OrgSub.OrgSubID)).OrderBy(sr => sr.ResponseDate).ToList();
                else
                    surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.Survey.OrgSub).Where(x => x.Survey.SurveyID == surveyId && x.Status != "DELETED" && orgSubDictionary.Keys.Contains(x.OrgSub.OrgSubID)).OrderByDescending(sr => sr.ResponseDate).ToList();

                if (string.IsNullOrWhiteSpace(filterKeyword))
                {
                    foreach (var eachResponse in surveyResponses)
                    {

                        eachResponse.DemographicData = findDemographicFieldsRequired(eachResponse.ModuleResponses.ToList()).ToString();
                        eachResponse.CusXPIndex = findCusXPIndex(eachResponse.ModuleResponses.ToList());
                    }
                }
                else
                {
                    List<string> filteredKeywords = filterKeyword.ToLower().Split(',').ToList();
                    findFilteredResponses(survey, ref surveyResponses, filteredKeywords);
                }

                survey.TotalResponses = surveyResponses.Count;
                survey.SurveyResponses = surveyResponses.Skip(skip).Take(pageSize).ToList();
                return survey;
            }
            else if (uRep.isManager(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId && s.Status != "DELETED" && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).FirstOrDefault();

                if (survey == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_INVALIDSURVEY);

                OrgSub orgSub = oRep.GetAnalyticsOrgSub(orgSubId, ref orgSubDictionary, ref tpDictionary, userId);

                var UserTouchpointIds = User.Touchpoints.Select(t => t.Touchpoint.TouchpointID).ToList();

                if (orderByDirection.ToUpper().Equals("ASCENDING"))
                    surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.Survey.OrgSub).Where(x => x.Touchpoint != null && UserTouchpointIds.Contains(x.Touchpoint.TouchpointID) && x.Survey.SurveyID == surveyId && x.Status != "DELETED" && orgSubDictionary.Keys.Contains(x.OrgSub.OrgSubID)).OrderBy(sr => sr.ResponseDate).ToList();
                else
                    surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.Survey.OrgSub).Where(x => x.Touchpoint != null && UserTouchpointIds.Contains(x.Touchpoint.TouchpointID) && x.Survey.SurveyID == surveyId && x.Status != "DELETED" && orgSubDictionary.Keys.Contains(x.OrgSub.OrgSubID)).OrderByDescending(sr => sr.ResponseDate).ToList();

                if (string.IsNullOrWhiteSpace(filterKeyword))
                {
                    foreach (var eachResponse in surveyResponses)
                    {

                        eachResponse.DemographicData = findDemographicFieldsRequired(eachResponse.ModuleResponses.ToList()).ToString();
                        eachResponse.CusXPIndex = findCusXPIndex(eachResponse.ModuleResponses.ToList());
                    }
                }
                else
                {
                    List<string> filteredKeywords = filterKeyword.ToLower().Split(',').ToList();
                    findFilteredResponses(survey, ref surveyResponses, filteredKeywords);
                }

                survey.TotalResponses = surveyResponses.Count;
                survey.SurveyResponses = surveyResponses.Skip(skip).Take(pageSize).ToList();
                return survey;
            }
            throw new UnauthorizedAccessException();
        }

        public Survey GetResponsesByResponseIds(int surveyId, string userId, int skip, int pageSize, string orderByDirection, string filterKeyword, List<long> surveyResponseIds)
        {
            List<SurveyResponse> surveyResponses = new List<SurveyResponse>();
            Survey survey = survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId).FirstOrDefault<Survey>();

            if (orderByDirection.ToUpper().Equals("ASCENDING"))
                surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.Survey.OrgSub).Where(x => x.Survey.SurveyID == surveyId && x.Status != "DELETED" && surveyResponseIds.Contains(x.SurveyResponseID)).OrderBy(sr => sr.ResponseDate).ToList();
            else
                surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Include(sr => sr.Survey.OrgSub).Where(x => x.Survey.SurveyID == surveyId && x.Status != "DELETED" && surveyResponseIds.Contains(x.SurveyResponseID)).OrderByDescending(sr => sr.ResponseDate).ToList();

            if (string.IsNullOrWhiteSpace(filterKeyword))
            {
                foreach (var eachResponse in surveyResponses)
                {

                    eachResponse.DemographicData = findDemographicFieldsRequired(eachResponse.ModuleResponses.ToList()).ToString();
                    eachResponse.CusXPIndex = findCusXPIndex(eachResponse.ModuleResponses.ToList());
                }
            }
            else
            {
                List<string> filteredKeywords = filterKeyword.ToLower().Split(',').ToList();
                findFilteredResponses(survey, ref surveyResponses, filteredKeywords);
            }

            survey.TotalResponses = surveyResponses.Count;
            survey.SurveyResponses = surveyResponses.Skip(skip).Take(pageSize).ToList();

            return survey;
        }


        private static void findFilteredResponses(Survey survey, ref List<SurveyResponse> surveyResponses, List<string> filterKeywords)
        {
            long demographicModuleID = 0;

            foreach (Module module in survey.Modules)
            {
                if (module.ModuleType == QUESTION)
                {
                    JObject moduleData = JObject.Parse(module.ModuleData.ToString());
                    if (moduleData == null) continue;

                    string questionType = moduleData["QuestionType"].ToString();

                    if (questionType == QUESTIONTYPES.DEMOGRAPHIC)
                    {
                        demographicModuleID = module.ModuleID;
                    }
                }
            }

            if (demographicModuleID == 0) return;

            for (int i = 0; i < surveyResponses.Count; i++)
            {
                SurveyResponse eachResponse = surveyResponses[i];
                ModuleResponse demographicMR = eachResponse.ModuleResponses.Where(mr => mr.Module != null && mr.Module.ModuleID == demographicModuleID).FirstOrDefault();
                if (demographicMR != null)
                {
                    JObject moduleData = JObject.Parse(demographicMR.ModuleData);
                    if (DGAnalyticsStrategy.IsFilterKeywordFound(moduleData, filterKeywords))
                    {
                        eachResponse.DemographicData = findDemographicFieldsRequired(eachResponse.ModuleResponses.ToList()).ToString();
                    }
                    else
                    {
                        surveyResponses.RemoveAt(i);
                        i--;
                    }
                }
                eachResponse.CusXPIndex = findCusXPIndex(eachResponse.ModuleResponses.ToList());
            }
        }

        public Survey GetSurveyResponseByID(long surveyResponseID, string userId)
        {
            int surveyId = dbContext.SurveyResponses.Include(sr => sr.Survey).Where(sr => sr.SurveyResponseID == surveyResponseID).Select(sr => sr.Survey.SurveyID).FirstOrDefault();
            if (IsSurveyExists(surveyId, userId))
            {
                SurveyResponse surveyResponse = dbContext.SurveyResponses.Include(sr => sr.OrgSub).Include(sr => sr.Touchpoint).Include(sr => sr.ModuleResponses).Include(sr => sr.Survey.Modules).Include(sr => sr.Survey.OrgSub.Touchpoints).Where(sr => sr.SurveyResponseID == surveyResponseID).FirstOrDefault();

                Survey survey = surveyResponse.Survey;
                survey.OrgSub = oRep.GetOrgSub(survey.OrgSub.OrgSubID, userId);

                if (surveyResponse.OrgSub != null)
                    survey.SelectedSubsidiaries = new string[] { surveyResponse.OrgSub.OrgSubID.ToString() };

                if (surveyResponse.Touchpoint != null)
                    survey.SelectedTouchpoints = new string[] { surveyResponse.Touchpoint.TouchpointID.ToString() };

                for (int i = 0; i < survey.Modules.Count; i++ )
                {
                    Module m = survey.Modules.ElementAt(i);
                    if (m.ModuleType.Equals(QUESTION))
                    {
                        ModuleResponse moduleResponse = surveyResponse.ModuleResponses.Where(mr => mr.Module.ModuleID == m.ModuleID).FirstOrDefault();
                        if (moduleResponse != null)
                        {
                            m.ModuleData = moduleResponse.ModuleData;
                        }
                        else
                        {
                            survey.Modules.Remove(m);
                            i--;
                        }
                    }
                }

                //Sort based on the module position
                survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();
                JObject SummaryData = new JObject();
                SummaryData["SurveyResponseID"] = surveyResponse.SurveyResponseID;
                SummaryData["CusXPIndex"] = findCusXPIndex(surveyResponse.ModuleResponses.ToList());
                SummaryData["RegisteredName"] = survey.OrgSub.RegisteredName;
                SummaryData["BrandName"] = survey.OrgSub.BrandName;
                SummaryData["Country"] = survey.OrgSub.Country;
                Touchpoint tp = surveyResponse.Touchpoint;
                if(tp != null)
                SummaryData["Location"] = tp.TouchpointName;
                SummaryData["Date"] = !string.IsNullOrEmpty(surveyResponse.strResponseDate) ? surveyResponse.strResponseDate.Split('\n')[0] : "";
                SummaryData["Time"] = !string.IsNullOrEmpty(surveyResponse.strResponseDate) ? surveyResponse.strResponseDate.Split('\n')[1] : "";
                SummaryData["DGFields"] = findDemographicFields(survey.Modules.ToList());
                survey.SummaryData = SummaryData.ToString();
                return survey;
            }
            return null;
        }

        public void AddSurveyResponse(Survey survey)
        {
            Survey matchedSurvey = dbContext.Surveys.Find(survey.SurveyID);
            OrgSub selOrgSub = null;
            Touchpoint selTouchpoint = null;

            foreach (string strTouchpointID in survey.SelectedTouchpoints)
            {
                int touchpointID = Convert.ToInt32(strTouchpointID);
                selTouchpoint = dbContext.Touchpoints.Include(t=>t.OrgSub).Where(t => t.TouchpointID == touchpointID).FirstOrDefault();
                selOrgSub = selTouchpoint.OrgSub;
            }

            if (survey.SelectedTouchpoints.Length <= 0 && survey.SelectedSubsidiaries.Length > 0)
            {
                foreach (string strOrgSubId in survey.SelectedSubsidiaries)
                {
                    int orgSubID = Convert.ToInt32(strOrgSubId);
                    selOrgSub = dbContext.OrgSubs.Where(o => o.OrgSubID == orgSubID).FirstOrDefault();
                }
            }

            SurveyResponse newResponse = new SurveyResponse
            {
                ResponseDate = survey.PublishedDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone),
                Survey = matchedSurvey,
                OrgSub = selOrgSub,
                Touchpoint = selTouchpoint
            };

            newResponse.ModuleResponses = new List<ModuleResponse>();
            

            foreach (Module module in survey.Modules)
            {
                if (module.ModuleType == QUESTION)
                {
                    Module matchedModule = dbContext.Modules.Find(module.ModuleID);

                    ModuleResponse answerModule = new ModuleResponse
                    {
                        ModuleData = module.ModuleData,
                        ModuleType = module.ModuleType,
                        Page = module.Page,
                        Position = module.Position,
                        Module = matchedModule
                    };

                    newResponse.ModuleResponses.Add(answerModule);
                }

                if (module.ModuleType == APPRECIATION)
                {
                    if (survey.isSendAppreciationCode == true)
                    {
                        JObject moduleData = JObject.Parse(module.ModuleData);
                        sendAppreciationEmail(moduleData, survey);
                    }
                }
            }
            dbContext.SurveyResponses.Add(newResponse);
            dbContext.SaveChanges();
        }

        private JToken findDemographicFields(List<Module> surveyModules)
        {
            Dictionary<string, string[]> dgDictionary = new Dictionary<string, string[]>();
            foreach (Module module in surveyModules)
            {
                if (module.ModuleType == QUESTION)
                {
                    JObject moduleData = JObject.Parse(module.ModuleData.ToString());
                    if (moduleData == null) continue;

                    string questionType = moduleData["QuestionType"].ToString();

                    if (questionType == QUESTIONTYPES.DEMOGRAPHIC)
                    {
                        Dictionary<string, string[]> eachDGDictionary = DGAnalyticsStrategy.BuildDGDictionary(moduleData);
                        foreach (var eachKVPair in eachDGDictionary)
                        {
                            dgDictionary.Add(eachKVPair.Key, eachKVPair.Value);
                        }
                    }
                }
            }

            JArray values = new JArray();
            foreach (var dgField in dgDictionary)
            {
                JObject newField = new JObject();
                newField["Key"] = dgField.Key;
                if (dgField.Value.Length > 0)
                    newField["Value"] = string.Join(",", dgField.Value);
                values.Add(newField);
            }
            return values;
        }

        private string findFirstEmailField(List<Module> surveyModules)
        {
            foreach (Module module in surveyModules)
            {
                if (module.ModuleType == QUESTION)
                {
                    JObject moduleData = JObject.Parse(module.ModuleData.ToString());
                    if (moduleData == null) continue;

                    string questionType = moduleData["QuestionType"].ToString();

                    if (questionType == QUESTIONTYPES.DEMOGRAPHIC)
                    {
                        string email = DGAnalyticsStrategy.findFirstEmailField(moduleData);
                        if (email != null) return email;
                    }
                }
            }
            return null;
        }

        private void sendAppreciationEmail(JObject moduleData, Survey survey)
        {
            string termsandConditions = "";
            if (moduleData["TermsConditions"] != null)
                termsandConditions = moduleData["TermsConditions"].ToString();

            string email = findFirstEmailField(survey.Modules.ToList());
            if (!string.IsNullOrEmpty(email))
            {
                SMTPEmailService mailService = new SMTPEmailService();
                MailMessage newMessage = new MailMessage();
                newMessage.Subject = "Token of Appreciation";
                newMessage.From = new MailAddress(WebConfigurationManager.AppSettings["MailFrom"]);
                newMessage.Body = string.Format("Thanks for sharing your Experience. Please find out token of appreciation below <br/>" +
                                                "Your appreciation code : {0} <br/> " +
                                                "Terms and conditions : <br/> {1} ", survey.AppreciationCode, termsandConditions);
                newMessage.IsBodyHtml = true;
                newMessage.To.Add(email);
                Task.Run(() => SMTPEmailService.SendAsync(newMessage));
            }
        }

        internal static JToken findDemographicFieldsRequired(List<ModuleResponse> moduleResponses)
        {
            Dictionary<string, string[]> demographicFieldsRequired = new Dictionary<string, string[]>();

            demographicFieldsRequired.Add("Given Names", new string[] { "John" });
            demographicFieldsRequired.Add("SurName", new string[] { "Doe" });

            foreach (ModuleResponse module in moduleResponses)
            {
                JObject moduleData = JObject.Parse(module.ModuleData);
                if (moduleData == null) continue;

                string questionType = moduleData["QuestionType"].ToString();

                if (questionType == "Demographic")
                {
                    DGAnalyticsStrategy.searchDemographicsFieldsRequired(demographicFieldsRequired, moduleData);
                }
            }
            JArray values = new JArray();
            foreach (var dgField in demographicFieldsRequired)
            {
                JObject newField = new JObject();
                newField["Key"] = dgField.Key;
                if (dgField.Value.Length > 0)
                    newField["Value"] = string.Join(",", dgField.Value);
                values.Add(newField);
            }
            return values;
        }

        internal static double findCusXPIndex(List<ModuleResponse> moduleResponses)
        {
            double cusXPIndex = 0;

            foreach (ModuleResponse module in moduleResponses)
            {
                JObject moduleData = JObject.Parse(module.ModuleData);
                if (moduleData == null) continue;

                string questionType = moduleData["QuestionType"].ToString();

                if (questionType == QUESTIONTYPES.RATE)
                {
                    cusXPIndex = RatingAnalyticsStrategy.GetFirstRating(moduleData);
                    return cusXPIndex;
                }

                if (questionType == QUESTIONTYPES.MULTIPLE)
                {
                    cusXPIndex = MultipleAnalyticsStrategy.GetFirstRating(moduleData) * 10.0;
                    return cusXPIndex;
                }
            }
            return cusXPIndex;
        }

        public bool IsSurveyExists(int surveyId, string userId)
        {
            Survey survey = null;

            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Where(s => s.SurveyID == surveyId
                          && s.Status != "DELETED").FirstOrDefault();
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId) || uRep.isManager(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                if (User == null)
                    return false;
                survey = dbContext.Surveys.Where(s => s.SurveyID == surveyId && s.Status != "DELETED"
                    && s.CreatedBy.OrgSub.OrgSubID == User.OrgSub.OrgSubID).OrderBy(s => s.IsTemplate).FirstOrDefault();
            }

            if (survey != null)
                return true;
            return false;
        }

        public void DeleteSurveyResponse(int surveyResponseID, string userId)
        {
            if (uRep.isSuperAdministrator(userId))
            {
                SurveyResponse surveyResponse = dbContext.SurveyResponses.Include(s => s.Survey).Where(sr => sr.SurveyResponseID == surveyResponseID).FirstOrDefault();
                if (surveyResponse != null)
                {
                    surveyResponse.Status = "DELETED";
                    dbContext.Entry(surveyResponse).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }
                else throw new Exception("Survey Not Found");
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId))
            {
                long orgSubID = uRep.GetCurrentUser(userId).OrgSub.OrgSubID;
                SurveyResponse surveyResponse = dbContext.SurveyResponses.Include(s => s.Survey.OrgSub).Where(sr => sr.SurveyResponseID == surveyResponseID && sr.Survey.OrgSub.OrgSubID == orgSubID).FirstOrDefault();
                if (surveyResponse != null)
                {
                    surveyResponse.Status = "DELETED";
                    dbContext.Entry(surveyResponse).State = EntityState.Modified;
                    dbContext.SaveChanges();
                }
                else throw new Exception("Survey Not Found");
            }
            else throw new UnauthorizedAccessException();
        }

        public byte[] writeResponsetoExcel(int surveyId, string userId)
        {
            byte[] excelBytes = null;

            using (MemoryStream mem = new MemoryStream())
            {
                SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(mem, SpreadsheetDocumentType.Workbook);
                WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
                workbookpart.Workbook = new Workbook();

                WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
                worksheetPart.Worksheet = new Worksheet(new SheetData());

                Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

                Sheet sheet = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "SurveyData" };
                sheets.Append(sheet);


                if (IsSurveyExists(surveyId, userId))
                {
                    var survey = dbContext.Surveys.Where(s => s.SurveyID == surveyId && s.Status != "DELETED").Include(s => s.Modules).FirstOrDefault();

                    survey.Modules = survey.Modules.Where(m => m.ModuleType == QUESTION).ToList();

                    uint rowIndex = 1;
                    string columnName = "A";

                    List<SurveyResponse> surveyResponses = dbContext.SurveyResponses.Include(sr => sr.ModuleResponses).Where(sr => sr.Survey.SurveyID == surveyId && sr.Status != "DELETED").ToList();

                    var beginColumn = columnName;

                    //Write Survey Response ID, Response Date , Response Time, Touchpoint  

                    Cell cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
                    cell.CellValue = new CellValue("SurveyResponse ID");

                    cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                    cell.CellValue = new CellValue("Response Date");

                    cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                    cell.CellValue = new CellValue("Response Time");

                    cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                    cell.CellValue = new CellValue("Touchpoint");

                    cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                    cell.CellValue = new CellValue("CusXPIndex");

                    ExcelUtility.GetNextRow(ref rowIndex);

                    cell = ExcelUtility.InsertCellInWorksheet(columnName, rowIndex, worksheetPart);
                    cell.CellValue = new CellValue("");

                    cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                    cell.CellValue = new CellValue("");

                    cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                    cell.CellValue = new CellValue("");

                    cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                    cell.CellValue = new CellValue("");

                    cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                    cell.CellValue = new CellValue("");

                    foreach (SurveyResponse surveyResponse in surveyResponses)
                    {
                        columnName = beginColumn;

                        cell = ExcelUtility.InsertCellInWorksheet(columnName, ExcelUtility.GetNextRow(ref rowIndex), worksheetPart);
                        cell.CellValue = new CellValue(surveyResponse.SurveyResponseID.ToString());

                        cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                        cell.CellValue = new CellValue(!string.IsNullOrEmpty(surveyResponse.strResponseDate) ? surveyResponse.strResponseDate.Split('\n')[0] : "");

                        cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                        cell.CellValue = new CellValue(!string.IsNullOrEmpty(surveyResponse.strResponseDate) ? surveyResponse.strResponseDate.Split('\n')[1] : "");

                        cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                        cell.CellValue = new CellValue(surveyResponse.Touchpoint!=null?surveyResponse.Touchpoint.TouchpointName: "");

                        cell = ExcelUtility.InsertCellInWorksheet(ExcelUtility.GetNextColumn(ref columnName), rowIndex, worksheetPart);
                        string cusXPValue = findCusXPIndex(surveyResponse.ModuleResponses.ToList()).ToString("N1").Replace(",", ".");
                        cell.CellValue = new CellValue(cusXPValue);
                    }

                    rowIndex = 1;
                    columnName = ExcelUtility.GetNextColumn(ref columnName);

                    foreach (Module module in survey.Modules)
                    {
                        JObject moduleData = JObject.Parse(module.ModuleData.ToString());
                        if (moduleData == null) continue;

                        if (moduleData["QuestionType"] == null) continue;
                        string questionType = moduleData["QuestionType"].ToString();

                        switch (questionType)
                        {
                            /**************************  RATE *****************************/
                            case QUESTIONTYPES.RATE:
                                RatingAnalyticsStrategy.writetoExcel(ref columnName, ref rowIndex, worksheetPart, module.ModuleID, moduleData, surveyResponses);
                                break;

                            /**************************  STAFF *****************************/
                            case QUESTIONTYPES.STAFF:
                                StaffAnalyticsStrategy.writetoExcel(ref columnName, ref rowIndex, worksheetPart, module.ModuleID, moduleData, surveyResponses);
                                break;

                            /**************************  TAG *****************************/
                            case QUESTIONTYPES.TAG:
                                TagAnalyticsStrategy.writetoExcel(ref columnName, ref rowIndex, worksheetPart, module.ModuleID, moduleData, surveyResponses);
                                break;

                            /**************************  DEMOGRAPHIC *****************************/
                            case QUESTIONTYPES.DEMOGRAPHIC:
                                DGAnalyticsStrategy.writetoExcel(ref columnName, ref rowIndex, worksheetPart, module.ModuleID, moduleData, surveyResponses);
                                break;

                            /**************************  TEXT *****************************/
                            case QUESTIONTYPES.TEXT:
                                TextAnalyticsStrategy.writetoExcel(ref columnName, ref rowIndex, worksheetPart, module.ModuleID, moduleData, surveyResponses);
                                break;

                        }
                    }
                }

                workbookpart.Workbook.Save();
                spreadsheetDocument.Close();
                excelBytes = mem.ToArray();
            }
            return excelBytes;
        }

        internal void GetResponseCount(int surveyId, string userId, Alert alert, ref int totalResponses, ref int lastWeekResponses)
        {
            try { 
            Survey survey = null;
            if (uRep.isSuperAdministrator(userId))
            {
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId
                               && s.Status != "DELETED" && s.IsPublished == true).FirstOrDefault();

                var counts = dbContext.SurveyResponses.Where(x => x.Survey.SurveyID == survey.SurveyID && x.Status != "DELETED").GroupBy(x => x.Survey).Select(g => new { g.Key, Count = g.Count() }).SingleOrDefault();
                var lastWeekcount = dbContext.SurveyResponses.Where(x => x.Survey.SurveyID == survey.SurveyID && x.Status != "DELETED" && x.ResponseDate != null && x.ResponseDate >= DbFunctions.AddDays(DateTime.Now,-7)).GroupBy(x => x.Survey).Select(g => new { g.Key, Count = g.Count() }).SingleOrDefault();
                if (counts != null)
                    totalResponses = counts.Count;
                if (lastWeekcount != null)
                    lastWeekResponses = counts.Count;
                return;
            }
            else if (uRep.isAdministrator(userId) || uRep.isExecutive(userId) || uRep.isCusXPProfessional(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId && s.Status != "DELETED"
                    && s.IsPublished == true && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).FirstOrDefault();

                var counts = dbContext.SurveyResponses.Where(x => x.Survey.SurveyID == survey.SurveyID && x.Status != "DELETED").GroupBy(x => x.Survey).Select(g => new { g.Key, Count = g.Count() }).SingleOrDefault();
                var lastWeekcount = dbContext.SurveyResponses.Where(x => x.Survey.SurveyID == survey.SurveyID && x.Status != "DELETED" && x.ResponseDate != null && x.ResponseDate >= DbFunctions.AddDays(DateTime.Now, -7)).GroupBy(x => x.Survey).Select(g => new { g.Key, Count = g.Count() }).SingleOrDefault();
                if (counts != null)
                    totalResponses = counts.Count;
                if (lastWeekcount != null)
                    lastWeekResponses = counts.Count;
                return;
            }
            else if (uRep.isManager(userId))
            {
                CusExpUser User = uRep.GetCurrentUser(userId);
                survey = dbContext.Surveys.Include(s => s.Modules).Include(s => s.OrgSub.Touchpoints).Where(s => s.SurveyID == surveyId && s.Status != "DELETED"
                     && s.IsPublished == true && s.OrgSub.OrgSubID == User.OrgSub.OrgSubID).FirstOrDefault();

                var UserTouchpointNames = User.Touchpoints.Select(t => t.Touchpoint.TouchpointName).ToList();

                var counts = dbContext.SurveyResponses.Where(x => UserTouchpointNames.Contains(x.Touchpoint.TouchpointName) && x.Survey.SurveyID == survey.SurveyID && x.Status != "DELETED").GroupBy(x => x.Survey)
                .Select(g => new { g.Key, Count = g.Count() }).SingleOrDefault();
                var lastWeekcount = dbContext.SurveyResponses.Where(x => UserTouchpointNames.Contains(x.Touchpoint.TouchpointName) && x.Survey.SurveyID == survey.SurveyID && x.Status != "DELETED" && x.ResponseDate != null && x.ResponseDate >= DbFunctions.AddDays(DateTime.Now, -7)).GroupBy(x => x.Survey)
                .Select(g => new { g.Key, Count = g.Count() }).SingleOrDefault();
                if (counts != null)
                    totalResponses = counts.Count;
                if (lastWeekcount != null)
                    lastWeekResponses = counts.Count;
                return;
            }
            throw new UnauthorizedAccessException();
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
            }
        }
    }
}