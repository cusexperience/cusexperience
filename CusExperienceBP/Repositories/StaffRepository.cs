﻿using CusExperience.DAL;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.Repositories
{
    public class StaffRepository
    {
        public CusExpContext dbContext = new CusExpContext();
        UserRepository uRep = new UserRepository();
        public RoleManager<IdentityRole> RoleManager { get; set; }

        public StaffRepository()
        {
            RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dbContext));
        }
        

        public List<string> GetUserNames(long orgSubID, string touchpointName, string startsWith)
        {
            List<string> userNames = new List<string>();
            if (!string.IsNullOrEmpty(touchpointName)) {
                userNames =  ((DbSet<Entities.CusExpUser>)dbContext.Users).Include(u => u.Roles).Where(u => u.OrgSub.OrgSubID == orgSubID &&
                u.Touchpoints.Any(t => t.Touchpoint.TouchpointName == touchpointName) &&
                (u.FirstName.Contains(startsWith) || u.LastName.Contains(startsWith)) && u.Status != "DELETED").Select(u => u.FirstName + " " + u.LastName.ToUpper() + ", " + u.Designation).ToList();
            }
            else
            {
                userNames = ((DbSet<Entities.CusExpUser>)dbContext.Users).Include(u => u.Roles).Where(u => u.OrgSub.OrgSubID == orgSubID &&
                (u.FirstName.Contains(startsWith) || u.LastName.Contains(startsWith)) && u.Status != "DELETED").Select(u => u.FirstName + " " + u.LastName.ToUpper() + ", " + u.Designation).ToList();
            }
            return userNames;
        }
    }
}
