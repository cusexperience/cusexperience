﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.ServiceContract;
using CusExperience.MessageContract;
using System.IO;
using System.Configuration;
using CusExperience.Exceptions;
using CusExperience.Utilities;

namespace CusExperience.Repositories
{
    public class FileRepository : IFileRepository
    {
        public void UploadFile(FileUploadMessage request)
        {
            string filePath = ConfigurationManager.AppSettings["FileStoragePath"] + request.Metadata.RemoteFilePath;
            string directory =  Path.GetDirectoryName(filePath);
            bool exists = Directory.Exists(directory);
            if (!exists) Directory.CreateDirectory(directory);
            int length = (int) request.FileByteStream.Length;
            File.WriteAllBytes(filePath, request.FileByteStream);
        }

        public FileDownloadReturnMessage DownloadFile(FileDownloadMessage request)
        {
            try { 
            string filePath = ConfigurationManager.AppSettings["FileStoragePath"] + request.FileMetaData.RemoteFilePath;
            if (!File.Exists(filePath)) throw new Exception("File Not Found");
            FileDownloadReturnMessage file = new FileDownloadReturnMessage();
            file.FileByteStream = File.ReadAllBytes(filePath);
            return file;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
