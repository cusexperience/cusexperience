﻿using CusExperience.DAL;
using CusExperience.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using CusExperience.Repositories;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.Utilities;
using System.Web;
using System.Security.Principal;
using CusExperience.Exceptions;
using System.Transactions;
using System.Net.Mail;
using System.Configuration;
using System.Threading.Tasks;


namespace CusExperience.Repositories
{
    public class UserRepository
    {
        CusExpContext dbContext = new CusExpContext();
        public CusExpUserManager UserManager { get; private set; }
        public RoleManager<IdentityRole> RoleManager { get; set; }

        public UserRepository()
        {
            UserManager = new CusExpUserManager(new UserStore<CusExpUser>(dbContext));
            RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(dbContext));
        }

        public String findCurrentRole(string userId)
        {
            if (UserManager.IsInRole(userId, ROLES.SUPERADMINISTRATOR))
                return ROLES.SUPERADMINISTRATOR;
            else if (UserManager.IsInRole(userId, ROLES.ADMINISTRATOR))
                return ROLES.ADMINISTRATOR;
            else if (UserManager.IsInRole(userId, ROLES.CUSXPPROFESSIONAL))
                return ROLES.CUSXPPROFESSIONAL;
            else if (UserManager.IsInRole(userId, ROLES.MANAGER))
                return ROLES.MANAGER;
            else if (UserManager.IsInRole(userId, ROLES.EXECUTIVE))
                return ROLES.EXECUTIVE;
            else if (UserManager.IsInRole(userId, ROLES.FRONTLINER))
                return ROLES.FRONTLINER;
            else
                return null;
        }

        internal List<String> findAllAvailableRoles(string userId)
        {
            List<String> availableRoles = new List<String>();
            if (UserManager.IsInRole(userId, ROLES.SUPERADMINISTRATOR))
            {
                availableRoles.Add(ROLES.SUPERADMINISTRATOR);
                availableRoles.Add(ROLES.ADMINISTRATOR);
                availableRoles.Add(ROLES.CUSXPPROFESSIONAL);
                availableRoles.Add(ROLES.EXECUTIVE);
                availableRoles.Add(ROLES.MANAGER);
                availableRoles.Add(ROLES.FRONTLINER);
            }
            else if (UserManager.IsInRole(userId, ROLES.ADMINISTRATOR))
            {
                availableRoles.Add(ROLES.CUSXPPROFESSIONAL);
                availableRoles.Add(ROLES.EXECUTIVE);
                availableRoles.Add(ROLES.MANAGER);
                availableRoles.Add(ROLES.FRONTLINER);
            }
            else if (UserManager.IsInRole(userId, ROLES.MANAGER))
            {
                availableRoles.Add(ROLES.MANAGER);
                availableRoles.Add(ROLES.FRONTLINER);
            }
            return availableRoles;
        }

        public CusExpUser GetCurrentUser(string userId)
        {
            CusExpUser User = dbContext.Users.Include(x => x.OrgSub).Include(x => x.Touchpoints.Select(t => t.Touchpoint)).Where(u => u.Id == userId).FirstOrDefault();
            if (User == null)
                return null;

            //Hide Security Credentials
            User.SecurityStamp = null;
            User.PasswordHash = null;

            return User;
        }

        public void SaveUserProfile(string userProfile, string userId)
        {
            JObject jsonUserProfile = JObject.Parse(userProfile);

            CusExpUser User = GetCurrentUser(userId);
            if (User == null)
                throw new Exception("User Not Found");

            if (jsonUserProfile != null)
            {
                if (jsonUserProfile["UserName"] != null && jsonUserProfile["Email"] != null && jsonUserProfile["PhoneNumber"] != null && jsonUserProfile["Country"] != null && jsonUserProfile["Title"] != null)
                {
                    if (!jsonUserProfile["UserName"].ToString().Equals(User.UserName))
                        throw new Exception("User is not same as logged in User");

                    string Email = jsonUserProfile["Email"].ToString();
                    string PhoneNo = jsonUserProfile["PhoneNumber"].ToString();
                    string Country = jsonUserProfile["Country"].ToString();
                    string Title = jsonUserProfile["Title"].ToString();
                    //string OthersTitle = jsonUserProfile["OthersTitle"].ToString();
                    User.Country = Country;
                    User.Email = Email;
                    User.PhoneNumber = PhoneNo;
                    User.Title = Title;
                    dbContext.Users.Attach(User);
                    dbContext.Entry(User).State = EntityState.Modified;
                    dbContext.SaveChanges();
                    return;
                }

                throw new Exception("Invalid User Profile Information");
            }
            throw new Exception("Invalid User Profile Information");
        }

        public bool isSuperAdministrator(string userId)
        {
            if (UserManager.IsInRole(userId, ROLES.SUPERADMINISTRATOR)) return true;
            return false;
        }

        public bool isAdministrator(string userId)
        {
            if (UserManager.IsInRole(userId, ROLES.ADMINISTRATOR)) return true;
            return false;
        }

        public bool isCusXPProfessional(string userId)
        {
            if (UserManager.IsInRole(userId, ROLES.CUSXPPROFESSIONAL)) return true;
            return false;
        }


        public bool isExecutive(string userId)
        {
            if (UserManager.IsInRole(userId, ROLES.EXECUTIVE)) return true;
            return false;
        }

        public bool isManager(string userId)
        {
            if (UserManager.IsInRole(userId, ROLES.MANAGER)) return true;
            return false;
        }

        public bool isFrontLiner(string userId)
        {
            if (UserManager.IsInRole(userId, ROLES.FRONTLINER)) return true;
            return false;
        }

        public List<CusExpUser> GetUsersByOrgSub(long orgSubID, string userId)
        {
            List<CusExpUser> orgUsers = new List<CusExpUser>();

            if (isSuperAdministrator(userId))
            {
                var Users = dbContext.Users.Include(x => x.OrgSub).Where(u => u.OrgSub.OrgSubID == orgSubID && u.Status != "DELETED").Select(u => new { u.Id, u.UserName }).ToList();
                foreach (var u in Users)
                {
                    CusExpUser user = new CusExpUser();
                    user.Id = u.Id;
                    user.UserName = u.UserName;
                    user.Role = findCurrentRole(u.Id);
                    user.SelectedTouchpoints = dbContext.CusExpUserTouchpoints.Where(cx => cx.User.Id == u.Id).Select(t => t.Touchpoint.TouchpointName).ToList();
                    orgUsers.Add(user);
                }
                return orgUsers;
            }
            else if (isAdministrator(userId))
            {
                if (orgSubID == GetCurrentUser(userId).OrgSub.OrgSubID)
                {
                    var Users = dbContext.Users.Include(x => x.OrgSub).Where(u => u.OrgSub.OrgSubID == orgSubID && u.Id != userId && u.Status != "DELETED").Select(u => new { u.Id, u.UserName }).ToList();
                    foreach (var u in Users)
                    {
                        CusExpUser user = new CusExpUser();
                        user.Id = u.Id;
                        user.UserName = u.UserName;
                        user.Role = findCurrentRole(u.Id);
                        user.SelectedTouchpoints = dbContext.CusExpUserTouchpoints.Where(cx => cx.User.Id == u.Id).Select(t => t.Touchpoint.TouchpointName).ToList();
                        orgUsers.Add(user);
                    }
                    return orgUsers;
                }
                throw new UnauthorizedAccessException();
            }
            throw new UnauthorizedAccessException();
        }

        public CusExpUser GetUser(string id, string userId)
        {
            if (isSuperAdministrator(userId))
            {
                CusExpUser User = dbContext.Users.Include(x => x.OrgSub).Include(x => x.OrgSub.Touchpoints).Where(u => u.Id == id && u.Status != "DELETED").FirstOrDefault();
                if (User == null) return null;
                else
                {
                    CusExpUser userProfile = new CusExpUser();
                    userProfile.Id = User.Id;
                    userProfile.UserName = User.UserName;
                    userProfile.Title = User.Title;
                    userProfile.FirstName = User.FirstName;
                    userProfile.LastName = User.LastName;
                    userProfile.Email = User.Email;
                    userProfile.PhoneNumber = User.PhoneNumber;
                    userProfile.Designation = User.Designation;
                    userProfile.Country = User.Country;
                    userProfile.OrgSub = new OrgSub();
                    userProfile.OrgSub.OrgSubID = User.OrgSub.OrgSubID;
                    userProfile.OrgSub.RegisteredName = User.OrgSub.RegisteredName;
                    userProfile.Role = findCurrentRole(User.Id);
                    userProfile.AvailableRoles = findAllAvailableRoles(userId);
                    userProfile.AvailableTouchpoints = User.OrgSub.Touchpoints.Select(t => t.TouchpointName).ToList();
                    userProfile.SelectedTouchpoints = dbContext.CusExpUserTouchpoints.Where(cx => cx.User.Id == User.Id).Select(t => t.Touchpoint.TouchpointName).ToList();
                    return userProfile;
                }
            }
            else if (isAdministrator(userId))
            {
                CusExpUser User = dbContext.Users.Include(x => x.OrgSub.Touchpoints).Where(u => u.Id == id && u.Status != "DELETED").FirstOrDefault();
                if (User == null) return null;
                if (User.OrgSub.OrgSubID == GetCurrentUser(userId).OrgSub.OrgSubID)
                {
                    CusExpUser userProfile = new CusExpUser();
                    userProfile.Id = User.Id;
                    userProfile.UserName = User.UserName;
                    userProfile.Title = User.Title;
                    userProfile.FirstName = User.FirstName;
                    userProfile.LastName = User.LastName;
                    userProfile.Email = User.Email;
                    userProfile.PhoneNumber = User.PhoneNumber;
                    userProfile.Country = User.Country;
                    userProfile.Designation = User.Designation;
                    userProfile.OrgSub = new OrgSub();
                    userProfile.OrgSub.OrgSubID = User.OrgSub.OrgSubID;
                    userProfile.OrgSub.RegisteredName = User.OrgSub.RegisteredName;
                    userProfile.Role = findCurrentRole(User.Id);
                    userProfile.AvailableRoles = findAllAvailableRoles(userId);
                    userProfile.AvailableTouchpoints = User.OrgSub.Touchpoints.Select(t => t.TouchpointName).ToList();
                    userProfile.SelectedTouchpoints = dbContext.CusExpUserTouchpoints.Where(cx => cx.User.Id == User.Id).Select(t => t.Touchpoint.TouchpointName).ToList();
                    return userProfile;
                }
            }
            throw new UnauthorizedAccessException();
        }

        public void AddUser(CusExpUser User, string userId)
        {
            if (isSuperAdministrator(userId))
            {
                InsertUser(User, userId);
                return;
            }
            else if (isAdministrator(userId))
            {
                if (GetCurrentUser(userId).OrgSub.OrgSubID == User.OrgSubID)
                {
                    InsertUser(User, userId);
                    return;
                }
            }
            throw new UnauthorizedAccessException();
        }

        private void InsertUser(CusExpUser User, string userId)
        {
            var randomPassword = System.Web.Security.Membership.GeneratePassword(8, 1);
            CusExpUser CusXPUser = dbContext.Users.Where(u => u.UserName == User.UserName && u.Status != "DELETED").FirstOrDefault();
            if (CusXPUser != null)
                throw new CusExpException(EXCEPTIONMSG.EX_USERALREADYEXISTS);
            else
            {
                var newUser = new CusExpUser
                {
                    FirstName = User.FirstName,
                    LastName = User.LastName,
                    Title = User.Title,
                    UserName = User.UserName,
                    Email = User.Email,
                    PhoneNumber = User.PhoneNumber,
                    DateCreated = DateTime.Now,
                    DateModified = DateTime.Now,
                    Designation = User.Designation,
                    OrgSub = dbContext.OrgSubs.Find(User.OrgSubID)
                };
                UserManager.Create(newUser, randomPassword);

                if (User.SelectedTouchpoints != null)
                {
                    foreach (String touchpointName in User.SelectedTouchpoints)
                    {
                        if (touchpointName != "All")
                        {
                            CusExpUserTouchpoint existingRel = dbContext.CusExpUserTouchpoints.Where(cx => cx.User.Id == newUser.Id && cx.Touchpoint.TouchpointName == touchpointName).FirstOrDefault();
                            if (existingRel == null)
                            {
                                Touchpoint selTouchpoint = dbContext.Touchpoints.Include(t => t.OrgSub).Where(t => t.TouchpointName == touchpointName && t.OrgSub.OrgSubID == newUser.OrgSub.OrgSubID).FirstOrDefault();
                                if (selTouchpoint != null)
                                {
                                    CusExpUserTouchpoint cxTouchpoint = new CusExpUserTouchpoint();
                                    cxTouchpoint.User = newUser;
                                    cxTouchpoint.Touchpoint = selTouchpoint;
                                    dbContext.Entry(cxTouchpoint).State = EntityState.Added;
                                }
                            }
                        }
                    }
                }
                IdentityRole role = RoleManager.FindByName(User.Role);
                IdentityUserRole newRole = new IdentityUserRole();
                newRole.UserId = newUser.Id;
                newRole.RoleId = role.Id;
                newUser.Roles.Add(newRole);
                dbContext.Entry(newRole).State = EntityState.Added;
                dbContext.SaveChanges();
                AddToPreviousPasswords(newUser, randomPassword);
            }
                string emailMessage = String.Format("User is registered successfully with the system. Please allow the user to login with the following password " + randomPassword);
                UserManager.SendEmail(userId, "CusExperience.com Password Assistance", emailMessage);
            }

        public void AddToPreviousPasswords(CusExpUser user, string password)
        {
            dbContext.PreviousPasswords.Add(new PreviousPassword() { UserId = user.Id, PasswordHash = UserManager.PasswordHasher.HashPassword(password) });
            dbContext.SaveChanges();
        }

        public void UpdateUser(CusExpUser User, string userId)
        {
            if (isSuperAdministrator(userId))
            {
                ModifyUser(User);
                return;
            }
            else if (isAdministrator(userId))
            {
                if (GetCurrentUser(userId).OrgSub.OrgSubID == User.OrgSubID)
                {
                    ModifyUser(User);
                    return;
                }
            }
            throw new UnauthorizedAccessException();
        }

        private void ModifyUser(CusExpUser User)
        {
            using (CusExpContext context = new CusExpContext())
            {
                CusExpUser existingUser = context.Users.Include(u => u.OrgSub).Include(u => u.Roles).Where(u => u.Id == User.Id && u.Status != "DELETED").FirstOrDefault();
                if (existingUser == null)
                    throw new CusExpException(EXCEPTIONMSG.EX_USERIDINVALID);
                context.Users.Attach(existingUser);
                context.Entry(existingUser).State = EntityState.Modified;
                existingUser.FirstName = User.FirstName;
                existingUser.LastName = User.LastName;
                existingUser.Title = User.Title;
                existingUser.UserName = User.UserName;
                existingUser.Email = User.Email;
                existingUser.PhoneNumber = User.PhoneNumber;
                existingUser.DateCreated = DateTime.Now;
                existingUser.DateModified = DateTime.Now;
                existingUser.Designation = User.Designation;
                List<CusExpUserTouchpoint> exCXTouchpoints = context.CusExpUserTouchpoints.Where(cx => cx.User.Id == existingUser.Id && !User.SelectedTouchpoints.Contains(cx.Touchpoint.TouchpointName)).ToList();
                context.CusExpUserTouchpoints.RemoveRange(exCXTouchpoints);

                foreach (String touchpointName in User.SelectedTouchpoints)
                {
                    if (touchpointName != "All")
                    {
                        CusExpUserTouchpoint existingRel = context.CusExpUserTouchpoints.Where(cx => cx.User.Id == existingUser.Id && cx.Touchpoint.TouchpointName == touchpointName).FirstOrDefault();
                        if (existingRel == null)
                        {
                            Touchpoint selTouchpoint = context.Touchpoints.Include(t => t.OrgSub).Where(t => t.TouchpointName == touchpointName && t.OrgSub.OrgSubID == existingUser.OrgSub.OrgSubID).FirstOrDefault();
                            if (selTouchpoint != null)
                            {
                                CusExpUserTouchpoint cxTouchpoint = new CusExpUserTouchpoint();
                                cxTouchpoint.User = existingUser;
                                cxTouchpoint.Touchpoint = selTouchpoint;
                                context.Entry(cxTouchpoint).State = EntityState.Added;
                            }
                        }
                    }
                }
                string CurrentRole = findCurrentRole(User.Id);
                if (CurrentRole != User.Role)
                {
                    if (!string.IsNullOrEmpty(CurrentRole))
                    {
                        IdentityRole role = RoleManager.FindByName(CurrentRole);
                        IdentityUserRole ur = existingUser.Roles.Where(r => r.RoleId == role.Id).FirstOrDefault();
                        if (ur != null)
                        {
                            existingUser.Roles.Remove(ur);
                            context.Entry(ur).State = EntityState.Deleted;
                        }
                    }
                    if (!string.IsNullOrEmpty(User.Role))
                    {
                        IdentityRole role = RoleManager.FindByName(User.Role);
                        IdentityUserRole newRole = new IdentityUserRole();
                        newRole.UserId = existingUser.Id;
                        newRole.RoleId = role.Id;
                        existingUser.Roles.Add(newRole);
                        context.Entry(newRole).State = EntityState.Added;
                    }
                }
                context.SaveChanges();
            }
        }

        public CusExpUser GetNewUser(long orgSubID, string userId)
        {
            if (isSuperAdministrator(userId))
            {
                OrgSub OrgSub = dbContext.OrgSubs.Include(o => o.Touchpoints).Where(o => o.OrgSubID == orgSubID && o.Status != "DELETED").FirstOrDefault();
                CusExpUser userProfile = new CusExpUser();
                userProfile.OrgSub = OrgSub;
                userProfile.AvailableRoles = findAllAvailableRoles(userId);
                if (OrgSub.Touchpoints != null)
                    userProfile.AvailableTouchpoints = OrgSub.Touchpoints.Select(t => t.TouchpointName).ToList();
                return userProfile;
            }
            else if (isAdministrator(userId))
            {
                if (GetCurrentUser(userId).OrgSub.OrgSubID == orgSubID)
                {
                    OrgSub OrgSub = dbContext.OrgSubs.Include(o => o.Touchpoints).Where(o => o.OrgSubID == orgSubID && o.Status != "DELETED").FirstOrDefault();
                    CusExpUser userProfile = new CusExpUser();
                    userProfile.OrgSub = OrgSub;
                    userProfile.AvailableRoles = findAllAvailableRoles(userId);
                    if (OrgSub.Touchpoints != null)
                        userProfile.AvailableTouchpoints = OrgSub.Touchpoints.Select(t => t.TouchpointName).ToList();
                    return userProfile;
                }
            }
            throw new UnauthorizedAccessException();
        }

        public void DeleteUser(string id, string userId)
        {
            var existingUser = dbContext.Users.Include(u => u.OrgSub).Where(u => u.Id == id).FirstOrDefault();
            if (existingUser == null)
                throw new CusExpException(EXCEPTIONMSG.EX_USERIDINVALID);
            if (isSuperAdministrator(userId))
            {
                existingUser.UserName = existingUser.UserName + RandomStringGenerator.RandomString(8) + "_DELETED";
                existingUser.Status = "DELETED";
                dbContext.Entry(existingUser).State = EntityState.Modified;
                dbContext.SaveChanges();
                return;
            }
            else if (isAdministrator(userId))
            {
                if (GetCurrentUser(userId).OrgSub == existingUser.OrgSub)
                {
                    existingUser.UserName = existingUser.UserName + RandomStringGenerator.RandomString(8) + "_DELETED";
                    existingUser.Status = "DELETED";
                    dbContext.Entry(existingUser).State = EntityState.Modified;
                    dbContext.SaveChanges();
                    return;
                }
            }
            throw new UnauthorizedAccessException();
        }

        internal bool isAnonymousUser(string userId)
        {
           return userId.Equals(ROLES.ANONYMOUS);
        }

        internal List<CusExpUser> GetAllUsersByRole(long orgSubID, string role, string userId)
        {
            List<CusExpUser> orgUsers = new List<CusExpUser>();

            if (isSuperAdministrator(userId))
            {
                var Users = dbContext.Users.Include(x => x.OrgSub).Where(u => u.OrgSub.OrgSubID == orgSubID && u.Status != "DELETED").ToList();
                foreach (var u in Users)
                {
                    u.Role = findCurrentRole(u.Id);
                    if (u.Role.Equals(role)) { 
                        u.SelectedTouchpoints = dbContext.CusExpUserTouchpoints.Where(cx => cx.User.Id == u.Id).Select(t => t.Touchpoint.TouchpointName).ToList();
                        orgUsers.Add(u);
                    }
                }
                return orgUsers;
            }
            else if (isAdministrator(userId))
            {
                if (orgSubID == GetCurrentUser(userId).OrgSub.OrgSubID)
                {
                    var Users = dbContext.Users.Include(x => x.OrgSub).Where(u => u.OrgSub.OrgSubID == orgSubID && u.Id != userId && u.Status != "DELETED").ToList();
                    foreach (var u in Users)
                    {
                        u.Role = findCurrentRole(u.Id);
                        if (u.Role.Equals(role))
                        {
                            u.SelectedTouchpoints = dbContext.CusExpUserTouchpoints.Where(cx => cx.User.Id == u.Id).Select(t => t.Touchpoint.TouchpointName).ToList();
                            orgUsers.Add(u);
                        }
                    }
                    return orgUsers;
                }
                throw new UnauthorizedAccessException();
            }
            throw new UnauthorizedAccessException();
        }
    }
}
