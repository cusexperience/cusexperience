﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.UI.WebControls;
using System.Configuration;

namespace CusExperience.DAL
{
    public class CusExpContext : IdentityDbContext<CusExpUser>
    {
        public CusExpContext() : base("CusExpContext")
        {
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.ValidateOnSaveEnabled = true;
            Database.SetInitializer<CusExpContext>(new CusExpInitializer());
        }

        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<ModuleResponse> ModuleResponses { get; set; }
        public DbSet<SurveyResponse> SurveyResponses { get; set; }
        public DbSet<OrgSub> OrgSubs { get; set; }
        public DbSet<Touchpoint> Touchpoints { get; set; }
        public DbSet<CusExpUserTouchpoint> CusExpUserTouchpoints { get; set; }
        public DbSet<Support> Supports { get; set; }
        public DbSet<PreviousPassword> PreviousPasswords { get; set; }
        public DbSet<ExpertComment> ExpertComments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           base.OnModelCreating(modelBuilder);
 	       modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

           modelBuilder.Entity<OrgSub>()
               .HasMany(s => s.Touchpoints)
               .WithRequired(s => s.OrgSub)
               .WillCascadeOnDelete();
        }
    }
}
