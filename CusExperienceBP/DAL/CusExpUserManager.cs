﻿using CusExperience.DAL;
using CusExperience.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using System.Text.RegularExpressions;

namespace CusExperience.DAL
{
    public class CusExpUserManager : UserManager<CusExpUser>
    {
        public CusExpUserManager(IUserStore<CusExpUser> store)
            : base(store)
        {
            var provider = new DpapiDataProtectionProvider("CusExperience");
            this.UserTokenProvider = new DataProtectorTokenProvider<CusExpUser>(provider.Create("EmailConfirmation"));
            this.EmailService = new EmailService();
            this.PasswordValidator = new CustomPasswordValidator(8);
        }

        public void AddUserToRole(
            string userId, string role)
        {
            var userRoleStore = (IUserRoleStore<CusExpUser, string>)Store;
            var user = FindByIdAsync(userId).Result;

            IList<String> userRoles = userRoleStore
               .GetRolesAsync(user).Result;

            if (!userRoles.Contains(role))
            {
                userRoleStore.AddToRoleAsync(user, role).ConfigureAwait(true);
                IdentityResult r = UpdateAsync(user).Result;
                if (r.Succeeded)
                    return;
                else
                    throw new Exception("Not Succeded");
            }
        }


        public virtual async Task<IdentityResult> AddUserToRolesAsync(
            string userId, IList<string> roles)
        {
            var userRoleStore = (IUserRoleStore<CusExpUser, string>)Store;
            var user = await FindByIdAsync(userId).ConfigureAwait(false);

            if (user == null)
            {
                throw new InvalidOperationException("Invalid user Id");
            }

            var userRoles = await userRoleStore
                .GetRolesAsync(user)
                .ConfigureAwait(false);

            // Add user to each role using UserRoleStore
            foreach (var role in roles.Where(role => !userRoles.Contains(role)))
            {
                await userRoleStore.AddToRoleAsync(user, role).ConfigureAwait(false);
            }

            // Call update once when all roles are added
            return await UpdateAsync(user).ConfigureAwait(false);
        }


        public virtual async Task<IdentityResult> RemoveUserFromRolesAsync(
            string userId, IList<string> roles)
        {
            var userRoleStore = (IUserRoleStore<CusExpUser, string>)Store;
            var user = await FindByIdAsync(userId).ConfigureAwait(false);

            if (user == null)
            {
                throw new InvalidOperationException("Invalid user Id");
            }

            var userRoles = await userRoleStore
                .GetRolesAsync(user)
                .ConfigureAwait(false);

            // Remove user to each role using UserRoleStore
            foreach (var role in roles.Where(userRoles.Contains))
            {
                await userRoleStore
                    .RemoveFromRoleAsync(user, role)
                    .ConfigureAwait(false);
            }

            // Call update once when all roles are removed
            return await UpdateAsync(user).ConfigureAwait(false);
        }
    }


    public class CustomPasswordValidator : IIdentityValidator<string>
    {

        public int RequiredLength { get; set; }

        public CustomPasswordValidator(int length)
        {
            RequiredLength = length;
        }
        public Task<IdentityResult> ValidateAsync(string item)
        {
            if (String.IsNullOrEmpty(item) || item.Length < RequiredLength)
            {
                return Task.FromResult(IdentityResult.Failed(String.Format("Password should be atleast 8 characters long, contain one alphetic character and one numeric character", RequiredLength)));

            }
            string pattern = @"^(?=.*[a-z])(?=.*[0-9]).*$";

            if (!Regex.IsMatch(item, pattern))
            {
                return Task.FromResult(IdentityResult.Failed("Password should be atleast 8 characters long, contain one alphetic character and one numeric character"));
            }
            return Task.FromResult(IdentityResult.Success);
        }
    }
}
