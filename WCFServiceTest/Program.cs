﻿using CusExperience.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCFServiceTest
{
    class Program
    {
        internal static ServiceHost myServiceHost = null;

        static void Main(string[] args)
        {

            myServiceHost = new ServiceHost(typeof(CusExperienceService));
            myServiceHost.Open();
            Console.WriteLine("Hosted Successfully");
            Console.Read();
        }
    }
}
