﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CusExperience.Entities
{
    [Table("AspNetUsers")]
    public class UserProfile : IdentityUser
    {

        [Column]
        [StringLength(100, MinimumLength = 1)]
        [Required]
        public string FirstName { get; set; }

        [Column]
        [StringLength(100, MinimumLength = 1)]
        [Required]
        public string LastName { get; set; }

        [Column]
        [Required]
        public string Title { get; set; }

        [Column]
        [System.ComponentModel.DefaultValue("NEW")]
        public string Status { get; set; }

        [Column]
        public DateTime DateCreated { get; set; }

        [Column]
        public DateTime? DateModified { get; set; }

        [NotMapped]
        public String FullName { get { return FirstName + " " + LastName; } }

        public virtual OrgSub OrgSub { get; set; }

    }
}