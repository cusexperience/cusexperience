﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CusExperience.Entities
{
    [Serializable]
    public class Touchpoint
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TouchpointID { get; set; }

        [DataMember]
        public string TouchpointName { get; set; }

    }
}