﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CusExperience.Entities
{
    [Table("SurveyResponse")]
    [DataContract(Name="Survey")]
    [Serializable]
    public class SurveyResponse
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SurveyResponseID { get; set; }

        [DataMember]
        public DateTime? ResponseDate { get; set; }

        [DataMember]
        [NotMapped]
        public string SurveyTitle { get; set; }

        [NotMapped]
        public string SurveyData { get; set; }

        [DataMember(Name = "SurveyData")]
        [NotMapped]
        public JToken SurveyJSONData { get; set; }

        [NotMapped]
        public string OrgSubData { get; set; }

        [DataMember(Name = "OrgSubData")]
        [NotMapped]
        public JToken OrgSubJSONData { get; set; }

        [DataMember]
        [NotMapped]
        public DateTime? CreatedDate { get; set; }

        [DataMember]
        [NotMapped]
        public string CreatedBy { get; set; }

        [DataMember]
        [NotMapped]
        public string Status { get; set; }

        [DataMember]
        [NotMapped]
        public bool IsTemplate { get; set; }

        [DataMember]
        [NotMapped]
        public bool IsPredefined { get; set; }

        [DataMember]
        [NotMapped]
        public string TemplateName { get; set; } 

        [DataMember]
        public virtual ICollection<ModuleResponse> ModuleResponses { get; set; }

        [Required]
        public virtual Survey Survey { get; set; }

    }
}