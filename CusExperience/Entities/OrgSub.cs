﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CusExperience.Entities
{
    public class OrgSub
    {

        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long OrgSubID { get; set; }

        [Required]
        public string RegisteredName { get; set; }

        [Required]
        public string RegisteredNumber { get; set; }

        [Required]
        public string BrandName { get; set; }

        [Required]
        public string BrandWebsite { get; set; }

        [Required]
        public string Industry { get; set; }

        public string Address { get; set; }

        public string GoogleMapsAddress { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string City { get; set; }

        public string FrontImage { get; set; }

        public string LogoImage { get; set; }

        public string PhoneNumber { get; set; }

        public string EmailAddress { get; set; }

        public virtual OrgSub parentOrgSub { get; set; }

        public virtual List<Touchpoint> TouchPoints { get; set; } 

    }
}