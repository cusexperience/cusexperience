﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.Entities
{
    [Table("Survey")]
    [DataContract(IsReference=true)]
    [Serializable]
    public class Survey
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SurveyID { get; set; }

        [DataMember]
        public string SurveyTitle { get; set; }

        public string SurveyData { get; set; }

        [DataMember(Name = "SurveyData")]
        [NotMapped]
        public JToken SurveyJSONData { get; set; }

        public string OrgSubData { get; set; }

        [DataMember(Name = "OrgSubData")]
        [NotMapped]
        public JToken OrgSubJSONData { get; set; }

        [DataMember]
        public DateTime? CreatedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public bool IsTemplate { get; set; }

        [DataMember]
        public bool IsPredefined { get; set; }

        [DataMember]
        public string TemplateName { get; set; }

        [DataMember]
        public bool IsPublished { get; set; }

        [DataMember]
        public DateTime? PublishedDate { get; set; }

        [NotMapped]
        [DataMember]
        public int TotalResponses { get; set; }

        [NotMapped]
        [DataMember]
        public string FromDateTime { get; set; }

        [NotMapped]
        [DataMember]
        public string ToDateTime { get; set; }

        [DataMember]
        public virtual ICollection<Module> Modules { get; set; }    
    }
}
