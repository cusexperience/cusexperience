﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CusExperience.Entities
{
    [DataContract(Name="Module")]
    [Serializable]
    [Table("ModuleResponse")]
    public class ModuleResponse
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ModuleResponseID { get; set; }

        public string ModuleData { get; set; }

        [DataMember]
        public string ModuleType { get; set; }

        [DataMember(Name = "ModuleData")]
        [NotMapped]
        public JToken ModuleJSONData { get; set; }

        [DataMember]
        [NotMapped]
        public int Page { get; set; }

        [DataMember]
        [NotMapped]
        public int Position { get; set; }

        [Required]
        public virtual Module Module { get; set; }

    }
}