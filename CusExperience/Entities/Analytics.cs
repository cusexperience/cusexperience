﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.Entities
{
    [DataContract(IsReference=true)]
    [Serializable]
    public class Analytics
    {
        [DataMember]
        public int SurveyID { get; set; }

        [DataMember]
        public string SurveyTitle { get; set; }

        public string SurveyData { get; set; }

        [DataMember(Name = "SurveyData")]
        [NotMapped]
        public JToken SurveyJSONData { get; set; }

        public string OrgSubData { get; set; }

        [DataMember(Name = "OrgSubData")]
        [NotMapped]
        public JToken OrgSubJSONData { get; set; }

        [DataMember]
        public DateTime? CreatedDate { get; set; }

        [DataMember]
        public string CreatedBy { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public bool IsTemplate { get; set; }

        [DataMember]
        public bool IsPredefined { get; set; }

        [DataMember]
        public string TemplateName { get; set; }

        [DataMember]
        public bool IsPublished { get; set; }

        [DataMember]
        public DateTime? PublishedDate { get; set; }

        [NotMapped]
        [DataMember]
        public int TotalResponses { get; set; }

        [DataMember]
        public virtual ICollection<AnalyticsModule> AnalyticsModules { get; set; }    
    }
}
