﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CusExperience.Startup))]
namespace CusExperience
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
