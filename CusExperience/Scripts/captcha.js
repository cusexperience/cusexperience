﻿var sweetcaptcha = new require('sweetcaptcha')('199571', 'ab49ac9514f831f2c1f3681832805b8e', 'bf3f9c7f2a02a6d99daa9ecbf2f2db35'); // your sweetCaptcha application credentials, see https://sweetcatpcha.com/accounts/signin

function getCaptcha(callback) {

}


// Validate using sckey and scvalue, hidden inputs from previous HTML

function validateCaptcha(captchaKey, captchaValue, callback) {

  // callback = function(err, isValid) {...}
  sweetcaptcha.api('check', {sckey: captchaKey, scvalue: captchaValue}, function(err, response){
    if (err) return callback(err);

    if (response === 'true') {
      // valid captcha
      return callback(null, true); 
    }

    // invalid captcha
    callback(null, false);

  });
  
}