$(function() {

    $("input,textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            var surname = $("input#surname").val();
            var givenname = $("input#givenname").val();
            var organisation = $("input#organisation").val();
            var country = $("input#country").val();
            var email = $("input#email").val();
            var phone = $("input#mobile").val();
            var message = $("textarea#message").val();
            var firstName = surname; // For Success/Failure Message
            // Check for white space in name for Success/Fail message
            if (firstName.indexOf(' ') >= 0) {
                firstName = name.split(' ').slice(0, -1).join(' ');
            }
            $('#submit-btn').text('Submitting');
            $.ajax({
                url: "https://mandrillapp.com/api/1.0/messages/send.json",
                type: "POST",
                
                data: {
              'key': 'hZHHYGuUhCxZIc9_BzLfSw',
              'message': {
                'from_email': 'hi@cusjo.com',
                'to': [
                    {
                      'email': 'hi@cusjo.com',
                      'name': 'nil',
                      'type': 'to'
              }
        ],
      'autotext': 'true',
      'subject': 'Contact Form',
      'html': '"You have received a new message from your website contact form.<br><br>"."Here are the details:<br><br>Given Name: '+givenname+'<br><br>Surname: '+surname+'<br><br>Country: '+country+'<br><br>Email: '+email+'<br><br>Mobile: '+phone+'<br><br>Message:<br><br>'+message
            }
    }
            }).done(function (response) {
                $('#submit-btn').text('Submit');
        // Success message
        $('#success').html("<div class='alert alert-success'>");
        $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
            .append("</button>");
        $('#success > .alert-success')
            .append("<strong>Your message has been sent. </strong>");
        $('#success > .alert-success')
            .append('</div>');

        //clear all fields
        $('#contactForm').trigger("reset");
    });
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

    $("a[data-toggle=\"tab\"]").click(function(e) {
        e.preventDefault();
        $(this).tab("show");
    });
});


/*When clicking on Full hide fail/success boxes */
$('#name').focus(function() {
    $('#success').html('');
});
