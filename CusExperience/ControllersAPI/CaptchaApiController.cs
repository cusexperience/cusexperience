﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace CusExperience.ControllersAPI
{
    public class CaptchaApiController : ApiController
    {

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/ValidateCaptcha")]
        public IHttpActionResult ValidateCaptcha(JToken supportObj)
        {
            try
            {
                string Captchakey = supportObj["CaptchaKey"] != null ? supportObj["CaptchaKey"].ToString() : "";
                string CaptchaValue = supportObj["CaptchaValue"] != null ? supportObj["CaptchaValue"].ToString() : "";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string request = String.Format(@"{{ ""method"": ""check"", ""app_id"" : ""199571"" , ""app_key"" : ""ab49ac9514f831f2c1f3681832805b8e"", ""sckey"" : ""{0}"" ,""scvalue"" : ""{1}"" ,""platform"" : ""api"" }}", Captchakey, CaptchaValue);
                HttpContent content = new StringContent(request);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage result = client.PostAsync("http://sweetcaptcha.com/api", content).Result;
                Stream receiveStream = result.Content.ReadAsStreamAsync().Result;
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string captchaResult = readStream.ReadToEnd();
                if (captchaResult.Equals("true"))
                    return Ok("Valid");
            
                return BadRequest("Captcha is Invalid");
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/ValidateCaptcha");
            }
        }
    }
}
