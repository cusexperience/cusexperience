﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using System.Web.Http.OData;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Http.OData.Query;
using CusExperience.Utilities;
using CusExperience.CusExperienceService;
using CusExperience.Entities;
using System.Web;

namespace CusExperience.ControllersAPI
{

    public class ResponseApiController : ApiController
    {
        ResponseRepositoryClient rRep = new ResponseRepositoryClient();
        AnalyticsRepositoryClient aRep = new AnalyticsRepositoryClient();

        public static string QUESTION = "Question";


        [Route("api/GetPSWithRC")]
        [ResponseType(typeof(List<Survey>))]
        public IHttpActionResult GetPublishedSurveysWithResponseCount()
        {
            try
            {
                return Ok(rRep.GetPbSurveysWithRespCount(User.Identity.GetUserId()));
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetPSWithRC.");
            }
        }

        [Route("api/GetSurveyResponsesBS/{orgsubid}/{surveyid}")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult GetSurveyResponsesBySurvey(string orgsubid, string surveyid, ODataQueryOptions<SurveyResponse> options)
        {
            try
            {
                int surveyID = 0;
                string filterKeyword = "";
                long orgSubID = 0;
                if (!Int32.TryParse(surveyid, out surveyID) || !Int64.TryParse(orgsubid, out orgSubID))
                    return BadRequest();
                if (options.Filter != null) filterKeyword = options.Filter.RawValue;
                return Ok(rRep.GetResponsesBySurvey(surveyID, orgSubID, User.Identity.GetUserId(), options.Skip.Value, options.Top.Value, options.OrderBy.OrderByClause.Direction.ToString(), filterKeyword));
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetSurveyResponsesBS/{id}.");
            }
        }


        [Route("api/GetSurveyResponsesBS/{orgsubid}/{surveyid}")]
        [HttpPost]
        [ResponseType(typeof(List<SurveyResponse>))]
        public IHttpActionResult GetSurveyResponsesBySurvey(string orgsubid, string surveyid, ODataQueryOptions<SurveyResponse> options, JToken cvData)
        {
            try
            {
                int surveyID = 0;
                string filterKeyword = "";
                long orgSubID = 0;
                if (!Int32.TryParse(surveyid, out surveyID) || !Int64.TryParse(orgsubid, out orgSubID))
                    return BadRequest();
                if (options.Filter != null) filterKeyword = options.Filter.RawValue;
                return Ok(aRep.GetMatchedResponses(surveyID,  options.Skip.Value, options.Top.Value, options.OrderBy.OrderByClause.Direction.ToString(), filterKeyword, cvData.ToString(), User.Identity.GetUserId()));
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetSurveyResponsesBS/{id}.");
            }
        }

        [Route("api/GetSurveyResponse/{id}")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult GetSurveyResponse(string id)
        {
            try
            {
                int surveyResponseID = 0;
                if (!Int32.TryParse(id, out surveyResponseID))
                    return BadRequest();

                Survey surveyResponse = rRep.GetSurveyResponseByID(surveyResponseID,User.Identity.GetUserId());

                if (surveyResponse == null)
                {
                    return NotFound();
                }
                return Ok(surveyResponse);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetSurveyResponse/{id}.");
            }
        }

        [AllowAnonymous]
        [Route("api/PostSurveyResponse")]
        public IHttpActionResult PostSurveyResponse(Survey survey)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                rRep.AddSurveyResponse(survey);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostSurveyResponse.");
            }
        }

        [Route("api/DeleteSurveyResponse/{id}")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult DeleteSurveyResponse(string id)
        {
            try
            {
                int surveyResponseID = 0;
                if (!Int32.TryParse(id, out surveyResponseID))
                    return BadRequest();

                rRep.DeleteReponse(surveyResponseID, User.Identity.GetUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/DeleteSurveyResponse/{id}.");
            }
        }
    } 
}