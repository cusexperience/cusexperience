﻿using CusExperience.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Security.Claims;
using WebMatrix.WebData;
using Newtonsoft.Json.Linq;
using System.Web.Http.Description;
using CusExperience.CusExperienceService;
using System.Web;
using CusExperience.Entities;
using CusExperience.Exceptions;
using System.ServiceModel;
using CusExperience.FaultContract;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;
using Google.Apis.Services;
using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using System.Threading;
using System.Security.Cryptography.X509Certificates;
using System.IO;


namespace CusExperience.ControllersAPI
{
    public class AccountApiController : ApiController
    {
        AccountRepositoryClient accRep = new AccountRepositoryClient();
        UserRepositoryClient uRep = new UserRepositoryClient();

        private const string LocalLoginProvider = "Local";
        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/Account/Login")]
        [ResponseType(typeof(string))]
        public IHttpActionResult Login(JToken loginObj)
        {
            try
            {
                ClaimsIdentity identity = accRep.LoginUser(loginObj.ToString());
                bool isPersistant = loginObj["IsRememberLogin"] != null && loginObj["IsRememberLogin"].ToString().Equals("True") ? true : false;
                var AuthenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistant },identity);
                return Ok("Success");
            }
            catch (FaultException<CusExpFault> ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                ModelStateDictionary error = new ModelStateDictionary();
                error.AddModelError("Summary", ex.Detail.Reason);
                return BadRequest(error);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/Account/Login");
            }
        }

        [AllowAnonymous]
        [Route("api/Account/RedirectURL")]
        [ResponseType(typeof(string))]
        public IHttpActionResult GetRedirectURL()
        {
            try
            {
                string redirectURL = "Error";
                if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()))
                    redirectURL = "/OrgSub";
                else if (uRep.isManager(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()))
                    redirectURL = "/Analytics/Index/" + uRep.GetCurrentUser(User.Identity.GetUserId()).OrgSub.OrgSubID.ToString();
                else redirectURL = "/Account/UserProfile";
                return Ok(redirectURL);
            }
            catch (Exception ex)
            {
                 Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                 return BadRequest("Error occured in api/Account/RedirectURL");
            }
        }

        [AllowAnonymous]
        [Route("api/Account/Logout")]
        [ResponseType(typeof(string))]
        public IHttpActionResult Logout()
        {
            try
            {
                var AuthenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/Account/Logout");
            }
             return Ok("Success");
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/Account/ForgotPassword")]
        [ResponseType(typeof(string))]
        public IHttpActionResult ForgotPassword(JToken forgotPwdObj)
        {
            try
            {
                accRep.ForgotPassword(forgotPwdObj.ToString());
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/Account/ForgotPassword");
            }
            return Ok("Success");
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("api/Account/ResetPassword")]
        [ResponseType(typeof(string))]
        public IHttpActionResult ResetPassword(JToken resetPasswordObj)
        {
            try
            {
                accRep.ResetPassword(resetPasswordObj.ToString());
                return Ok("Success");
            }
            catch (FaultException<CusExpFault> ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                ModelStateDictionary error = new ModelStateDictionary();
                error.AddModelError("Summary", ex.Detail.Reason);
                return BadRequest(error);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/Account/ResetPassword");
            }
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("api/Account/ChangePassword")]
        [ResponseType(typeof(string))]
        public IHttpActionResult ChangePassword(JToken changePasswordObj)
        {
            try
            {
                accRep.ChangePassword(changePasswordObj.ToString(),User.Identity.GetUserId());
                return Ok("Success");
            }
            catch (FaultException<CusExpFault> ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                ModelStateDictionary error = new ModelStateDictionary();
                error.AddModelError("Summary", ex.Detail.Reason);
                return BadRequest(error);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/Account/ChangePassword");
            }
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [System.Web.Mvc.ValidateAntiForgeryToken]
        [Route("api/Account/Register")]
        public IHttpActionResult Register(RegisterModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var user = new CusExpUser
                {
                    FirstName = model.UserProfile.FirstName,
                    LastName = model.UserProfile.LastName,
                    Title = model.UserProfile.Title,
                    UserName =  model.UserProfile.UserName,
                    Email = model.UserProfile.Email,
                    PhoneNumber = model.UserProfile.PhoneNumber
                };
                user.DateCreated = DateTime.Now;
                user.DateModified = DateTime.Now;
                return Ok("Success");
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/Account/Register");
            }
        }

        [System.Web.Mvc.ValidateAntiForgeryToken]
        [Route("api/Account/Google")]
        public IHttpActionResult GetGoogleApiAccessToken()
        {
            string path = "";
            try
            {

            string[] scopes = new string[] {
                                                AnalyticsService.Scope.AnalyticsReadonly
                                            };

            String serviceAccountEmail = "43991275007-qh60ekjrl1rl7ra6kn8suj801kngs258@developer.gserviceaccount.com";

            path = HttpContext.Current.Server.MapPath("~/Google/CusExperience.p12");

            var certificate = new X509Certificate2(path, "notasecret", X509KeyStorageFlags.MachineKeySet |
                                     X509KeyStorageFlags.PersistKeySet |
                                     X509KeyStorageFlags.Exportable);

            ServiceAccountCredential credential = new ServiceAccountCredential(
               new ServiceAccountCredential.Initializer(serviceAccountEmail)
               {
                   Scopes = scopes
               }.FromCertificate(certificate));

            bool requestCredential =  credential.RequestAccessTokenAsync(CancellationToken.None).Result;

            return Ok(credential.Token.AccessToken);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/Account/Google"); ;
            }
        }
    }
}
