﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.CusExperienceService;
using CusExperience.Entities;
using System.Web;


namespace CusExperience.ControllersAPI
{
    public class UserApiController : ApiController
    {
        UserRepositoryClient uRep = new UserRepositoryClient();

        [Route("api/GetCurrentUser")]
        [ResponseType(typeof(CusExpUser))]
        public IHttpActionResult GetCurrentUser()
        {
            try
            {
                return Ok(uRep.GetCurrentUser(User.Identity.GetUserId()));
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetCurrentUser");
            }
        }


        [Route("api/CreateUser")]
        [HttpPost]
        public IHttpActionResult CreateUser(CusExpUser user)
        {
            try
            {
                uRep.AddUser(user,User.Identity.GetUserId());
                return Ok("Success");
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/CreateUser");
            }
        }

        [Route("api/PostUser")]
        [HttpPost]
        public IHttpActionResult PostUser(CusExpUser user)
        {
            try
            {
                uRep.UpdateUser(user, User.Identity.GetUserId());
                return Ok("Success");
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostUser");
            }
        }

        [Route("api/PostUserProfile")]
        [HttpPost]
        public IHttpActionResult PostUserProfile(JToken userProfile)
        {
            try
            {
                uRep.SaveUserProfile(userProfile.ToString(), User.Identity.GetUserId());
                return Ok("Success");
            }
            catch(Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostUserProfile");
            }
        }

        [Route("api/DeleteUser/{id}")]
        [ResponseType(typeof(CusExpUser))]
        public IHttpActionResult DeleteUser(string id)
        {
            try
            {
                CusExpUser CusXPUser = uRep.GetUser(id, User.Identity.GetUserId());
                if (CusXPUser == null)
                {
                    return NotFound();
                }
                uRep.DeleteUser(id, User.Identity.GetUserId());
                return Ok(User);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/DeleteUser/{id}");
            }
        }
    }
}
