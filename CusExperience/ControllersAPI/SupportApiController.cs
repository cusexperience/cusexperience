﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using System.Web.Http.OData;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net.Http.Headers;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using CusExperience.CusExperienceService;
using System.Web;

namespace CusExperience.ControllersAPI
{
    public class SupportApiController : ApiController
    {
        AccountRepositoryClient accRep = new AccountRepositoryClient();
        SupportRepositoryClient sprRep = new SupportRepositoryClient();

        [HttpPost]
        [AllowAnonymous]
        [Route("api/PostSupport")]
        public IHttpActionResult PostSupport(JToken supportObj)
        {
            try
            {
                string Captchakey = supportObj["CaptchaKey"] != null ? supportObj["CaptchaKey"].ToString() : "";
                string CaptchaValue = supportObj["CaptchaValue"] != null ? supportObj["CaptchaValue"].ToString() : "";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string request = String.Format(@"{{ ""method"": ""check"", ""app_id"" : ""199571"" , ""app_key"" : ""ab49ac9514f831f2c1f3681832805b8e"", ""sckey"" : ""{0}"" ,""scvalue"" : ""{1}"" ,""platform"" : ""api"" }}", Captchakey, CaptchaValue);
                HttpContent content = new StringContent(request);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage result = client.PostAsync("http://sweetcaptcha.com/api", content).Result;
                Stream receiveStream = result.Content.ReadAsStreamAsync().Result;
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string captchaResult = readStream.ReadToEnd();
                if (captchaResult.Equals("true"))
                {
                    accRep.submitSupportForm(supportObj.ToString());
                }
                else
                {
                    return BadRequest("Captcha is Invalid");
                }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostSupport");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/SendSupport/{id}")]
        public IHttpActionResult SendSupport(string id,JToken supportObj)
        {
            try
            {
                string Captchakey = supportObj["CaptchaKey"] != null ? supportObj["CaptchaKey"].ToString() : "";
                string CaptchaValue = supportObj["CaptchaValue"] != null ? supportObj["CaptchaValue"].ToString() : "";
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string request = String.Format(@"{{ ""method"": ""check"", ""app_id"" : ""199571"" , ""app_key"" : ""ab49ac9514f831f2c1f3681832805b8e"", ""sckey"" : ""{0}"" ,""scvalue"" : ""{1}"" ,""platform"" : ""api"" }}", Captchakey, CaptchaValue);
                HttpContent content = new StringContent(request);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                HttpResponseMessage result = client.PostAsync("http://sweetcaptcha.com/api", content).Result;
                Stream receiveStream = result.Content.ReadAsStreamAsync().Result;
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string captchaResult = readStream.ReadToEnd();
                if (captchaResult.Equals("true"))
                {
                    sprRep.SendSupport(id,supportObj.ToString());
                }
                else
                {
                    return BadRequest("Captcha is Invalid");
                }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/SendSupport/{id}");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/PostContact")]
        public IHttpActionResult PostContact(JToken contactFormObj)
        {
            try
            {
                accRep.submitContactForm(contactFormObj.ToString());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }
            return Ok("Success");
        }
    }
}
