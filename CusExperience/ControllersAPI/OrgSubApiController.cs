﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web;
using System.IO;
using CusExperience.CusExperienceService;
using CusExperience.Entities;

namespace CusExperience.ControllersAPI
{
    public class OrgSubApiController : ApiController
    {
        OrgSubRepositoryClient oRep = new OrgSubRepositoryClient();
        
        [Route("api/GetOrgSubs")]
        [ResponseType(typeof(OrgSub[]))]
        public IHttpActionResult GetOrgSubs()
        {
            try
            {
            return Ok(oRep.GetOrgSubs(User.Identity.GetUserId()));
        }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetOrgSubs.");
            }
        }

        [Route("api/GetOrgSub/{id}")]
        [ResponseType(typeof(OrgSub))]
        public IHttpActionResult GetOrgSub(string id)
        {
            try
            {
            int orgSubID = 0;
            if (!Int32.TryParse(id, out orgSubID))
                return BadRequest();

            return Ok(oRep.GetOrgSub(orgSubID, User.Identity.GetUserId()));
        }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetOrgSub/{id}.");
            }
        }

       /* [Route("api/GetOrgSub")]
        [ResponseType(typeof(OrgSub))]
        public IHttpActionResult GetOrgSub()
        {
            try
            {
            return Ok(oRep.GetOrgSubByUser(User.Identity.GetUserId()));
        }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetOrgSub");
            }
        }*/

        [Route("api/PostOrgSub")]
        [HttpPost]
        [ResponseType(typeof(string))]
        public IHttpActionResult PostOrgSub(OrgSub orgSub)
        {
            try
            {
                oRep.AddorUpdateOrgSub(orgSub, User.Identity.GetUserId());
                return Ok("Success");
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostOrgSub");
            }
        }

        [Route("api/DeleteOrgSub/{id}")]
        [ResponseType(typeof(string))]
        public IHttpActionResult DeleteOrgSub(int id)
        {
            try
            {
                oRep.DeleteOrgSub(id, User.Identity.GetUserId());
                return Ok("Success");
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostOrgSub");
            }
        }

        [Route("api/PostTouchpoints/{id}")]
        [HttpPost]
        [ResponseType(typeof(string))]
        public IHttpActionResult PostTouchpoints(string id, List<Touchpoint> Touchpoints)
        {
            try
            {
                int orgSubID = 0;
                if (!Int32.TryParse(id, out orgSubID))
                    return BadRequest();

                oRep.SaveTouchpoints(User.Identity.GetUserId(), orgSubID, Touchpoints.ToArray());
                return Ok("Success");
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostTouchpoints/{id}");
            }
        }
    }
}
