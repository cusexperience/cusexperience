﻿using CusExperience.Utilities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace CusExperience.ControllersAPI
{
    public class ScreenShotApiController : ApiController
    {

        public static readonly List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };

        public static readonly List<string> VideoExtensions = new List<string> { ".OGG", ".MP4" };

        [Route("api/PostScreenShot")]
        [HttpPost]
        public IHttpActionResult PostScreenShot()
        {
            try
            {
            HttpContent requestContent = Request.Content;
            string base64data = requestContent.ReadAsStringAsync().Result;
            string guid = Guid.NewGuid().ToString();
           // ScreenShotCreator sc = new ScreenShotCreator(1024, 978, 1024, 978, htmlContent);
           // Bitmap bt = sc.GenerateThumbnail();
            string relativePath = @"/SupportScreenShot/" + guid + "_" + "Issue.jpg";
            var filePath = HttpContext.Current.Server.MapPath("~" + relativePath);
            byte[] bytes = Convert.FromBase64String(base64data);

            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }

            image.Save(filePath, System.Drawing.Imaging.ImageFormat.Png);
            return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
