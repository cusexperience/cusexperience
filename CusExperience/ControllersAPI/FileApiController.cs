﻿using CusExperience.CusExperienceService;
using CusExperience.DataContract;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace CusExperience.ControllersAPI
{
    public class FileApiController : ApiController
    {
        FileRepositoryClient fr = new FileRepositoryClient();
        SurveyRepositoryClient sRep = new SurveyRepositoryClient();
        public static readonly List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
        public static readonly List<string> VideoExtensions = new List<string> { ".MPEG", ".MPG", ".MOV", ".OGG", ".FLV", ".MP4" };
        public static readonly double MAXFILESIZE = 1;

        [HttpPost]
        [ResponseType(typeof(string))]
        [Route("api/PostSurveyImage")]
        public IHttpActionResult PostSurveyImage()
        {
            try
            {
                HttpResponseMessage result = null;
                var httpRequest = HttpContext.Current.Request;
                string relativePath = "";
                if (httpRequest.Files.Count > 0)
                {
                    var postedFile = httpRequest.Files[0];
                    string guid = Guid.NewGuid().ToString();
                    relativePath = @"/SurveyImages/" + guid + "_" + postedFile.FileName;
                    if (ImageExtensions.Contains(Path.GetExtension(relativePath).ToUpperInvariant()))
                    {
                        FileMetaData uploadFileMetadata = new FileMetaData();
                        uploadFileMetadata.RemoteFilePath = relativePath;
                        var memoryStream = new MemoryStream();
                        postedFile.InputStream.CopyTo(memoryStream);
                        fr.UploadFile(uploadFileMetadata, memoryStream.ToArray());
                        result = Request.CreateResponse(HttpStatusCode.Created);
                        return Ok(relativePath);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostSurveyImage");
            }
            return BadRequest("Files Not Found");
        }

        // POST api/SurveyImage
        [HttpPost]
        [Route("api/PostTouchpointImage/{id}")]
        [ResponseType(typeof(JObject))]
        public IHttpActionResult PostTouchpoingImage(string id)
        {
            try
            {
                JObject fileUploadInfo = new JObject();
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    var postedFile = httpRequest.Files[0];
                    string guid = Guid.NewGuid().ToString();
                    string fileType = "UNKNOWN";
                    string folderPath = @"/TouchpointImages" + "/" + id;
                    string relativePath = folderPath + "/" + guid + "_" + postedFile.FileName;

                    if (ImageExtensions.Contains(Path.GetExtension(relativePath).ToUpperInvariant()))
                    {
                        fileType = "IMAGE";
                        FileMetaData uploadFileMetadata = new FileMetaData();
                        uploadFileMetadata.RemoteFilePath = relativePath;
                        var memoryStream = new MemoryStream();
                        postedFile.InputStream.CopyTo(memoryStream);
                        fr.UploadFile(uploadFileMetadata, memoryStream.ToArray());
                        fileUploadInfo.Add("FileName", postedFile.FileName);
                        fileUploadInfo.Add("FilePath", relativePath);
                        fileUploadInfo.Add("FileType", fileType);
                        return Ok(fileUploadInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostTouchpointImage/{id}");
            }
            return BadRequest("Files Not Found");
        }

        [AllowAnonymous]
        [HttpPost]
        [ResponseType(typeof(JObject))]
        [Route("api/PostSurveyAttachment/{sId}/{mId}")]
        public IHttpActionResult PostSurveyAttachment(string sId, string mId)
        {
            try
            {
                int surveyId = 0;
                long moduleId = 0;
                if (!int.TryParse(sId, out surveyId) || !long.TryParse(mId, out moduleId))
                    return BadRequest("Invalid Arguments");
                JObject fileUploadInfo = new JObject();
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count <= 0)
                    return BadRequest("Error in file upload. Please try again.");
                var postedFile = httpRequest.Files[0];
                string guid = Guid.NewGuid().ToString();
                string fileType = "UNKNOWN";
                string folderPath = @"/SurveyAttachment/" + surveyId;
                string relativePath = folderPath + "/" + guid + "_" + postedFile.FileName;
                string[] AllowedFileExtensions = sRep.GetAttachmentExtensions(surveyId, moduleId);
                if (postedFile.ContentLength >= MAXFILESIZE * 1000000)
                    return BadRequest("File uploaded exceeded the maximum file size " + MAXFILESIZE + " MB");
                if (!AllowedFileExtensions.Contains(Path.GetExtension(relativePath).ToUpperInvariant()))
                    return BadRequest("Only the following filetypes can be uploaded " + string.Join(",", AllowedFileExtensions));

                FileMetaData uploadFileMetadata = new FileMetaData();
                uploadFileMetadata.RemoteFilePath = relativePath;
                var memoryStream = new MemoryStream();
                postedFile.InputStream.CopyTo(memoryStream);
                fr.UploadFile(uploadFileMetadata, memoryStream.ToArray());
                if (ImageExtensions.Contains(Path.GetExtension(relativePath).ToUpperInvariant()))
                    fileType = "IMAGE";
                else if (VideoExtensions.Contains(Path.GetExtension(relativePath).ToUpperInvariant()))
                    fileType = "VIDEO";
                fileUploadInfo.Add("FileName", postedFile.FileName);
                fileUploadInfo.Add("FilePath", relativePath);
                fileUploadInfo.Add("FileType", fileType);
                return Ok(fileUploadInfo);

            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostSurveyAttachment/{id}");
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("api/PostSupportAttachment")]
        [ResponseType(typeof(JObject))]
        public IHttpActionResult PostSupportAttachment()
        {
            try
            {
                JObject fileUploadInfo = new JObject();
                var httpRequest = HttpContext.Current.Request;

                if (httpRequest.Files.Count > 0)
                {
                    var postedFile = httpRequest.Files[0];
                    string guid = Guid.NewGuid().ToString();
                    string fileType = "UNKNOWN";
                    string folderPath = @"/SupportAttachment";
                    string relativePath = folderPath + "/" + guid + "_" + postedFile.FileName;

                    FileMetaData uploadFileMetadata = new FileMetaData();
                    uploadFileMetadata.RemoteFilePath = relativePath;
                    var memoryStream = new MemoryStream();
                    postedFile.InputStream.CopyTo(memoryStream);
                    fr.UploadFile(uploadFileMetadata, memoryStream.ToArray());
                    if (ImageExtensions.Contains(Path.GetExtension(relativePath).ToUpperInvariant()))
                        fileType = "IMAGE";
                    else if (VideoExtensions.Contains(Path.GetExtension(relativePath).ToUpperInvariant()))
                        fileType = "VIDEO";
                    fileUploadInfo.Add("FileName", postedFile.FileName);
                    fileUploadInfo.Add("FilePath", relativePath);
                    fileUploadInfo.Add("FileType", fileType);
                }
                else
                {
                    return BadRequest();
                }

                return Ok(fileUploadInfo);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostSupportAttachment");
            }
        }
    }
}
