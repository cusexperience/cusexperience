﻿using CusExperience.CusExperienceService;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace CusExperience.ControllersAPI
{
    public class STAFFApiController : ApiController
    {
        StaffRepositoryClient dRep = new StaffRepositoryClient();

       [AllowAnonymous]
        [Route("api/STAFF/UserNames")]
        [ResponseType(typeof(List<string>))]
        [HttpPost]
        public IHttpActionResult GetUserNames(JToken queryDetails)
        {
            try
            {
                JObject jsonQueryDetails = JObject.Parse(queryDetails.ToString());
                if (jsonQueryDetails["QueryTerm"] != null && jsonQueryDetails["OrgSubID"]!= null)
                {
                    string QueryTerm = jsonQueryDetails["QueryTerm"].ToString();
                    long OrgSubID = Convert.ToInt32(jsonQueryDetails["OrgSubID"].ToString());
                    string TouchpointName = jsonQueryDetails["TouchpointName"] != null ? jsonQueryDetails["TouchpointName"].ToString() : "";
                    return Ok(dRep.GetUserNames(OrgSubID,TouchpointName,QueryTerm));
                }
                return BadRequest("Invalid Arguments");
            } catch(Exception ex){
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/ValidateCaptcha");
            }
        }
    }
}
