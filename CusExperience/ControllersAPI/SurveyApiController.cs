﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.CusExperienceService;
using Newtonsoft.Json;
using CusExperience.Entities;
using System.Web;
using CusExperience.Exceptions;

namespace CusExperience.Controllers
{
    [Authorize]
    public class SurveyApiController : ApiController
    {

        protected SurveyRepositoryClient sRep = new SurveyRepositoryClient();
        protected AnalyticsRepositoryClient aRep = new AnalyticsRepositoryClient();

        [Route("api/GetSurveys")]
        [ResponseType(typeof(Survey[]))]
        public IHttpActionResult GetSurveys()
        {
            try
            {
                return Ok(sRep.GetSurveys(User.Identity.GetUserId()));
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetSurveys");
            }
        }


        [Route("api/GetSurvey/{id}")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult GetSurvey(string id)
        {
            try
            {
                int surveyID = 0;
                if (!Int32.TryParse(id, out surveyID))
                    return BadRequest();
                Survey survey = sRep.GetSurvey(surveyID, User.Identity.GetUserId());
                if (survey == null)
                {
                    return NotFound();
                }
                return Ok(survey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetSurvey/{id}");
            }
        }


        [Route("api/GetTemplateSurvey/{id}")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult GetTemplateSurvey(string id)
        {
            try
            {
                Survey survey = sRep.GetTemplateSurvey(id, User.Identity.GetUserId());
                if (survey == null)
                {
                    return NotFound();
                }
                return Ok(survey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetTemplateSurvey/{id}");
            }
        }

        [Route("api/GetDuplicateSurvey/{id}")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult GetDuplicateSurvey(string id)
        {
            try
            {
                int surveyID = 0;
                if (!Int32.TryParse(id, out surveyID))
                    return BadRequest();
                Survey survey = sRep.DuplicateSurvey(surveyID, User.Identity.GetUserId());
                if (survey == null)
                {
                    return NotFound();
                }
                return Ok(survey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetDuplicateSurvey/{id}");
            }
        }


        [AllowAnonymous]
        [Route("api/GetPublishedSurvey/{*url}")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult GetPublishedSurvey(string url)
        {
            try
            {
                if (string.IsNullOrEmpty(url))
                    return BadRequest();

                Survey survey = sRep.GetPublishedSurvey(url);
                if (survey == null)
                {
                    return NotFound();
                }
                return Ok(survey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetPublishedSurvey/{*url}");
            }
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/UnPublishedSurvey/{id}")]
        public IHttpActionResult UnPublishedSurvey(string id)
        {
            try
            {
                int surveyID = 0;
                if (!Int32.TryParse(id, out surveyID))
                    return BadRequest();

                sRep.UnPublishedSurvey(surveyID, User.Identity.GetUserId());
                return Ok();
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/UnPublishedSurvey/{id}");
            }
        }

        [HttpPost]
        [Route("api/PostSurvey")]
        [ResponseType(typeof(string))]
        public IHttpActionResult PostSurvey(Survey survey)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                sRep.AddorUpdateSurvey(ref survey, User.Identity.GetUserId());
                return Ok(survey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostSurvey");
            }
        }


        [HttpPost]
        [Route("api/PostPublishedSurvey")]
        [ResponseType(typeof(string))]
        public IHttpActionResult PostPublishedSurvey(Survey survey)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                sRep.AddorUpdatePublishedSurvey(ref survey, User.Identity.GetUserId());
                return Ok(survey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/PostPublishedSurvey");
            }
        }

        // DELETE api/SurveyData/5
        [Route("api/DeleteSurvey/{id}")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult DeleteSurvey(int id)
        {
            try
            {
                Survey survey = sRep.GetSurvey(id, User.Identity.GetUserId());
                if (survey == null)
                {
                    return NotFound();
                }
                sRep.DeleteSurvey(survey, User.Identity.GetUserId());
                return Ok(survey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/DeleteSurvey/{id}");
            }
        }

        [HttpGet]
        [Route("api/UpdatePublishID/{id}/{publishID}")]
        public IHttpActionResult UpdatePublishID(string id, string publishID)
        {
            try
            {
                int surveyID = 0;
                if (!Int32.TryParse(id, out surveyID))
                    return BadRequest();
                sRep.UpdatePublishID(surveyID, publishID, User.Identity.GetUserId());
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                if (ex.Message.Equals("PublishID Already Exists"))
                    return BadRequest(ex.Message);
                return BadRequest("Error occured in api/UpdatePublishID/{id}/{publishID}");
            }
            return Ok("Success");
        }
    }
}