﻿using CusExperience.Utilities;
using LinqToTwitter;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;

namespace CusExperience.ControllersAPI
{
    public class SocialApiController : ApiController
    {
        static string _address = "https://api.twitter.com/";

        [HttpPost]
        [Route("api/GetTwitterMessages/{orgsubid}/{surveyid}")]
        public JToken GetTwitterMessages(JToken hashTagObject)
        {
            JObject twitterAnalytics = new JObject();
            JArray tweetsJSON = new JArray();
            JObject datetimeCount = new JObject();
            JArray locationCount = new JArray();

            int todayCount = 0, thisWeekCount = 0, lastWeekCount = 0, thisMonthCount = 0, thisYearCount = 0;
            Dictionary<string,int> locations = new Dictionary<string,int>();
            List<Status> searchResults = new List<Status>();

            if (hashTagObject == null)
                return tweetsJSON;

            JArray reqHashTags = JArray.Parse(hashTagObject.ToString());

            string query = "";

            foreach (string eachTag in reqHashTags)
            {
                if (!string.IsNullOrEmpty(eachTag))
                {
                    query = string.IsNullOrEmpty(query) ? eachTag : query + " OR " + eachTag;
                }
            }

            if (string.IsNullOrEmpty(query)) return tweetsJSON;

            var auth = new SingleUserAuthorizer
            {

                CredentialStore = new SingleUserInMemoryCredentialStore
                {
                    ConsumerKey = "1LlqEnjm0Dq0lLd73CB5LTiG1",
                    ConsumerSecret = "4HRNYhTUceEJrFGAHtNy5XzqJat80OZ3DCGrGWHFImLkuBKLAE",
                    OAuthToken = "2859386238-PepkxDMA6vyzAAT6UPSaiIPRho4dPmWytl8us2R",
                    OAuthTokenSecret = "dYJfHNAs9hvsJFf05wgVlfpxQKlfFmO84aTXcdaFbAhV4"
                } 
            };

            using (var twitterCtx = new TwitterContext(auth))
            {
                Search eachResponse = null;
                //do
                //{
                    eachResponse = (from search in twitterCtx.Search
                                    where search.Type == SearchType.Search &&
                                          search.Query == query
                                    select search).SingleOrDefault();
                    if(eachResponse != null && eachResponse.Statuses.Count > 0)
                        searchResults.AddRange(eachResponse.Statuses);
               // } while (eachResponse != null && eachResponse.Statuses.Count > 0);

                if (searchResults.Count > 0) { 
                    foreach (Status tweet in searchResults)
                    {
                        JObject tweetJSON = new JObject();
                        tweetJSON["Name"] = tweet.User.ScreenNameResponse;
                        tweetJSON["Text"] = tweet.Text;
                        tweetJSON["Location"] = tweet.User.Location;
                        tweetJSON["Date"] = tweet.CreatedAt;

                        if (locations.ContainsKey(tweet.User.Location)) locations[tweet.User.Location]++;
                        else locations.Add(tweet.User.Location, 1);

                        if (tweet.CreatedAt.Year == DateTime.Today.Year)
                        {
                            thisYearCount++;
                            if (tweet.CreatedAt.Month == DateTime.Today.Month)
                            {
                                thisMonthCount++;
                                if (tweet.CreatedAt.DayOfWeek == DateTime.Today.DayOfWeek)
                                    thisWeekCount++;
                                if (tweet.CreatedAt.DayOfWeek == DateTime.Today.AddDays(7).DayOfWeek)
                                    lastWeekCount++;
                                if (tweet.CreatedAt.Date == DateTime.Today.Date)
                                    todayCount++;
                            }
                            tweetsJSON.Add(tweetJSON);
                        }         
                    }
                }
                datetimeCount["TodayCount"] = todayCount;
                datetimeCount["ThisWeekCount"] = thisWeekCount;
                datetimeCount["ThisMonthCount"] = thisMonthCount;
                datetimeCount["LastWeekCount"] = lastWeekCount;
                datetimeCount["ThisYearCount"] = thisYearCount;

                foreach (KeyValuePair<string, int> eachLocation in locations)
                {
                    JObject eachJSONLocation = new JObject();
                    eachJSONLocation["Location"] = eachLocation.Key;
                    eachJSONLocation["Count"] = eachLocation.Value;
                    locationCount.Add(eachJSONLocation);
                }
                twitterAnalytics["tweetsJSON"] = tweetsJSON;
                twitterAnalytics["locationCount"] = locationCount;
                twitterAnalytics["datetimeCount"] = datetimeCount;
                return twitterAnalytics;
            }
        }
    }
}
