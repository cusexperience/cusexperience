﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json.Linq;
using CusExperience.Utilities;
using CusExperience.CusExperienceService;
using Newtonsoft.Json;
using CusExperience.Entities;
using System.Web;

namespace CusExperience.ControllersAPI
{
    [Authorize]
    public class AnalyticsApiController : ApiController
    {
        protected SurveyRepositoryClient sRep = new SurveyRepositoryClient();
        protected AnalyticsRepositoryClient aRep = new AnalyticsRepositoryClient();

        [Route("api/GetAnalytics/{orgsubid}/{surveyid}")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult GetAnalytics(string orgsubid, string surveyid)
        {
            try
            {
                int surveyID = 0;
                long orgSubID = 0;
                if (!Int32.TryParse(surveyid, out surveyID) && !Int64.TryParse(orgsubid, out orgSubID))
                    return BadRequest();
                Survey survey = aRep.GetAnalytics(surveyID, orgSubID, User.Identity.GetUserId());
                return Ok(survey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetAnalytics/{id}");
            }
        }

        [Route("api/GetSearchAnalytics")]
        [HttpPost]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult GetSearchAnalytics(Survey searchSurvey)
        {
            try
            {
                Survey survey = aRep.GetSearchAnalytics(searchSurvey, User.Identity.GetUserId());
                return Ok(survey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetSearchAnalytics");
            }
        }

        [Route("api/GetComments")]
        [HttpPost]
        [ResponseType(typeof(Dictionary<int,string>))]
        public IHttpActionResult GetComments(JToken jsonCommentsObj)
        {
            try
            {
                return Ok(aRep.GetComments(jsonCommentsObj.ToString(), User.Identity.GetUserId()));
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetComments");
            }
        }

        [Route("api/GetCommentsByTag")]
        [HttpPost]
        [ResponseType(typeof(Dictionary<int, string>))]
        public IHttpActionResult GetCommentsByTag(JToken jsonTagsObj)
        {
            try
            {
                return Ok(aRep.GetCommentsByTag(jsonTagsObj.ToString(), User.Identity.GetUserId()));
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetCommentsByTag");
            }
        }

        [AllowAnonymous]
        [Route("api/GetSurveyResults/{*url}")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult GetSurveyResults(string url)
        {
            try
            {
                if (string.IsNullOrEmpty(url))
                    return NotFound();

                Survey survey = aRep.GetQuantitativeAnalytics(url);
                return Ok(survey);
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/GetSurveyResults/{*url}");
            }
        }


        [Route("api/AddExpertComment")]
        [ResponseType(typeof(Survey))]
        public IHttpActionResult AddExpertComment(JToken expertCommentData)
        {
            try
            {
                return Ok(aRep.AddExpertComment(expertCommentData.ToString(), User.Identity.GetUserId()));
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/SaveExpertComment/{id}");
            }
        }

        [Route("api/DeleteExpertComment/{id}")]
        [ResponseType(typeof(Survey))]
        [HttpGet]
        public IHttpActionResult DeleteExpertComment(string id)
        {
            try
            {
                long expertCommentID;
                if (!Int64.TryParse(id, out expertCommentID)) return BadRequest(); 
                return Ok(aRep.DeleteExpertComment(expertCommentID, User.Identity.GetUserId()));
            }
            catch (Exception ex)
            {
                Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex));
                return BadRequest("Error occured in api/DeleteExpertComment/{id}");
            }
        }
    }
}
