// JavaScript Document
var isNS4 = (navigator.appName=="Netscape")?1:0;
function doValidate(){
      //age = $("#byear").val() + "/" + $("#bmonth").val() + "/" + $("#bday").val();
      
      //var dateOfArrivalDate = new Date(dateOfArrivalVal);
      //alert(date);
     
      if(toCheckDateOfArrival && !checkDateSelect(document.sg50babyinternal.bmonth,dateOfArrivalIfEmpty)) return false;
      if(toCheckDateOfArrival && !checkDateSelect(document.sg50babyinternal.bday,dateOfArrivalIfEmpty)) return false;
      if(toCheckTwins && !checkSpecificSelection(document.sg50babyinternal,'health',termsCondsIfEmpty))return false;

      if(document.sg50babyinternal.name.value != ""){
        if(toCheckMomName && !checkMandatory(document.sg50babyinternal.name, momNameIfEmpty)) return false;
        if(toCheckMomNRIC && !isNricFinNo(document.sg50babyinternal.nricNo,momNRICIfEmpty)) return false;
        if(toCheckMomContactNo && !checkMandatory(document.sg50babyinternal.contactNo1,momContactNoIfEmpty) ) return false;
        if(toCheckMomContactNo && !isPhoneNumber(document.sg50babyinternal.contactNo1,momContactNoIfInvalid) ) return false;
        if(toCheckMomEmail && !checkMandatory(document.sg50babyinternal.email,momEmailIfEmpty)) return false;
        if(toCheckMomEmail && !isEmail(document.sg50babyinternal.email,momEmailIfInvalid)) return false;
        if(document.sg50babyinternal.name2.value != ""){
          //if(toCheckDadName && !checkMandatory(document.sg50babyinternal.name2, dadNameIfEmpty)) return false;
          if(toCheckDadNRIC && !isNricFinNo(document.sg50babyinternal.nricNo2,dadNRICIfEmpty)) return false;
          if(toCheckDadContactNo && !checkMandatory(document.sg50babyinternal.contactNo2,dadContactNoIfEmpty) ) return false;
          if(toCheckDadContactNo && !isPhoneNumber(document.sg50babyinternal.contactNo2,dadContactNoIfInvalid) ) return false;
          if(toCheckDadEmail && !checkMandatory(document.sg50babyinternal.email2,dadEmailIfEmpty)) return false;
          if(toCheckDadEmail && !isEmail(document.sg50babyinternal.email2,dadEmailIfInvalid)) return false;
        }
      }else{
        if(toCheckDadName && !checkMandatory(document.sg50babyinternal.name2, dadNameIfEmpty)) return false;
        if(toCheckDadNRIC && !isNricFinNo(document.sg50babyinternal.nricNo2,dadNRICIfEmpty)) return false;
        if(toCheckDadContactNo && !checkMandatory(document.sg50babyinternal.contactNo2,dadContactNoIfEmpty) ) return false;
        if(toCheckDadContactNo && !isPhoneNumber(document.sg50babyinternal.contactNo2,dadContactNoIfInvalid) ) return false;
        if(toCheckDadEmail && !checkMandatory(document.sg50babyinternal.email2,dadEmailIfEmpty)) return false;
        if(toCheckDadEmail && !isEmail(document.sg50babyinternal.email2,dadEmailIfInvalid)) return false;
      }

      if(toCheckMailingAddress && !checkMandatory(document.sg50babyinternal.addressLine1,mailingAddressIfEmpty)) return false;
      if(toCheckPostalCode && !checkMandatory(document.sg50babyinternal.addressLine2,postalCodeIfEmpty)) return false;
      if(toCheckPostalCode && !isNumeric(document.sg50babyinternal.addressLine2,postalCodeIfInvalid) ) return false;
      if(toCheckPostalCode && !isPostalCode(document.sg50babyinternal.addressLine2,postalCodeIfInvalid) ) return false;
      if(toCheckTermsConds && !checkSpecificSelection(document.sg50babyinternal,'termsconditions',termsCondsIfEmpty)) return false;
      //if(toCheckCaptha && !checkMandatory(document.sg50babyinternal.recaptcha_response_field,captchaIfEmpty)) return false;
      



      var dob =  $("#bday").val() + "/"+ $("#bmonth").val()+"/"+$("#byear").val();
      document.sg50babyinternal.dob.value = dob;
      var daysInMonth = [31,29,31,30,31,30,31,31,30,31,30,31];
      if($("#bday").val() > daysInMonth[$("#bmonth").val()-1]) {
        alert('Invalid date : ' + $("#bday").val()+'/'+$("#bmonth").val()+'/'+$("#byear").val());
        return false;
      }

      var date = new Date();
      var dateOfArrivalVal = document.sg50babyinternal.bday.value + '/'+document.sg50babyinternal.bmonth.value+'/'+'2005';
     
      var dateOfArrivalDate = new Date(2015,document.sg50babyinternal.bmonth.value-1,document.sg50babyinternal.bday.value,0,0,0,0);
      var now = new Date();
     
      /*if(monthDiff(now,dateOfArrivalDate) >=3){
        $(".giftPackDeliveryTime").html('two months before the baby delivery date');
      }else{
        $(".giftPackDeliveryTime").html('in three weeks');
      }*/
    
    return true;
}

function doFormSubmit(frmvalue,that,animating) {
    $.ajax({
        type : "POST",
        url : formActionTarget,
        data : frmvalue,
        success : function(ajaxResp){
          if(captchaEnabled){
            var meta = $(ajaxResp).filter('meta[http-equiv="refresh"]').attr("content");
            if(meta){
              $.ajax({
                type : "POST",
                url : "send_mail.jsp",
                data : frmvalue,
                success : function(ajaxResp){
                  animateStepFinal(that,animating);
                }, 
                error:function(xhr, ajaxOptions, thrownError){ 
                  //alert('Your submission has been submitted, however an error occurred while sending out email :' + thrownError);
                  animateStepFinal(that,animating);
                },
              });
            }else{ 
              var duplicates = $(ajaxResp).find('td').text();
              var s ="already participated";
              console.log(duplicates);
              if(duplicates && duplicates.indexOf(s) > -1){
                $(".step2 .par1").html("");
                $(".step2 .par2").html('<strong>We noticed that you have already registered for your baby</strong>')
                $(".step2 .par3").html("");
                animateStepFinal(that,animating);

              }else{
                alert(captchaIfInvalid);
                $(".submit-loading").hide();
                that.show();
              }
  		      }
          }else{
            var duplicates = $(ajaxResp).find('td').text();
            var s ="already participated";
              
            if(duplicates && duplicates.indexOf(s) > -1){
              $(".step2 .par1").html("");
              $(".step2 .par2").html('<strong>We noticed that you have already registered for your baby</strong>')
              $(".step2 .par3").html("");
              animateStepFinal(that,animating);
            }else{
              $.ajax({
                type : "POST",
                url : "send_mail.jsp",
                data : frmvalue,
                success : function(ajaxResp){
                  animateStepFinal(that,animating);
                }, 
                error:function(xhr, ajaxOptions, thrownError){ 
                    //alert('Your submission has been submitted, however an error occurred while sending out email :' + thrownError);
                  animateStepFinal(that,animating);
                },
              });
            }
          }
        }, 
        error:function(xhr, ajaxOptions, thrownError){ 
         alert('An error occurred while when submitting form :' + thrownError);
         $(".submit-loading").hide();
         that.show();
        },
    });
  }
