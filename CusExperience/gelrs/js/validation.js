    function isFileAllowed(fileField, allowedFileExtStr, errMsg)
    {
        var allowedFileExt = allowedFileExtStr.split(",");
        filename = trim(fileField.value);
        if (filename == "")
        {
            return true;
        }
        
        liIndex = filename.lastIndexOf(".");
        var allowed = false;
        if (liIndex>0)
        {
            fileExtension = filename.substr(liIndex+1);
            for (var i=0; i<allowedFileExt.length; i++)
            {
                if (fileExtension.toLowerCase() == allowedFileExt[i].toLowerCase())
                {
                    allowed = true;
                    break;
                }
            }//for i
        }
        
        if (!allowed)
        {
            alert(errMsg);
        }
        
        return allowed;
    }//isFileAllowed
    
    
    //dFieldnames: String[][0] is the field name; String[][1] is the error msg
    function checkAmountFields(dFieldnamesMsgs,decimalCount)
    {
        var dField = null;
        var dMsg = null;
        for (var i=0; i<dFieldnamesMsgs.length; i++)
        {
            dField = eval(dFieldnamesMsgs[i][0]);
            if (dField != null)
            {
                dMsg = dFieldnamesMsgs[i][1];
                if(!isAmount(dField, dMsg, decimalCount))
                {
                    return false;
                }
            }
        }//for i
        
        return true;
    }//checkAmountFields
    
    
    //dFieldnames: String[][0] is the field name; String[][1] is the error msg
    function checkFloatFields(dFieldnamesMsgs)
    {
        var dField = null;
        var dMsg = null;
        for (var i=0; i<dFieldnamesMsgs.length; i++)
        {
            dField = eval(dFieldnamesMsgs[i][0]);
            if (dField != null)
            {
                dMsg = dFieldnamesMsgs[i][1];
                if(!isFloat(dField, dMsg))
                {
                    return false;
                }
            }
        }//for i
        
        return true;
    }//checkFloatFields
    
    //dFieldnames: String[][0] is the field name; String[][1] is the error msg
    function checkIntegerFields(dFieldnamesMsgs)
    {
        var dField = null;
        var dMsg = null;
        for (var i=0; i<dFieldnamesMsgs.length; i++)
        {
            dField = eval(dFieldnamesMsgs[i][0]);
            if (dField != null)
            {
                dMsg = dFieldnamesMsgs[i][1];
                if(!isNumeric(dField, dMsg))
                {
                    return false;
                }
            }
        }//for i
        
        return true;
    }//checkIntegerFields
    
    
    //dFieldnames: String[][0] is the field name; String[][1] is the error msg
    function checkMandatoryFields(dFieldnamesMsgs)
    {
        var dField = null;
        var dMsg = null;
        for (var i=0; i<dFieldnamesMsgs.length; i++)
        {
            dField = eval(dFieldnamesMsgs[i][0]);
            if (dField != null)
            {
                dMsg = dFieldnamesMsgs[i][1];
                if(!checkMandatory(dField, dMsg))
                {
                    return false;
                }
            }
        }//for i
        
        return true;
    }//checkMandatoryFields
    
    function trimFields(dFieldnames)
    {
        var dField = null;
        for (var i=0; i<dFieldnames.length; i++)
        {
            dField = eval(dFieldnames[i]);
            if (dField != null)
            {
                dField.value = trim(dField.value);
            }
        }//for i
        
    }//trimFields
    
    
    function getRadioSelectionValue(fField, buttonCount, aMessage)
    {
        if (buttonCount<2)
        {
            if (!fField.checked)
            {
                alert(aMessage);
                return "";
            }
            
            return fField.value;
        }
        
        for (var i=0; i<buttonCount; i++)
        {
            if (fField[i].checked)
            {
                return fField[i].value;
            }
        }//for i
        
        alert(aMessage);
        return "";
    }//getRadioSelectionValue
    
    
    function checkRadioSelection(fFieldield, buttonCount, aMessage)
    {
        
        if (buttonCount<2)
        {
           if (!fFieldield.checked)
            {
                alert(aMessage);
                fFieldield.focus();
                return false;
            }
            
            return true;
        }
        
        for (var i=0; i<buttonCount; i++)
        {
            if (fFieldield[i].checked)
            {
                return true;
            }
        }//for i
        
        fFieldield[0].focus();
        alert(aMessage);
        return false;
    }//checkRadioSelection
    
    function checkSpecificSelection(aForm, fieldNamePrefix, aMessage)
    {
        for (var i=0; i<aForm.elements.length; i++)
        {
            if (aForm.elements[i].type=="checkbox")
            {
                if (aForm.elements[i].name.indexOf(fieldNamePrefix)>=0)
                {
                    if (aForm.elements[i].checked)
                    {
                        return true;
                    }
                }
            }
        }//for i
        
        alert(aMessage);
        return false;
    } //checkSpecificSelection
    
    function checkSelection(aForm, aMessage)
    {
        for (var i=0; i<aForm.elements.length; i++)
        {
            if (aForm.elements[i].type=="checkbox")
            {
                if (aForm.elements[i].checked)
                {
                    return true;
                }
            }
        }//for i
        
        alert(aMessage);
        return false;
    } //checkSelection
    
    // Check whether string s is empty.
    function isEmpty(s)
    {   return ((s == null) || (trim(s).length == 0))
    }
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    function isNumeric(iText,message)
    {
        if(iText.value != null && trim(iText.value) != '')
        {
            var sNumeric='0123456789';
            for (var i=iText.value.length-1;i>=0;i--)
            {
                if (sNumeric.indexOf(iText.value.charAt(i),0)==-1)
                {
                    alert(message);
                    iText.focus();
                    return false;
                }//if
            }//for
        }
        return true;
    }//isNumeric
    
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    function isPositiveInteger(iText,message)
    {
        if(iText.value != null && trim(iText.value) != '')
        {
            if(isNumeric(iText,message))
            {
                var val=parseInt(iText.value);
                if(val >0 )
                {
                    return true;
                }
                else
                {
                    alert(message);
                    iText.focus();
                    return false;
                }//if
                
            }//if
            else
            {
                return false;
            }
        }
        return true;
    }//isPositiveInteger
    
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    function isPhoneNumber(iText,message)
    {
       
        if(iText.value != null && trim(iText.value) != '')
        {
            var sNumeric='0123456789()-+#*,extEXT ';
            for (var i=iText.value.length-1;i>=0;i--)
            {
                if (sNumeric.indexOf(iText.value.charAt(i),0)==-1)
                {
                    alert(message);
                    iText.focus();
                    return false;
                }//if
            }//for
            
            var sValue=(iText.value).toUpperCase();
            var liE=sValue.indexOf("E");
            if(liE != -1)
            {
                if(sValue.length<liE+3 || sValue.substring(liE,liE+3) != "EXT")
                {
                    alert(message);
                    iText.focus();
                    return false;
                }//if
            }//if
            
            var liT=sValue.indexOf("T");
            if(liT != -1)
            {
                if(sValue.length< 3 || sValue.substring(liT-2,liT+1) != "EXT")
                {
                    alert(message);
                    iText.focus();
                    return false;
                }//if
            }//if
            
            if(iText.value.length<6)
            {
                return false;
            }//if
        }

        if(iText.value.charAt(0) == "8" || iText.value.charAt(0)=="9"){

        }else{
            alert(message);
            return false;
        }

        if(iText.value.length != 8){
            alert(message);
            return false;
        }

        return true;
    }//isNumeric
    
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    function isFloat(iText,message)
    {
        if(iText.value != null && trim(iText.value) != '')
        {
            //var numDecimalPlaces = 2; //max number of decimal places allowed
            var sNumeric='0123456789.';
            if(trim(iText.value)==".")
            {
                alert(message);
                iText.focus();
                return false;
            }
            
            var decimalPointCount=0;
            for (var i=iText.value.length-1;i>=0;i--)
            {
                if (sNumeric.indexOf(iText.value.charAt(i),0)==-1)
                {
                    alert(message);
                    iText.focus();
                    return false;
                }//if
                if(iText.value.charAt(i) == ".")
                {
                    decimalPointCount=decimalPointCount+1;
                }
            }//for
            
            if(decimalPointCount>1)
            {
                alert(message);
                iText.focus();
                return false;
            }
            
            //check number of decimal
            if(decimalPointCount>0)
            {
                extract = iText.value.substr(iText.value.indexOf(".")+1);
                if (extract.length == 0)
                {
                    alert(message);
                    iText.focus();
                    return false;
                }
                
                //if (extract.length>numDecimalPlaces)
                //{
                //alert("Value cannot exceed "+numDecimalPlaces+" decimal points.");
                //iText.focus();
                //return false;
                //}
            }
        }
        return true;
    } //isFloat
    
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    function isAmount(iText,message,decimalCount)
    {
        if(iText.value != null && trim(iText.value) != '')
        {
            var sNumeric='0123456789.,';
            var liCount=0;
            if(trim(iText.value)==".")
            {
                alert(message);
                iText.focus();
                return false;
            }
            else
            {
                for (var i=iText.value.length-1;i>=0;i--)
                {
                    if (sNumeric.indexOf(iText.value.charAt(i),0)==-1)
                    {
                        alert(message);
                        iText.focus();
                        return false;
                    }//if
                    if(iText.value.charAt(i) == ".")
                    {
                        liCount=liCount+1;
                    }
                }//for
                if(liCount>1)
                {
                    alert(message);
                    iText.focus();
                    return false;
                }
            }//else
            
            var val=iText.value;
            if(val.indexOf(".") != -1)
            {
                //if(val.indexOf(".")<(val.length-(decimalCount+1)))
                
                if (val.substr(val.indexOf(".")+1).length>decimalCount)
                {
                    alert(decimalAmountMsg);
                    
                    iText.focus();
                    return false;
                }
            }
        }
        return true;
    } //isAmount
    
    
    // Parameter iText is the name of the Text Field.
    function removeCommaFromAmount(iText)
    {
        var amount=trim(iText.value);
        var amount1="";
        var commaIndex=0;
        while(amount.indexOf(',') != -1)
        {
            commaIndex=amount.indexOf(',');
            if(commaIndex==0)
            {
                amount=amount.substring(1);
            }
            else
            {
                if(amount.length>commaIndex)
                {
                    amount1=amount.substring(commaIndex+1);
                }
                amount=amount.substring(0,commaIndex)+amount1;
            }//else
            
        }//while
        iText.value=amount;
    }//removeCommaFromAmount
    
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    function isDiscountFloat(iText,message)
    {
        if(iText.value != null && trim(iText.value) != '')
        {
            var sNumeric='0123456789.';
            var liCount=0;
            if(trim(iText.value)==".")
            {
                alert(message);
                iText.focus();
                return false;
            }
            else
            {
                for (var i=iText.value.length-1;i>=0;i--)
                {
                    if (sNumeric.indexOf(iText.value.charAt(i),0)==-1)
                    {
                        alert(message);
                        iText.focus();
                        return false;
                    }//if
                    if(iText.value.charAt(i) == ".")
                    {
                        liCount=liCount+1;
                    }
                }//for
                if(liCount>1)
                {
                    alert(message);
                    iText.focus();
                    return false;
                }
                var liValue=0;
                var liIndex=0;
                liIndex=(iText.value).indexOf(".");
                
                if(liIndex != -1)
                {
                    liValue=eval((iText.value).substring(0,liIndex)*1);
                    
                    if(liValue>99)
                    {
                        alert(message);
                        iText.focus();
                        return false;
                    }//if
                }//if
                if(liIndex == -1)
                {
                    liValue=eval((iText.value)*1);
                    
                    if(liValue>99)
                    {
                        alert(message);
                        iText.focus();
                        return false;
                    }//if
                }//else
                
                if(liIndex != -1)
                {
                    if((iText.value).length>(liIndex+3))
                    {
                        alert(message);
                        iText.focus();
                        return false;
                    }
                }//if
            }//else
        }
        return true;
    } //isDiscountFloat
    
    
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    // return true if iText.value > 0.00
    function isPositiveFloat(iText, message)
    {
        if(iText.value != null && trim(iText.value) != '')
        {
            if(	!isFloat(iText, message)) return false;
            var x = eval(iText.value);
            if(x<=0)
            {
                alert(message);
                iText.focus();
                return false;
            }
        }
        return true;
    }
    
    
    function isValidEmailStr(emailStr, message)
    {
        var i = 1;
        var sLength=trim(emailStr).length;
        
        if(sLength > 0)
        {
            
            if (emailStr.indexOf(" ")>-1)
            {
                alert(message);
                return false;
            }
            
            // look for @
            while ((i < sLength) && (emailStr.charAt(i) != "@"))
            { i++
            }
            
            if ((i >= sLength) || (emailStr.charAt(i) != "@"))
            {
                alert(message);
                return false;
            }
            else
                i += 2;
            
            // look for .
            while ((i < sLength) && (emailStr.charAt(i) != "."))
            { i++
            }
            
            // there must be at least one character after the .
            if ((i >= sLength - 1) || (emailStr.charAt(i) != "."))
            {
                alert(message);
                return false;
            }
            else
                return true;
        }//if
        
        return true;
    }//isValidEmailStr
    
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    function isEmail(iText,message)
    {
        if (!isValidEmailStr(iText.value, message))
        {
            iText.focus();
            return false;
        }
        
        return true;
    }//isEmail
    
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    function checkMandatory(iText, message)
    {
        if(iText.type == null ||
            iText.type == "text" ||
                iText.type == "password" ||
                iText.type == "hidden" ||
                iText.type == "file" ||
                iText.type =="" ||
                iText.type == "textarea" )
        {
            
            if (trim(iText.value).length<1)
            {
                if(iText.type == "hidden")
                {
                    alert(message);
                    return false;
                }
                else
                {
                    alert(message);
                    iText.focus();
                    return false;
                }//else
            }//if
        }//if
        else
        {
            if (iText.options[iText.selectedIndex].value.length<1)
            {
                alert(message);
                iText.focus();
                return false;
            }
        }
        
        return true;
    }//checkMandatory
    
    
    // Parameter psFromDate is the from date in DD/MM/YYYY format as string.
    // Parameter psToDate is the to date in DD/MM/YYYY format as string.
    // Parameter message is the message to be displayed.
    // true if psFromDate <= psToDate
    function compareDate(psFromDate, psToDate,psMessage)
    {
        dateFromObject = getDateObject(psFromDate);
        dateToObject = getDateObject(psToDate)
            
            if(dateToObject-dateFromObject<0)
            {
                alert (psMessage);
                return false;
            }//if
            else
            {
                return true;
            }//else
        }//compareDate
    
    
    // true if psFromDate < psToDate
    function isDateGreaterThan(psFromDate, psToDate,psMessage)
    {
        dateFromObject = getDateObject(psFromDate);
        dateToObject = getDateObject(psToDate)
            
            if(dateToObject-dateFromObject<=0)
            {
                alert (psMessage);
                return false;
            }//if
            else
            {
                return true;
            }//else
        }//compareDate
    
    
    
    // Parameter psDateString is the date in DD/MM/YYYY format as string.
    function getDateObject(psDateString)
    {
        var nSlashPos1=psDateString.indexOf("/",0);
        var nSlashPos2=psDateString.indexOf("/",nSlashPos1+1);
        
        var lsDay=eval(psDateString.substring(0,nSlashPos1)*1);
        var lsMon=eval(psDateString.substring(nSlashPos1+1,nSlashPos2)*1);
        var lsYear=eval(psDateString.substring(nSlashPos2+1,psDateString.length)*1);
        liMonth = Number(lsMon)-1;
        
        return new Date(lsYear,liMonth,lsDay,00,00,00);
    }//getDateObject
    
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    function dateDDMMYYYYvalidate(iText,message)
    {
        
        var psDateString=iText.value;
        if(psDateString != null && trim(psDateString).length > 0)
        {
            var nSlashPos1=psDateString.indexOf("/",0);
            if(nSlashPos1==-1)
            {
                alert(message);
                iText.focus();
                return false;
            }//if
            
            if(nSlashPos1 != 2)
            {
                alert(message);
                iText.focus();
                return false;
            }
            
            var nSlashPos2=psDateString.indexOf("/",nSlashPos1+1);
            
            if(nSlashPos2 != 5)
            {
                alert(message);
                iText.focus();
                return false;
            }
            
            if(nSlashPos2==-1)
            {
                alert(message);
                iText.focus();
                return false;
            }//if
            var lsDay=eval(psDateString.substring(0,nSlashPos1)*1);
            if(!isNumericString(psDateString.substring(0,nSlashPos1)))
            {
                alert(message);
                iText.focus();
                return false;
            }
            var lsMon=eval(psDateString.substring(nSlashPos1+1,nSlashPos2)*1);
            if(!isNumericString(psDateString.substring(nSlashPos1+1,nSlashPos2)))
            {
                alert(message);
                iText.focus();
                return false;
            }
            var lsYear=eval(psDateString.substring(nSlashPos2+1,psDateString.length)*1);
            if(lsYear == null || lsYear<999 || lsYear>9999)
            {
                alert(message);
                iText.focus();
                return false;
            }
            if(!isNumericString(psDateString.substring(nSlashPos2+1,psDateString.length)))
            {
                alert(message);
                iText.focus();
                return false;
            }
            if(lsYear>=0)
            {
                if(lsMon==1 || lsMon==3 ||lsMon==5 ||lsMon==7 ||lsMon==8 ||lsMon==10 ||lsMon==12)
                {
                    if(lsDay>0 && lsDay<32)
                    {
                        return true;
                    }//if day
                    else
                    {
                        alert(message);
                        iText.focus();
                        return false;
                    }//else
                    
                }//if month
                else if(lsMon==4 || lsMon==6 ||lsMon==9 ||lsMon==11)
                {
                    if(lsDay>0 && lsDay<31)
                    {
                        return true;
                    }//if day
                    else
                    {
                        alert(message);
                        iText.focus();
                        return false;
                    }//else
                    
                }//if month
                else if(lsMon==2)
                {
                    if(lsYear % 4 == 0)
                    {
                        if(lsDay>0 && lsDay<30)
                        {
                            return true;
                        }//if day
                        else
                        {
                            alert(message);
                            iText.focus();
                            return false;
                        }//else
                    }// if year
                    else
                    {
                        if(lsDay>0 && lsDay<29)
                        {
                            return true;
                        }//if day
                        else
                        {
                            alert(message);
                            iText.focus();
                            return false;
                        }//else
                    }//else
                    
                }//if month
                else
                {
                    alert(message);
                    iText.focus();
                    return false;
                }
            }// if year
            else
            {
                alert(message);
                iText.focus();
                return false;
            }//else
        }//if
        
    }//dateDDMMYYYYvalidate
    
    
    // Parameter psString is the string to check.
    function isNumericString(psString)
    {
        var sNumeric='0123456789';
        for (var i=psString.length-1;i>=0;i--)
        {
            if (sNumeric.indexOf(psString.charAt(i),0)==-1)
            {
                return false;
            }//if
        }//for
        
        return true;
    }//isNumericString
    
    
    
    
    // Parameter sText is the string to trim.
    function trim(sText)
    {
        var nTxtLen=sText.length-1;
        for (var nStart=0;nStart<=nTxtLen && sText.charAt(nStart)==' ';nStart++);
        if (nStart>nTxtLen)
            return '';
        for (var nEnd=nTxtLen;nEnd>=0 && sText.charAt(nEnd)==' ';nEnd--);
        return sText.substring(nStart,nEnd+1);
    } //trim
    
    
    // Parameter iText is the name of the Text Field.
    // Parameter message is the message to be displayed.
    function checkContactPerson(iText,message)
    {
        iText.value=(iText.value).toUpperCase();
        lsString=iText.value;
        if((trim(lsString)).length >0)
        {
            liIndex=lsString.indexOf(",");
            if(liIndex==-1)
            {
                alert(message);
                iText.focus();
                return false;
            }//if
            else
            {
                lsLastName=lsString.substring(0,liIndex);
                if(lsLastName == null || trim(lsLastName)=="")
                {
                    alert(message);
                    iText.focus();
                    return false;
                }
                lsFirstName=lsString.substring(liIndex+1);
                if(lsFirstName == null || trim(lsFirstName)=="")
                {
                    alert(message);
                    iText.focus();
                    return false;
                }
            }//else
            
        }//if
        return true;
    }//checkContactPerson
    
    
    // Parameter iText is the name of the Text Field.
    function changeToUpperCase(iText)
    {
        iText.value=(iText.value).toUpperCase();
    } //changeToUpperCase
    
    
    function PercentageCheck(Num)
    {
        
        var numlen = Num.value.length;
        var numtext =trim(Num.value);
        var i,j;
        var ctr=0;
        for( i=0,j=1; i<numlen && numtext != '' ; i++,j++)
        {
            var val = numtext.substring(i,j);
            if (val !=".")
                if(isNaN( val) ||  val == " ")
                {
                    alert(" Please enter the Numeric Character ");
                    Num.select();
                    Num.focus();
                    ctr=1;
                    break;
                } //End if
            
        }// End for
        if (numtext >= 100)
        {
            alert(" Please enter the number < 100 ");
            Num.select();
            Num.focus();
        }
        
        var count = 0;
        for (ndx = 0; ndx < numtext.length; ndx++)
            if (numtext.charAt(ndx) == ".") count++;
        
        if (count > 1)
        {
            alert("Invalid number of decimals.");
            Num.select();
            Num.focus();
        } else
        {
            Num.value =numtext;
        }
    }// End PercentageCheck
    
    // Checks if time is in hh:mm
    function isValidTime(timeStr, message)
    {
        var timePat = /^(\d{2}):(\d{2})?$/;
        
        var matchArray = timeStr.match(timePat);
        if (matchArray == null)
        {
            alert(message);
            return false;
        }
        
        hour = matchArray[1];
        minute = matchArray[2];
        
        if (hour < 0  || hour > 23)
        {
            alert(message);
            return false;
        }
        
        if (minute < 0 || minute > 59)
        {
            alert(message);
            return false;
        }
        
        return true;
    }//isValidTime
    
    
    
    function mandatoryAnySelection(aForm, fieldNamePrefix, aMessage)
    {
        var aField = null;
        for (var i=0; i<aForm.elements.length; i++)
        {
            aField = aForm.elements[i];
            if (aField.name.indexOf(fieldNamePrefix)>=0)
            {
                if (aField.selectedIndex >= 0)
                {
                    return true;
                }
            }
        }//for i
        
        alert(aMessage);
        return false;
    }//mandatoryAnySelection
    
    
    function mandatoryAllSelection(aForm, fieldNamePrefix, aMessage)
    {
        var aField = null;
        for (var i=0; i<aForm.elements.length; i++)
        {
            aField = aForm.elements[i];
            if (aField.name.indexOf(fieldNamePrefix)>=0)
            {
                if (aField.selectedIndex < 0)
                {
                    aField.focus();
                    alert(aMessage);
                    return false;
                }
            }
        }//for i
        
        return true;
    }//mandatoryAllSelection
    
    
    
    function validYear(iText, message)
    {
        if (isNaN(iText.value))
        {
            alert(message);
            iText.focus();
            return false;
        }
        
        var intYear=eval(iText.value*1);
        if(intYear == null || intYear<999 || intYear>9999)
        {
            alert(message);
            iText.focus();
            return false;
        }
        
        return true;
    }//validYear
    
    
    function roundToNdp(X, N) {
        var T = Number("1e" + N);
        return Math.round(X * T) / T;
    }
    
    function isNricFinNo(iText, message)
    {
        var NRIC = iText.value;
        //Function Checks The NRIC's Validity
        //NRIC=eval("documents.forms[0]."+field+".value")
        var NumberArray = new Array();
        NumberArray[0]=0;
        NumberArray[1]=2;
        NumberArray[2]=7;
        NumberArray[3]=6;
        NumberArray[4]=5;
        NumberArray[5]=4;
        NumberArray[6]=3;
        NumberArray[7]=2;
        NumberArray[8]=0;
        var CheckDigit=new Array();
        Prefix=NRIC.substring(0,1);
        var Total=0;
        
        if (NRIC.length != NumberArray.length)
        {
            alert(message);
			iText.focus();
            return false;
        }
        
        for(i=1;i<NRIC.length-1;i++)
        {
            CurrDigit=parseInt(NRIC.substring(i,i+1))
                if(isNaN(CurrDigit))
                {
                    alert(message);
					iText.focus();
                    return false;
                }
                Total=Total+CurrDigit*NumberArray[i]
            }
        
        //Add the Weigtage for New NRICs
        //if(Prefix=="S"||Prefix=="G")
        if(Prefix=="T"||Prefix=="G")
        {
            Total=Total+4
            }
        //if Foreign
        if(Prefix=="G" || Prefix=="F")
        {
            CheckDigit[0]=""
                CheckDigit[1]="K"
                CheckDigit[2]="L"
                CheckDigit[3]="M"
                CheckDigit[4]="N"
                CheckDigit[5]="P"
                CheckDigit[6]="Q"
                CheckDigit[7]="R"
                CheckDigit[8]="T"
                CheckDigit[9]="U"
                CheckDigit[10]="W"
                CheckDigit[11]="X"
            }
        else if(Prefix=="S" || Prefix=="T" || Prefix=="s" || Prefix=="t" || Prefix=="E" || Prefix=="e")
        {
            //Singaporian
            CheckDigit[0]=""
                CheckDigit[1]="A"
                CheckDigit[2]="B"
                CheckDigit[3]="C"
                CheckDigit[4]="D"
                CheckDigit[5]="E"
                CheckDigit[6]="F"
                CheckDigit[7]="G"
                CheckDigit[8]="H"
                CheckDigit[9]="I"
                CheckDigit[10]="Z"
                CheckDigit[11]="J"
            }
        else
        {
            //Wrong Prefix Raise Error
            alert(message);
			iText.focus();
            return false;
        }
        
        R1=Total%11;
        P=11-R1;
        //P=parseInt(P);
        SCheckDigit=CheckDigit[P];
        ACheckDigit=NRIC.substring(NRIC.length-1,NRIC.length)
            
            if (SCheckDigit!=ACheckDigit)
            {
                //alert(message);
                //return false;
            }
            return true;
    }
	
	/* function checkCheckBoxSelection(aForm, fieldName, buttonCount, aMessage)
    {		
		for (var i=1; i<=buttonCount; i++)
		{
		var aField = fieldName + "_"+i;
		if(document.sg50wishes[aField].checked)
			{
			return true;
			}
		}
							
		alert(aMessage);
        return false;
    }//checkCheckBoxSelection*/


    function checkTextCharactersNumber(iText,maxLength,message){

        if(iText.value.length > maxLength){
            alert(message);
            return false;
        }else{
            return true;
        }
    }

    function checkDateSelect(iText,message){
        if(iText.value==""){
            alert(message);
            return false;
        }
        return true;
    }

    function isPostalCode(iText,message)
    {
       if(iText.value.length!=6){
        alert(message);
        return false;
       }
       return true;
    }//isNumeric

    function monthDiff(d1, d2) {
        var months;
        months = (d2.getFullYear() - d1.getFullYear()) * 12;
        months -= d1.getMonth() + 1;
        months += d2.getMonth();
        return months <= 0 ? 0 : months;
    }
