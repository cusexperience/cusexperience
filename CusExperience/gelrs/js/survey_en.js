
	var lang = "en";
	var formId = "sg50babyinternal"; // Unique Form ID. Please use a new id for each form if you don't want to mix them up
	var TargetSubmitID = "sg50babyinternal";
	var appMode = "LIVE"; // DEV / LIVE
	var surveyType = "DD"; //DD to disallow, DA to allow unique NIRC 
	var captchaEnabled = false; // true - captcha enabled, false - captcha disabled
	//var imageSubmitUrl = "image-retr.jsp";
	//var imageConfirmUrl = "image-cnfrm.jsp";

	if (appMode == "LIVE"){
		var formActionTarget = "/apps/Survey";
		var toCheckDateOfArrival = true;
		var toCheckTwins = false;
		var toCheckChildNumber  = true;

		var toCheckMomName = true;
		var toCheckMomNRIC = true;
		var toCheckMomContactNo = true;
		var toCheckMomEmail = true;

		var toCheckDadName = true;
		var toCheckDadNRIC = true;
		var toCheckDadContactNo = true;
		var toCheckDadEmail = true;

		var toCheckMailingAddress = true;
		var toCheckPostalCode = true;
		var toCheckTermsConds = true;
		var toCheckCaptha = false;
		
	}else{
		var formActionTarget = "/apps/Survey";
		var toCheckDateOfArrival = true;
		var toCheckTwins = false;
		var toCheckChildNumber  = false;

		var toCheckMomName = false;
		var toCheckMomNRIC = false;
		var toCheckMomContactNo = false;
		var toCheckMomEmail = false;

		var toCheckDadName = false;
		var toCheckDadNRIC = false;
		var toCheckDadContactNo = false;
		var toCheckDadEmail = false;

		var toCheckMailingAddress = false;
		var toCheckPostalCode = false;
		var toCheckTermsConds = true;
		var toCheckCaptha = false;
	}

	//Prompt Message if empty 
	var dateOfArrivalIfEmpty = "Please provide the baby’s date of arrival or date of birth.";
	var twinsIfEmpty = "Please tell us if you are expecting twins";
	var childNumberIfEmpty = "Please provide the number of child you are expecting";
	var momNameIfEmpty = "Only one parent information is required and compulsory.\nPlease provide at least one parent’s complete information.";
	var momNRICIfEmpty = "Please enter the correct NRIC / FIN / Passport number.";
	var momContactNoIfEmpty = "Please enter your 8 digit mobile number that starts with ‘8’ or ‘9’.";
	var momEmailIfEmpty = "Please enter a valid email address.";
	var dadNameIfEmpty = "Only one parent information is required and compulsory.\nPlease provide at least one parent’s complete information.";
	var dadNRICIfEmpty = "Please enter dad's NRIC / FIN / Passport number.";
	var dadContactNoIfEmpty = "Please enter dad's 8 digit mobile number that starts with ‘8’ or ‘9’.";
	var dadEmailIfEmpty = "Please enter dad's email address. ";
	var mailingAddressIfEmpty = "Please enter your mailing address";
	var postalCodeIfEmpty = "Please enter your 6 digit Singapore postal code";
	var captchaIfEmpty = "Please enter the CAPTCHA validation.";
	var termsCondsIfEmpty = "Please agree to the terms and conditions.";


	var dateOfArrivalIfInvalid = "Please provide the baby’s date of arrival or date of birth.";
	var twinsIfInvalid = "Please tell us if you are expecting twins";
	var childNumberIfInvalid = "Please provide the number of child you are expecting";
	var momNameIfInvalid = "Only one parent information is required and compulsory.\nPlease provide at least one parent’s complete information.";
	var momNRICIfInvalid = "Please enter the correct NRIC / FIN / Passport number.";
	var momContactNoIfInvalid = "Please enter your 8 digit mobile number that starts with ‘8’ or ‘9’.";
	var momEmailIfInvalid = "Please enter a valid email address.";
	var dadNameIfInvalid = "Only one parent information is required and compulsory.\nPlease provide at least one parent’s complete information.";
	var dadNRICIfInvalid = "Please enter dad's NRIC / FIN / Passport number.";
	var dadContactNoIfInvalid = "Please enter dad's 8 digit mobile number that starts with ‘8’ or ‘9’.";
	var dadEmailIfInvalid = "Please enter dad's email address. ";
	var mailingAddressIfInvalid = "Please enter your mailing address";
	var postalCodeIfInvalid = "Please enter your 6 digit Singapore postal code";
	var captchaIfInvalid = "Invalid CAPTCHA validation.";
	var termsCondsIfInvalid = "Please agree to the terms and conditions.";
