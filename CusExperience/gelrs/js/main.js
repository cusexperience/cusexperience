$(document).ready(function(){
  if(!captchaEnabled){
    $("#recaptchaDiv").css('display','none');
  }
  if ($('.lt-ie9').length) {
    $("#section-about").backstretch("images/bg1a.jpg");
    $("#section-register").backstretch("images/bg2.jpg");
  };

   $("#nationality").blur(function(){
    if($(this).val()=="") $(this).val(0);
    if(parseFloat($(this).val())>20){
      $(this).val(20); 
    }
    if(parseFloat($(this).val())<=1){
      $(this).val(1); 
    }
    var newVal = $(this).val(); 
     var prefix = "";
      if (newVal==1) prefix ="st";
      if (newVal==2) prefix ="nd";
      if (newVal==3) prefix ="rd";
      if (newVal>3) prefix = "th";

      $(this).parent().find("input").val(newVal);
      
      if(newVal>9){
        $(this).parent().find(".prefix").removeClass('single-digit');
        $(this).parent().find(".prefix").addClass('double-digit');
      }else{
        $(this).parent().find(".prefix").removeClass('double-digit');
        $(this).parent().find(".prefix").addClass('single-digit');
      }
    
      $(this).parent().find(".prefix").text(prefix);


   })

  // $(".menu >li> a").click(function(e){
  //   if($(window).width()<768){
  //     var subMenu = $(this).parent().find('ul');
  //     var scrollLink = $(this).parent('.sub-menu');
  //     if (subMenu.length || scrollLink.length){
  //       e.preventDefault();
  //       if(subMenu.css('display')=='none'){
  //         subMenu.show(0,
  //           function(){
  //             $('.menu li.has-child').addClass('drop');
  //             $(this).find('li a').click(function(){
  //               $('.menu li.has-child').removeClass('drop');
  //               subMenu.hide();
  //               $('.mobile-nav span').trigger('click');
  //               $(this).unbind('click');
  //             })
  //           });
  //       }else{
  //         $('.menu li.has-child').removeClass('drop');
  //         subMenu.hide();
  //       }
  //       return false;
  //     }
  //   }

  // });

  $(".menu >li> a").click(function(e){
    if($(window).width()<768){
      var subMenu = $(this).parent().find('ul');
      var scrollLink = $(this).parent('.sub-menu');
      if (subMenu.length || scrollLink.length){
        e.preventDefault();
        if(subMenu.css('display')=='none'){
          subMenu.show(0,
            function(){
              // $('.menu li.has-child').addClass('drop');
              $(this).parent('.has-child').addClass('drop');
              $(this).find('li a').click(function(){
                // $('.menu li.has-child').removeClass('drop');
                $(this).parent('.has-child').removeClass('drop');
                subMenu.hide();
                $('.mobile-nav span').trigger('click');
                $(this).unbind('click');
              })
            });
        }else{
          $(this).parent('.has-child').removeClass('drop');
          subMenu.hide();
        }
        return false;
      }
    }

  });


  // mobile nav smooth animate
    $('.mobile-nav span').on('click', function() {
      if ($(window).width()<768) {
        var xx = $('header ul.menu').css('top')=='-350px'?($(window).width()<=480)?'120px':'159px':'-350px';
        $('header ul.menu').animate({top: xx}, 'fast');
        $('.list-inline.sub-menu').each(function() {
          $(this).hide();
        });
        $('.menu .has-child').removeClass('drop');
      }
    });


  responsiveLightbox();

  // lightbox for T & C
  var widthBrow = $(window).width();
  var checkWidthBrow = (widthBrow<720)?widthBrow-60:700;
  $('.prettyPhoto, .terms2').prettyPhoto({
    animation_speed:'normal',
    default_width: checkWidthBrow,
    social_tools: false,
    theme:'light_rounded'
  });

  if(window.location.hash == "#terms2"){
    $(".terms2").click();
  }


  //Window scrolling
  $(window).scroll(function() {
    var y = $(window).scrollTop();
    var x = $(window).width()>=768;
    var z = $('body').css('width');
    var o = $(window).width();
    //console.log(o+'  '+z);
    if (x) {
        $('header .row.three').removeClass('fixed');
    } else {
      $('header .row.three').removeClass('fixed');
    }
  });

  $('.scroll-to').click(function() {
    var xx = ($(window).width()>=768)?42:0;
    var tt = $(this).attr('href');
    $('html, body').animate({
            scrollTop: $(tt).offset().top-xx
        }, 500);
    return false;
  });

  $(".input-wrapper").append('<div class="inc button-me">+</div><div class="dec button-me">-</div>');
  $(".button-me").on("click", function() {
    var $button = $(this);
    var oldValue = $button.parent().find("input").val();
   
    if ($button.text() == "+") {
      if(parseFloat(oldValue) > 19){
        var newVal = parseFloat(oldValue) ;
      }else{
        var newVal = parseFloat(oldValue) + 1;
      }
      
    } else {
     // Don't allow decrementing below zero
      if (oldValue > 1) {
        var newVal = parseFloat(oldValue) - 1;
      } else {
        newVal = 1;
      }
    }
    var prefix = "";
    if (newVal==1) prefix ="st";
    if (newVal==2) prefix ="nd";
    if (newVal==3) prefix ="rd";
    if (newVal>3) prefix = "th";

    $button.parent().find("input").val(newVal);
    
    if(newVal>9){
      $button.parent().find(".prefix").removeClass('single-digit');
      $button.parent().find(".prefix").addClass('double-digit');
    }else{
      $button.parent().find(".prefix").removeClass('double-digit');
      $button.parent().find(".prefix").addClass('single-digit');
    }
  
    $button.parent().find(".prefix").text(prefix);

  });


  /* Animation for multi step forms */
  var current_fs, next_fs, previous_fs; //fieldsets
  var left, opacity, scale; //fieldset properties which we will animate
  var animating; //flag to prevent quick multi-click glitches
  var validate;
  var suc;
  $(".first-button").click(function(){
      if(doValidate()){
	    s.linkTrackVars='events,eVar29';
	    s.linkTrackEvents='event41';
	    s.events='event41';
	    s.eVar29="Register baby"
	   s.tl(this,'o','form baby');
        var that = $(this);
        $('#surveyType').val(surveyType);
        $(".submit-loading").show();
        that.hide();
        var frmvalue = $('#sg50babyinternal').serialize();
        doFormSubmit(frmvalue,that,animating);
      }
  });

  $("#sg50babyinternal .submit").click(function(){
    return false;
  });
  $(".new-wish").click(function(){
    location.reload();
    return false;
  });

});


$(window).load(function() {

  // dynamic form wrapper
  var fieldHeight = parseInt($('#sg50babyinternal fieldset.first').css('height'), 10);
  var responsiveFieldHeight = ($(window).width()<=767)?fieldHeight:fieldHeight+100;
  $('#sg50babyinternal').animate({height: responsiveFieldHeight+'px'}, 'normal');

});


var id;
$(window).resize(function(){
  
  clearTimeout(id);
  id = setTimeout(doneResizing, 1000);

});


// we store all necessary codes and run once when resize is finished
function doneResizing(){
  // run now

  // update variable width of lightbox
  widthBrow = $(window).width();
  checkWidthBrow = (widthBrow<720)?widthBrow-90:700;
  $('.prettyPhoto, .terms2').prettyPhoto({
    animation_speed:'normal',
    default_width: checkWidthBrow,
    theme:'light_rounded'
  });
  // clear sticky nav bug
  if ($('.row.three.fixed').length) {
    $('.row.three').removeClass('fixed');
  };

  // delay for 1 sec
  var respond = function() {
    responsiveLightbox();
    // responsiveInputStep2();
    // clear inline style left by mobile 
    if ($(window).width()>=768) {
      $('header .row.three').css('top', '');
    };
  };
  setTimeout(respond, 1000);

  // delay for 2 sec 
  var respond2 = function() {
    var fieldHeight = parseInt($('#sg50babyinternal fieldset.active').css('height'), 10);
    var responsiveFieldHeight = ($(window).width()<=767)?fieldHeight:fieldHeight+100;
    $('#sg50babyinternal').animate({height: responsiveFieldHeight+'px'}, 'normal');
  };
  setTimeout(respond2, 2000);
  clearTimeout(id);
}


function responsiveLightbox() {
  var ww = $(window).width();
  var wh = $(window).height();
  var im = (ww-60)*0.35;

  if (ww<1300) {
    $('.terms-content-wrapper').css('height', wh-150);
  } else {
    $('.terms-content-wrapper').css('height', '');
  };
  if ((ww<720) && (ww>415)) {
    $('p.content').css('height', im);
  } else{
    $('p.content').css('height', '');
  };
}

function animateStepFinal(that,animating) {
  if(animating) return false;
  animating = true;
  current_fs = that.parents('fieldset');
  next_fs = that.parents('fieldset').next();
  currentHeight = next_fs.height();
  responsiveHeight = ($(window).width()<=767)?currentHeight+50:currentHeight+150;
  current_fs.removeClass('active');
  $('.section-register-content-wrapper .title-wrapper  p').text('Submission successful!');
  if ($('.lt-ie9').length==0) {
    $('#sg50babyinternal').animate({height: responsiveHeight}, 'normal');
        $('html, body').animate({
          scrollTop: $("#section-register").offset().top
      }, 500);
  };
  
  //show the next fieldset
  next_fs.show().addClass('active'); 
  //hide the current fieldset with style
  current_fs.animate({opacity: 0}, {
    step: function(now, mx) {
      //as the opacity of current_fs reduces to 0 - stored in "now"
      //1. scale current_fs down to 80%
      scale = 1 - (1 - now) * 0.2;
      //2. bring next_fs from the right(50%)
      left = (now * 50)+"%";
      //3. increase opacity of next_fs to 1 as it moves in
      opacity = 1 - now;
      current_fs.css({'transform': 'scale('+scale+')'});
      next_fs.css({'left': left, 'opacity': opacity});
    }, 
    duration: 800, 
    complete: function(){
      current_fs.hide();
      animating = false;
      if ($('.lt-ie9').length) {
        $('#sg50babyinternal').animate({height: responsiveHeight}, 'normal');
            $('html, body').animate({
              scrollTop: $("#section-register").offset().top
          }, 500);
      };
    }, 
    //this comes from the custom easing plugin
    easing: 'easeInOutBack'
  });
}

