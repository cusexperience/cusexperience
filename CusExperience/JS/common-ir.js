﻿ko.bindingHandlers.movePreviousSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index >= 0) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('prev');
            }
        });
        $(element).parent().swiperight(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index >= 0) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('prev');
            }
        });
    }
}

ko.bindingHandlers.inlineEditor = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        CKEDITOR.inline(element);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}


ko.bindingHandlers.moveNextSlide = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index <= bindingContext.$data.QuestionData().QuestionItems().length - 1) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('next');
            }
        });
        $(element).parent().swipeleft(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index <= bindingContext.$data.QuestionData().QuestionItems().length - 1) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('next');
            }
        });
    }
}

ko.bindingHandlers.changeCurrentSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var csIndex = bindingContext.$index();
            $(element).parent().parent().carousel(csIndex);
            bindingContext.$parent.QuestionData().CurrentIndex(csIndex);
        });
    }
}

ko.bindingHandlers.toggleDemographicSection = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (bindingContext.$root.IsDemographicViewOpen() == true) {
            var surveyResponseDiv = $(element).parent().find('.demographicSection');
            surveyResponseDiv.show();
            $(element).find('span').removeClass("icon-plus");
            $(element).find('span').addClass("icon-minus");
        } else {
            var surveyResponseDiv = $(element).parent().find('.demographicSection');
            surveyResponseDiv.hide();
            $(element).find('span').removeClass("icon-minus");
            $(element).find('span').addClass("icon-plus");
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if ($(element).find('span').hasClass("icon-plus")) {
                var surveyResponseDiv = $(element).parent().find('.demographicSection');
                surveyResponseDiv.show();
                $(element).find('span').removeClass("icon-plus");
                $(element).find('span').addClass("icon-minus");
                bindingContext.$root.IsDemographicViewOpen(true);
            } else {
                var surveyResponseDiv = $(element).parent().find('.demographicSection');
                surveyResponseDiv.hide();
                $(element).find('span').removeClass("icon-minus");
                $(element).find('span').addClass("icon-plus");
                bindingContext.$root.IsDemographicViewOpen(false);
            }
        });
    }
}


ko.bindingHandlers.toggleQuestionSection = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (bindingContext.$root.IsQuestionViewOpen() == true) {
            var surveyResponseDiv = $(element).parent().find('.questionSection');
            surveyResponseDiv.show();
            $(element).find('span').removeClass("icon-plus");
            $(element).find('span').addClass("icon-minus");
        } else {
            var surveyResponseDiv = $(element).parent().find('.questionSection');
            surveyResponseDiv.hide();
            $(element).find('span').removeClass("icon-minus");
            $(element).find('span').addClass("icon-plus");
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if ($(element).find('span').hasClass("icon-plus")) {
                var surveyResponseDiv = $(element).parent().find('.questionSection');
                surveyResponseDiv.show();
                $(element).find('span').removeClass("icon-plus");
                $(element).find('span').addClass("icon-minus");
                bindingContext.$root.IsQuestionViewOpen(true);
            } else {
                var surveyResponseDiv = $(element).parent().find('.questionSection');
                surveyResponseDiv.hide();
                $(element).find('span').removeClass("icon-minus");
                $(element).find('span').addClass("icon-plus");
                bindingContext.$root.IsQuestionViewOpen(false);
            }
        });
    }
}


ko.bindingHandlers.closeQuestionSection = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var surveyResponseDiv = $(element).parent().parent().parent();
            $(element).parent().parent().parent().parent().find(".toggle-question").removeClass("icon-minus");
            $(element).parent().parent().parent().parent().find(".toggle-question").addClass("icon-plus");
            surveyResponseDiv.hide();
            bindingContext.$root.IsQuestionViewOpen(false);
        });
    }
}



ko.bindingHandlers.toggleTouchPointSection = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (bindingContext.$root.IsTouchpointViewOpen() == true) {
            var surveyResponseDiv = $(element).parent().find('.touchpointSection');
            surveyResponseDiv.show();
            $(element).find('span').removeClass("icon-plus");
            $(element).find('span').addClass("icon-minus");
        } else {
            var surveyResponseDiv = $(element).parent().find('.touchpointSection');
            surveyResponseDiv.hide();
            $(element).find('span').removeClass("icon-minus");
            $(element).find('span').addClass("icon-plus");
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if ($(element).find('span').hasClass("icon-plus")) {
                var surveyResponseDiv = $(element).parent().find('.touchpointSection');
                surveyResponseDiv.show();
                $(element).find('span').removeClass("icon-plus");
                $(element).find('span').addClass("icon-minus");
                bindingContext.$root.IsTouchpointViewOpen(true);
            } else {
                var surveyResponseDiv = $(element).parent().find('.touchpointSection');
                surveyResponseDiv.hide();
                $(element).find('span').removeClass("icon-minus");
                $(element).find('span').addClass("icon-plus");
                bindingContext.$root.IsTouchpointViewOpen(false);
            }
        });
    }
}

ko.bindingHandlers.selectChoice = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).toggleClass('choice-selected');
        bindingContext.$parent.SearchData().ToggleSelectedOption(bindingContext.$index());
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            $(element).toggleClass('choice-selected');
            bindingContext.$parent.SearchData().ToggleSelectedOption(bindingContext.$index());
        });
    }
}

ko.bindingHandlers.toggleOSView = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        bindingContext.$data.TouchpointAnalyticsData().CurrentView('hy');
            $('.org-frequency-view').hide();
            $('.org-experience-view').hide();
            $('.org-exp-view').removeClass('view-selected');
            $('.org-fq-view').removeClass('view-selected');
            $('.org-all-view').removeClass('view-selected');
            $('.org-hierarchy-view').show();
            $('.org-hy-view').addClass('view-selected');
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var currentView = valueAccessor();
            bindingContext.$data.TouchpointAnalyticsData().CurrentView(currentView);
            if (currentView == 'fq') {
                $('.org-hierarchy-view').hide();
                $('.org-experience-view').hide();
                $('.org-hierarchy-view').removeClass('view-selected');
                $('.org-exp-view').removeClass('view-selected');
                $('.org-all-view').removeClass('view-selected');
                $('.org-frequency-view').show();
                $('.org-fq-view').addClass('view-selected');
            } else if (currentView == 'hy') {
                $('.org-frequency-view').hide();
                $('.org-experience-view').hide();
                $('.org-exp-view').removeClass('view-selected');
                $('.org-fq-view').removeClass('view-selected');
                $('.org-all-view').removeClass('view-selected');
                $('.org-hierarchy-view').show();
                $('.org-hy-view').addClass('view-selected');
            } else if (currentView == 'exp') {
                $('.org-frequency-view').hide();
                $('.org-hierarchy-view').hide();
                $('.org-hy-view').removeClass('view-selected');
                $('.org-fq-view').removeClass('view-selected');
                $('.org-all-view').removeClass('view-selected');
                $('.org-experience-view').show();
                $('.org-exp-view').addClass('view-selected');
            } else if (currentView == 'all') {
                $('.org-experience-view').show();
                $('.org-frequency-view').show();
                $('.org-hierarchy-view').show();
                $('.org-exp-view').removeClass('view-selected');
                $('.org-fq-view').removeClass('view-selected');
                $('.org-hy-view').removeClass('view-selected');
                $('.org-all-view').addClass('view-selected');
            }
        });
    }
};

ko.bindingHandlers.toggleTPView = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var currentView = bindingContext.$data.TouchpointAnalyticsData().CurrentView();
        if (currentView == 'fq') {
            $('.tp-experience-view').hide();
            $('.tp-exp-view').removeClass('view-selected');
            $('.tp-all-view').removeClass('view-selected');
            $('.tp-frequency-view').show();
            $('.tp-fq-view').addClass('view-selected');
        } else if (currentView == 'exp') {
            $('.tp-experience-view').show();
            $('.tp-exp-view').addClass('view-selected');
            $('.tp-frequency-view').hide();
            $('.tp-fq-view').removeClass('view-selected');
            $('.tp-all-view').removeClass('view-selected');
        } else if (currentView == 'all') {
            $('.tp-experience-view').show();
            $('.tp-exp-view').removeClass('view-selected');
            $('.tp-frequency-view').show();
            $('.tp-fq-view').removeClass('view-selected');
            $('.tp-all-view').addClass('view-selected');
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var currentView = valueAccessor();
            bindingContext.$data.TouchpointAnalyticsData().CurrentView(currentView);
            if (currentView == 'fq') {
                $('.tp-experience-view').hide();
                $('.tp-exp-view').removeClass('view-selected');
                $('.tp-all-view').removeClass('view-selected');
                $('.tp-frequency-view').show();
                $('.tp-fq-view').addClass('view-selected');
            } else if (currentView == 'exp') {
                $('.tp-experience-view').show();
                $('.tp-exp-view').addClass('view-selected');
                $('.tp-frequency-view').hide();
                $('.tp-fq-view').removeClass('view-selected');
                $('.tp-all-view').removeClass('view-selected');
            } else if (currentView == 'all') {
                $('.tp-experience-view').show();
                $('.tp-exp-view').removeClass('view-selected');
                $('.tp-frequency-view').show();
                $('.tp-fq-view').removeClass('view-selected');
                $('.tp-all-view').addClass('view-selected');
            }
        });
    }
};


ko.bindingHandlers.toggleDirView = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var currentView = bindingContext.$data.AnalyticsData().CurrentView();
        if (currentView == 'fq') {
            $('.dir-experience-view').hide();
            $('.dir-exp-view').removeClass('view-selected');
            $('.dir-all-view').removeClass('view-selected');
            $('.dir-frequency-view').show();
            $('.dir-fq-view').addClass('view-selected');
        } else if (currentView == 'exp') {
            $('.dir-experience-view').show();
            $('.dir-exp-view').addClass('view-selected');
            $('.dir-frequency-view').hide();
            $('.dir-fq-view').removeClass('view-selected');
            $('.dir-all-view').removeClass('view-selected');
        } else if (currentView == 'all') {
            $('.dir-experience-view').show();
            $('.dir-exp-view').removeClass('view-selected');
            $('.dir-frequency-view').show();
            $('.dir-fq-view').removeClass('view-selected');
            $('.dir-all-view').addClass('view-selected');
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var currentView = valueAccessor();
            bindingContext.$data.AnalyticsData().CurrentView(currentView);
            if (currentView == 'fq') {
                $('.dir-experience-view').hide();
                $('.dir-exp-view').removeClass('view-selected');
                $('.dir-all-view').removeClass('view-selected');
                $('.dir-frequency-view').show();
                $('.dir-fq-view').addClass('view-selected');
            } else if (currentView == 'exp') {
                $('.dir-experience-view').show();
                $('.dir-exp-view').addClass('view-selected');
                $('.dir-frequency-view').hide();
                $('.dir-fq-view').removeClass('view-selected');
                $('.dir-all-view').removeClass('view-selected');
            } else if (currentView == 'all') {
                $('.dir-experience-view').show();
                $('.dir-exp-view').removeClass('view-selected');
                $('.dir-frequency-view').show();
                $('.dir-fq-view').removeClass('view-selected');
                $('.dir-all-view').addClass('view-selected');
            }
        });
    }
};

ko.bindingHandlers.chooseMultipleRatingRange = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var ModuleData = bindingContext.$parent;
        $(element).removeClass(ModuleData.QuestionData().MultipleType());
        $(element).addClass(ModuleData.QuestionData().MultipleType() + '-Selected');
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var ModuleData = bindingContext.$parent;
            if ((bindingContext.$parent.SearchData().FromRating() == null && bindingContext.$parent.SearchData().ToRating() == null) ||
                (bindingContext.$parent.SearchData().FromRating() != null && bindingContext.$parent.SearchData().ToRating() != null)) {
                bindingContext.$parent.SearchData().FromRating(bindingContext.$data);
                bindingContext.$parent.SearchData().ToRating(null);
                $(element).parent().parent().find('span').removeClass(ModuleData.QuestionData().MultipleType() + '-Selected');
                $(element).parent().parent().find('span').addClass(ModuleData.QuestionData().MultipleType());
                $(element).removeClass(ModuleData.QuestionData().MultipleType());
                $(element).addClass(ModuleData.QuestionData().MultipleType() + '-Selected');
            }
            else if (bindingContext.$parent.SearchData().FromRating() == bindingContext.$data) {
                bindingContext.$parent.SearchData().FromRating(null);
                bindingContext.$parent.SearchData().ToRating(null);
                $(element).parent().parent().find('span').removeClass(ModuleData.QuestionData().MultipleType() + '-Selected');
                $(element).parent().parent().find('span').addClass(ModuleData.QuestionData().MultipleType());
            } else if (bindingContext.$parent.SearchData().FromRating() > bindingContext.$data) {
                bindingContext.$parent.SearchData().FromRating(bindingContext.$data);
                $(element).parent().parent().find('span').removeClass(ModuleData.QuestionData().MultipleType() + '-Selected');
                $(element).parent().parent().find('span').addClass(ModuleData.QuestionData().MultipleType());
                $(element).removeClass(ModuleData.QuestionData().MultipleType());
                $(element).addClass(ModuleData.QuestionData().MultipleType() + '-Selected');
            } else {
                bindingContext.$parent.SearchData().ToRating(bindingContext.$data);
                $(element).parent().parent().find('span').removeClass(ModuleData.QuestionData().MultipleType() + '-Selected');
                $(element).parent().parent().find('span').addClass(ModuleData.QuestionData().MultipleType());
                for (var index = bindingContext.$parent.SearchData().FromRating() ; index < bindingContext.$parent.SearchData().ToRating() + 1 ; index++) {
                    $(element).parent().parent().find("li").eq(index).find('span').removeClass(ModuleData.QuestionData().MultipleType());
                    $(element).parent().parent().find("li").eq(index).find('span').addClass(ModuleData.QuestionData().MultipleType() + '-Selected');
                }
            }
        });
    }
}


ko.bindingHandlers.selectMultipleChoice = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).toggleClass('choice-selected');
        bindingContext.$parent.SearchData().ToggleSelectedOption(bindingContext.$index());
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            $(element).toggleClass('choice-selected');
            bindingContext.$parent.SearchData().ToggleSelectedOption(bindingContext.$index());
        });
    }
}

ko.bindingHandlers.addSearchField = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        bindingContext.$parent.SearchData().AddSearchField(bindingContext.$data, bindingContext.$index());
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
};


ko.bindingHandlers.selectMultipleOrgSub = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        var selectedOptions = allBindingsAccessor().selectedOptions;

        $(element).bind('multiselectopen', function (event, ui) {
            $(element).multiselect("widget").find(":checkbox").each(function () {
                if ($.inArray(this.value, selectedOptions().map(String)) != -1) {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                } else {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').removeClass('multiselect-sel');
                }
            });
        });

        $(element).on('multiselectclick', function (event, ui) {
            if (ui.value === "All" && ui.checked == true) {
                $(element).multiselect('checkAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                });

            } else if (ui.value === "All" && ui.checked == false) {
                $(element).multiselect('uncheckAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if ($(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').removeClass('multiselect-sel');
                });
            } else {

                var alltrue = true;
                var allElement;
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (this.value === "All") {
                        allElement = this;

                    }
                    if (this.checked == true) {
                        if (!$(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').addClass('multiselect-sel');
                    } else {
                        if (this.value != "All") alltrue = false;
                        if ($(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').removeClass('multiselect-sel');
                    }
                });

                if (alltrue == true && allElement.checked == false) {
                    selectedOptions.push("All");
                    $(allElement).closest('label').addClass('multiselect-sel');
                    $(allElement).prop('checked', true);
                } else if (alltrue == false && allElement.checked == true) {
                    selectedOptions.remove("All");
                    $(allElement).closest('label').removeClass('multiselect-sel');
                    $(allElement).prop('checked', false);
                }
            }
            bindingContext.$root.PublishedSurvey().SelectedTouchpoints([]);
            var allTp = true;
            for (index = 0 ; index < bindingContext.$root.Touchpoints().length ; index++) {
                var selValues = $(element).multiselect("getChecked").map(function () {
                    return parseInt(this.value);
                }).get();
                if ($.inArray(bindingContext.$root.Touchpoints()[index].OrgSubID(), selValues) != -1) {
                    bindingContext.$root.PublishedSurvey().SelectedTouchpoints.push(bindingContext.$root.Touchpoints()[index].value());
                } else allTp = false;
            }
            if(allTp)
                bindingContext.$root.PublishedSurvey().SelectedTouchpoints.push('All');

        });
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

ko.bindingHandlers.selectMultiple = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        var selectedOptions = allBindingsAccessor().selectedOptions;

        $(element).bind('multiselectopen', function (event, ui) {
            $(element).multiselect("widget").find(":checkbox").each(function () {
                if ($.inArray(this.value, selectedOptions().map(String)) != -1) {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                } else {
                    if ($(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').removeClass('multiselect-sel');
                }
            });
        });

        $(element).on('multiselectclick', function (event, ui) {
            if (ui.value === "All" && ui.checked == true) {
                $(element).multiselect('checkAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                });

            } else if (ui.value === "All" && ui.checked == false) {
                $(element).multiselect('uncheckAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if ($(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').removeClass('multiselect-sel');
                });
            } else {

                var alltrue = true;
                var allElement;
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (this.value === "All") {
                        allElement = this;

                    }
                    if (this.checked == true) {
                        if (!$(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').addClass('multiselect-sel');
                    } else {
                        if (this.value != "All") alltrue = false;
                        if ($(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').removeClass('multiselect-sel');
                    }
                });

                if (alltrue == true && allElement.checked == false) {
                    selectedOptions.push("All");
                    $(allElement).closest('label').addClass('multiselect-sel');
                    $(allElement).prop('checked', true);
                } else if (alltrue == false && allElement.checked == true) {
                    selectedOptions.remove("All");
                    $(allElement).closest('label').removeClass('multiselect-sel');
                    $(allElement).prop('checked', false);
                }
            }
        });


        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

ko.bindingHandlers.selectMultipleQuestion = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);

        $(element).bind('multiselectopen', function (event, ui) {
            $(element).multiselect("widget").find(":checkbox").each(function () {
                if (this.checked == true) {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                }
            });
        });

        $(element).on('multiselectclick', function (event, ui) {
            if (ui.value === "All" && ui.checked == true) {
                $(element).multiselect('checkAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                });

            } else if (ui.value === "All" && ui.checked == false) {
                $(element).multiselect('uncheckAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if ($(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').removeClass('multiselect-sel');
                });
            } else {

                var alltrue = true;
                var allElement;
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (this.value === "All") {
                        allElement = this;
                    }
                    if (this.checked == true) {
                        if (!$(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').addClass('multiselect-sel');
                    } else {
                        if (this.value != "All") alltrue = false;
                        if ($(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').removeClass('multiselect-sel');
                    }
                });

                if (alltrue == true && allElement.checked == false) {
                    bindingContext.$root.SelectedQuestions.push("All");
                    $(allElement).closest('label').addClass('multiselect-sel');
                    $(allElement).prop('checked', true);
                } else if (alltrue == false && allElement.checked == true) {
                    bindingContext.$root.SelectedQuestions.remove("All");
                    $(allElement).closest('label').removeClass('multiselect-sel');
                    $(allElement).prop('checked', false);
                }
            }
        });


        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

ko.bindingHandlers.datetimePicker = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var options = ko.utils.unwrapObservable(valueAccessor());
        $(element).datetimepicker({
            format: 'd/m/Y @ H:i'
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
};

ko.bindingHandlers.datetimePickerFromDate = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var options = ko.utils.unwrapObservable(valueAccessor());
        $(element).datetimepicker({
            format: 'd/m/Y @ H:i'
        });
        $(element).change(function (d) {
          
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
};

ko.bindingHandlers.datetimePickerToDate = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var options = ko.utils.unwrapObservable(valueAccessor());
        $(element).datetimepicker({
            format: 'd/m/Y @ H:i'
        });
        $(element).change(function () {
          
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
};



ko.bindingHandlers.showSearchResults = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.showSearchResults(element);
        });
    }
};


ko.bindingHandlers.enterKey = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).keypress(function (event) {
            var keyCode = (event.which ? event.which : event.keyCode);
            if (keyCode === 13) {
                bindingContext.$root.showSearchResults($('#search-analytics-btn')[0]);
            }
            return true;
        });
    }
};


ko.bindingHandlers.showDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var OverallRating = bindingContext.$data.AnalyticsData().OverallRating();
        
        var dataset = {
            rating: [OverallRating, 100 - OverallRating]
        };
        var margin = { top: 16, right: 16, bottom: 16, left: 16 };
        var width = $(element).width() - 14,
                height = $(element).height(),
                radius = Math.min(width, height) / 2;
        var vBW = 592;
        var vBH = 384;
        var x230 = -122.8;
        var y2 = -169.6;
        var x270 = 122.8;
        var xLeftLegend = -190.8;
        var yLeftLegend = 24.6;
        var lineFont = "20px";
        var y2Line = y2 + 7;
        var StrokeWidth = 3;
        var x1 = -110.52;
        var y1 = -152.64;
        var x90 = 198;
        var y90 = -64.334;
        var x290 = 178;
        var y290 = -57.83;
        var x100 = 187;
        var y100 = -1;
        var x2100 = 209;
        var y2100 = -1;
        var y290Line = y290 - 6;
        var x290Line = 225;
        var topTool = "80px";
        var leftTool = "167px";
        var lineThickness = 5;

        var y0pos = 7;
        var x30pos = x230 - 28;
        var y30pos = y2Line;
        var y90pos = y290 - 2;
        var y100pos = 5;

        var x2100Line = x2100 + 22;
        if (width > 300) {

        } else {
            y0pos = 5;
            x30pos = x30pos / 2 - 3;
            y30pos = y30pos / 2 + 0;
            y90pos = y90pos / 2;
            y100pos = 3;
            topTool = "50px";
            leftTool = "78px";
            margin = { top: 8, right: 8, bottom: 8, left: 8 };
            vBH = 192
            vBW = 305;
            x230 = x230 / 2;
            y2 = y2 / 2;
            x270 = x270 / 2;
            lineFont = "12px";
            y2Line = y2 - 2;
            y290Line = y290/2;
            StrokeWidth = 2;
            x1 = x1 / 2;
            y1 = y1 / 2;
            xLeftLegend = -94.8;
            yLeftLegend = 14.6;
            x90 = 198/2;
            y90 = -64.334 / 2;
            x290 = 178/2;
            y290 = -57.83 / 2;
            x290Line = 225 / 2 + 2;
            lineThickness = 3;
            x2100Line = x2100Line / 2 +4;
            x2100 = x2100 / 2;
            x100 = x100 / 2;
        }
       
        var barColor = ["#EA6953", "#e8e8e8"];
        if (OverallRating >= 90) {

            barColor = ["#009bdf", "#e8e8e8"];

        }
        else if (OverallRating >= 70) {

            barColor = ["#82CA9C", "#e8e8e8"];

        } else if (OverallRating >= 30) {
            barColor = ["#FDC689", "#e8e8e8"];
        }
        
        var color = d3.scale.category20().range(barColor);
        
        var degree = Math.PI / 180; // just to convert the radian-numbers
       
        var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

        var arc = d3.svg.arc()
            .innerRadius(radius - lineThickness)
            .outerRadius(radius);

        var div = d3.select(element).append("div")
                    .attr("class", "tooltip")
                    .style("opacity", 0);

        var svg = d3.select(element).append("svg")
            .attr("width", '100%')
            .attr("height", '100%')
            .attr('viewBox', '0 0 ' + vBW + ' ' + vBH)
            .attr('preserveAspectRatio', 'xMinYMin')
            .append("g")
            .attr("transform", "translate(" + vBW /2 + "," + vBH / 2 + ")");

        var path = svg.selectAll("path")
            .data(pie(dataset.rating))
            .enter().append("path")
            .attr("fill", function (d, i) {
                return color(i);
            })
            .attr("d", arc)
            .on("mouseover", function (d, i) {
              div.transition()
                  .duration(10)
                  .style("opacity", .9);
             
              div.html(d.value.toFixed(1) + "%")
                  .style("left", leftTool)
                  .style("top", topTool)
                  .style("color", "#333");
          })
        .on("mouseout", function (d) {
            div.transition()
                .duration(500)
                .style("opacity", 0);
        });


        var line = svg.append("line")
            .attr("x1", -x1)
            .attr("y1", y1)
            .attr("x2", x270)
            .attr("y2", y2)
            .attr("stroke-width", StrokeWidth)
            .attr("stroke", "#82CA9C");

        var line = svg.append("line")
          .attr("x1", x90)
          .attr("y1", y90)
          .attr("x2", x290)
          .attr("y2", y290)
          .attr("stroke-width", StrokeWidth)
          .attr("stroke", "#009bdf");

        var line = svg.append("line")
         .attr("x1", x100)
         .attr("y1", y100)
         .attr("x2", x2100)
         .attr("y2", y2100)
         .attr("stroke-width", StrokeWidth)
         .attr("stroke", "#009bdf");

        var line = svg.append("line")
        .attr("x1", -x100)
        .attr("y1", -y100)
        .attr("x2", -x2100)
        .attr("y2", -y2100)
        .attr("stroke-width", StrokeWidth)
        .attr("stroke", "#ea6953");
       
       
        var line = svg.append("line")
            .attr("x1", x1)
            .attr("y1", y1)
            .attr("x2", x230)
            .attr("y2", y2).attr("stroke-width", StrokeWidth)
            .attr("stroke", "#fdc689");

        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", x30pos)
            .attr("y", y30pos)
            .text("30.0%")
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");

        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", Math.abs(x30pos))
            .attr("y", y30pos)
            .text("70.0%")
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");

        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", x290Line)
            .attr("y", y90pos)
            .text("90.0%")
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");


        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", x2100Line)
            .attr("y", y100pos)
            .text("100%")
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");

        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", -x2100Line)
            .attr("y", y0pos)
            .text("0.0%")
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");

        svg.append("text")
           .attr("class", "x label")
           .attr("text-anchor", "middle")
           .attr("x", xLeftLegend)
           .attr("y", yLeftLegend)
           .text(bindingContext.$data.QuestionData().LowestRateText())
           .style("font-size", lineFont)
           .style("fill", "#8d8d8d");

        svg.append("text")
           .attr("class", "x label")
           .attr("text-anchor", "middle")
           .attr("x", Math.abs(xLeftLegend))
           .attr("y", yLeftLegend)
           .text(bindingContext.$data.QuestionData().HighestRateText())
           .style("font-size", lineFont)
           .style("fill", "#8d8d8d");
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.showChoiceDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var choiceWCounts = bindingContext.$data.AnalyticsData().ChoiceWCounts();
        var doughnutData = [];
        $.each(choiceWCounts, function (i, choiceWCount) {
            var doughnutSegment = { value: parseFloat(choiceWCount.Percentage()), color: CHARTCOLOURS[i] }
            doughnutData.push(doughnutSegment);
        });


        var doughnutName = [];

        var doughnutDataSort = [];
        $.each(choiceWCounts, function (i, choiceWCount) {
            doughnutName.push(choiceWCount.ChoiceText());
            doughnutDataSort.push(parseFloat(choiceWCount.Percentage()));
            
        });
        var topTool = "80px";
        var leftTool = "174px";
        var width = $(element).width(),
                height = $(element).height(),
                radius = Math.min(width, height) / 2;

        var vBW = 592;
        var vBH = 384;
        var lineThickness = 5;

        if (width >=592) {

        } else {
            topTool = "50px";
            leftTool = "70px";
            margin = { top: 8, right: 8, bottom: 8, left: 8 };
            vBH = 192
            vBW = 305;
            lineThickness = 3;
        }

        var dataset = {
            choiceSelection: doughnutDataSort,
        };

        var color = d3.scale.category20().range(CHARTCOLOURS);

        var degree = Math.PI / 180; // just to convert the radian-numbers
        var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

        var arc = d3.svg.arc()
            .innerRadius(radius - lineThickness)
            .outerRadius(radius)
       
        var div = d3.select(element).append("div")
                    .attr("class", "tooltip")
                    .style("opacity", 0);

        var svg = d3.select(element).append("svg")
            .attr("width", '100%')
            .attr("height", '100%')
            .attr('viewBox', '0 0 ' + vBW + ' ' + vBH)
            .attr('preserveAspectRatio', 'xMinYMin')
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
        
        var path = svg.selectAll("path")
            .data(pie(dataset.choiceSelection))
            .enter().append("path")
            .attr("fill", function (d, i) { return color(i); })
            .attr("d", arc)
            .on("mouseover", function (d, i) {
                   div.transition()
                  .duration(10)
                  .style("opacity", .9);
             
            div.html(d.value.toFixed(1) + "% - " + doughnutName[i])
                            .style("left", leftTool)
                            .style("top", topTool)
                            .style("color", "#333");
          })
        .on("mouseout", function (d) {
            div.transition()
               .duration(500)
               .style("opacity", 0);
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.showMultipleRatingDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var index = bindingContext.$index();
        if (bindingContext.$parent.AnalyticsData().AnalyticsItems().length > 0 && !$.isEmptyObject(bindingContext.$parent.AnalyticsData().AnalyticsItems()[index])) {
            var OverallRating = bindingContext.$parent.AnalyticsData().AnalyticsItems()[index].OverallRating();
           
            var dataset = {
                rating: [OverallRating, 100 - OverallRating]
            };
            var margin = { top: 16, right: 16, bottom: 16, left: 16 };
            var width = $(element).width() - 14,
                 height = $(element).height(),
                 radius = Math.min(width, height) / 2;
            var vBW = 592;
            var vBH = 384;
            var x230 = -122.8;    
            var y2 = -169.6;
            var x270 = 122.8;
            var lineFont = "20px";
            var y2Line = y2 +7;
            var StrokeWidth = 3;
            var x1 = -110.52;
            var y1 = -152.64
            var xLeftLegend = -190.8;
            var yLeftLegend = 24.6;
            var x90 = 198;
            var y90 = -64.334;
            var x290 = 178;
            var y290 = -57.83;
            var x100 = 187;
            var y100 = -1;
            var x2100 = 209;
            var y2100 = -1;
            var y290Line = y290 - 6;       
            var x290Line = 225;
            var lineThickness = 5;
            var x2100Line = x2100 + 22;
            var y0pos = 7;
            var x30pos = x230 - 28;
            var y30pos = y2Line;
            var y90pos = y290 - 2;
            var y100pos = 5;

            var topTool = "80px";
            var leftTool = "167px";

            if (width > 300) {

            } else {
                topTool = "50px";
                leftTool = "78px";
                margin = { top: 8, right: 8, bottom: 8, left: 8 };
                vBH = 192
                vBW = 305;
                x230 = x230 / 2;
                y0pos = 5;
                x30pos = x30pos / 2 -3;
                y30pos = y30pos / 2 + 0;
                y90pos = y90pos / 2;
                y100pos = 3;
                y2 = y2 / 2;
                x270 = x270 / 2;
                lineFont = "12px";
                y2Line = y2 - 2;
                StrokeWidth = 2;
                x1 = x1 / 2;
                y1 = y1 / 2;
                xLeftLegend = -94.8;
                yLeftLegend = 14.6;
                x90 = 198 / 2;
                y90 = -64.334 / 2;
                x290 = 178 / 2;
                y290 = -57.83 / 2;
                x290Line = 225 / 2 + 4;
                y290Line = y290 / 2 -14 ;
                lineThickness = 3;
                x2100Line = x2100Line / 2 + 4;
                x2100 = x2100 / 2;
                x100 = x100 / 2;
            }
            var barColor = ["#EA6953", "#e8e8e8"];
            if (OverallRating >= 90) {

                barColor = ["#009bdf", "#e8e8e8"];

            }
            else if (OverallRating >= 70) {

                barColor = ["#82CA9C", "#e8e8e8"];

            } else if (OverallRating >= 30) {
                barColor = ["#FDC689", "#e8e8e8"];
            }
            var color = d3.scale.category20().range(barColor);

            var degree = Math.PI / 180; // just to convert the radian-numbers
            var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

            var arc = d3.svg.arc()
                .innerRadius(radius - lineThickness)
                .outerRadius(radius);

            var div = d3.select(element).append("div")
   .attr("class", "tooltip")
   .style("opacity", 0);


            var svg = d3.select(element).append("svg")
          		.attr("width", '100%')
        		.attr("height", '100%')
             	.attr('viewBox', '0 0 ' + vBW + ' ' + vBH)
        		.attr('preserveAspectRatio', 'xMinYMin')
        	    .append("g")
			    .attr("transform", "translate(" + vBW / 2 + "," + vBH / 2 + ")");

            var path = svg.selectAll("path")
                .data(pie(dataset.rating))
                .enter().append("path")
                .attr("fill", function (d, i) {
                    return color(i);
                })
                .attr("d", arc) 
                .on("mouseover", function (d, i) {
                    div.transition()
                        .duration(10)
                        .style("opacity", .9);

                    div.html(d.value.toFixed(1) + "%")
                .style("left", leftTool)
                .style("top", topTool)
                .style("color", "#333");
                })
            .on("mouseout", function (d) {
                div.transition()
                    .duration(500)
                    .style("opacity", 0);
            });

        var line = svg.append("line")
            .attr("x1", -x1)
            .attr("y1", y1)
            .attr("x2", x270)
            .attr("y2", y2)
            .attr("stroke-width", StrokeWidth)
            .attr("stroke", "#82CA9C");


        var line = svg.append("line")
            .attr("x1", x1)
            .attr("y1", y1)
            .attr("x2", x230)
            .attr("y2", y2).attr("stroke-width", StrokeWidth)
            .attr("stroke", "#fdc689");

        var line = svg.append("line")
            .attr("x1", x90)
            .attr("y1", y90)
            .attr("x2", x290)
            .attr("y2", y290)
            .attr("stroke-width", StrokeWidth)
            .attr("stroke", "#009bdf");

        var line = svg.append("line")
            .attr("x1", x100)
            .attr("y1", y100)
            .attr("x2", x2100)
            .attr("y2", y2100)
            .attr("stroke-width", StrokeWidth)
            .attr("stroke", "#009bdf");

        var line = svg.append("line")
            .attr("x1", -x100)
            .attr("y1", -y100)
            .attr("x2", -x2100)
            .attr("y2", -y2100)
            .attr("stroke-width", StrokeWidth)
            .attr("stroke", "#ea6953");

       

        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", xLeftLegend)
            .attr("y", yLeftLegend)
            .text(bindingContext.$parent.QuestionData().LowestRateText())
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");

        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", Math.abs(xLeftLegend))
            .attr("y", yLeftLegend)
            .text(bindingContext.$parent.QuestionData().HighestRateText())
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");

        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", x30pos)
            .attr("y", y30pos)
            .text("30.0%")
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");

        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", Math.abs(x30pos))
            .attr("y", y30pos)
            .text("70.0%")
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");

        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", x290Line)
            .attr("y", y90pos)
            .text("90.0%")
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");


        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", x2100Line)
            .attr("y", y100pos)
            .text("100%")
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");

        svg.append("text")
            .attr("class", "x label")
            .attr("text-anchor", "middle")
            .attr("x", -x2100Line)
            .attr("y", y0pos)
            .text("0.0%")
            .style("font-size", lineFont)
            .style("fill", "#8d8d8d");
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.selectSingle = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        $(element).bind("multiselectclick", function (event, ui) {
            event.preventDefault();
        });
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
        $(element).bind("multiselectclick", function (event, ui) {
            event.preventDefault();
        });
    }
};


ko.bindingHandlers.showEVBarChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var analyticsData = valueAccessor();
        var Statistics = analyticsData.Statistics();
        var data = [];
        $.each(Statistics, function (i, eachItem) {
            var eachStat = { "label": eachItem.Text(), "n": parseFloat(eachItem.Score()), "count": eachItem.Count(), "score": parseFloat(eachItem.Score()), "search ": eachItem.IsSearchKeyword() }
            data.push(eachStat);
        });

        if (analyticsData.OrderByValueEV() == 'Lowest to Highest') data.reverse();

        displayStatisticsChartEV(element, data, bindingContext);
        var TotalElements = analyticsData.Statistics().length;
        var NoofVisible = analyticsData.NoofVisibleEV();
        var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;
        var height = (30 * VisibleElements) + 45;
        var dHeight = $(document).width();
        if (dHeight >= 640) {
            height = (42 * VisibleElements) + 40;
        }
        var charDivElement = $(element).parent().parent().find('#expviewchart')[0];
        $(charDivElement).attr("height", height + "px");
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}


ko.bindingHandlers.showHVBarChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var analyticsData = valueAccessor();
        var Statistics = analyticsData.HStatistics();
        var data = [];
        $.each(Statistics, function (i, eachItem) {
            var eachStat = { "label": eachItem.Text(), "n": parseFloat(eachItem.Score()), "count": eachItem.Count(), "score": parseFloat(eachItem.Score()), "search ": eachItem.IsSearchKeyword() }
            data.push(eachStat);
        });

        if (analyticsData.OrderByValueEV() == 'Lowest to Highest') data.reverse();

        displayStatisticsChartHV(element, data, bindingContext);
        var TotalElements = analyticsData.HStatistics().length;
        var NoofVisible = analyticsData.NoofVisibleEV();
        var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;
        var height = (30 * VisibleElements) + 45;
        var dHeight = $(document).width();
        if (dHeight >= 640) {
            height = (42 * VisibleElements) + 40;
        }
        var charDivElement = $(element).parent().parent().find('#hyviewchart')[0];
        $(charDivElement).attr("height", height + "px");
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.showFVBarChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        function compareL(a, b) {
            if (a.n < b.n)
                return -1;
            if (a.n > b.n)
                return 1;
            if (a.score < b.score)
                return 1;
            if (a.score > b.score)
                return -1;
            return 0;
        }
        function compareH(a, b) {
            if (a.n < b.n)
                return 1;
            if (a.n > b.n)
                return -1;
            if (a.score < b.score)
                return 1;
            if (a.score > b.score)
                return -1;
            return 0;
        }
        var analyticsData = valueAccessor();
        var Statistics = analyticsData.Statistics();
        var data = [];
        $.each(Statistics, function (i, eachItem) {
            var eachStat = { "label": eachItem.Text(), "n": parseFloat(eachItem.Count()), "count": eachItem.Count(), "score": parseFloat(eachItem.Score()), "search ": eachItem.IsSearchKeyword() }
            data.push(eachStat);
        });

        if (analyticsData.OrderByValueFV() == "Highest to Lowest") data.sort(compareH);
        else data.sort(compareL);

        displayStatisticsChartFV(element, data, bindingContext);
        var TotalElements = Statistics.length;
        var NoofVisible = analyticsData.NoofVisibleEV();
        var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;
        var height = (30 * VisibleElements) + 45;
        var dHeight = $(document).width();
        if (dHeight >= 640) {
            height = (42 * VisibleElements) + 40;
        }
        var charDivElement = $(element).parent().parent().find('#freqviewchart')[0];
        $(charDivElement).attr("height", height + "px");
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
       
    }
}

ko.bindingHandlers.showMultipleChoiceDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var index = bindingContext.$index();
        if (bindingContext.$parent.AnalyticsData().AnalyticsItems().length > 0) {
            var choiceWCounts = bindingContext.$parent.AnalyticsData().AnalyticsItems()[index].ChoiceWCounts();
            var doughnutData = [];
            var doughnutName = [];
            $.each(choiceWCounts, function (i, choiceWCount) {
                doughnutName.push(choiceWCount.ChoiceText());
                doughnutData.push(parseFloat(choiceWCount.Percentage()));
            });
            var dataset = {
                rating: doughnutData
            };


            var margin = { top: 16, right: 16, bottom: 16, left: 16 };
            var width = $(element).width() - 14,
                    height = $(element).height(),
                    radius = Math.min(width, height) / 2;
            var vBW = 592;
            var vBH = 384;
            var topTool = "80px";
            var leftTool = "167px";
            var lineThickness = 5;
            
            if (width >= 592) {
                
            } else {
                topTool = "50px";
                leftTool = "78px";
                margin = { top: 8, right: 8, bottom: 8, left: 8 };
                vBH = 192
                vBW = 280;
                lineThickness = 3;
            }
            var color = d3.scale.category20().range(CHARTCOLOURS);

            var degree = Math.PI / 180; // just to convert the radian-numbers
            var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

            var arc = d3.svg.arc()
                .innerRadius(radius - lineThickness)
                .outerRadius(radius);

            var div = d3.select(element).append("div")
                        .attr("class", "tooltip")
                        .style("opacity", 0);

            var svg = d3.select(element).append("svg")
          		.attr("width", '100%')
        		
                .attr('viewBox', '0 0 ' + vBW + ' ' + vBH)
        		.attr('preserveAspectRatio', 'xMinYMin')
        	.append("g")
			    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

            var path = svg.selectAll("path")
                .data(pie(dataset.rating))
                .enter().append("path")
                .attr("fill", function (d, i) {
                    return color(i);
                })
                .attr("d", arc).on("mouseover", function (d, i) {
                    div.transition()
                        .duration(10)
                        .style("opacity", .9);
                   
                    div.html(d.value.toFixed(1) + "% - " + doughnutName[i])
                        .style("left", leftTool)
                        .style("top", topTool)
                        .style("color", "#333")
                    // .style("color", color(i))
                    .style("font-weight","");

                })
                .on("mouseout", function (d) {
                    div.transition()
                        .duration(500)
                        .style("opacity", 0);
                });
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.showComments = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var commentText = $('<div>').append(bindingContext.$data.Word()).text();
            bindingContext.$root.showComments(bindingContext.$parents[1].ModuleID(), commentText);
        });
    }
}

ko.bindingHandlers.toggleDemographicInfo = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {

            if ($(element).find('span').hasClass("icon-plus")) {
                $(element).parent().find('.demodiv').show();
                $(element).find('span').removeClass("icon-plus");
                $(element).find('span').addClass("icon-minus");
            } else {
                $(element).find('span').removeClass("icon-minus");
                $(element).find('span').addClass("icon-plus");
                $(element).parent().find('.demodiv').hide();
            }
        });
    }
}

ko.bindingHandlers.showCommentsByTag = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.showCommentsByTag(bindingContext.$parents[1].ModuleID(), bindingContext.$data.TagName(), bindingContext.$data.Count());
        });
    }
}

ko.bindingHandlers.initNoofVisible = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).on('show.bs.modal', function (e) {
            bindingContext.$root.NoofVisible(10);
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}


ko.bindingHandlers.LoadMore = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$data.AnalyticsData().NoofVisible(bindingContext.$data.AnalyticsData().NoofVisible() + 10);
        });
    }
}

ko.bindingHandlers.LoadAll = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$data.AnalyticsData().NoofVisible(bindingContext.$data.AnalyticsData().WordCloud().length);
        });
    }
}

ko.bindingHandlers.IncreaseHeightEV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () {
            var analyticsData = valueAccessor();
            var Statistics = analyticsData.Statistics();
            var TotalElements = Statistics.length;
            var NoofVisible = analyticsData.NoofVisibleEV() + 10;
            var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;
           
            var height = (30 * VisibleElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = (42 * VisibleElements) + 40;
            }
            var charDivElement = $(element).parent().parent().find('#expviewchart')[0];
            $(charDivElement).attr("height", height + "px");
            analyticsData.NoofVisibleEV(NoofVisible);
        });
    }
}


ko.bindingHandlers.IncreaseHeightHV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () {
            var analyticsData = valueAccessor();
            var Statistics = analyticsData.HStatistics();
            var TotalElements = Statistics.length;
            var NoofVisible = analyticsData.NoofVisibleHV() + 10;
            var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;

            var height = (30 * VisibleElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = (42 * VisibleElements) + 40;
            }
            var charDivElement = $(element).parent().parent().find('#hyviewchart')[0];
            $(charDivElement).attr("height", height + "px");
            analyticsData.NoofVisibleHV(NoofVisible);
        });
    }
}

ko.bindingHandlers.IncreaseHeightFV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () { 
            var analyticsData = valueAccessor();
            console.log(analyticsData);
            var Statistics = analyticsData.Statistics();
            var TotalElements = Statistics.length;
            var NoofVisible = analyticsData.NoofVisibleFV() + 10;
            var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;
            var height = (30 * VisibleElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = 42 * VisibleElements + 40;
            }
            var charDivElement = $(element).parent().parent().find('#freqviewchart')[0];
            $(charDivElement).attr("height", height + "px");
            analyticsData.NoofVisibleFV(NoofVisible);
        });
    }
}


ko.bindingHandlers.FullHeightHV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () {
            var analyticsData = valueAccessor();
            var Statistics = analyticsData.HStatistics();
            var TotalElements = Statistics.length;
            var height = (30 * TotalElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = (42 * TotalElements) + 40;
            }
            var charDivElement = $(element).parent().parent().find('#hyviewchart')[0];
            $(charDivElement).attr("height", height + "px");
            analyticsData.NoofVisibleHV(TotalElements);
        });
    }
}

ko.bindingHandlers.FullHeightEV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () {
            var analyticsData = valueAccessor();
            var Statistics = analyticsData.Statistics();
            var TotalElements = Statistics.length;
            var height = (30 * TotalElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = (42 * TotalElements) + 40;
            }
            var charDivElement = $(element).parent().parent().find('#expviewchart')[0];
            $(charDivElement).attr("height", height + "px");
            analyticsData.NoofVisibleEV(TotalElements);
        });
    }
}

ko.bindingHandlers.FullHeightFV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () {
            var analyticsData = valueAccessor();
            var Statistics = analyticsData.Statistics();
            var TotalElements = Statistics.length;
            var height = (30 * TotalElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = 42 * TotalElements + 40;
            }
            var charDivElement = $(element).parent().parent().find('#freqviewchart')[0];
            $(charDivElement).attr("height", height + "px");
            analyticsData.NoofVisibleFV(TotalElements);
        });
    }
}

ko.bindingHandlers.LoadMoreAttachments = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$data.AnalyticsData().NoofVisible(bindingContext.$data.AnalyticsData().NoofVisible() + 12);
        });
    }
}


ko.bindingHandlers.LoadAllAttachments = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$data.AnalyticsData().NoofVisible(bindingContext.$data.AnalyticsData().AllAttachments().length);
        });
    }
}

ko.bindingHandlers.LoadMoreComments = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.NoofVisible(bindingContext.$root.NoofVisible() + 10);
        });
    }
}

ko.bindingHandlers.LoadAllComments = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.NoofVisible(bindingContext.$root.Comments().length);
        });
    }
}

ko.bindingHandlers.showImagePopup = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.AttachmentDetail.AttachmentURL(bindingContext.$data.FilePath);
            bindingContext.$root.AttachmentDetail.AttachmentComment(bindingContext.$data.CommentText());
            if ($(document).width()>=640)
                $('textarea').css('min-width', '538px').trigger('autosize.resizeIncludeStyle');
            else
                $('textarea').css('min-width', '258px').trigger('autosize.resizeIncludeStyle');
            $("#attachmentModal").modal('show');
        });
    }
}

ko.bindingHandlers.autosize = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).autosize();
        if ($(document).width() <= 320)
            $(element).css('height', '38px');
        else
            $(element).css('height', '72px');
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

function displayStatisticsChartHV(element, data, bindingContext) {
    var width = $(document).width();
    var textLoc = 0;
    var barHeight = 0;
    var fontSize = "13px";
    var barSpace = 0;
    var dy = 45;
    var vBW = 608;
    var margin = { top: 16, right: 16, bottom: 16, left: 16 };
    var iniH = 1;
    var numOfRows = Math.ceil(data.length / 10) * 10;
    var textMidLoc = 0;
    var lineH = 0;

    if (width >= 640) {
        lineH = numOfRows * 43.5;
        width = 608;
        textLoc = 39;
        barHeight = 3;
        barSpace = 20;
        dy = 42;
        fontSize = "16px";
        textMidLoc = 295;
    } else {
        lineH = numOfRows * 30.22;
        width = 288;
        textLoc = 33;
        barHeight = 3;
        barSpace = 20;
        fontSize = "10px";
        dy = 30;
        vBW = 320;
        textMidLoc = 132;
    }

    var w = width - margin.left - margin.right,
        h = width * 3 / 4.08;

    var color = d3.scale.category20().range(CHARTCOLOURS);
    var svg = d3.select(element)
        .append("svg")
        .attr("width", '100%')
        .attr("id", "hyviewchart")
        .attr('viewBox', '0 0 ' + vBW + ' ' + iniH)
        .attr('preserveAspectRatio', 'xMidYMin')
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top * 2 + ")");

    var dx = w / 100;
    // bars
    var bars = svg.selectAll(".bar")
        .data(data)
        .enter()
        .append("rect")
        .attr("fill", function (d, i) { return color(i); })
        .attr("class", function (d, i) { return "horizontal-bar" })
        .attr("x", function (d, i) { return 0; })
        .attr("y", function (d, i) { return (dy * i) + barSpace; })
        .attr("width", function (d, i) { return dx * d.n })
        .attr("height", barHeight);

    // labels
    var text = svg.selectAll("text")
        .data(data)
        .enter()
        .append("text")
        .attr("class", function (d, i) { return "label " + d.label; })
        .attr("x", 5)
        .attr("y", function (d, i) { return dy * i + textLoc; })
        .text(function (d, i) { return i + 1 + ". \xA0\xA0\xA0" + d.label + " (" + d.count + ") - " + d.score.toFixed(1) + "%" })
        .style("font-size", fontSize)
        .style("fill", "#009bdf")
        .style("cursor", "pointer")
        .style("font-weight", function (d, i) { if (d.search == true) return "bold"; else return "normal"; })
        .on("click", function (d, i) {
            var url = document.URL;
            var params = url.split("Detail/");
            var id = params[1];
            $("#customerView").get(0).setAttribute('action', '/Analytics/CustomerView/' + id);

            var searchSurvey = bindingContext.$root.getSearchSurvey();
            var searchSurveyData = ko.toJSON(searchSurvey);

            var cvData = {
                moduleID: typeof (bindingContext.$parent.ModuleID) === 'undefined' ? '' : bindingContext.$parent.ModuleID,
                DGmoduleID: typeof (bindingContext.$parents[1]) === 'undefined' ? '' : bindingContext.$parents[1].ModuleID,
                questionText: typeof (bindingContext.$data.DGQuestionField) === 'undefined' ? '' : bindingContext.$data.DGQuestionField,
                searchSurvey: searchSurveyData,
                keyword: d.label
            }
            var strCVData = ko.toJSON(cvData);
            $("#cvData").val(strCVData);
            $("#customerView").submit();
        });

    var xAxisScale = d3.scale.linear().domain([0, 100]).range([0, w]);
    var yAxisScale = d3.scale.linear().range([0, lineH]);

    var xAxis = d3.svg.axis().scale(xAxisScale).orient("top").tickSize(1);
    var yAxis = d3.svg.axis().scale(yAxisScale).orient("right").ticks(0).tickSize(1);

    var xAxisGroup = svg.append("g").call(xAxis).style("fill", "#8d8d8d");
    var yAxisGroup = svg.append("g").call(yAxis).style("fill", "#8d8d8d");

    svg.append("text")
        .attr("class", "x label")
        .attr("text-anchor", "end")
        .attr("x", textMidLoc)
        .attr("y", -20)
        .text("%").style("font-size", fontSize)
        .style("fill", "#8d8d8d");
}


function displayStatisticsChartEV(element, data, bindingContext) {
    var width = $(document).width();
    var textLoc = 0;
    var barHeight = 0;
    var fontSize = "13px";
    var barSpace = 0;
    var dy = 45;
    var vBW = 608;
    var margin = { top: 16, right: 16, bottom: 16, left: 16 };
    var iniH = 1;
    var numOfRows = Math.ceil(data.length / 10) * 10;
    var textMidLoc = 0;
    var lineH = 0;

    if (width >= 640) {
        lineH = numOfRows * 43.5;
        width = 608;
        textLoc = 39;
        barHeight = 3;
        barSpace = 20;
        dy = 42;
        fontSize = "16px";
        textMidLoc = 295;
    } else {
        lineH = numOfRows * 30.22;
        width = 288;
        textLoc = 33;
        barHeight = 3;
        barSpace = 20;
        fontSize = "10px";
        dy = 30;
        vBW = 320;
        textMidLoc = 132;
    }

    var w = width - margin.left - margin.right,
        h = width * 3 / 4.08;

    var color = d3.scale.category20().range(CHARTCOLOURS);
    var svg = d3.select(element)
        .append("svg")
        .attr("width", '100%')
        .attr("id", "expviewchart")
        .attr('viewBox', '0 0 ' + vBW + ' ' + iniH)
        .attr('preserveAspectRatio', 'xMidYMin')
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top * 2 + ")");

    var dx = w / 100;
    // bars
    var bars = svg.selectAll(".bar")
        .data(data)
        .enter()
        .append("rect")
        .attr("fill", function (d, i) { return color(i); })
        .attr("class", function (d, i) { return "horizontal-bar" })
        .attr("x", function (d, i) { return 0; })
        .attr("y", function (d, i) { return (dy * i) + barSpace; })
        .attr("width", function (d, i) { return dx * d.n })
        .attr("height", barHeight);

    // labels
    var text = svg.selectAll("text")
        .data(data)
        .enter()
        .append("text")
        .attr("class", function (d, i) { return "label " + d.label; })
        .attr("x", 5)
        .attr("y", function (d, i) { return dy * i + textLoc; })
        .text(function (d, i) { return i + 1 + ". \xA0\xA0\xA0" + d.label + " (" + d.count + ") - " + d.score.toFixed(1) + "%" })
        .style("font-size", fontSize)
        .style("fill", "#009bdf")
        .style("cursor", "pointer")
        .style("font-weight", function (d, i) { if (d.search == true) return "bold"; else return "normal"; })
        .on("click", function (d, i) {
             var url = document.URL;
             var params = url.split("Detail/");
             var id = params[1];
             $("#customerView").get(0).setAttribute('action', '/Analytics/CustomerView/' + id);

             var searchSurvey = bindingContext.$root.getSearchSurvey();
             var searchSurveyData = ko.toJSON(searchSurvey);

            var cvData = {
                moduleID: typeof (bindingContext.$parent.ModuleID) === 'undefined' ? '' : bindingContext.$parent.ModuleID ,
                DGmoduleID: typeof (bindingContext.$parents[1]) === 'undefined' ? '' : bindingContext.$parents[1].ModuleID,
                questionText: typeof (bindingContext.$data.DGQuestionField) === 'undefined' ? '' : bindingContext.$data.DGQuestionField,
                searchSurvey: searchSurveyData,
                keyword: d.label
            }
            var strCVData = ko.toJSON(cvData);
            $("#cvData").val(strCVData);
            $("#customerView").submit();
        });
    
    var xAxisScale = d3.scale.linear().domain([0, 100]).range([0, w]);
    var yAxisScale = d3.scale.linear().range([0, lineH]);

    var xAxis = d3.svg.axis().scale(xAxisScale).orient("top").tickSize(1);
    var yAxis = d3.svg.axis().scale(yAxisScale).orient("right").ticks(0).tickSize(1);

    var xAxisGroup = svg.append("g").call(xAxis).style("fill", "#8d8d8d");
    var yAxisGroup = svg.append("g").call(yAxis).style("fill", "#8d8d8d");

    svg.append("text")
        .attr("class", "x label")
        .attr("text-anchor", "end")
        .attr("x", textMidLoc)
        .attr("y", -20)
        .text("%").style("font-size", fontSize)
        .style("fill", "#8d8d8d");
}


/*
function wrap(text, width) {
    console.log("text"+ text+"width "+width);
    text.each(function () {
        var text = d3.select(this),
            words = text.text().split(/\s+/).reverse(),
            word,
            line = [],
            lineNumber = 0,
            lineHeight = 1.1, // ems
            y = text.attr("y"),
            dy = parseFloat(text.attr("dy")),
            tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
        while (word = words.pop()) {
            line.push(word);
            tspan.text(line.join(" "));
            if (tspan.node().getComputedTextLength() > width) {
                line.pop();
                tspan.text(line.join(" "));
                line = [word];
                tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
            }
        }
    });
}
*/

function displayStatisticsChartFV(element, data, bindingContext) {

    var width = $(document).width();
    var textLoc = 0;
    var barHeight = 0;
    var fontSize = "13px";
    var barSpace = 0;
    var dy = 45;
    var vBW = 608;
    var margin = { top: 16, right: 16, bottom: 16, left: 16 };
    var iniH = 1;
    var numOfRows = Math.ceil(data.length / 10) * 10;
    var textMidLoc = 0;
    var lineH = 0;
    var linerH = 0;
    if (width >= 640) {
        lineH = numOfRows * 43.5;
        linerH = data.length * 52;
        width = 608;
        textLoc = 39;
        barHeight = 3;
        barSpace = 20;
        dy = 42;
        fontSize = "16px";
        textMidLoc = 292;
    } else {
        lineH = numOfRows * 30.22;
        width = 288;
        textLoc = 33;
        barHeight = 3;
        barSpace = 20;
        fontSize = "10px";
        dy = 30;
        vBW = 288;
        textMidLoc = 131;
    }
    var w = width - margin.left - margin.right,
        h = width * 3 / 4.08;


    var color = d3.scale.category20().range(CHARTCOLOURS);
    var svg = d3.select(element)
        .append("svg")
        .attr("width", '100%')
        .attr("id", "freqviewchart")
        .attr('viewBox', '0 0 ' + vBW + ' ' + iniH)
        .attr('preserveAspectRatio', 'xMidYMin')
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top * 2 + ")");

    var largest = 1;
    for (var i = 0; i < data.length; i++) {
        if (data[i].n > largest) {
            largest = data[i].n;
        }
    }

    var upperDomain = Math.ceil(largest / 10) * 10;
    var dx = w / upperDomain;

    // bars
    var bars = svg.selectAll(".bar")
        .data(data)
        .enter()
        .append("rect")
        .attr("fill", function (d, i) { return color(i); })
        .attr("class", function (d, i) { return "horizontal-bar" })
        .attr("x", function (d, i) { return 0; })
        .attr("y", function (d, i) { return (dy * i) + barSpace; })
        .attr("width", function (d, i) { return dx * d.n })
        .attr("height", barHeight);

    // labels
    var text = svg.selectAll("text")
        .data(data)
        .enter()
        .append("text")
        .attr("class", function (d, i) { return "label " + d.label; })
        .attr("x", 5)
        .attr("y", function (d, i) { return dy * i + textLoc; })
        .text(function (d, i) { return i + 1 + ". \xA0\xA0\xA0" + d.label + " (" + d.count + ") - " + d.score.toFixed(1) + "%" })
        .style("font-size", fontSize)
        .style("fill", "#009bdf")
        .style("cursor", "pointer")
        .style("font-weight", function (d, i) { if (d.search == true) return "bold"; else return "normal"; })
        .on("click", function (d, i) {
        var url = document.URL;
        var params = url.split("Detail/");
        var id = params[1];
        $("#customerView").get(0).setAttribute('action', '/Analytics/CustomerView/' + id);

        var searchSurvey = bindingContext.$root.getSearchSurvey();
        var searchSurveyData = ko.toJSON(searchSurvey);

        var cvData = {
            searchSurvey: searchSurveyData,
            moduleID: typeof (bindingContext.$parent.ModuleID) === 'undefined' ? '' : bindingContext.$parent.ModuleID,
            DGmoduleID: typeof (bindingContext.$parents[1]) === 'undefined' ? '' : bindingContext.$parents[1].ModuleID,
            questionText: typeof (bindingContext.$data.DGQuestionField) === 'undefined' ? '' : bindingContext.$data.DGQuestionField,
            keyword: d.label
        }
        var strCVData = ko.toJSON(cvData);
        $("#cvData").val(strCVData);
        $("#customerView").submit();
    });

    var xAxisScale = d3.scale.linear().domain([0, upperDomain]).range([0, w]);
    var yAxisScale = d3.scale.linear().range([0, lineH]);

    var xAxis = d3.svg.axis().scale(xAxisScale).orient("top").tickSize(1);
    var yAxis = d3.svg.axis().scale(yAxisScale).orient("left").ticks(0).tickSize(1);

    var xAxisGroup = svg.append("g").call(xAxis).style("fill", "#8d8d8d");
    var yAxisGroup = svg.append("g").attr("class", "y axis").call(yAxis).style("fill", "#8d8d8d");

    svg.selectAll(".y.axis .tick text")
        .style("margin-top", "10px");


    svg.append("text").attr("class", "x label")
        .attr("text-anchor", "end")
        .attr("x", textMidLoc)
        .text("#")
        .attr("y", -20)
        .style("font-size", fontSize)
        .style("fill", "#8d8d8d");
}

ko.bindingHandlers.selectEV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
        $(element).on('multiselectclick', function (event, ui) {
            var analyticsData = valueAccessor();
            var Statistics = analyticsData.Statistics();
            var data = [];

            $.each(Statistics, function (i, eachItem) {
                var eachStat = { "label": eachItem.Text(), "n": parseFloat(eachItem.Score()), "count": eachItem.Count(), "score": parseFloat(eachItem.Score()) }
                data.push(eachStat);
            });

            if (ui.value == 'Lowest to Highest') data.reverse();
            var barDivElement = $(element).parent().find('.barchart')[0];
            $(barDivElement).empty();
            displayStatisticsChartEV(barDivElement, data, bindingContext);

            var TotalElements = Statistics.length;
            var NoofVisible = analyticsData.NoofVisibleEV();
            var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;
            var height = (30 * VisibleElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = (42 * VisibleElements) + 40;
            }
            var charDivElement = $(element).parent().find('#expviewchart')[0];
            $(charDivElement).attr("height", height + "px");
        });
        
    }
};

ko.bindingHandlers.selectHV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
        $(element).on('multiselectclick', function (event, ui) {
            var analyticsData = valueAccessor();
            var Statistics = analyticsData.HStatistics();
            var data = [];

            $.each(Statistics, function (i, eachItem) {
                var eachStat = { "label": eachItem.Text(), "n": parseFloat(eachItem.Score()), "count": eachItem.Count(), "score": parseFloat(eachItem.Score()) }
                data.push(eachStat);
            });

            if (ui.value == 'Lowest to Highest') data.reverse();
            var barDivElement = $(element).parent().find('.barchart')[0];
            $(barDivElement).empty();
            displayStatisticsChartHV(barDivElement, data, bindingContext);

            var TotalElements = Statistics.length;
            var NoofVisible = analyticsData.NoofVisibleHV();
            var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;
            var height = (30 * VisibleElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = (42 * VisibleElements) + 40;
            }
            var charDivElement = $(element).parent().find('#hyviewchart')[0];
            $(charDivElement).attr("height", height + "px");
        });

    }
};

ko.bindingHandlers.selectFV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
        function compareL(a, b) {
            if (a.n < b.n)
                return -1;
            if (a.n > b.n)
                return 1;
            if (a.score < b.score)
                return 1;
            if (a.score > b.score)
                return -1;
            return 0;
        }
        function compareH(a, b) {
            if (a.n < b.n)
                return 1;
            if (a.n > b.n)
                return -1;
            if (a.score < b.score)
                return 1;
            if (a.score > b.score)
                return -1;
            return 0;
        }
        $(element).on('multiselectclick', function (event, ui) {
            var analyticsData = valueAccessor();
            var Statistics = analyticsData.Statistics();
            var data = [];
            $.each(Statistics, function (i, eachItem) {
                var eachStat = { "label": eachItem.Text(), "n": parseFloat(eachItem.Count()), "count": eachItem.Count(), "score": parseFloat(eachItem.Score()) }
                data.push(eachStat);
            });

            if (ui.value == "Highest to Lowest") data.sort(compareH);
            else data.sort(compareL);

            var charDivElement = $(element).parent().find('.barchart')[0];
            $(charDivElement).empty();
            displayStatisticsChartFV(charDivElement, data, bindingContext);

            var TotalElements = Statistics.length;
            var NoofVisible = analyticsData.NoofVisibleEV();
            var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;
            var height = (30 * VisibleElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = (42 * VisibleElements) + 40;
            }
            var charDivElement = $(element).parent().find('#freqviewchart')[0];
            $(charDivElement).attr("height", height + "px");
        });
    }
};


ko.bindingHandlers.IncreaseHeightDemographicEV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () {
            var TotalElements = bindingContext.$data.Statistics().length;
            var NoofVisible = bindingContext.$data.NoofVisibleEV() + 10;
            var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;

            var height = (30 * VisibleElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = (42 * VisibleElements) + 40;
            }
            var charDivElement = $(element).parent().parent().find('#expviewchart')[0];
            $(charDivElement).attr("height", height + "px");
            bindingContext.$data.NoofVisibleEV(NoofVisible);
        });
    }
}


ko.bindingHandlers.IncreaseHeightDemographicFV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () {
            var TotalElements = bindingContext.$data.Statistics().length;
            var NoofVisible = bindingContext.$data.NoofVisibleFV() + 10;
            var VisibleElements = NoofVisible < TotalElements ? NoofVisible : TotalElements;
            var height = (30 * VisibleElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = 42 * VisibleElements + 40;
            }
            var charDivElement = $(element).parent().parent().find('#freqviewchart')[0];
            $(charDivElement).attr("height", height + "px");
            bindingContext.$data.AnalyticsData().NoofVisibleFV(NoofVisible);
        });
    }
}

ko.bindingHandlers.FullHeightDemographicEV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () {
            var VisibleElements = bindingContext.$data.Statistics().length;
            var height = (30 * VisibleElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = (42 * VisibleElements) + 40;
            }
            var charDivElement = $(element).parent().parent().find('#expviewchart')[0];
            $(charDivElement).attr("height", height + "px");
            bindingContext.$data.NoofVisibleEV(VisibleElements);
        });
    }
}

ko.bindingHandlers.FullHeightDemographicFV = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () {
            var VisibleElements = bindingContext.$data.Statistics().length;
            var height = (30 * VisibleElements) + 45;
            var dHeight = $(document).width();
            if (dHeight >= 640) {
                height = 42 * VisibleElements + 40;
            }
            var charDivElement = $(element).parent().parent().find('#freqviewchart')[0];
            $(charDivElement).attr("height", height + "px");
            bindingContext.$data.NoofVisibleFV(VisibleElements);
        });
    }
}

ko.bindingHandlers.autoComplete = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).autocomplete({
            source: function (request, response) {
                var data = ko.toJSON({
                    OrgSubID: bindingContext.$root.Survey().OrgSub().OrgSubID,
                    TouchpointName: bindingContext.$root.Survey().SurveyConfigs().length > 0 ? bindingContext.$root.Survey().SurveyConfigs()[0].TouchpointName : '',
                    QueryTerm: request.term
                });
                $.ajax({
                    type: "POST",
                    url: "/api/STAFF/UserNames",
                    contentType: "application/json",
                    data: data,
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                bindingContext.$data.STAFFEntryText(ui.item.value);
                $(element).val(ui.item.value);
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
    },
    update: function (element) {
    }
};


function AttachmentDetailModel() {
    var self = this;
    self.AttachmentURL = ko.observable();
    self.AttachmentComment = ko.observable();
}