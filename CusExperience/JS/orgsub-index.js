﻿
ko.bindingHandlers.displayText = {
    init: function (element, valueAccessor) {
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).text($('<div>').append(value).text());
    }
};


ko.bindingHandlers.deleteOrg = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.deleteOrg(bindingContext.$index(), bindingContext.$data, bindingContext.$parent);
        });
    }
}

ko.bindingHandlers.deleteSub = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.deleteSub(bindingContext.$index(), bindingContext.$data, bindingContext.$parent);
        });
    }
}


function AppViewModel() {
    var self = this;
    self.OrgSubs = ko.observableArray(initJSONData != null ? $.map(initJSONData, function (item) { return new OrgSubModel(item) }) : []);
    self.hierarchyName = '';
    var url = document.URL;

    self.createOrg = function () {
        return "/OrgSub/Create";
    }

    self.createSub = function (data) {
        return "/OrgSub/CreateSub/" + data.OrgSubID();
    }

    self.showOrgSub = function (data) {
        return "/OrgSub/Detail/" + data.OrgSubID();
    }

    self.manageSurveys = function (data) {
        return "/Survey/Index/" + data.OrgSubID();
    }

    self.analyzeSurveys = function (data) {
        return "/Analytics/Index/" + data.OrgSubID();
    }

    self.deleteOrg = function (index, data, parent) {
        var r = confirm("All the Users in this Organisation will be deleted. Do you want to delete the Organisation?");
        if (r == true) {
             $.ajax({
                url: '/api/DeleteOrgSub/' + data.OrgSubID(),
                type: 'DELETE',
                success: function () { 
                    window.location = "/OrgSub"
                }, error: function (result) {
                }
            }); 
        }
    }

    self.deleteSub = function (index, data, parent) {
        var r = confirm("All the Users in this Organisation will be deleted. Do you want to delete the Organisation?");
        if (r == true) {
            $.ajax({
                url: '/api/DeleteOrgSub/' + data.OrgSubID(),
                type: 'DELETE',
                success: function () {
                     window.location = "/OrgSub"
                }, error: function (result) {
                } 
            });  
        }
    }
}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('body-content'));
    $('body').show();
});