﻿ko.bindingHandlers.displayText = {
    init: function (element, valueAccessor) {
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).text($('<div>').append(value).text());
    }
};

function AppViewModel() {
    var self = this;
    self.SurveyInfos = ko.observableArray(initJSONData);
    var url = document.URL;
    self.HomeURL = url.substring(0, url.indexOf("/Public"));
    self.HomeURL = self.HomeURL.replace("cusexperience", "CusExperience");
}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('body-content'));
    $('body').show();
});