﻿/* Binding Handler to flip an Image */
ko.bindingHandlers.flipImage = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var toggleDiv = $(element).parent().parent().find('#flip-toggle');
            toggleDiv.toggleClass('flip');
        });
    }
}

ko.bindingHandlers.drag = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).css('top', bindingContext.$data.TopPos());
        $(element).css('left', bindingContext.$data.LeftPos());
        var dragElement = $(element);
        dragElement.draggable({
            stop: function () {
                if ($(document).width <= 320) {
                    bindingContext.$data.TopPos($(element).css('top') * 2);
                    bindingContext.$data.LeftPos($(element).css('left') * 2);
                } else {
                    bindingContext.$data.TopPos($(element).css('top'));
                    bindingContext.$data.LeftPos($(element).css('left'));
                }
            }
        });
    }
};

ko.bindingHandlers.fileUpload = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).change(function () {
            var options = ko.utils.unwrapObservable(valueAccessor()),
            property = ko.utils.unwrapObservable(options.property),
            url = ko.utils.unwrapObservable(options.url);
            component = ko.utils.unwrapObservable(options.component);
            if (property && url) {
                if (element.files.length) {
                    var $this = $(this),
                        fileName = $this.val();
                    var formElement = $(element).closest('form');
                    var formData = new FormData(formElement[0]);
                    $.ajax({
                        url: '/api/PostTouchpointImage/' + bindingContext.$root.OrgSubID(),
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            switch (component) {
                                case "FrontImage":
                                    bindingContext.$data.FrontImage(data.FilePath);
                                    break;
                                case "LogoImage":
                                    bindingContext.$data.LogoImage(data.FilePath);
                                    break;
                            }
                            $(element).val('');
                            bindingContext.$data.SliderValue(50);
                            bindingContext.$data.LeftPos('0px');
                            bindingContext.$data.TopPos('0px');
                        },
                        error: function () {
                            alert('Error in uploading File. Please try again.');
                        }
                    });
                }
            }
        });
    }
};


ko.bindingHandlers.slider = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var options = allBindingsAccessor().sliderOptions || {};
        $(element).slider(options);
        ko.utils.registerEventHandler(element, "slidechange", function (event, ui) {
            var observable = valueAccessor();
            observable(ui.value);
        })
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (isNaN(value)) value = 0;
        $(element).slider("option", allBindingsAccessor().sliderOptions);
        $(element).slider("value", value);
    }
};


ko.bindingHandlers.editableText = {
    init: function (element, valueAccessor) {
        $(element).on('blur', function () {
            var observable = valueAccessor();
            observable($(this).html());
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var div = $('<div>').html(value);
        div.find('*').each(function () {
            if ($(this).is('[data-bind]')) {
                $(this).removeAttr('data-bind');
            }
        });
        value = div.html();
        $(element).html(value);
    }
};

function AppViewModel() {
    var self = this;
    self.Touchpoints = ko.observableArray($.map(initJSONData, function (item) { return new TouchpointModel(item) }));
    var url = document.URL;
    var params = url.split("Touchpoint/");
    var id = params[1];
    self.OrgSubID = ko.observable(id);

    self.showOrgSub = function (data) {
        return "/OrgSub/Detail/" + id;
    }

    self.showTouchpoint = function () {
        return "/OrgSub/Touchpoint/" + id;
    }

    self.showUsers = function () {
        return "/Users/" + id;
    }

    self.addTouchpoint = function (index, data) {
        var addedTouchpoint = new TouchpointModel(null);
        self.Touchpoints.push(addedTouchpoint);
    }

    self.removeTouchpoint = function (data) {
        if (confirm("Touchpoint would be deleted permanently. are you sure you want to delete the touchpoint? ")) {
            self.Touchpoints.remove(data);
        }
    }

    self.saveTouchpoints = function () {
        var data = ko.toJSON(self.Touchpoints());
        $('#savebutton').text("Saving");

        $.ajax({
            type: "POST",
            url: '/api/PostTouchpoints/' + id,
            data: data,
            contentType: 'application/json'


        }).success(function (allData) {
            $('#savebutton').addClass("btn-success");
            $('#savebutton').text("Saved");
            $('#savebutton').attr("disabled", true);

            setTimeout(function () {
                $('#savebutton').text("Save");
                $('#savebutton').removeClass("btn-success");
                $('#savebutton').attr("disabled", false);
            }, 5000);


        }).fail(function (allDate) {
            $('#savebutton').addClass("btn-fail");
            $('#savebutton').text("Save Fail");
            $('#savebutton').attr("disabled", true);

            setTimeout(function () {
                $('#savebutton').text("Save");
                $('#savebutton').removeClass("btn-fail");
                $('#savebutton').attr("disabled", false);
            }, 5000);
        });
    }
}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('body-content'));
    $('body').show();
});
