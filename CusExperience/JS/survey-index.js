﻿
ko.bindingHandlers.enterKey = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        
        
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var originalFunction = valueAccessor();
        $(element).on('input', function (event) {
            var keyCode = (event.which ? event.which : event.keyCode);      
                return originalFunction.call(viewModel);
        });
    }
};


ko.bindingHandlers.displayText = {
    init: function (element, valueAccessor) {
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).text($('<div>').append(value).text());
    }
};


ko.bindingHandlers.deleteSurvey = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.deleteSurvey(bindingContext.$index(), bindingContext.$data);
        });
    }
}


ko.bindingHandlers.duplicateSurvey = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.duplicateSurvey(bindingContext.$index(), bindingContext.$data);
        });
    }
}

ko.bindingHandlers.pagination = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).pagination({
            items: bindingContext.$root.Surveys.ItemsCount(),
            itemsOnPage: bindingContext.$root.Surveys.ItemsPerPage(),
            currentPage: bindingContext.$root.Surveys.CurrentPage(),
            displayedPages: 3,
            cssStyle: "light-theme",
            onPageClick: bindingContext.$root.Surveys.pageClick
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
};

function AppViewModel() {
    var self = this;
    self.Surveys = new ListModel(initJSONData);
    self.BaseURL = ko.observable(null);
    self.HierarchyURL = ko.observable(null);
    var url = document.URL;
    self.HomeURL = url.substring(0, url.indexOf("/Survey"));
    self.HomeURL = self.HomeURL.replace("cusexperience", "CusExperience");
    var params = url.split("Index/");
    var id = params[1];
    if (self.Surveys.Items().length > 0) {
        self.BaseURL(self.Surveys.Items()[0].OrgSub.BaseURL);
        self.HierarchyURL(self.Surveys.Items()[0].OrgSub.HierarchyURL);
    }

    self.FullPublishedURL = function (data) {
        return ko.computed({
            read: function () {
                var URL = self.HomeURL + (self.BaseURL() !== null && self.BaseURL() !== '' ? '/' + self.BaseURL() : '') + "/" + data.PublishedID + (self.HierarchyURL() !== null && self.HierarchyURL() !== '' ? '/' + self.HierarchyURL() : '');
                return  URL;
            }
        })
    }

    self.openlinkinNewTab = function (data) {
        d = new Date();
        var URL = self.HomeURL + "/" + data.PublishedURL;
        if (data.PublishedID !== "") URL = URL + "/" + data.PublishedID;
        var x = window.open(URL, "Window_" + d.getMilliseconds());
        x.focus();
    }

    self.createSurvey = function (data) {
        if(typeof(id)==='undefined' || id==null) 
            return "/Survey/Create";
        else  
            return "/Survey/Create/" + id;
    }

    self.editSurvey = function (data) {
        var d = new Date();
        if (!data.IsTemplate) {
            return "/Survey/Detail/" + id +  "/" + data.SurveyID;
        } else {
            if (typeof (id) === 'undefined' || id == null) {
                return "/Survey/Create/" + data.TemplateName;
            }
            else {
                return "/Survey/Create/" + id + "/"+data.TemplateName;
            }
        }
    }

    self.afterAdd = function (element, index, data) {
    }

    self.duplicateSurvey = function (index, data) {
        $.ajax({
            url: '/api/GetDuplicateSurvey/' + data.SurveyID,
            type: 'GET',
            success: function (allData) {
                $.when(self.Surveys.CurrentItems.splice(index + 1, 0, allData)).then(function () {
                        $(".survey").find(".list-div").eq(index + 1).addClass("duplicated-survey");
                        setTimeout(function () {
                            $(".survey").find(".list-div").eq(index + 1 ).removeClass("duplicated-survey");
                        }, 5000);
                        $.scrollTo($(".survey").find(".list-div").eq(index + 1).offset().top - 55, 100);
                        self.Surveys.Items.push(allData);
                });
            }, error: function (result) {
                console.log(ko.toJSON(result));
            }
        });
    }
     
    self.deleteSurvey = function (index, data) {
      var HTMLST = data.SurveyTitle;
      var cleanText = HTMLST.replace(/<\/?[^>]+(>|$)/g, "");
      cleanText = cleanText.replace("&nbsp;", "");
        var r = confirm("Do you want to delete \"" + cleanText + "\"?");
        if (r == true) {
            $.ajax({
                url: '/api/DeleteSurvey/' + data.SurveyID,
                type: 'DELETE',
                success: function (result) {
                    self.Surveys.CurrentItems.splice(index, 1);
                    self.Surveys.Items.remove(data);
                }, error: function (result) {
                    alert("Error in Deleting Survey. Please try again or refresh the page.");
                }
            });
        }
    }
}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('body-content'));
    $('body').show();
});