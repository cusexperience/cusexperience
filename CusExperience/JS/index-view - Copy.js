﻿ko.bindingHandlers.showDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var OverallRating = bindingContext.$data.AnalyticsData().OverallRating();
        var dataset = {
            rating: [OverallRating, 100 - OverallRating]
        };
        var width = $(element).width() - 14,
                height = $(element).height(),
                radius = Math.min(width, height) / 2;

        var color = d3.scale.category20().range(RATECHARTCOLOURS);

        var degree = Math.PI / 180; // just to convert the radian-numbers
        var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

        var arc = d3.svg.arc()
            .innerRadius(radius - 17)
            .outerRadius(radius);

        var svg = d3.select(element).append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var path = svg.selectAll("path")
            .data(pie(dataset.rating))
            .enter().append("path")
            .attr("fill", function (d, i) {
                return color(i);
            })
            .attr("d", arc);

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.showChoiceDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var choiceWCounts = bindingContext.$data.AnalyticsData().ChoiceWCounts();
        var doughnutData = [];
        $.each(choiceWCounts, function (i, choiceWCount) {
            var doughnutSegment = { value: parseFloat(choiceWCount.Percentage()), color: CHARTCOLOURS[i] }
            doughnutData.push(doughnutSegment);
        });

        var doughnutDataSort = [];
        $.each(choiceWCounts, function (i, choiceWCount) {
            doughnutDataSort.push(parseFloat(choiceWCount.Percentage()));
        });

        var width = $(element).width(),
                height = $(element).height(),
                radius = Math.min(width, height) / 2;

        var dataset = {
            choiceSelection: doughnutDataSort,
        };

        var color = d3.scale.category20().range(CHARTCOLOURS);

        var degree = Math.PI / 180; // just to convert the radian-numbers
        var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

        var arc = d3.svg.arc()
            .innerRadius(radius - 17)
            .outerRadius(radius);

        var svg = d3.select(element).append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var path = svg.selectAll("path")
            .data(pie(dataset.choiceSelection))
            .enter().append("path")
            .attr("fill", function (d, i) { return color(i); })
            .attr("d", arc);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.showMultipleRatingDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var index = bindingContext.$index();
        if (bindingContext.$parent.AnalyticsData().AnalyticsItems().length > 0 && !$.isEmptyObject(bindingContext.$parent.AnalyticsData().AnalyticsItems()[index])) {
            var OverallRating = bindingContext.$parent.AnalyticsData().AnalyticsItems()[index].OverallRating();

            var dataset = {
                rating: [OverallRating, 100 - OverallRating]
            };

            var width = $(element).width(),
                height = $(element).height(),
                radius = Math.min(width, height) / 2;

            var color = d3.scale.category20().range(RATECHARTCOLOURS);

            var degree = Math.PI / 180; // just to convert the radian-numbers
            var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

            var arc = d3.svg.arc()
                .innerRadius(radius - 17)
                .outerRadius(radius);

            var svg = d3.select(element).append("svg")
                .attr("width", width)
                .attr("height", height)
                .append("g")
                .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

            var path = svg.selectAll("path")
                .data(pie(dataset.rating))
                .enter().append("path")
                .attr("fill", function (d, i) {
                    return color(i);
                })
                .attr("d", arc);
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}


ko.bindingHandlers.showTouchpointBarChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        /*
        var width = $(element).width();
        var textLoc = 0;
        var barHeight = 0;
        var fontSize = "13px";
        var barSpace = 0;
        var dy = 45;
        var margin = { top: 16, right: 16, bottom: 16, left: 16 };
        if (width > 320) {

            textLoc = 76 ;
            barHeight = 20 * 2;
            barSpace = 50;
            dy = 42*2;
            fontSize = "20px";
            
        } else {
            margin = { top: 8, right: 8, bottom: 8, left: 8 };
            textLoc = 64;
            barHeight = 20;
            barSpace = 50;
            dy = 42;
            fontSize = "13px";
            
        }
        */
        var width = $(element).width();
        var textLoc = 0;
        var barHeight = 0;
        var fontSize = "13px";
        var barSpace = 0;
        var dy = 45;
        var margin = { top: 16, right: 16, bottom: 16, left: 16 };
        if (width > 300) {

            textLoc = 64;
            barHeight = 20;
            barSpace = 50;
            dy = 42;
            fontSize = "13px";
        } else {
            margin = { top: 8, right: 8, bottom: 8, left: 8 };
            textLoc = 28;
            barHeight = 10;
            barSpace = 20;
            fontSize = "10px";
            dy = 20;
        }
        var w = width - margin.left - margin.right,
		    h = width * 3 / 4.08;

        var color = d3.scale.category20().range(CHARTCOLOURS);

        var svg = d3.select(element)
			.append("svg")
			.attr("width", w + margin.left + margin.right)
			.attr("height", h + margin.top + margin.bottom + margin.top)
            .attr("id", "tpchart")
        
        .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top*2+ ")");

        var TouchpointStats = bindingContext.$root.Survey().TouchpointAnalyticsData().TouchpointStats();
        var data = [];

        $.each(TouchpointStats, function (i, TouchpointStat) {
            var eachStat = { "label": TouchpointStat.TPName(), "n": TouchpointStat.TPCount() }
            data.push(eachStat);
        });
        var largest = 0;
        for (var i = 0; i < data.length;i++){
          
            if (data[i].n > largest) {
                largest = data[i].n;
            }
           
        }
        var upperDomain =Math.round(largest / 10) * 10;
        console.log(upperDomain);
        var dx = w / upperDomain;
       

        // bars
        var bars = svg.selectAll(".bar")
            .data(data)
            .enter()
            .append("rect")
            .attr("fill", function (d, i) { return color(i); })
            .attr("class", function (d, i) { return "horizontal-bar" })
            .attr("x", function (d, i) { return 0; })
            .attr("y", function (d, i) { return (dy * i) + barSpace; })
            .attr("width", function (d, i) { return dx * d.n })
            .attr("height", barHeight);

        // labels
        var text = svg.selectAll("text")
            .data(data)
            .enter()
            .append("text")
            .attr("class", function (d, i) { return "label " + d.label; })
            .attr("x", 20)
            .attr("y", function (d, i) { return dy * i + textLoc; })
            .text(function (d) { return d.label + " - " + d.n })
            .style("font-size", fontSize);

        var xAxisScale = d3.scale.linear().domain([0, upperDomain]).range([0, w]);
        var yAxisScale = d3.scale.linear().range([0, h]);

        var xAxis = d3.svg.axis().scale(xAxisScale).orient("top").tickSize(1);
        var yAxis = d3.svg.axis().scale(yAxisScale).orient("right").ticks(0).tickSize(1);

        var xAxisGroup = svg.append("g").call(xAxis);
        var yAxisGroup = svg.append("g").call(yAxis);

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

    var chart = $("#chart"),
    aspect = 608 / 429,
    container = chart.parent();
        console.log($(window).width());
        $(window).on("resize", function () {
            var targetWidth = 0;
            if ($(window).width() < 640) {
                targetWidth = 288;
            } else {
                targetWidth = 608;
            }
        
            chart.attr("width", targetWidth);
            chart.attr("height", Math.round(targetWidth / aspect));
            console.log(targetWidth);
            console.log(Math.round(targetWidth / aspect));
        }).trigger("resize");
        $("#tpchart").attr("width", 400);


ko.bindingHandlers.showDirectoryBarChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
       
        var width = $(element).width();
        var textLoc = 0;
        var barHeight = 0;
        var fontSize = "13px";
        var barSpace = 0;
        var dy = 45;
        var margin = { top: 16, right: 16, bottom: 16, left: 16 };
        if (width > 300) {
           
            textLoc = 64;
            barHeight = 20;
            barSpace = 50;
            dy = 42;
            fontSize = "13px";
        } else {
            margin = { top: 8, right: 8, bottom: 8, left: 8 };
            textLoc = 28;
            barHeight = 10;
            barSpace = 20;
            fontSize = "10px";
            dy = 20;
        }
        var w = width - margin.left - margin.right,
		    h = width * 3 / 4.08 ;
     

        var color = d3.scale.category20().range(CHARTCOLOURS);

        var svg = d3.select(element)
			.append("svg")
			.attr("width", w + margin.left + margin.right )
			.attr("height", h + margin.top + margin.bottom )
        .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
     

        var DirectoryStats = bindingContext.$data.AnalyticsData().DirectoryStats();
        var data = [];

        $.each(DirectoryStats, function (i, DirectoryStat) {
            var eachStat = { "label": DirectoryStat.DEText() , "n": parseFloat(DirectoryStat.DEScore()), "count" : DirectoryStat.DECount() }
            data.push(eachStat);
        });

            var dx = w / 100;
            

            // bars
            var bars = svg.selectAll(".bar")
				.data(data)
				.enter()
				.append("rect")
				.attr("fill", function (d, i) { return color(i); })
                .attr("class", function(d, i) { return "horizontal-bar"})
				.attr("x", function (d, i) { return 0; })
				.attr("y", function (d, i) { return (dy * i) + barSpace; })
				.attr("width", function (d, i) { return dx * d.n })
				.attr("height", barHeight);

            // labels
            var text = svg.selectAll("text")
				.data(data)
				.enter()
				.append("text")
				.attr("class", function (d, i) { return "label " + d.label; })
				.attr("x", 10)
				.attr("y", function (d, i) { return dy * i + textLoc; })
				.text(function (d) { return d.label + " - " + d.n.toFixed(1) + " % (" + d.count + ")"})
				.style("font-size", fontSize);

          var xAxisScale = d3.scale.linear().domain([0, 100]).range([0, w]);
          var yAxisScale = d3.scale.linear().range([0, h]);

          var xAxis = d3.svg.axis().scale(xAxisScale).tickSize(1);
          var yAxis = d3.svg.axis().scale(yAxisScale).orient("right").ticks(0).tickSize(1);

          var xAxisGroup = svg.append("g").call(xAxis);
          var yAxisGroup = svg.append("g").call(yAxis);

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.multiSelectSingle = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        $(element).bind("multiselectclick", function (event, ui) {
            event.preventDefault();
        });
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiSelect);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
        $(element).bind("multiselectclick", function (event, ui) {
            event.preventDefault();
        });
    }
};

ko.bindingHandlers.multiSelectMultiple = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        $(element).bind("multiselectclick", function (event, ui) {
            event.preventDefault();
        });
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiSelect);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
        $(element).bind("multiselectclick", function (event, ui) {
            event.preventDefault();
        });
    }
};

ko.bindingHandlers.showMultipleChoiceDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var index = bindingContext.$index();
        if (bindingContext.$parent.AnalyticsData().AnalyticsItems().length > 0) {
            var choiceWCounts = bindingContext.$parent.AnalyticsData().AnalyticsItems()[index].ChoiceWCounts();
            var doughnutData = [];
            $.each(choiceWCounts, function (i, choiceWCount) {

                doughnutData.push(parseFloat(choiceWCount.Percentage()));
            });
            var dataset = {
                rating: doughnutData
            };

            var width = $(element).width(),
                    height = $(element).height(),
                    radius = Math.min(width, height) / 2;
            var color = d3.scale.category20().range(CHARTCOLOURS);

            var degree = Math.PI / 180; // just to convert the radian-numbers
            var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

            var arc = d3.svg.arc()
                .innerRadius(radius - 17)
                .outerRadius(radius);

            var svg = d3.select(element).append("svg")
                .attr("width", width)
                .attr("height", height)
                .append("g")
                .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

            var path = svg.selectAll("path")
                .data(pie(dataset.rating))
                .enter().append("path")
                .attr("fill", function (d, i) {
                    return color(i);
                })
                .attr("d", arc);
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.showComments = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.showComments(bindingContext.$data.SRIDs, bindingContext.$parents[1].ModuleID(), bindingContext.$data.Word());
        });
    }
}

ko.bindingHandlers.toggleDemographicInfo = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {

            if ($(element).find('span').hasClass("icon-plus")) {
                $(element).parent().find('.demodiv').show();
                $(element).find('span').removeClass("icon-plus");
                $(element).find('span').addClass("icon-minus");
            } else {
                $(element).find('span').removeClass("icon-minus");
                $(element).find('span').addClass("icon-plus");
                $(element).parent().find('.demodiv').hide();
            }
        });
    }
}

ko.bindingHandlers.showCommentsByTag = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.showCommentsByTag(bindingContext.$data.SRIDs, bindingContext.$parents[1].ModuleID(), bindingContext.$data.TagName());
        });
    }
}

ko.bindingHandlers.initNoofVisible = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).on('show.bs.modal', function (e) {
            bindingContext.$root.NoofVisible(10);
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        
    }
}


ko.bindingHandlers.LoadMore = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$data.AnalyticsData().NoofVisible(bindingContext.$data.AnalyticsData().NoofVisible() + 10);
        });
    }
}


ko.bindingHandlers.LoadMoreAttachments = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$data.AnalyticsData().NoofVisible(bindingContext.$data.AnalyticsData().NoofVisible() + 12);
        });
    }
}

ko.bindingHandlers.LoadMoreComments = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.NoofVisible(bindingContext.$root.NoofVisible() + 10);
        });
    }
}

ko.bindingHandlers.showImagePopup = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.AttachmentDetail.AttachmentURL(bindingContext.$data.FilePath);
            bindingContext.$root.AttachmentDetail.AttachmentComment(bindingContext.$data.CommentText());
             $("#attachmentModal").find('textarea').autosize({ append: "\n" }).show().trigger('autosize.resizeIncludeStyle');
            $("#attachmentModal").modal('show');
       });
    }
}

ko.bindingHandlers.autosize = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).autosize();
    }
}

function AttachmentDetailModel() {
    var self = this;
    self.AttachmentURL = ko.observable();
    self.AttachmentComment = ko.observable();
}

function IndexViewModel() {
    var self = this;
    self.Survey = ko.observable(null);
    self.Comments = ko.observableArray([]);
    self.NoofVisible = ko.observable(10);
    self.AttachmentDetail = new AttachmentDetailModel();

    var url = document.URL;
    var params = url.split("Detail/");
    var id = params[1];
    var urlString = "/api/GetAnalytics/" + id;

    self.wordHeight = function (data) {
        var random = Math.floor((Math.random() * 7) + 1);
        if (random == 1) return "grid3-font";
        else if (random == 2) return "grid3-font";
        else if (random == 3) return "grid3-font";
        else if (random == 4) return "grid3-font";
        else if (random == 5) return "grid3-font";
        else if (random == 6) return "grid3-font";
        else "grid7-font";
    };

    self.loadAnalytics = function () {
        if(self.Survey() == null){
            $.ajax({
                url: urlString,
                type: "GET",
                headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
            }).success(function (allData) {
                $('#loading-spinner').hide();
                //alert(ko.toJSON(allData));
                self.Survey(new SurveyModel(allData, false, true, false));
                $("#view").load("/Analytics/IndexView/", function () {
                    ko.cleanNode(document.getElementById('view'));
                    ko.applyBindings(self, document.getElementById('view'));
                });
            }).fail(function (httpObj, data) {
                
            });
        } else {
            $("#view").load("/Analytics/IndexView/", function () {
                ko.cleanNode(document.getElementById('view'));
                ko.applyBindings(self, document.getElementById('view'));
            });
        }
    }
    
    self.loadSearchAnalytics = function (searchSurvey,element){
        var data = ko.toJSON(searchSurvey);
        $.ajax({
            type: "POST",
            data: data,
            url: "/api/GetSearchAnalytics",
            contentType: "application/json"
        }).success(function (allData) {
            self.Survey(new SurveyModel(allData, false, true, false));
            $("#view").load("/Analytics/IndexView/", function () {
                ko.cleanNode(document.getElementById('view'));
                ko.applyBindings(self, document.getElementById('view'));
            });
            $(element).addClass("search-button-success");
            $(element).text("Results Found");
            $(element).delay(3000).queue(function (n) {
                $(element).text("Search");
                $(element).removeClass("search-button-success");
                n();
            });
        }).fail(function (data) {
            alert(ko.toJSON(data));
            $(element).addClass("search-button-fail");
            $(element).text("Results Not Found");
            $(element).delay(3000).queue(function (n) {
                $(element).text("Search");
                $(element).removeClass("search-button-fail");
                n();
            });
        });
    }

    self.showResponseView = function (data) {
        var responseURL = "/Analytics/SurveyResponse/" + data.SurveyResponseID;
        var x = window.open(responseURL, responseURL);
        x.focus();
    }

    self.showComments = function (SRIDs, moduleID, keyWord) {
        self.Comments([]);
        $("#commentModal").modal('show');
        $("#comments-loading-bar").show();
        var data = { moduleID: moduleID, SRIDs: SRIDs, keyWord: keyWord } ;
        var commentURLString = "/api/GetComments";
        $.ajax(commentURLString, { 
            type: "POST",
            data: data,
            headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
        }).success(function (allData) {
            self.Comments($.map(allData, function(key,value) { return new CommentModel(key,value) }));
            $("#comments-loading-bar").hide();
        }).fail(function (httpObj, data) {
        });
    }

    self.showCommentsByTag = function (SRIDs, moduleID, tagName) {
        self.Comments([]);
        $("#commentModal").modal('show');
        $("#comments-loading-bar").show();
        var data = { moduleID: moduleID, SRIDs: SRIDs, tagName: tagName };
        var commentURLString = "/api/GetCommentsByTag";
        $.ajax(commentURLString, {
            type: "POST",
            data: data,
            headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
        }).success(function (allData) {
            self.Comments($.map(allData, function (key, value) { return new CommentModel(key, value) }));
            $("#comments-loading-bar").hide();
        }).fail(function (httpObj, data) {
        });
    }
}

