﻿ko.bindingHandlers.checkBox = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var observable = valueAccessor();
        if (observable() == true) {
            $(element).removeClass("icon-check-empty");
            $(element).addClass("icon-check-sign");
        } else if (observable() == false) {
            $(element).removeClass("icon-check-sign");
            $(element).addClass("icon-check-empty");
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}

ko.bindingHandlers.selectTag = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if($.inArray(bindingContext.$index(), bindingContext.$parent.SearchData().SelectedTags()) > -1)
                $(element).toggleClass('tag-selected');
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}

ko.bindingHandlers.chooseRatingRange = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (bindingContext.$parent.SearchData().ToRating() == null) bindingContext.$parent.SearchData().ToRating(bindingContext.$parent.QuestionData().NumLimit());
        if (bindingContext.$parent.SearchData().FromRating() <= bindingContext.$data && bindingContext.$parent.SearchData().ToRating() >= bindingContext.$data) {
            var ModuleData = bindingContext.$parent;
            $(element).removeClass(ModuleData.QuestionData().RateType());
            $(element).addClass(ModuleData.QuestionData().RateType() + '-Selected');
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}

ko.bindingHandlers.resetSTAFFNames = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}


function ReportViewModel(data) {
    var self = this;
    self.Comments = ko.observableArray(data!=null && $.isEmptyObject(data.Comments)? data.Comments : []);
    self.NoofVisible = ko.observable(data != null ? data.NoofVisible : 10);
    self.AttachmentDetail = new AttachmentDetailModel();
    self.AnalyticsTitle = ko.observable(null);
    self.Questions = ko.observableArray(data != null ? data.Questions : [{ value: "All", key: "All" }]);
    self.SelectedQuestions = ko.observableArray(data != null ? data.SelectedQuestions : []);
    self.Touchpoints = ko.observableArray([]);
    self.PublishedSurvey = ko.observable(data != null ? new SurveyModel(data.PublishedSurvey, false, false, true) : null);
    self.AnalyticsTitle(self.PublishedSurvey().SurveyTitle() + " Analytics Report");
    self.Survey = ko.observable(data != null ? new SurveyModel(data.Survey, false, true, false): null);
    self.OrderByOptions = ko.observableArray(['Lowest to Highest', 'Highest to Lowest']);
    self.IsTouchpointViewOpen = ko.observable(data != null ? data.IsTouchpointViewOpen : false);
    self.IsQuestionViewOpen = ko.observable(data != null ? data.IsQuestionViewOpen : false);
    self.IsDemographicViewOpen = ko.observable(data != null ? data.IsDemographicViewOpen : false);
    self.DGQuestionFields = ko.observableArray(data != null ? data.DGQuestionFields : []);
    self.SelectedDGQuestionFields = ko.observableArray(data != null ? data.SelectedDGQuestionFields : []);
    self.CurrentUser = ko.observable(data != null  && !$.isEmptyObject(data.CurrentUser) ? data.CurrentUser : new UserModel(data.CurrentUser));
    self.ExpertCommentCounts = ko.observableArray(data != null ? data.ExpertCommentCounts : []);

    var url = document.URL;
    var params = url.split("Detail/");
    var id = params[1];

    self.loadCustomerView = function () {
        window.location.href = "/Analytics/CustomerView/" + id;
    }

    self.loadSocialView = function () {
        window.location.href = "/Analytics/SocialView/" + id;
    }

    self.showSearchResults = function (element) {
        $(element).blur();
        $(element).removeClass('hover');
        $(element).trigger('mouseup');
        $(element).unbind("mouseenter mouseleave");
        $(element).text("Searching...");
        $(element).trigger('mouseup');

        var searchSurvey = new SurveyModel(null, false, false, true);
        searchSurvey.SurveyID = self.PublishedSurvey().SurveyID;
        searchSurvey.IsAllowViewResults = self.PublishedSurvey().IsAllowViewResults;
        searchSurvey.IsAllowViewSocial = self.PublishedSurvey().IsAllowViewSocial;
        searchSurvey.FromDateTime = self.PublishedSurvey().FromDateTime;
        searchSurvey.ToDateTime = self.PublishedSurvey().ToDateTime;
        searchSurvey.Keywords = self.PublishedSurvey().Keywords;
        searchSurvey.WOKeywords = self.PublishedSurvey().WOKeywords;
        searchSurvey.SelectedTouchpoints = self.PublishedSurvey().SelectedTouchpoints;

        for (var index in self.PublishedSurvey().Modules()) {
            if (self.PublishedSurvey().Modules()[index].ModuleType() == MODULETYPES.QUESTION) {
                if (self.PublishedSurvey().Modules()[index].ModuleData().QuestionType() != QUESTIONTYPES.DEMOGRAPHIC) {
                    if ($.inArray(self.PublishedSurvey().Modules()[index].ModuleID(), self.SelectedQuestions()) > -1) {
                        searchSurvey.Modules.push(self.PublishedSurvey().Modules()[index]);
                    }
                }
                else {
                    searchSurvey.Modules.push(self.PublishedSurvey().Modules()[index]);
                }
            }
        }

        var data = ko.toJSON(searchSurvey);
        $.ajax({
            type: "POST",
            data: data,
            url: "/api/GetSearchAnalytics",
            contentType: "application/json"
        }).success(function (allData) {
            self.Survey(new SurveyModel(allData, false, true, false));
            $(element).addClass("search-button-success");
            $(element).text("Results Found");
            $(element).delay(3000).queue(function (n) {
                $(element).text("Search");
                $(element).removeClass("search-button-success");
                n();
            });
        }).fail(function (data) {
            alert(ko.toJSON(data));
            $(element).addClass("search-button-fail");
            $(element).text("Results Not Found");
            $(element).delay(3000).queue(function (n) {
                $(element).text("Search");
                $(element).removeClass("search-button-fail");
                n();
            });
        });
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    self.showResponseView = function (data) {
        var responseURL = "/Analytics/SurveyResponse/" + data.SurveyResponseID;
        var x = window.open(responseURL, responseURL);
        x.focus();
    }

    self.downloadReport = function (data) {
        alert(ko.toJSON(self));
        $("#analyticsData").val(ko.toJSON(self));
        $("#downloadReport").submit();
    }

    self.showComments = function (SRIDs, moduleID, keyWord) {
        self.Comments([]);
        $("#commentModal").modal('show');
        $("#comments-loading-bar").show();
        var data = { moduleID: moduleID, SRIDs: SRIDs, keyWord: keyWord };
        var commentURLString = "/api/GetComments";
        $.ajax(commentURLString, {
            type: "POST",
            data: data,
            headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
        }).success(function (allData) {
            self.Comments($.map(allData, function (key, value) { return new CommentModel(key, value) }));
            $("#comments-loading-bar").hide();
        }).fail(function (httpObj, data) {
        });
    }

    self.showCommentsByTag = function (SRIDs, moduleID, tagName) {
        self.Comments([]);
        $("#commentModal").modal('show');
        $("#comments-loading-bar").show();
        var data = { moduleID: moduleID, SRIDs: SRIDs, tagName: tagName };
        var commentURLString = "/api/GetCommentsByTag";
        $.ajax(commentURLString, {
            type: "POST",
            data: data,
            headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
        }).success(function (allData) {
            self.Comments($.map(allData, function (key, value) { return new CommentModel(key, value) }));
            $("#comments-loading-bar").hide();
        }).fail(function (httpObj, data) {
        });
    }
}

$(document).ready(function () {
    var reportViewModel = new ReportViewModel(initJSONData);
    ko.applyBindings(reportViewModel, document.getElementById('body-content'));
    $('body').show();
});