﻿ko.bindingHandlers.movePreviousSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index >= 0) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('prev');
            }
        });
        for (var method in jQuery.fn) {
            //console.log(method);
        }
        $(element).parent().swiperight(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index >= 0) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('prev');
            }
        });
        
    }
}

ko.bindingHandlers.moveNextSlide = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index <= bindingContext.$data.QuestionData().QuestionItems().length - 1) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('next');
            }
        });
        $(element).parent().swipeleft(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index <= bindingContext.$data.QuestionData().QuestionItems().length - 1) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('next');
            }
        });
    }
}

ko.bindingHandlers.changeCurrentSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var csIndex = bindingContext.$index();
            $(element).parent().parent().carousel(csIndex);
            bindingContext.$parent.QuestionData().CurrentIndex(csIndex);
        });
    }
}

ko.bindingHandlers.placePosition = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if ($(document).width() <= 639) {
            $(element).css('top', bindingContext.$data.TopPos().replace('px', '') / 2 + 'px');
            $(element).css('left', bindingContext.$data.LeftPos().replace('px', '') / 2 + 'px');
        } else {
            $(element).css('top', bindingContext.$data.TopPos());
            $(element).css('left', bindingContext.$data.LeftPos());
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var imgwidth = 0;
        var mobilewidth = 0;
        var desktopwidth = 0;
        if ($(document).width() <= 639) {


            $(element).on('load', function () {
                imgwidth = $(this).width();
                desktopwidth = imgwidth * 2;
                mobilewidth = imgwidth;

            });
        } else {
            $(element).on('load', function () {
                imgwidth = $(this).width();
                mobilewidth = imgwidth / 2;
                desktopwidth = imgwidth;

            });

        }


        $(window).resize(function () {
            if ($(document).width() <= 639) {
                $(element).css("width", mobilewidth + 'px');
                $(element).css('top', bindingContext.$data.TopPos().replace('px', '') / 2 + 'px');
                $(element).css('left', bindingContext.$data.LeftPos().replace('px', '') / 2 + 'px');
            } else {
                $(element).css("width", desktopwidth + 'px');
                $(element).css('top', bindingContext.$data.TopPos());
                $(element).css('left', bindingContext.$data.LeftPos());
            }

        });
    }
}


ko.bindingHandlers.selectOrgSub = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
        $(element).on('multiselectclick', function (event, ui) {
            bindingContext.$root.AvlblTouchpoints([]);
            for (index = 0 ; index < bindingContext.$root.Touchpoints().length ; index++) {
                var selValues = $(element).multiselect("getChecked").map(function () {
                    return parseInt(this.value);
                }).get();
                if ($.inArray(bindingContext.$root.Touchpoints()[index].OrgSubID , selValues) != -1) {
                    bindingContext.$root.AvlblTouchpoints.push(bindingContext.$root.Touchpoints()[index]);
                }
            }
            bindingContext.$root.Survey().SelectedTouchpoints = ko.observableArray([]);
            if (bindingContext.$root.AvlblTouchpoints().length > 0) {
                bindingContext.$root.Survey().SelectedTouchpoints().push(bindingContext.$root.AvlblTouchpoints()[0].value);
                setTimeout(function () {
                    $(".touchpoint-select").multiselect("widget").find(":radio:first").each(function () {
                        this.click();
                    });
                }, 0);
            }
        });
    }
};

ko.bindingHandlers.autosize = {
    init: function (element, valueAccessor) {
        $(element).autosize();
        if ($(document).width() <= 320)
            $(element).css('height', '38px');
        else
            $(element).css('height', '72px');
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
          
    }
}

/* Binding Handler to flip an Image */
ko.bindingHandlers.flipImage = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var toggleDiv = $(element).closest('.file-input-wrapper').find('#flip-toggle');
            toggleDiv.toggleClass('flip');
        })
    }
}

ko.bindingHandlers.flipAppreciation = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var toggleDiv = $(element).closest('.file-input-wrapper').find('#flip-toggle-app');
            toggleDiv.toggleClass('flip');
        });
    }
}

ko.bindingHandlers.flipLocation = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
            function showPosition(position) {
               // $(element).closest('.location-dropzone').hide();
                //$(element).parent().find('.map-canvas').show();
                $('#location-marker').attr("src", "/Images/Location-Icon-Green-XL.png");
                $('.location-thank-you').toggle();
                var mapCanvas = $(element).parent().find('.map-canvas')[0];
                bindingContext.$data.AnswerData().Latitude(position.coords.latitude);
                bindingContext.$data.AnswerData().Longitude(position.coords.longitude);
                var mapOptions = {
                    center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                    zoom: 15,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }
                var image = '/Images/Location-Icon-Blue copy.png';
                var map = new google.maps.Map(mapCanvas, mapOptions);
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                    map: map,
                    animation: google.maps.Animation.DROP,
                    icon: image
                });
            }
        });
    }
}


ko.bindingHandlers.chooseRating = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$parent.AnswerData().Rating(bindingContext.$data);
            var root = bindingContext.$root;
            var rateType = bindingContext.$parent.QuestionData().RateType();

            switch (rateType) {
                case RATETYPES.NUMBER:
                    $(element).parent().parent().find('span').removeClass('rate-selected');
                    $(element).addClass('rate-selected');
                    $(element).closest('.survey-sections').removeClass('validation-error');
                    break;
                case RATETYPES.COMMENT:
                    $(element).parent().find('div').removeClass('tag-selected');
                    $(element).addClass('tag-selected');
                    $(element).closest('.survey-sections').removeClass('validation-error');
                    break;
                case RATETYPES.STAR:
                    $(element).parent().parent().find('span').removeClass('Star-Selected');
                    $(element).parent().parent().find('span').addClass('Star');
                    $(element).parent().prevAll().find('span').removeClass('Star');
                    $(element).parent().prevAll().find('span').addClass('Star-Selected');
                    $(element).removeClass('Star');
                    $(element).addClass('Star-Selected');
                    $(element).closest('.survey-sections').removeClass('validation-error');
                    break;
                case RATETYPES.HEART:
                    $(element).parent().parent().find('span').removeClass('Heart-Selected');
                    $(element).parent().parent().find('span').addClass('Heart');
                    $(element).parent().prevAll().find('span').removeClass('Heart');
                    $(element).parent().prevAll().find('span').addClass('Heart-Selected');
                    $(element).removeClass('Heart');
                    $(element).addClass('Heart-Selected');
                    $(element).closest('.survey-sections').removeClass('validation-error');
                    break;
                case RATETYPES.DAISIES:
                    $(element).parent().parent().find('span').removeClass('Daisy-Selected');
                    $(element).parent().parent().find('span').addClass('Daisy');
                    $(element).parent().prevAll().find('span').removeClass('Daisy');
                    $(element).parent().prevAll().find('span').addClass('Daisy-Selected');
                    $(element).removeClass('Daisy');
                    $(element).addClass('Daisy-Selected');
                    $(element).closest('.survey-sections').removeClass('validation-error');
                    break;
            }

            if (bindingContext.$root.ErrorSections().length <= 0) {
                setTimeout(function () {
                    if ($(".survey-sections").length > $(element).closest(".survey-sections").index())
                        $.scrollTo($(".survey-sections").eq($(element).closest(".survey-sections").index() + 1), 100, { offset: -100 })
                }, 100);
            }
            else {
                if (root.ErrorSections().length == 1) {
                    setTimeout(function () { $.scrollTo($('#share-your-exp').closest(".survey-sections"), 100, { offset: -100 }) }, 100);
                }
                else {
                    var removeIndex = -1;
                    var currentSurveySection = $(element).closest('.survey-sections');
                    $.each(root.ErrorSections(), function (index, value) {
                        if (currentSurveySection.get(0) === value.get(0))
                            removeIndex = index;
                    });
                    if (removeIndex != -1) root.ErrorSections.splice(removeIndex, 1);
                    setTimeout(function () { $.scrollTo(root.ErrorSections()[0], 100, { offset: -100 }) }, 100);
                }
            }
        });
    }
}

ko.bindingHandlers.chooseMultipleRating = {

    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var ModuleData = bindingContext.$parents[1];
        var carouselElement = $(element).parent().parent().parent().parent().parent().find('.carousel');
        carouselElement.on('slid.bs.carousel', function () {
            var index = carouselElement.find('div.active').index();
            ModuleData.QuestionData().CurrentIndex(index);
        });
        ModuleData.AnswerData().AddAnswerItem(bindingContext.$parentContext.$index());
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var ModuleData = bindingContext.$parents[1];
            var root = bindingContext.$root;
            var csIndex = bindingContext.$parentContext.$index();
            ModuleData.AnswerData().SetAnswerItem(bindingContext.$parentContext.$index(), bindingContext.$data);
            var carouselElement = $(element).parent().parent().parent().parent().parent().find('.carousel');
            if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.NUMBER) {
                $(element).parent().parent().find('span').removeClass('rate-selected');
                $(element).addClass('rate-selected');
                $(element).closest('.survey-sections').removeClass('validation-error');
            }
            else if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.COMMENT) {
                $(element).parent().removeClass('tag-selected');
                $(element).addClass('tag-selected');
                $(element).closest('.survey-sections').removeClass('validation-error');
            }
            else if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.STAR) {
                $(element).parent().parent().find('span').removeClass('Star-Selected');
                $(element).parent().parent().find('span').addClass('Star');
                $(element).parent().prevAll().find('span').removeClass('Star');
                $(element).parent().prevAll().find('span').addClass('Star-Selected');
                $(element).removeClass('Star');
                $(element).addClass('Star-Selected');
                $(element).closest('.survey-sections').removeClass('validation-error');
            }
            else if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.HEART) {
                $(element).parent().parent().find('span').removeClass('Heart-Selected');
                $(element).parent().parent().find('span').addClass('Heart');
                $(element).parent().prevAll().find('span').removeClass('Heart');
                $(element).parent().prevAll().find('span').addClass('Heart-Selected');
                $(element).removeClass('Heart');
                $(element).addClass('Heart-Selected');
                $(element).closest('.survey-sections').removeClass('validation-error');
            }
            else if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.DAISIES) {
                $(element).parent().parent().find('span').removeClass('Daisy-Selected');
                $(element).parent().parent().find('span').addClass('Daisy');
                $(element).parent().prevAll().find('span').removeClass('Daisy');
                $(element).parent().prevAll().find('span').addClass('Daisy-Selected');
                $(element).removeClass('Daisy');
                $(element).addClass('Daisy-Selected');
                $(element).closest('.survey-sections').removeClass('validation-error');
            }

            carouselElement.find('li').eq(ModuleData.QuestionData().CurrentIndex()).removeClass('not-completed');
            carouselElement.find('li').eq(ModuleData.QuestionData().CurrentIndex()).addClass('completed');


            if (csIndex <= ModuleData.QuestionData().QuestionItems().length - 2) {
                if (bindingContext.$root.ErrorSections().length <= 0) {
                    carouselElement.carousel('next');
                } else {
                    var liElement = carouselElement.find('li').eq(ModuleData.QuestionData().CurrentIndex());
                    var nextNCIndex = liElement.nextAll('.not-completed:first').index();
                    if (nextNCIndex != -1) {
                        carouselElement.carousel(nextNCIndex);
                    } else {
                        var removeIndex = -1;
                        var currentSurveySection = $(element).closest('.survey-sections');
                        $.each(root.ErrorSections(), function (index, value) {
                            if (currentSurveySection.get(0) === value.get(0))
                                removeIndex = index;
                        });
                        if (removeIndex != -1) root.ErrorSections.splice(removeIndex, 1);
                        root.ErrorSections.remove($(element).closest('survey-sections'));
                        setTimeout(function () { $.scrollTo(root.ErrorSections()[0], 100, { offset: -100 }) }, 100);
                    }
                }
            } else if (csIndex == ModuleData.QuestionData().QuestionItems().length - 1) {
                if (bindingContext.$root.ErrorSections().length <= 0) {
                    setTimeout(function () { $.scrollTo($(".survey-sections").eq($(element).closest(".survey-sections").index() + 1), 100, { offset: -100 }) }, 100);
                }
                else {
                    if (root.ErrorSections().length == 1) {
                        setTimeout(function () { $.scrollTo($('#share-your-exp').closest(".survey-sections"), 100, { offset: -100 }) }, 100);
                    }
                    else {
                        var removeIndex = -1;
                        var currentSurveySection = $(element).closest('.survey-sections');
                        $.each(root.ErrorSections(), function (index, value) {
                            if (currentSurveySection.get(0) === value.get(0))
                                removeIndex = index;
                        });
                        if (removeIndex != -1) root.ErrorSections.splice(removeIndex, 1);
                        root.ErrorSections.remove($(element).closest('survey-sections'));
                        setTimeout(function () { $.scrollTo(root.ErrorSections()[0], 100, { offset: -100 }) }, 100);
                    }
                }
            }
        });
    }
}

/* Binding Handler for the file upload functionality */
ko.bindingHandlers.uploadAttachment = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).change(function () {

            var options = ko.utils.unwrapObservable(valueAccessor()),
            property = ko.utils.unwrapObservable(options.property);

            var surveyID = bindingContext.$root.Survey().SurveyID();
            var moduleID = bindingContext.$parent.ModuleID();
            if (property) {
                var $this = $(this),
                    fileName = $this.val();
                var formElement = $(element).closest('form');
                var formData = new FormData(formElement[0]);
                $(".progress-bar-success").show();
                $(".progress-bar-success").css("width", '5%');
                $.ajax({
                    url: '/api/PostSurveyAttachment/' + surveyID + "/" + moduleID,
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    xhr: function () {
                        var req = $.ajaxSettings.xhr();
                        if (req) {
                            req.upload.addEventListener('progress', function (event) {
                                if (event.lengthComputable) {
                                    var percent = 100 * event.loaded / event.total;
                                    $(".progress-bar-success").css("width", percent + '%');
                                }
                            }, false);
                        }
                        return req;
                    },
                    success: function (data) {
                        setTimeout(function () {
                            $(".progress-bar-success").hide();
                        }
                            , 100);
                        bindingContext.$data.AnswerData().addAttachment(data);
                    },
                    error: function (data) {
                        $(".progress-bar-success").hide();
                        alert(data.responseText.Message);
                    }
                });
            }
        });
    }
};

ko.bindingHandlers.selectChoice = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var root = bindingContext.$root;
            if (bindingContext.$parent.QuestionData().ChoiceType() == CHOICES.MULTIPLECHOICE) {
                $(element).toggleClass('choice-selected');
                bindingContext.$parent.AnswerData().ToggleSelectedOption(bindingContext.$index());
                $(element).closest('.survey-sections').removeClass('validation-error');
                var removeIndex = -1;
                var currentSurveySection = $(element).closest('.survey-sections');
                $.each(root.ErrorSections(), function (index, value) {
                    if (currentSurveySection.get(0) === value.get(0))
                        removeIndex = index;
                });
                if (removeIndex != -1) root.ErrorSections.splice(removeIndex, 1);
            }
            else {
                $(element).parent().parent().find('.s-blue-div-hover').removeClass('choice-selected');
                $(element).addClass('choice-selected');
                bindingContext.$parent.AnswerData().AddSelectedOption(bindingContext.$index());
                $(element).closest('.survey-sections').removeClass('validation-error');
                setTimeout(function () {
                    if ($(".survey-sections").length > $(element).closest(".survey-sections").index())
                        $.scrollTo($(".survey-sections").eq($(element).closest(".survey-sections").index() + 1), 100, { offset: -100 })
                }, 100);

                if (root.ErrorSections().length <= 0) {
                    setTimeout(function () {
                        if ($(".survey-sections").length > $(element).closest(".survey-sections").index())
                            $.scrollTo($(".survey-sections").eq($(element).closest(".survey-sections").index() + 1), 100, { offset: -100 })
                    }, 100);
                }
                else {
                    if (root.ErrorSections().length == 1) {
                        setTimeout(function () { $.scrollTo($('#share-your-exp').closest(".survey-sections"), 100, { offset: -100 }) }, 100);
                    }
                    else {
                        var removeIndex = -1;
                        var currentSurveySection = $(element).closest('.survey-sections');
                        $.each(root.ErrorSections(), function (index, value) {
                            if (currentSurveySection.get(0) === value.get(0))
                                removeIndex = index;
                        });
                        if (removeIndex != -1) root.ErrorSections.splice(removeIndex, 1);
                        setTimeout(function () { $.scrollTo(root.ErrorSections()[0], 100, { offset: -100 }) }, 100);
                    }
                }
            }
        });
    }
}

ko.bindingHandlers.selectMultipleChoice = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var ModuleData = bindingContext.$parents[1];
        var carouselElement = $(element).parent().parent().parent().parent().parent().find('.carousel');
        carouselElement.on('slid.bs.carousel', function () {
            var index = carouselElement.find('div.active').index();
            ModuleData.QuestionData().CurrentIndex(index);
        });
        ModuleData.AnswerData().AddAnswerItem(bindingContext.$parentContext.$index());
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var ModuleData = bindingContext.$parents[1];
            var root = bindingContext.$root;
            var csIndex = bindingContext.$parentContext.$index();
            var carouselElement = $(element).parent().parent().parent().parent().parent().find('.carousel');

            if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.MULTIPLECHOICE) {
                $(element).toggleClass('choice-selected');
                ModuleData.AnswerData().AnswerItems()[csIndex].ToggleSelectedOption(bindingContext.$index());
                $(element).closest('.survey-sections').removeClass('validation-error');
                carouselElement.find('li').eq(ModuleData.QuestionData().CurrentIndex()).removeClass('not-completed');
                carouselElement.find('li').eq(ModuleData.QuestionData().CurrentIndex()).addClass('completed');
                if (csIndex <= ModuleData.QuestionData().QuestionItems().length - 2) {
                    if (bindingContext.$root.ErrorSections().length <= 0) {
                        carouselElement.carousel('next');
                    } else {
                        var liElement = carouselElement.find('ol').find('.active');
                        var nextNCIndex = liElement.nextAll('.not-completed:first').index();
                        if (nextNCIndex != -1) {
                            carouselElement.carousel(nextNCIndex);
                        } else {
                            var removeIndex = -1;
                            var currentSurveySection = $(element).closest('.survey-sections');
                            $.each(root.ErrorSections(), function (index, value) {
                                if (currentSurveySection.get(0) === value.get(0))
                                    removeIndex = index;
                            });
                            if (removeIndex != -1) root.ErrorSections.splice(removeIndex, 1);
                            setTimeout(function () { $.scrollTo(root.ErrorSections()[0], 100, { offset: -100 }) }, 100);
                        }
                    }
                }
            }
            else {
                $(element).parent().parent().find('.s-blue-div-hover').removeClass('choice-selected');
                $(element).addClass('choice-selected');
                ModuleData.AnswerData().AnswerItems()[csIndex].AddSelectedOption(bindingContext.$index());
                $(element).closest('.survey-sections').removeClass('validation-error');
                carouselElement.find('li').eq(ModuleData.QuestionData().CurrentIndex()).removeClass('not-completed');
                carouselElement.find('li').eq(ModuleData.QuestionData().CurrentIndex()).addClass('completed');
                if (csIndex <= ModuleData.QuestionData().QuestionItems().length - 2) {
                    if (bindingContext.$root.ErrorSections().length <= 0) {
                        carouselElement.carousel('next');
                    } else {
                        var liElement = carouselElement.find('ol').find('.active');
                        var nextNCIndex = liElement.nextAll('.not-completed:first').index();
                        if (nextNCIndex != -1) {
                            carouselElement.carousel(nextNCIndex);
                        } else {
                            var removeIndex = -1;
                            var currentSurveySection = $(element).closest('.survey-sections');
                            $.each(root.ErrorSections(), function (index, value) {
                                if (currentSurveySection.get(0) === value.get(0))
                                    removeIndex = index;
                            });
                            if (removeIndex != -1) root.ErrorSections.splice(removeIndex, 1);
                            setTimeout(function () { $.scrollTo(root.ErrorSections()[0], 100, { offset: -100 }) }, 100);
                        }
                    }
                } else if (csIndex == ModuleData.QuestionData().QuestionItems().length - 1) {
                    if (bindingContext.$root.ErrorSections().length <= 0) {
                        setTimeout(function () {
                            $.scrollTo($(".survey-sections").eq($(element).closest(".survey-sections").index() + 1), 100, { offset: -100 })
                        }, 100);
                    }
                    else {
                        if (root.ErrorSections().length == 1) {
                            setTimeout(function () { $.scrollTo($('#share-your-exp').closest(".survey-sections"), 100, { offset: -100 }) }, 100);
                        }
                        else {
                            var removeIndex = -1;
                            var currentSurveySection = $(element).closest('.survey-sections');
                            $.each(root.ErrorSections(), function (index, value) {
                                if (currentSurveySection.get(0) === value.get(0))
                                    removeIndex = index;
                            });
                            if (removeIndex != -1) root.ErrorSections.splice(removeIndex, 1);
                            setTimeout(function () { $.scrollTo(root.ErrorSections()[0], 100, { offset: -100 }) }, 100);
                        }
                    }
                }
            }
        });
    }
}

ko.bindingHandlers.select = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
        $(element).on("multiselectclick", function (event, ui) {
            console.log($(element).val());
        });
    }
};


ko.bindingHandlers.selectTouchpoint = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);

    }
};

ko.bindingHandlers.selectSingle = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        var options = allBindingsAccessor().options();
        
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        if (options.length == 1) {
            $(element).multiselect("disable");
        }
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);

        $(element).on("multiselectclick", function (event, ui) {
            var button = $(element).multiselect("getButton");
            $(button).removeClass('error-required');
            $(element).closest('.survey-sections').removeClass('validation-error');
            $(element).next().addClass("ui-widget-sel");
        });
   }
};

ko.bindingHandlers.autoComplete = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var touchpointname = "CustomerServiceCenter";
        $(element).autocomplete({
            source: function (request, response) {
                var data = ko.toJSON({
                    OrgSubID: bindingContext.$root.Survey().OrgSub().OrgSubID,
                    TouchpointName: bindingContext.$root.Survey().SurveyConfigs().length > 0 ? bindingContext.$root.Survey().SurveyConfigs()[0].TouchpointName : '',
                    QueryTerm: request.term
                });
                $.ajax({
                    type: "POST",
                    url: "/api/STAFF/UserNames",
                    contentType: "application/json",
                    data: data,
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                bindingContext.$data.STAFFEntryText(ui.item.value);
                $(element).val(ui.item.value);
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
    },
    update: function (element) {
    }
};

ko.bindingHandlers.selectMultiple = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);

        $(element).bind('multiselectopen', function (event, ui) {
            $(element).multiselect("widget").find(":checkbox").each(function () {
                if (this.checked == true) {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                }
            });
        });

        $(element).on('multiselectclick', function (event, ui) {
            if (ui.value === "All" && ui.checked == true) {
                $(element).multiselect('checkAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                });

            } else if (ui.value === "All" && ui.checked == false) {
                $(element).multiselect('uncheckAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if ($(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').removeClass('multiselect-sel');
                });
            } else {
                var alltrue = true;
                var allElement;
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (this.value === "All") allElement = this;
                    if (this.checked == true) {
                        if (!$(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').addClass('multiselect-sel');
                    } else {
                        if (this.value != "All") alltrue = false;
                        if ($(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').removeClass('multiselect-sel');
                    }
                });
                if (alltrue == true && allElement.checked == false) {
                    bindingContext.$parent.AnswerData().DGAnswerFields()[bindingContext.$index()].FieldValues.push("All");
                    $(allElement).closest('label').addClass('multiselect-sel');
                    $(allElement).prop('checked', true);
                } else if (alltrue == false && allElement.checked == true) {
                    bindingContext.$parent.AnswerData().DGAnswerFields()[bindingContext.$index()].FieldValues.remove("All");
                    $(allElement).closest('label').removeClass('multiselect-sel');
                    $(allElement).prop('checked', false);
                }
            }
        });


        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};


ko.bindingHandlers.captureLocation = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(element).click(function () {
            var geolocation = navigator.geolocation;
            geolocation.getCurrentPosition(bindingContext.$root.showPosition, bindingContext.$root.showError);
        });
    }
}

ko.bindingHandlers.addAnswerField = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var currentIndex = bindingContext.$index();
        var currentQuestion = bindingContext.$parent.QuestionData().DGQuestionFields()[currentIndex];
        bindingContext.$parent.AnswerData().AddAnswerField(currentIndex, currentQuestion.FieldType(), currentQuestion.DefaultValue(), currentQuestion.DefaultValues());
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
};

ko.bindingHandlers.addSurveyConfig = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (bindingContext.$root.Survey().SurveyConfigs().length <= 0) {
            bindingContext.$root.Survey().SurveyConfigs.push(new ConfigDataModel(null));
            bindingContext.$root.Survey().SurveyConfigs()[0].TouchpointName(bindingContext.$root.Survey().OrgSub().Touchpoints[0].TouchpointName());
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
};

ko.bindingHandlers.selectTag = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$parent.AnswerData().ToggleSelectedTag(bindingContext.$index());
            $(element).toggleClass('tag-selected');
        });
    }
}

ko.bindingHandlers.showToolTip = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = bindingContext.$index();
        var direction = value % 3 == 0 ? 'left' : (value % 3 == 2) ? 'right' : 'top';
        $(element).tooltip({ placement: direction });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}

ko.bindingHandlers.commentBox = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).keypress(function () {
            var removeIndex = -1;
            var root = bindingContext.$root;
            $(element).closest('.survey-sections').removeClass('validation-error');
            var currentSurveySection = $(element).closest('.survey-sections');
            $.each(root.ErrorSections(), function (index, value) {
                if (currentSurveySection.get(0) === value.get(0))
                    removeIndex = index;
            });
            if (removeIndex != -1) root.ErrorSections.splice(removeIndex, 1);
        });
    }
}

ko.bindingHandlers.submitExperience = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.submitExperience();
        });
    }
}

ko.bindingHandlers.displayText = {
    init: function (element, valueAccessor) {
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).text($('<div>').append(value).text());
    }
};

ko.bindingHandlers.shareFacebookDialog = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            FB.ui(
            {
                method: 'feed',
                name: bindingContext.$data.FBCaption(),
                picture: bindingContext.$root.CurrentBaseURL + '/' + bindingContext.$data.FBImage(),
                link: bindingContext.$root.CurrentURL,
                caption: bindingContext.$root.CurrentURL,
                description: bindingContext.$data.FBDescription()
            }, function (response) { });
            if (bindingContext.$root.Survey().IsAllowViewResults())
                bindingContext.$root.loadResultsView();
        });
    },
    update: function (element, valueAccessor) {

    }
};

ko.bindingHandlers.addTouchpoints = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var Touchpoints = bindingContext.$root.Survey().OrgSub().Touchpoints;
        for (var index in Touchpoints) {
            bindingContext.$data.Touchpoints().push(new TouchpointModel(null));
            bindingContext.$data.Touchpoints()[index].TouchpointName(bindingContext.$root.Survey().OrgSub().Touchpoints[index].TouchpointName());
            bindingContext.$data.Touchpoints()[index].RegisteredName(bindingContext.$root.Survey().OrgSub().Touchpoints[index].RegisteredName());
            bindingContext.$data.Touchpoints()[index].BrandWebsite(bindingContext.$root.Survey().OrgSub().Touchpoints[index].BrandWebsite());
            bindingContext.$data.Touchpoints()[index].PhoneNumber(bindingContext.$root.Survey().OrgSub().Touchpoints[index].PhoneNumber());
            bindingContext.$data.Touchpoints()[index].EmailAddress(bindingContext.$root.Survey().OrgSub().Touchpoints[index].EmailAddress());
            bindingContext.$data.Touchpoints()[index].Address(bindingContext.$root.Survey().OrgSub().Touchpoints[index].Address());
            bindingContext.$data.Touchpoints()[index].FrontImage(bindingContext.$root.Survey().OrgSub().Touchpoints[index].FrontImage());
            bindingContext.$data.Touchpoints()[index].LogoImage(bindingContext.$root.Survey().OrgSub().Touchpoints[index].LogoImage());
            bindingContext.$data.Touchpoints()[index].LeftPos(bindingContext.$root.Survey().OrgSub().Touchpoints[index].LeftPos());
            bindingContext.$data.Touchpoints()[index].TopPos(bindingContext.$root.Survey().OrgSub().Touchpoints[index].TopPos());
            bindingContext.$data.Touchpoints()[index].SliderValue(bindingContext.$root.Survey().OrgSub().Touchpoints[index].SliderValue());
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}