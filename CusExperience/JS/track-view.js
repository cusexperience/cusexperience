﻿
ko.bindingHandlers.toggleSurveyResponse = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if ($(element).find('span').hasClass("icon-plus")) {
                var surveyResponseDiv = $(element).parent().find('.surveyResponse');
                var domElement = surveyResponseDiv.get(0);
                ko.cleanNode(domElement);
                var surveyResponse = new ResponseViewModel();
                surveyResponse.loadSurveyResponse(domElement, bindingContext.$data.SurveyResponseID);
                $(element).find('span').removeClass("icon-plus");
                $(element).find('span').addClass("icon-minus");
            } else {
                var surveyResponseDiv = $(element).parent().find('.surveyResponse');
                surveyResponseDiv.hide();
                surveyResponseDiv.empty();
                $(element).find('span').removeClass("icon-minus");
                $(element).find('span').addClass("icon-plus");
            }
        });
    }
}

var SURL = "";

var DATE = "";
var SDATE = "";
var GLOBALWIDTH = 320;
var GLOBALHEIGHT = 385;
var GLOBALCHARTAREAWIDTH = 280;
var GLOBALMAPHEIGHT = 200;
if (window.innerWidth > 639) {
    GLOBALWIDTH = 640;
    GLOBALCHARTAREAWIDTH = 520;
    GLOBALMAPHEIGHT = 400;
}
//var GLOBALWIDTH = 640;
//var GLOBALCHARTAREAWIDTH = 520;
var resizeId;
$(window).resize(function () {
    clearTimeout(resizeId);
    resizeId = setTimeout(doneResizing, 500);
});
function doneResizing() {
    if (window.innerWidth > 639) {
        GLOBALWIDTH = 640;
        GLOBALCHARTAREAWIDTH = 520;
        GLOBALMAPHEIGHT = 400;
    } else {
        GLOBALWIDTH = 320;
        GLOBALCHARTAREAWIDTH = 280;
        GLOBALMAPHEIGHT = 200;
    }
   
   // queryAccounts();
    if ($('#bounce-container').is(":visible")) {
        queryAccountsBoun();
    } else if ($('#dura-container').is(":visible")) {
        queryAccountsDura();
    } else if ($('#time-container').is(":visible")) {
        queryAccountsTime();
    } else if ($('#time-container').is(":visible")) {
        queryAccountsTime();
    } else if ($('#motion-container').is(":visible")) {
        queryAccountsTime();
    } else {
        queryAccounts();
    }

}
function CustomerViewModel() {
    var self = this;
    self.Survey = ko.observable((new SurveyModel(initJSONData, false, false, false)));
    
    self.CurrentPage = ko.observable(1);
    self.SurveyResponses = ko.observableArray();
    self.TotalResponses = ko.observable(self.Survey().TotalResponses());
    self.NoofRecords = ko.observable(10);
    self.OrderByValue = ko.observable('desc');
    self.AnalyticsTitle = ko.observable(self.Survey().SurveyTitle() + " Analytics");
    var multiplier = 1;
    var url = document.URL;
    SURL = "/" + self.Survey().OrgSub().SurveyURL() + "/" + self.Survey().PublishedID();
  
    DATE = self.Survey().StringCreatedDate();
    var YYYY = DATE.substring(6, 10);
    var MM = DATE.substring(3, 5);
    var DD = DATE.substring(0, 2);
    SDATE = YYYY + "-" + MM + "-" + DD;
    self.OrderByOptions = ko.observableArray([
        { key: 'desc', value: 'Latest to Earliest' },
        { key: 'asc', value: 'Earliest to Latest' }
    ]);

    var url = document.URL;
    var params = url.split("TrackingView/");
    var id = params[1];
    var responseUrlString = "/api/GetSurveyResponsesBS/" + id;
    
    self.GetCreatedDate = function (data) {
        return ko.computed({
            read: function () {
                
                DATE = self.Survey().StringCreatedDate();
                DATE = DATE.substring(0, 10);
                DATE = DATE.replace(/\//g, "-");
                return DATE;
            }
        })
    }

    var queryString = responseUrlString + "?$top=" + self.NoofRecords() + "&&$skip=0&$orderby=ResponseDate desc";
    if (cvData == null) {
        $.ajax(queryString, {
            type: "GET",
            headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
        }).success(function (allData) {
            //alert(ko.toJSON(allData));
            self.SurveyResponses(allData);
            $('#loading-spinner').hide();
        });
    } else {
        $.ajax(queryString, {
            type: "POST",
            data: cvData,
            headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
        }).success(function (allData) {
            self.SurveyResponses(allData);
            $('#loading-spinner').hide();
        });
    }

    self.generateSNo = function (index) {
        return ((self.CurrentPage() - 1) * self.NoofRecords()) + index() + 1;
    }

    self.loadIndexView = function () {
        return "/Analytics/Detail/" + id;
    }

    self.loadCustomerView = function () {
        return "/Analytics/CustomerView/" + id;
    }

    self.loadSocialView = function () {
        return "/Analytics/SocialView/" + id;
    }

    self.loadTrackingView = function () {
        return "/Analytics/TrackingView/" + id;
    }

    self.loadSurveyResponses = function () {
        var currentPage = $("#paginationHolder").pagination('getCurrentPage');
        var queryString = responseUrlString + "?$top=" + self.NoofRecords() + "&$skip=" + ((currentPage - 1) * self.NoofRecords()) + "&$orderby=ResponseDate " + self.OrderByValue();
        if (cvData == null) {
            $.ajax(queryString, {
                type: "GET",
                headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
            }).success(function (allData) {
                self.SurveyResponses(allData);
                self.CurrentPage(currentPage);
            });
        } else {
            $.ajax(queryString, {
                type: "POST",
                data: cvData,
                headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
            }).success(function (allData) {
                self.SurveyResponses(allData);
                self.CurrentPage(currentPage);
            });
        }
    }
}

$(document).ready(function () {
    var customerViewModel = new CustomerViewModel();
    ko.applyBindings(customerViewModel, document.getElementById('body-content'));
    $('body').show();
    $("#track-table").tablesorter();
    $("#myTable").tablesorter();
});

// Replace with your client ID from the developer console.
//Client ID for Localhost:
//var CLIENT_ID = '43991275007-7r9h28dnbgnu7bgggkcaqcq48uh0vg0c.apps.googleusercontent.com';

//Client ID for service account:
//var CLIENT_ID = '43991275007-qh60ekjrl1rl7ra6kn8suj801kngs258.apps.googleusercontent.com';

//Client ID for www.CusExperience.com:
//var CLIENT_ID = '43991275007-6j5l6ptlo3tjik0e5022ni2p9j5qo26b.apps.googleusercontent.com';

var CLIENT_ID = '94440065062-0jmevfp7i2k9fe77nrh6j6fq4ca66ii1.apps.googleusercontent.com';

// Set authorized scope.
var SCOPES = ['https://www.googleapis.com/auth/analytics.readonly'];


function authorize(event) {
    // Handles the authorization flow.
    // `immediate` should be false when invoked from the button click.

    /*var useImmdiate = event ? false : true;
    var authData = {
        client_id: CLIENT_ID,
        scope: SCOPES,
        immediate: useImmdiate
    };
    gapi.auth.authorize(authData, function (response) {
        if (response.error) {
            console.log(response);
        }
        else {
            queryAccounts();
        }
    }); */

    $.ajax({
        type: "GET",
        url: "/api/Account/Google",
        contentType: "application/json",
        success: function (data) {
            gapi.auth.setToken({
                access_token: data
            });
            queryAccountsSess();
        },
        error: function (data) {
            self.surveyStatusMsg('Oops! Please try again');
        }
    });
}


function queryAccounts() {
    
    // Load the Google Analytics client library.
    gapi.client.load('analytics', 'v3').then(function () {

        // Get a list of all Google Analytics accounts for this user
        gapi.client.analytics.management.accounts.list().then(handleAccounts);
    });
}
function queryAccountsSess() {
    
    // Load the Google Analytics client library.
    gapi.client.load('analytics', 'v3').then(function () {

        // Get a list of all Google Analytics accounts for this user
        gapi.client.analytics.management.accounts.list().then(handleAccountsSess);
    });
}
function queryAccountsTime() {
    
    // Load the Google Analytics client library.
    gapi.client.load('analytics', 'v3').then(function () {

        // Get a list of all Google Analytics accounts for this user
        gapi.client.analytics.management.accounts.list().then(handleAccountsTime);
    });
}
function queryAccountsDura() {

    // Load the Google Analytics client library.
    gapi.client.load('analytics', 'v3').then(function () {

        // Get a list of all Google Analytics accounts for this user
        gapi.client.analytics.management.accounts.list().then(handleAccountsDura);
    });
}
function queryAccountsBoun() {

    // Load the Google Analytics client library.
    gapi.client.load('analytics', 'v3').then(function () {

        // Get a list of all Google Analytics accounts for this user
        gapi.client.analytics.management.accounts.list().then(handleAccountsBoun);
    });
}
function handleAccountsSess(response) {
    // Handles the response from the accounts list method.
    if (response.result.items && response.result.items.length) {
        // Get the first Google Analytics account.
        var firstAccountId = response.result.items[0].id;

        // Query for properties.
        queryPropertiesSess(firstAccountId);
    } else {
        console.log('No accounts found for this user.');
    }
}
function handleAccountsTime(response) {
    // Handles the response from the accounts list method.
    if (response.result.items && response.result.items.length) {
        // Get the first Google Analytics account.
        var firstAccountId = response.result.items[0].id;

        // Query for properties.
        queryPropertiesTime(firstAccountId);
    } else {
        console.log('No accounts found for this user.');
    }
}
function handleAccountsDura(response) {
    // Handles the response from the accounts list method.
    if (response.result.items && response.result.items.length) {
        // Get the first Google Analytics account.
        var firstAccountId = response.result.items[0].id;

        // Query for properties.
        queryPropertiesDura(firstAccountId);
    } else {
        console.log('No accounts found for this user.');
    }
}
function handleAccountsBoun(response) {
    // Handles the response from the accounts list method.
    if (response.result.items && response.result.items.length) {
        // Get the first Google Analytics account.
        var firstAccountId = response.result.items[0].id;

        // Query for properties.
        queryPropertiesBoun(firstAccountId);
    } else {
        console.log('No accounts found for this user.');
    }
}
function handleAccounts(response) {
    // Handles the response from the accounts list method.
    if (response.result.items && response.result.items.length) {
        // Get the first Google Analytics account.
        var firstAccountId = response.result.items[0].id;

        // Query for properties.
        queryProperties(firstAccountId);
    } else {
        console.log('No accounts found for this user.');
    }
}

function queryPropertiesSess(accountId) {
    // Get a list of all the properties for the account.
    gapi.client.analytics.management.webproperties.list(
        { 'accountId': accountId })
      .then(handlePropertiesSess)
      .then(null, function (err) {
          // Log any errors.
          console.log(err);
      });
}
function queryPropertiesTime(accountId) {
    // Get a list of all the properties for the account.
    gapi.client.analytics.management.webproperties.list(
        { 'accountId': accountId })
      .then(handlePropertiesTime)
      .then(null, function (err) {
          // Log any errors.
          console.log(err);
      });
}
function queryPropertiesDura(accountId) {
    // Get a list of all the properties for the account.
    gapi.client.analytics.management.webproperties.list(
        { 'accountId': accountId })
      .then(handlePropertiesDura)
      .then(null, function (err) {
          // Log any errors.
          console.log(err);
      });
}
function queryPropertiesBoun(accountId) {
    // Get a list of all the properties for the account.
    gapi.client.analytics.management.webproperties.list(
        { 'accountId': accountId })
      .then(handlePropertiesBoun)
      .then(null, function (err) {
          // Log any errors.
          console.log(err);
      });
}
function queryProperties(accountId) {
    // Get a list of all the properties for the account.
    gapi.client.analytics.management.webproperties.list(
        { 'accountId': accountId })
      .then(handleProperties)
      .then(null, function (err) {
          // Log any errors.
          console.log(err);
      });
}

function handlePropertiesSess(response) {
    // Handles the response from the webproperties list method.
    if (response.result.items && response.result.items.length) {

        // Get the first Google Analytics account
        var firstAccountId = response.result.items[0].accountId;

        // Get the first property ID
        var firstPropertyId = response.result.items[0].id;

        // Query for Views (Profiles).
        queryProfilesSess(firstAccountId, firstPropertyId);
    } else {
        console.log('No properties found for this user.');
    }
}

function handlePropertiesTime(response) {
    // Handles the response from the webproperties list method.
    if (response.result.items && response.result.items.length) {

        // Get the first Google Analytics account
        var firstAccountId = response.result.items[0].accountId;

        // Get the first property ID
        var firstPropertyId = response.result.items[0].id;

        // Query for Views (Profiles).
        queryProfilesTime(firstAccountId, firstPropertyId);
    } else {
        console.log('No properties found for this user.');
    }
}
function handlePropertiesDura(response) {
    // Handles the response from the webproperties list method.
    if (response.result.items && response.result.items.length) {

        // Get the first Google Analytics account
        var firstAccountId = response.result.items[0].accountId;

        // Get the first property ID
        var firstPropertyId = response.result.items[0].id;

        // Query for Views (Profiles).
        queryProfilesDura(firstAccountId, firstPropertyId);
    } else {
        console.log('No properties found for this user.');
    }
}
function handlePropertiesBoun(response) {
    // Handles the response from the webproperties list method.
    if (response.result.items && response.result.items.length) {

        // Get the first Google Analytics account
        var firstAccountId = response.result.items[0].accountId;

        // Get the first property ID
        var firstPropertyId = response.result.items[0].id;

        // Query for Views (Profiles).
        queryProfilesBoun(firstAccountId, firstPropertyId);
    } else {
        console.log('No properties found for this user.');
    }
}
function handleProperties(response) {
    // Handles the response from the webproperties list method.
    if (response.result.items && response.result.items.length) {

        // Get the first Google Analytics account
        var firstAccountId = response.result.items[0].accountId;

        // Get the first property ID
        var firstPropertyId = response.result.items[0].id;

        // Query for Views (Profiles).
        queryProfiles(firstAccountId, firstPropertyId);
    } else {
        console.log('No properties found for this user.');
    }
}
function queryProfilesSess(accountId, propertyId) {
    // Get a list of all Views (Profiles) for the first property
    // of the first Account.
    gapi.client.analytics.management.profiles.list({
        'accountId': accountId,
        'webPropertyId': propertyId
    })
    .then(handleProfilesSess)
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}
function queryProfilesTime(accountId, propertyId) {
    // Get a list of all Views (Profiles) for the first property
    // of the first Account.
    gapi.client.analytics.management.profiles.list({
        'accountId': accountId,
        'webPropertyId': propertyId
    })
    .then(handleProfilesTime)
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}
function queryProfilesDura(accountId, propertyId) {
    // Get a list of all Views (Profiles) for the first property
    // of the first Account.
    gapi.client.analytics.management.profiles.list({
        'accountId': accountId,
        'webPropertyId': propertyId
    })
    .then(handleProfilesDura)
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}
function queryProfilesBoun(accountId, propertyId) {
    // Get a list of all Views (Profiles) for the first property
    // of the first Account.
    gapi.client.analytics.management.profiles.list({
        'accountId': accountId,
        'webPropertyId': propertyId
    })
    .then(handleProfilesBoun)
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}
function queryProfiles(accountId, propertyId) {
    // Get a list of all Views (Profiles) for the first property
    // of the first Account.
    gapi.client.analytics.management.profiles.list({
        'accountId': accountId,
        'webPropertyId': propertyId
    })
    .then(handleProfiles)
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}
function handleProfilesSess(response) {
    // Handles the response from the profiles list method.
    if (response.result.items && response.result.items.length) {
        // Get the first View (Profile) ID.
        var firstProfileId = response.result.items[0].id;

        // Query the Core Reporting API.
        
        queryCoreReportingApiSessionView(firstProfileId);
       
        // queryCoreReportingApi(firstProfileId);
    } else {
        console.log('No views (profiles) found for this user.');
    }
}
function handleProfilesTime(response) {
    // Handles the response from the profiles list method.
    if (response.result.items && response.result.items.length) {
        // Get the first View (Profile) ID.
        var firstProfileId = response.result.items[0].id;

        // Query the Core Reporting API.

        queryCoreReportingApiUserAllTimeView(firstProfileId);

        // queryCoreReportingApi(firstProfileId);
    } else {
        console.log('No views (profiles) found for this user.');
    }
}

function handleProfilesDura(response) {
    // Handles the response from the profiles list method.
    if (response.result.items && response.result.items.length) {
        // Get the first View (Profile) ID.
        var firstProfileId = response.result.items[0].id;

        // Query the Core Reporting API.

        queryCoreReportingApiUserDurationTimeView(firstProfileId);

        // queryCoreReportingApi(firstProfileId);
    } else {
        console.log('No views (profiles) found for this user.');
    }
}
function handleProfilesBoun(response) {
    // Handles the response from the profiles list method.
    if (response.result.items && response.result.items.length) {
        // Get the first View (Profile) ID.
        var firstProfileId = response.result.items[0].id;

        // Query the Core Reporting API.
        queryCoreReportingApiUserBounceView(firstProfileId);
        

        // queryCoreReportingApi(firstProfileId);
    } else {
        console.log('No views (profiles) found for this user.');
    }
}
function handleProfiles(response) {
    // Handles the response from the profiles list method.
    if (response.result.items && response.result.items.length) {
        // Get the first View (Profile) ID.
        var firstProfileId = response.result.items[0].id;

        // Query the Core Reporting API.
        queryCoreReportingApiAllView(firstProfileId);
        queryCoreReportingApiCountryView(firstProfileId);
    } else {
        console.log('No views (profiles) found for this user.');
    }
}


function queryCoreReportingApiAllView(profileId) {
    // Query the Core Reporting API for the number sessions for
    // the past seven days.
   
   
    gapi.client.analytics.data.ga.get({
        'ids': 'ga:' + profileId,
        'start-date': SDATE,
        'end-date': 'today',
        'metrics': 'ga:sessions, ga:percentNewSessions, ga:newUsers, ga:bounceRate, ga:pageviewsPerSession,  ga:avgSessionDuration',
        'dimensions': 'ga:country,ga:browser, ga:browserVersion',
        'filters': 'ga:landingPagePath==' + SURL
    })
    .then(function (response) {
       
        var formattedJson = JSON.stringify(response.result, null, 2);
        var resultRos = "";
        resultRos = "<table border='1' align='center' class='track-table' id='track-table'><thead><tr class='track-header'><th rowspan='2'>Country</th><th rowspan='2'>Browser</th><th rowspan='2'>Browser Version</th><th colspan='3'>Acquisition</th><th colspan='3'>Behaviour</th></tr><tr class='track-header'><th >Sessions <span class='icon-info-sign' title='A session is a group of interactions that take place on your website within a given time frame. For example a single session can contain multiple screen or page views, events, social interactions, and ecommerce transactions.'></span></th><th>% New Sessions <span class='icon-info-sign' title='A percentage of sessions in which are unique.'></span></th><th >New Users <span class='icon-info-sign' title='A new user is a user whose session is marked as a first-time visiting session.'></span></th><th>Bounce Rate <br><span class='icon-info-sign' title='Bounce Rate is the percentage of single-page sessions (i.e. sessions in which the person left your site from the entrance page without interacting with the page).'></span></th><th>Pages / Session <span class='icon-info-sign' title='Number of pages browsed based on a single session.'></th><th >Avg. Session Duration <br><span class='icon-info-sign' title='Average time a user is spending on the page.'></span></th></thead></tr>";
       // resultRos = "<table border='1' align='center' class='track-table' id='track-table'><thead><tr class='track-header'><th>Country</th><th>Browser</th><th>Browser Version</th><th>Sessions</th><th>% New Sessions</th><th>New Users</th><th>Bounce Rate</th><th>Pages / Session</th><th>Avg. Session Duration</th></thead></tr>";
        resultRos += "<tbody>";
        for (var index in response.result.rows) {
            resultRos += "<tr>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][0] + "</span>" + "</td>"
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][1] + "</span>" + "</td>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][2] + "</span>" + "</td>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][3] + "</span>" + "</td>";
            resultRos += "<td><span class='table-cell-font'>" + parseFloat(response.result.rows[index][4]).toFixed(2) + "%</span>" + "</td>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][5] + "</span>" + "</td>";
            resultRos += "<td><span class='table-cell-font'>" + parseFloat(response.result.rows[index][6]).toFixed(2) + "%</span>" + "</td>";
            resultRos += "<td><span class='table-cell-font'>" + parseFloat(response.result.rows[index][7]).toFixed(2) + "</span>" + "</td>";
            resultRos += "<td><span class='table-cell-font'>" + parseFloat(response.result.rows[index][8]).toFixed(2) + "s</span>" + "</td>";
            
            resultRos += "</tr>";
        }
        resultRos += "</tr></tbody></table>"
        var resulter = JSON.stringify(response.result.totalsForAllResults, null, 2)
        document.getElementById('all-view').innerHTML = resultRos;
        $("#track-table").tablesorter();
        
       
    })
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}
function queryCoreReportingApiSessionView(profileId) {
    // Query the Core Reporting API for the number sessions for
    // the past seven days.
   

    gapi.client.analytics.data.ga.get({
        'ids': 'ga:' + profileId,
        'start-date': SDATE,
        'end-date': 'today',
        'metrics': 'ga:sessions',
        'dimensions': 'ga:browser',
        'filters': 'ga:landingPagePath==' + SURL
    })
    .then(function (response) {
        
        
        var formattedJson = JSON.stringify(response.result, null, 2);
        var resultRos = "";
        var totalCount = 0;
        var pieData = [
                
        ];
        var rowData = new google.visualization.DataTable();
        resultRos = "<table border='1' align='center' class='track-table' id='session-table'><thead><tr class='track-header'><th>Browser</th><th>Session <span class='icon-info-sign' title='A session is a group of interactions that take place on your website within a given time frame. For example a single session can contain multiple screen or page views, events, social interactions, and ecommerce transactions.'></span></th></thead></tr>";
        // resultRos = "<table border='1' align='center' class='track-table' id='track-table'><thead><tr class='track-header'><th>Country</th><th>Browser</th><th>Browser Version</th><th>Sessions</th><th>% New Sessions</th><th>New Users</th><th>Bounce Rate</th><th>Pages / Session</th><th>Avg. Session Duration</th></thead></tr>";
        resultRos += "<tbody>";
        for (var index in response.result.rows) {
            resultRos += "<tr>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][0] + "</span>" + "</td>"
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][1] + "</span>" + "</td>";
           

            resultRos += "</tr>";
            pieData.push([response.result.rows[index][0], parseInt(response.result.rows[index][1])]);
            totalCount += parseInt(response.result.rows[index][1]);
        }
        resultRos += "</tbody><tr>";
        resultRos += "<td><span class='table-cell-font'><strong>Total</strong></span>" + "</td>"
        resultRos += "<td><span class='table-cell-font'><strong>" + totalCount + "</strong></span>" + "</td>";
        resultRos += "</tr></table>";
        
        rowData.addColumn('string', 'Browser');
        rowData.addColumn('number', 'Session');
        
        rowData.addRows(pieData);
        
        drawChart(rowData);
        resultRos += ""
        var resulter = JSON.stringify(response.result.totalsForAllResults, null, 2)
        document.getElementById('session-view').innerHTML = resultRos;
        $("#session-table").tablesorter();
      

    })
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}
function queryCoreReportingApiUserAllTimeView(profileId) {
    // Query the Core Reporting API for the number sessions for
    // the past seven days.


    gapi.client.analytics.data.ga.get({
        'ids': 'ga:' + profileId,
        'start-date': SDATE,
        'end-date': 'today',
        'metrics': 'ga:users, ga:newUsers',
        'dimensions': 'ga:date',
        'filters': 'ga:landingPagePath==' + SURL
    })
    .then(function (response) {
       
        var formattedJson = JSON.stringify(response.result, null, 2);
        var resultRos = "";
        var totalCount = 0;
        var pieData = [
        ];
        var pieData2 = [];
        var rowData = new google.visualization.DataTable();
        var rowData2 = new google.visualization.DataTable();
        resultRos = "<table border='1' align='center' class='track-table' id='session-table'><thead><tr class='track-header'><th>Browser</th><th>Session</th></thead></tr>";
        // resultRos = "<table border='1' align='center' class='track-table' id='track-table'><thead><tr class='track-header'><th>Country</th><th>Browser</th><th>Browser Version</th><th>Sessions</th><th>% New Sessions</th><th>New Users</th><th>Bounce Rate</th><th>Pages / Session</th><th>Avg. Session Duration</th></thead></tr>";
        resultRos += "<tbody>";
        for (var index in response.result.rows) {
            resultRos += "<tr>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][0] + "</span>" + "</td>"
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][1] + "</span>" + "</td>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][2] + "</span>" + "</td>";
           
            resultRos += "</tr>";
           
            var YYYY = response.result.rows[index][0].substring(0, 4);
            var MM = response.result.rows[index][0].substring(4, 6) - 1;
            var DD = response.result.rows[index][0].substring(6, 8);
            totalCount += parseInt(response.result.rows[index][1]);
            pieData.push(['User', new Date (YYYY,MM,DD), parseInt(response.result.rows[index][1])]);
            pieData.push(['New Users', new Date(YYYY, MM, DD), parseInt(response.result.rows[index][2])]);
            pieData.push(['Total Users', new Date(YYYY, MM, DD), totalCount]);
            //pieData2.push([new Date(YYYY, MM, DD), parseInt(response.result.rows[index][1]), parseInt(response.result.rows[index][2]), totalCount]);
            pieData2.push([new Date(YYYY, MM, DD), parseInt(response.result.rows[index][1]), parseInt(response.result.rows[index][2])]);
        }

        
     

        rowData.addColumn('string', 'Type of User');
        rowData.addColumn('date', 'Date');
        rowData.addColumn('number', 'Session');
        rowData2.addColumn('date', 'Date');
        rowData2.addColumn('number', 'User(s)');
        rowData2.addColumn('number', 'New User(s)');
       // rowData2.addColumn('number', 'Total User(s)');
        
        rowData.addRows(pieData);
        rowData2.addRows(pieData2);
        drawMotionChart(rowData);
        drawLineChart(rowData2);
        resultRos += "</tr></tbody></table>"
        var resulter = JSON.stringify(response.result.totalsForAllResults, null, 2)
        //document.getElementById('session-view').innerHTML = resultRos;
        $("#session-table").tablesorter();
        
    })
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}

function queryCoreReportingApiUserDurationTimeView(profileId) {
    // Query the Core Reporting API for the number sessions for
    // the past seven days.


    gapi.client.analytics.data.ga.get({
        'ids': 'ga:' + profileId,
        'start-date': SDATE,
        'end-date': 'today',
        'metrics': 'ga:sessionDuration, ga:avgSessionDuration',
        'dimensions': 'ga:date',
        'filters': 'ga:landingPagePath==' + SURL
    })
    .then(function (response) {

        var formattedJson = JSON.stringify(response.result, null, 2);
        var resultRos = "";
        var totalCount = 0;
        var pieData = [
        ];
        var pieData2 = [];
        var rowData = new google.visualization.DataTable();
        var rowData2 = new google.visualization.DataTable();
        resultRos = "<table border='1' align='center' class='track-table' id='session-table'><thead><tr class='track-header'><th>Browser</th><th>Session</th></thead></tr>";
        // resultRos = "<table border='1' align='center' class='track-table' id='track-table'><thead><tr class='track-header'><th>Country</th><th>Browser</th><th>Browser Version</th><th>Sessions</th><th>% New Sessions</th><th>New Users</th><th>Bounce Rate</th><th>Pages / Session</th><th>Avg. Session Duration</th></thead></tr>";
        resultRos += "<tbody>";
        for (var index in response.result.rows) {
            resultRos += "<tr>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][0] + "</span>" + "</td>"
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][1] + "</span>" + "</td>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][2] + "</span>" + "</td>";

            resultRos += "</tr>";
            
            var YYYY = response.result.rows[index][0].substring(0, 4);
            var MM = response.result.rows[index][0].substring(4, 6) - 1;
            var DD = response.result.rows[index][0].substring(6, 8);
           
            totalCount += parseInt(response.result.rows[index][1]);
            pieData.push(['User', new Date(YYYY, MM, DD), parseInt(response.result.rows[index][1])]);
            pieData.push(['New Users', new Date(YYYY, MM, DD), parseInt(response.result.rows[index][2])]);
            pieData.push(['Total Users', new Date(YYYY, MM, DD), totalCount]);
            //pieData2.push([new Date(YYYY, MM, DD), parseInt(response.result.rows[index][1]), parseInt(response.result.rows[index][2]), totalCount]);
            pieData2.push([new Date(YYYY, MM, DD), parseInt(response.result.rows[index][1]), parseInt(response.result.rows[index][2])]);
            
        }




        rowData.addColumn('string', 'Type of User');
        rowData.addColumn('date', 'Date');
        rowData.addColumn('number', 'Session');
        rowData2.addColumn('date', 'Date');
        rowData2.addColumn('number', 'Total Session Duration - second(s)');
        rowData2.addColumn('number', 'Average Session Duration - second(s)');
        // rowData2.addColumn('number', 'Total User(s)');

        rowData.addRows(pieData);
        rowData2.addRows(pieData2);
        //drawMotionChart(rowData);
        drawLineDuraChart(rowData2);
        resultRos += "</tr></tbody></table>"
        var resulter = JSON.stringify(response.result.totalsForAllResults, null, 2)
        //document.getElementById('session-view').innerHTML = resultRos;
        $("#session-table").tablesorter();

    })
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}
function queryCoreReportingApiUserBounceView(profileId) {
    // Query the Core Reporting API for the number sessions for
    // the past seven days.


    gapi.client.analytics.data.ga.get({
        'ids': 'ga:' + profileId,
        'start-date': SDATE,
        'end-date': 'today',
        'metrics': 'ga:bounceRate',
        'dimensions': 'ga:date',
        'filters': 'ga:landingPagePath==' + SURL
    })
    .then(function (response) {

        var formattedJson = JSON.stringify(response.result, null, 2);
        var resultRos = "";
        var totalCount = 0;
        var pieData = [
        ];
        var pieData2 = [];
        var rowData = new google.visualization.DataTable();
        var rowData2 = new google.visualization.DataTable();
        resultRos = "<table border='1' align='center' class='track-table' id='session-table'><thead><tr class='track-header'><th>Browser</th><th>Session</th></thead></tr>";
        // resultRos = "<table border='1' align='center' class='track-table' id='track-table'><thead><tr class='track-header'><th>Country</th><th>Browser</th><th>Browser Version</th><th>Sessions</th><th>% New Sessions</th><th>New Users</th><th>Bounce Rate</th><th>Pages / Session</th><th>Avg. Session Duration</th></thead></tr>";
        resultRos += "<tbody>";
        for (var index in response.result.rows) {
            resultRos += "<tr>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][0] + "</span>" + "</td>"
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][1] + "</span>" + "</td>";
           

            resultRos += "</tr>";

            var YYYY = response.result.rows[index][0].substring(0, 4);
            var MM = response.result.rows[index][0].substring(4, 6) - 1;
            var DD = response.result.rows[index][0].substring(6, 8);
            totalCount += parseInt(response.result.rows[index][1]);
            pieData.push(['User', new Date(YYYY, MM, DD), parseInt(response.result.rows[index][1])]);
            pieData.push(['Bounce Rate', new Date(YYYY, MM, DD), parseInt(response.result.rows[index][2])]);
            
            //pieData2.push([new Date(YYYY, MM, DD), parseInt(response.result.rows[index][1]), parseInt(response.result.rows[index][2]), totalCount]);
            pieData2.push([new Date(YYYY, MM, DD), parseInt(response.result.rows[index][1])]);
        }




       
        rowData2.addColumn('date', 'Date');
        rowData2.addColumn('number', 'Bounce Rate');
        
        // rowData2.addColumn('number', 'Total User(s)');

        //rowData.addRows(pieData);
        rowData2.addRows(pieData2);
        console.log(rowData2);
        //drawMotionChart(rowData);
        drawLineBounceChart(rowData2);
        resultRos += "</tr></tbody></table>"
        var resulter = JSON.stringify(response.result.totalsForAllResults, null, 2)
        //document.getElementById('session-view').innerHTML = resultRos;
        $("#session-table").tablesorter();

    })
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}
function queryCoreReportingApiUserAllTimeView(profileId) {
    // Query the Core Reporting API for the number sessions for
    // the past seven days.


    gapi.client.analytics.data.ga.get({
        'ids': 'ga:' + profileId,
        'start-date': SDATE,
        'end-date': 'today',
        'metrics': 'ga:users, ga:newUsers',
        'dimensions': 'ga:date',
        'filters': 'ga:landingPagePath==' + SURL
    })
    .then(function (response) {
       
        var formattedJson = JSON.stringify(response.result, null, 2);
        var resultRos = "";
        var totalCount = 0;
        var pieData = [
        ];
        var pieData2 = [];
        var rowData = new google.visualization.DataTable();
        var rowData2 = new google.visualization.DataTable();
        resultRos = "<table border='1' align='center' class='track-table' id='session-table'><thead><tr class='track-header'><th>Browser</th><th>Session</th></thead></tr>";
        // resultRos = "<table border='1' align='center' class='track-table' id='track-table'><thead><tr class='track-header'><th>Country</th><th>Browser</th><th>Browser Version</th><th>Sessions</th><th>% New Sessions</th><th>New Users</th><th>Bounce Rate</th><th>Pages / Session</th><th>Avg. Session Duration</th></thead></tr>";
        resultRos += "<tbody>";
        for (var index in response.result.rows) {
            resultRos += "<tr>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][0] + "</span>" + "</td>"
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][1] + "</span>" + "</td>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][2] + "</span>" + "</td>";
           
            resultRos += "</tr>";
           
            var YYYY = response.result.rows[index][0].substring(0, 4);
            var MM = response.result.rows[index][0].substring(4, 6) - 1;
            var DD = response.result.rows[index][0].substring(6, 8);
            totalCount += parseInt(response.result.rows[index][1]);
            pieData.push(['User', new Date (YYYY,MM,DD), parseInt(response.result.rows[index][1])]);
            pieData.push(['New Users', new Date(YYYY, MM, DD), parseInt(response.result.rows[index][2])]);
            pieData.push(['Total Users', new Date(YYYY, MM, DD), totalCount]);
            //pieData2.push([new Date(YYYY, MM, DD), parseInt(response.result.rows[index][1]), parseInt(response.result.rows[index][2]), totalCount]);
            pieData2.push([new Date(YYYY, MM, DD), parseInt(response.result.rows[index][1]), parseInt(response.result.rows[index][2])]);
        }

        
     

        rowData.addColumn('string', 'Type of User');
        rowData.addColumn('date', 'Date');
        rowData.addColumn('number', 'Session');
        rowData2.addColumn('date', 'Date');
        rowData2.addColumn('number', 'User(s)');
        rowData2.addColumn('number', 'New User(s)');
       // rowData2.addColumn('number', 'Total User(s)');
        
        rowData.addRows(pieData);
        rowData2.addRows(pieData2);
        drawMotionChart(rowData);
        drawLineChart(rowData2);
        resultRos += "</tr></tbody></table>"
        var resulter = JSON.stringify(response.result.totalsForAllResults, null, 2)
        //document.getElementById('session-view').innerHTML = resultRos;
        $("#session-table").tablesorter();
        
    })
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}

function queryCoreReportingApiCountryView(profileId) {
    // Query the Core Reporting API for the number sessions for
    // the past seven days.
  

    gapi.client.analytics.data.ga.get({
        'ids': 'ga:' + profileId,
        'start-date': SDATE,
        'end-date': 'today',
        'metrics': 'ga:sessions',
        'dimensions': 'ga:country',
        'filters': 'ga:landingPagePath==' + SURL
    })
    .then(function (response) {
      
       
        var formattedJson = JSON.stringify(response.result, null, 2);
        var resultRos = "";
        var totalCount = 0;
        var pieData = [

        ];
        var rowData = new google.visualization.DataTable();
        resultRos = "<table border='1' align='center' class='track-table' id='session-table'><thead><tr class='track-header'><th>Country</th><th>Session</th></thead></tr>";
        // resultRos = "<table border='1' align='center' class='track-table' id='track-table'><thead><tr class='track-header'><th>Country</th><th>Browser</th><th>Browser Version</th><th>Sessions</th><th>% New Sessions</th><th>New Users</th><th>Bounce Rate</th><th>Pages / Session</th><th>Avg. Session Duration</th></thead></tr>";
        resultRos += "<tbody>";
        for (var index in response.result.rows) {
            resultRos += "<tr>";
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][0] + "</span>" + "</td>"
            resultRos += "<td><span class='table-cell-font'>" + response.result.rows[index][1] + "</span>" + "</td>";
            resultRos += "</tr>";
            pieData.push([response.result.rows[index][0], parseInt(response.result.rows[index][1])]);
            totalCount += parseInt(response.result.rows[index][1]);
        }
        resultRos += "<tr>";
        resultRos += "<td><span class='table-cell-font'><strong>Total</strong></span>" + "</td>"
        resultRos += "<td><span class='table-cell-font'><strong>" + totalCount + "</strong></span>" + "</td>";
        resultRos += "</tr>";

        rowData.addColumn('string', 'Country');
        rowData.addColumn('number', 'Session');

        rowData.addRows(pieData);
        drawMap(rowData);
        resultRos += "</tr></tbody></table>"
        var resulter = JSON.stringify(response.result.totalsForAllResults, null, 2)
       // document.getElementById('map-view').innerHTML = resultRos;
       // $("#session-table").tablesorter();
       

    })
    .then(null, function (err) {
        // Log any errors.
        console.log(err);
    });
}
google.load('visualization', '1', { packages: ['corechart', 'line'] });

function drawLineChart(data) {

    


    var options = {
       
       
        hAxis: {
            title: 'Date'
        },
        vAxis: {
            title: 'Sessions'
        },
        width: GLOBALWIDTH,
        height: 385,
        colors: CHARTCOLOURS,
        chartArea: {
            width: GLOBALCHARTAREAWIDTH
        }
        
    };
    options['legend'] = 'top';
    var chart = new google.visualization.LineChart(document.getElementById('session-line'));
   //var chart = new google.charts.LineChart(document.getElementById('session-line'));

    chart.draw(data, options);
}
function drawLineDuraChart(data) {

    var options = {

        
        
        hAxis: {
            title: 'Date'
        },
        vAxis: {
            title: 'Seconds',
            format: '#,###'
        },
        width: GLOBALWIDTH,
        height: 385,
        colors: CHARTCOLOURS,
        chartArea: {
            width: GLOBALCHARTAREAWIDTH
        }

    };
    options['legend'] = 'top';
    console.log(options);
    var chart = new google.visualization.LineChart(document.getElementById('session-linedura'));
    //var chart = new google.charts.LineChart(document.getElementById('session-line'));

    chart.draw(data, options);
}
function drawLineBounceChart(data) {


    var options = {

       
        hAxis: {
            title: 'Date',
        },
        vAxis: {
            title: 'Bounce Rate',
            format: '#\'%\'',
        },
        width: GLOBALWIDTH,
        height: 385,
        colors: CHARTCOLOURS,
        chartArea: {
            width: GLOBALCHARTAREAWIDTH
        }

    };
    options['legend'] = 'top';
    var chart = new google.visualization.LineChart(document.getElementById('session-linebounce'));
    //var chart = new google.charts.LineChart(document.getElementById('session-line'));

    chart.draw(data, options);
}
google.load("visualization", "1", { packages: ["geomap"] });


function drawMap(data) {


    var options = {
        backgroundColor: '#ffffff',
        displayMode: 'markers',
        datalessRegionColor: '#e5f5fb'
    };
    options['region'] = 'world';
    options['dataMode'] = 'markers';
    options['showLegend'] = true;
    options['colors'] = ['#b2e1f5', '#009bdf'];
    options['width'] = GLOBALWIDTH;
    options['height'] = GLOBALMAPHEIGHT;
   

    var container = document.getElementById('session-map');
    var geomap = new google.visualization.GeoChart(container);

    geomap.draw(data, options);
};
google.load("visualization", "1", { packages: ["corechart"] });
//google.setOnLoadCallback(drawChart);

function drawChart(data) {

    var options = {
        width:320,
        
        
       
        colors: CHARTCOLOURS,
    };
  
    var chart = new google.visualization.PieChart(document.getElementById('session-pie'));

    chart.draw(data, options);
}


// Add an event listener to the 'auth-button'.
google.load("visualization", "1", { packages: ["motionchart"] });

function drawMotionChart(data) {
    
   

    var chart = new google.visualization.MotionChart(document.getElementById('session-motion'));

    chart.draw(data, { width: GLOBALWIDTH, height: GLOBALHEIGHT });
}

ko.bindingHandlers.toggleTView = {
    init: function (element, valueAccessor) {
      
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var currentView = valueAccessor();
            
            if (currentView == 'sum') {
                $('#session-container').hide();
                $('#location-container').hide();
                $('#time-container').hide();
                $('#motion-container').hide();
                $('#bounce-container').hide();
                $('#dura-container').hide();
                $('#session-btn').removeClass('view-selected');
                $('#location-btn').removeClass('view-selected');
                $('#time-btn').removeClass('view-selected');
                $('#motion-btn').removeClass('view-selected');
                $('#bounce-btn').removeClass('view-selected');
                $('#duration-btn').removeClass('view-selected');
                $('#bounce-btn').removeClass('view-selected');
                $('#summary-container').show();
                $('#summary-btn').addClass('view-selected');
                
            } else if (currentView == 'ses') {
                $('#summary-container').hide();
                $('#location-container').hide();
                $('#time-container').hide();
                $('#motion-container').hide();
                $('#bounce-container').hide();
                $('#dura-container').hide();
                $('#summary-btn').removeClass('view-selected');
                $('#location-btn').removeClass('view-selected');
                $('#time-btn').removeClass('view-selected');
                $('#motion-btn').removeClass('view-selected');
                $('#duration-btn').removeClass('view-selected');
                $('#bounce-btn').removeClass('view-selected');
                $('#session-container').show();
                $('#session-btn').addClass('view-selected');
                queryAccountsSess();
            } else if (currentView == 'loc') {
                $('#summary-container').hide();
                $('#session-container').hide();
                $('#time-container').hide();
                $('#motion-container').hide();
                $('#bounce-container').hide();
                $('#dura-container').hide();
                $('#summary-btn').removeClass('view-selected');
                $('#session-btn').removeClass('view-selected');
                $('#time-btn').removeClass('view-selected');
                $('#motion-btn').removeClass('view-selected');
                $('#duration-btn').removeClass('view-selected');
                $('#bounce-btn').removeClass('view-selected');
                $('#location-container').show();
                $('#location-btn').addClass('view-selected');
            } else if (currentView == 'tim') {
                $('#summary-container').hide();
                $('#session-container').hide();
                $('#location-container').hide();
                $('#motion-container').hide();
                $('#bounce-container').hide();
                $('#dura-container').hide();
                $('#summary-btn').removeClass('view-selected');
                $('#session-btn').removeClass('view-selected');
                $('#location-btn').removeClass('view-selected');
                $('#motion-btn').removeClass('view-selected');
                $('#duration-btn').removeClass('view-selected');
                $('#bounce-btn').removeClass('view-selected');
                $('#time-container').show();
                $('#time-btn').addClass('view-selected');
                queryAccountsTime();
            }
            else if (currentView == 'mot') {
                $('#summary-container').hide();
                $('#session-container').hide();
                $('#time-container').hide();
                $('#location-container').hide();
                $('#bounce-container').hide();
                $('#dura-container').hide();
                $('#summary-btn').removeClass('view-selected');
                $('#session-btn').removeClass('view-selected');
                $('#time-btn').removeClass('view-selected');
                $('#location-btn').removeClass('view-selected');
                $('#duration-btn').removeClass('view-selected');
                $('#bounce-btn').removeClass('view-selected');
                $('#motion-container').show();
                $('#motion-btn').addClass('view-selected');
                queryAccountsTime();
            }
            else if (currentView == 'dur') {
                $('#summary-container').hide();
                $('#session-container').hide();
                $('#motion-container').hide();
                $('#time-container').hide();
                $('#location-container').hide();
                $('#bounce-container').hide();
                $('#summary-btn').removeClass('view-selected');
                $('#session-btn').removeClass('view-selected');
                $('#motion-btn').removeClass('view-selected');
                $('#time-btn').removeClass('view-selected');
                $('#location-btn').removeClass('view-selected');
                $('#bounce-btn').removeClass('view-selected');
                $('#dura-container').show();
                $('#duration-btn').addClass('view-selected');
                queryAccountsDura();
                
            }
            else if (currentView == 'bou') {
                $('#summary-container').hide();
                $('#session-container').hide();
                $('#motion-container').hide();
                $('#time-container').hide();
                $('#location-container').hide();
                $('#dura-container').hide();
                $('#summary-btn').removeClass('view-selected');
                $('#session-btn').removeClass('view-selected');
                $('#motion-btn').removeClass('view-selected');
                $('#time-btn').removeClass('view-selected');
                $('#location-btn').removeClass('view-selected');
                $('#duration-btn').removeClass('view-selected');
                $('#bounce-container').show();
                $('#bounce-btn').addClass('view-selected');
                queryAccountsBoun();
            }
        });
    }
};