﻿
function Organisation() {
        var self = this;
        self.RegisteredName = ko.observable("");
        self.RegisteredNumber = ko.observable();
        self.BrandName = ko.observable();
        self.BrandWebsite = ko.observable();    
        self.Industry = ko.observable();   
        self.Address = ko.observable();
        self.Country = ko.observable();   
        self.State = ko.observable();  
        self.City = ko.observable();
        self.LogoImage = ko.observable();
        self.PhoneNumber = ko.observable();
        self.EmailAddress = ko.observable();
        self.CountryPhone = ko.observable();
        self.Industry = ko.observable();
    }

function UserProfile() {
    var self = this;
    self.UserName = ko.observable();
    self.FirstName = ko.observable();
    self.LastName = ko.observable();
    self.EmailAddress = ko.observable();
    self.PhoneNumber = ko.observable();
    self.Title = ko.observable();
}

function RegisterModel() {
    var self = this;
    self.OrgSub = ko.observable(new Organisation());
    self.UserProfile = ko.observable(new UserProfile());
    self.Password = ko.observable();
}

$(document).ready(function () {

    function AppViewModel() {
        var self = this;
        self.RegisterName = ko.observable();
        self.Register = ko.observable(new RegisterModel());
        self.CountryPhones = ko.observableArray(countryPhones);
        self.Industries = ko.observableArray(industries);
        self.registerOrg = function () {
            var registerData = ko.toJSON(self.Register());
            $.ajax({
                type: "POST",
                url: '/api/Account/Register',
                data: registerData,
                contentType: 'application/json',
                dataType: 'json'
            }).done(function (data) {
                alert(JSON.stringify(data));
            }).fail(function (data) {
                alert(JSON.stringify(data));
            });
        }
    }

    ko.applyBindings(new AppViewModel());
});

