﻿
ko.bindingHandlers.checkBox = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var observable = ko.utils.unwrapObservable(valueAccessor());
        if (observable) {
            $('#show-pwd-checkbox').removeClass("icon-check-empty");
            $(element).addClass("icon-check-sign");
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var observable = valueAccessor();
            if ($('#show-pwd-checkbox').hasClass("icon-check-empty")) {
                $('#show-pwd-checkbox').removeClass("icon-check-empty");
                $('#show-pwd-checkbox').addClass("icon-check-sign");
                observable(true);
            } else if ($('#show-pwd-checkbox').hasClass("icon-check-sign")) {
                $('#show-pwd-checkbox').removeClass("icon-check-sign");
                $('#show-pwd-checkbox').addClass("icon-check-empty");
                observable(false);
            }
        });
    }
}

function LoginModel() {
    var self = this;
    self.UserName = ko.observable();
    self.Password = ko.observable();
    self.IsRememberLogin = ko.observable(false);
}

function AppViewModel() {
    var self = this;
    self.Login = ko.observable(new LoginModel());
    self.ServerMessages = ko.observable();

    self.loginUser = function (element) {
        var validator = $("#login-form").validate({
            errorPlacement: function (error, element) {
            },
            unhighlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea")) {
                    $(element).removeClass('error-required');
                }
            },
            highlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea"))
                    $(element).addClass('error-required');
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

        if (validator.form()) {
            self.ServerMessages(null);
            $('#login-btn').attr("disabled", true);
            $('#login-btn').text('Logging in');
            var data = ko.toJSON(self.Login());
            $.ajax({
                type: "POST",
                url: '/api/Account/Login',
                data: data,
                contentType: 'application/json',
                dataType: 'json'
            }).success(function (data) {
                $.ajax({
                    type: "GET",
                    url: '/api/Account/RedirectURL',
                    contentType: 'application/json',
                    dataType: 'json'
                }).success(function (redirectURL) {
                    window.location = redirectURL;
                }).fail(function () {
                    $('#login-btn').addClass("btn-fail");
                    $('#login-btn').text("Login failed");
                    $('#login-btn').attr("disabled", true);
                    $('#usernameField').addClass('login-fail');
                    $('#passwordField').addClass('login-fail');

                    setTimeout(function () {
                        $('#login-btn').text("Login");
                        $('#login-btn').removeClass("btn-fail");
                        $('#login-btn').attr("disabled", false);
                        $('#usernameField').removeClass('login-fail');
                        $('#passwordField').removeClass('login-fail');
                        $('#login-btn').bind();
                    }, 3000);
                });
            }).fail(function (data) {
                self.ServerMessages(data.responseJSON.ModelState);
                $('#login-btn').addClass("btn-fail");
                $('#login-btn').text("Login failed");
                $('#login-btn').attr("disabled", true);
                $('#usernameField').addClass('login-fail');
                $('#passwordField').addClass('login-fail');
                setTimeout(function () {
                    $('#login-btn').text("Login");
                    $('#login-btn').removeClass("btn-fail");
                    $('#login-btn').attr("disabled", false);
                    $('#usernameField').removeClass('login-fail');
                    $('#passwordField').removeClass('login-fail');
                    $('#login-btn').bind();
                }, 3000);
            });
        }
    }

    self.forgetPassword = function () {
        window.location = '/Account/ForgetPassword';
    }

    self.newOrganisation = function () {
        window.location = '/Account/RegisterOrg';
    }
}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('body-content'));
});
