﻿/***************************** Knockout Bindings **************************************************/


function AppViewModel() {
    var self = this;
    self.Survey = ko.observable(new SurveyModel());
    self.CurrentPage = ko.observable(1);
    self.ErrorSections = ko.observableArray([]);
    var url = document.URL;
    var params = url.split("/");
    var id = params.splice(3, params.length - 1).join("/");

    self.showPosition = function (position) {
        alert("Latitude: " + position.coords.latitude +
        "<br>Longitude: " + position.coords.longitude);
    }

    self.loadResultsView = function () {
        window.location.href = "/Result/" + id;
    }

    self.showError = function (error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation.");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.");
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out.");
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.");
                break;
        }
    }

    self.loadSurvey = function () {
        var urlString = "/api/GetPublishedSurvey/" + id;
        $.getJSON(urlString, function (allData) {
            //alert(ko.toJSON(allData));
            self.Survey(new SurveyModel(allData, true, false, false));
            $('.whiteoverlay').hide();
        });
    }

    self.validate = function () {       
        self.ErrorSections([]);

        var validateOverall = true;
        for (var i in self.Survey().Modules()) {
            if (self.Survey().Modules()[i].ModuleType() === MODULETYPES.QUESTION) {
                var moduleData = self.Survey().Modules()[i].ModuleData();
                var questionType = moduleData.QuestionType();
                var validateResult = false;
                if (moduleData.QuestionData().IsRequired()) {

                    switch (questionType) {

                        /* Validation for the rate,tag, text and choice questions */
                        case QUESTIONTYPES.RATE: 
                        case QUESTIONTYPES.TEXT:
                        case QUESTIONTYPES.TAG:
                        case QUESTIONTYPES.CHOICE:
                            validateResult = moduleData.AnswerData().Validate();
                            if (validateResult == false) {
                                $(".survey-sections").eq(i).addClass('validation-error');
                                self.ErrorSections.push($(".survey-sections").eq(i));
                            }
                            break;

                        /* Validation for the multiple type questions */
                        case QUESTIONTYPES.MULTIPLE:
                            validateResult = moduleData.AnswerData().Validate(moduleData.QuestionData().MultipleType());
                            if (validateResult == false) {
                                $(".survey-sections").eq(i).addClass('validation-error');
                                var surveySection = $(".survey-sections").eq(i);
                                self.ErrorSections.push(surveySection);
                                surveySection.find('.carousel-indicators').find("li").each(function (index, value) {
                                    if (!$(value).hasClass("completed")) {
                                        $(value).addClass("not-completed");
                                    }
                                    var carouselElement = surveySection.find('.carousel');
                                    carouselElement.carousel(surveySection.find('.carousel-indicators').find(".not-completed:first").index());
                                });
                            }
                            break;
                        
                        /* Validation for the demographic questions */
                        case QUESTIONTYPES.DEMOGRAPHIC:
                            var demographicForm = $(".survey-sections").eq(i).find("#demographics-form");

                            var validator = demographicForm.validate({
                                errorClass: 'validation-error'
                            });

                            
                            $.validator.addMethod("optionChoosen", function (value, element) {
                                return ($(element).multiselect("getChecked").length > 0);
                            });

                            $("#demographics-form").find('select').each(function (index) {
                                $(this).rules("add", {
                                    required: true,
                                    optionChoosen: true,
                                        messages: {
                                            required: "Please select an option",
                                            optionChoosen: "Please select an option"
                                        }
                                });
                            });

                            validateResult = validator.form();

                            $("#demographics-form").find('select').each(function (index) {
                                validateResult = $(this).valid();
                            });  

                            if (validateResult == false) {
                                var surveySection = $(".survey-sections").eq(i);
                                self.ErrorSections.push(surveySection);
                                surveySection.addClass('validation-error');
                            }

                            break;
                    }
                }
                if (validateOverall == true && validateResult == false) validateOverall = false;
            }
        }
        return validateOverall;
    }

    self.submitExperience = function () {
        var validateOverall = self.validate();
        if (validateOverall == false) {
            
            $.scrollTo(self.ErrorSections()[0], 100, { offset: -100 });
        }
        var d = document.getElementById("share-your-exp");
        if (validateOverall == true) {
            var data = ko.toJSON(self.Survey);
            $.ajax({
                type: "POST",
                data: data,
                url: "/api/PostSurveyResponse",
                contentType: "application/json",
                success: function (data) {
                    d.className = d.className + " share-your-exp-success";
                    self.CurrentPage(2);
                    $("html, body").animate({ scrollTop: 0 }, 100);
                },
                error: function () {
                    location.href = "/Error";
                }
            });
        }
    }
}


/*
$(document).ready(function () {
    var surveyResponse = new AppViewModel();
    surveyResponse.loadSurvey();
    ko.applyBindings(surveyResponse);
    $('.whiteoverlay').width($(document).width());
    $('.whiteoverlay').height($(document).height());
    var offsetTop = ($(window).height() - ($(window).outerHeight()) / 2 - ($('.loading-spinner').outerHeight()) / 2);
    $('.loading-spinner').css('padding-top', offsetTop + "px");
});
*/
