﻿function ForgotPwdModel() {
    var self = this;
    self.UserId = ko.observable();
    self.baseURL = window.location.protocol + "//" + window.location.host;
}

function AppViewModel() {
    var self = this;
    self.ForgotPwd = ko.observable(new ForgotPwdModel());

        self.sendVerificationLink = function () {

            var validator = $("#verification-form").validate({
                errorPlacement: function (error, element) {
                },
                unhighlight: function (element, errorClass, validClass) {
                    if ($(element).is("input") || $(element).is("textarea")) {
                        $(element).removeClass('error-required');
                    }
                },
                highlight: function (element, errorClass, validClass) {
                    if ($(element).is("input") || $(element).is("textarea"))
                        $(element).addClass('error-required');
                },
                invalidHandler: function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        validator.errorList[0].element.focus();
                    }
                }
            });

            if (validator.form()) {
            var data = ko.toJSON(self.ForgotPwd());
            $("#verification-button").text("Sending");
            $("#verification-button").unbind();
            $.ajax({
                type: "POST",
                url: '/api/Account/ForgotPassword',
                data: data,
                contentType: 'application/json'
            }).success(function () {
                $(".verification").hide();
                $(".confirmation").show();
            }).fail(function () {
                $("#verification-button").text("Please check your User ID.")
                $('#verification-button').addClass("btn-fail");
                $('#verification-button').attr("disabled", true);
                setTimeout(function () {
                    $('#verification-button').text("Send Verification Email");
                    $('#verification-button').removeClass("btn-fail");
                    $('#verification-button').attr("disabled", false);
                }, 3000);
            });
        }
        }

        self.resendVerificationLink = function () {
            var data = ko.toJSON(self.ForgotPwd());
            $("#resend-button").text("Resending Verification Email")
            $.ajax({
                type: "POST",
                url: '/api/Account/ForgotPassword',
                data: data,
                contentType: 'application/json'
            }).success(function () {

                self.ServerMessages(null);

                $("#resend-button").text("Resent Verification Email");
                $('#resend-button').addClass("btn-success");
                $('#resend-button').attr("disabled", true);

                setTimeout(function () {
                    $('#resend-button').text("Resend Verification Email");
                    $('#resend-button').removeClass("btn-success");
                    $('#resend-button').attr("disabled", false);

                }, 3000);

            }).fail(function () {

                self.ServerMessages(data.responseJSON.ModelState);
                $("#resend-button").text("Resending Failed.")
                $('#resend-button').addClass("btn-fail");
                $('#resend-button').attr("disabled", true);

                setTimeout(function () {
                    $('#verification-button').text("Resend Verification Email");
                    $('#verification-button').removeClass("btn-fail");
                    $('#verification-button').attr("disabled", false);

                }, 3000);
            });
        }
}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('body-content'));
    $('body').show();
});