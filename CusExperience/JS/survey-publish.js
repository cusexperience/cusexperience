﻿/***************************** Knockout Bindings **************************************************/


function AppViewModel() {
    var self = this;
    self.Survey = ko.observable(new SurveyModel(initJSONData, true, false, false));
    self.CurrentPage = ko.observable(1);
    self.ErrorSections = ko.observableArray([]);
    var url = document.URL;
    var params = url.split("/");
    var id = params.splice(3, params.length - 1).join("/");

    self.CurrentURL = document.URL;
    self.CurrentBaseURL = window.location.protocol + "//" + window.location.host;
    self.SurveyPID = id;
    self.Touchpoints = ko.observableArray([]);
    self.AvlblTouchpoints = ko.observableArray([]);
    self.Subsidiaries = ko.observableArray([]);

    self.Subsidiaries.push({ key: self.Survey().OrgSub().RegisteredName(), value: self.Survey().OrgSub().OrgSubID });

    for (var index = 0 ; index < self.Survey().OrgSub().Touchpoints.length ; index++) {
        self.Touchpoints.push({ key: self.Survey().OrgSub().Touchpoints[index].TouchpointName(), value: self.Survey().OrgSub().Touchpoints[index].TouchpointID, valueObject: self.Survey().OrgSub().Touchpoints[index], OrgSubID: self.Survey().OrgSub().OrgSubID });
        if (self.Survey().OrgSub().OrgSubID() == self.Survey().SelectedSubsidiaries()[0])
            self.AvlblTouchpoints.push({ key: self.Survey().OrgSub().Touchpoints[index].TouchpointName(), value: self.Survey().OrgSub().Touchpoints[index].TouchpointID, valueObject: self.Survey().OrgSub().Touchpoints[index], OrgSubID: self.Survey().OrgSub().OrgSubID });
    }

    function addSubsidiaries(subsidiaries) {
        if (subsidiaries.length >= 1) {
            for (var index = 0 ; index < subsidiaries.length ; index++) {
                self.Subsidiaries.push({ key: subsidiaries[index].RegisteredName(), value: subsidiaries[index].OrgSubID });
                for (var index3 = 0 ; index3 < subsidiaries[index].Touchpoints.length ; index3++) {
                    self.Touchpoints.push({ key: subsidiaries[index].Touchpoints[index3].TouchpointName(), value: subsidiaries[index].Touchpoints[index3].TouchpointID, valueObject: subsidiaries[index].Touchpoints[index3], OrgSubID: subsidiaries[index].OrgSubID() });
                    if (subsidiaries[index].OrgSubID() == self.Survey().SelectedSubsidiaries()[0]) {
                        self.AvlblTouchpoints.push({ key: subsidiaries[index].Touchpoints[index3].TouchpointName(), value: subsidiaries[index].Touchpoints[index3].TouchpointID, valueObject: subsidiaries[index].Touchpoints[index3], OrgSubID: subsidiaries[index].OrgSubID() });
                    }
                }
                addSubsidiaries(subsidiaries[index].Subsidiaries());
            }
        }
    }
    subsidiaries = self.Survey().OrgSub().Subsidiaries();

    for (var index1 = 0 ; index1 < subsidiaries.length ; index1++) {
        self.Subsidiaries.push({ key: subsidiaries[index1].RegisteredName(), value: subsidiaries[index1].OrgSubID });
        for (var index2 = 0 ; index2 < subsidiaries[index1].Touchpoints.length ; index2++) {
            self.Touchpoints.push({ key: subsidiaries[index1].Touchpoints[index2].TouchpointName(), value: subsidiaries[index1].Touchpoints[index2].TouchpointID, valueObject: subsidiaries[index1].Touchpoints[index2], OrgSubID: subsidiaries[index1].OrgSubID() });
            if (subsidiaries[index1].OrgSubID() === self.Survey().SelectedSubsidiaries()[0])
                self.AvlblTouchpoints.push({ key: subsidiaries[index1].Touchpoints[index2].TouchpointName(), value: subsidiaries[index1].Touchpoints[index2].TouchpointID, valueObject: subsidiaries[index1].Touchpoints[index2], OrgSubID: subsidiaries[index1].OrgSubID() });
        }
        addSubsidiaries(subsidiaries[index1].Subsidiaries());
    }



    self.getAddressLink = function (address) {
        return ko.computed({
            read: function () {
                address = $('<div>').append(address).text()
                return 'http://maps.google.com/?q=' + address.replace(/[#,-]/g, ' ');
            }
        });
    }

    self.showPosition = function (position) {
        alert("Latitude: " + position.coords.latitude +
        "<br>Longitude: " + position.coords.longitude);
    }

    self.loadResultsView = function () {
        window.location.href = "/Result/" + id;
    }

    self.showError = function (error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation.");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.");
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out.");
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.");
                break;
        }
    }

    self.validate = function () {
        self.ErrorSections([]);

        var validateOverall = true;

        for (var i in self.Survey().Modules()) {
            var validateResult = true;

            if (self.Survey().Modules()[i].ModuleType() === MODULETYPES.QUESTION) {
                var moduleData = self.Survey().Modules()[i].ModuleData();
                var questionType = moduleData.QuestionType();

                switch (questionType) {

                    /* Validation for the rate,tag, text and choice questions */
                    case QUESTIONTYPES.RATE:
                    case QUESTIONTYPES.TEXT:
                    case QUESTIONTYPES.TAG:
                    case QUESTIONTYPES.CHOICE:
                        if (moduleData.QuestionData().IsRequired()) {
                            validateResult = moduleData.AnswerData().Validate();
                            if (validateResult == false) {
                                $(".survey-sections").eq(i).addClass('validation-error');
                                self.ErrorSections.push($(".survey-sections").eq(i));
                            }
                        }
                        break;

                        /* Validation for the multiple type questions */
                    case QUESTIONTYPES.MULTIPLE:
                        if (moduleData.QuestionData().IsRequired()) {
                            validateResult = moduleData.AnswerData().Validate(moduleData.QuestionData().MultipleType());
                            if (validateResult == false) {
                                $(".survey-sections").eq(i).addClass('validation-error');
                                var surveySection = $(".survey-sections").eq(i);
                                self.ErrorSections.push(surveySection);
                                surveySection.find('.carousel-indicators').find("li").each(function (index, value) {
                                    if (!$(value).hasClass("completed")) {
                                        $(value).addClass("not-completed");
                                    }
                                    var carouselElement = surveySection.find('.carousel');
                                    carouselElement.carousel(surveySection.find('.carousel-indicators').find(".not-completed:first").index());
                                });
                            }
                        }
                        break;

                        /* Validation for the demographic questions */
                    case QUESTIONTYPES.DEMOGRAPHIC:

                        var demographicForm = $(".survey-sections").eq(i).find("#demographics-form");

                        $.validator.addMethod("phonevalidation",
                              function (value, element) {
                                  return /^[0-9-+# ]+$/.test(value) || value === "";
                              },
                             "Please enter a valid Phone Number"
                         );

                        $.validator.addMethod("emailvalidation",
                            function (value, element) {
                                    return /\S+@\S+\.\S+/.test(value);
                            },
                            "Please enter a valid Email"
                        );

                        $.validator.addClassRules('Email', { emailvalidation: true });
                        $.validator.addClassRules('Mobile', { phonevalidation: true });

                        var validator = demographicForm.validate({
                            errorPlacement: function (error, element) {
                                if (error.text() == "Please enter a valid Email") {
                                    error.insertAfter(element);
                                }
                                else if (error.text() == "Please enter a valid Phone Number") {
                                    error.insertAfter(element);
                                }
                            },
                            unhighlight: function (element, errorClass, validClass) {
                                if ($(element).is("input")) {
                                    $(element).removeClass('error-required');
                                    $(element).closest('.survey-sections').removeClass('validation-error');
                                }
                            },
                            highlight: function (element, errorClass, validClass) {
                                if ($(element).is("input"))
                                    $(element).addClass('error-required');
                                if ($(element).is("select")) {
                                    var button = $(element).multiselect("getButton");
                                    $(button).addClass('error-required');
                                }
                            },
                            onkeyup: function (element, event) {
                                if ($(element).val() == '') {
                                    var id = "#" + element.id + "-error";
                                    $(id).remove();
                                }
                            },
                        });

                        validateResult = validator.form();

                        demographicForm.find('select').each(function (index) {
                            var selectResult = $(this).valid();
                            if (selectResult == false)
                                validateResult = false;
                        });

                        if (validateResult == false) {
                            var surveySection = $(".survey-sections").eq(i);
                            self.ErrorSections.push(surveySection);
                            surveySection.addClass('validation-error');
                        }

                        break;
                }
            } else if (self.Survey().Modules()[i].ModuleType() === MODULETYPES.AUTHENTICATION) {
                var surveySection = $(".survey-sections").eq(i);
                var CaptchaKey = surveySection.find('input[name=sckey]').val();
                var CaptchaValue = surveySection.find('input[name=scvalue]').val();
                if (CaptchaValue === "0") {
                    validateResult = false;
                    self.ErrorSections.push(surveySection);
                    surveySection.addClass('validation-error');
                }
                var data = { "CaptchaKey": CaptchaKey, "CaptchaValue": CaptchaValue };
                if (validateResult != false) {
                    $.ajax({
                        type: "POST",
                        data: data,
                        dataType: 'json',
                        async: false,
                        url: "/api/ValidateCaptcha"
                    }).success(function () {

                    }).fail(function (httpObj, data) {
                        validateResult = false;
                        var surveySection = $(".survey-sections").eq(i);
                        self.ErrorSections.push(surveySection);
                        surveySection.addClass('validation-error');
                    });
                }
                if (validateResult != true) {
                    surveySection.find('.captchadiv').one('mousedown', function () {
                        surveySection.removeClass('validation-error');
                        self.ErrorSections.splice(i, 1);
                    });
                }
            }
            if (validateOverall == true && validateResult == false) validateOverall = false;
        }
        return validateOverall;
    }

    self.submitExperience = function () {
        var validateOverall = self.validate();
        if (validateOverall == false) {
            $.scrollTo(self.ErrorSections()[0], 100, { offset: -100 });
        }

        var d = document.getElementById("share-your-exp");
        if (validateOverall == true) {
            var data = ko.toJSON(self.Survey);
            //console.log(data);
            $('#share-submit-btn').text("Submitting");
            $("#share-your-exp").unbind();

            $.ajax({
                type: "POST",
                data: data,
                url: "/api/PostSurveyResponse",
                contentType: "application/json",
                success: function (data) {
                    d.className = d.className + " share-your-exp-success";
                    self.CurrentPage(2);
                    window.scrollTo(0, 0);
                    $("html, body").animate({ scrollTop: $("#Survey").offset().top }, 100);
                    $("body").animate({ scrollTop: $("#Survey").offset().top }, 100);

                },
                error: function () {
                    // location.href = "/Error";
                }
            });
        }
    }
}

$(document).ready(function () {
    var surveyResponse = new AppViewModel();
    ko.applyBindings(surveyResponse, document.getElementById('body-content'));
    $('body').show();
});


