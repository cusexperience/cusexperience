﻿function ResetPwdModel(userId, code) {
    var self = this;
    self.UserId = ko.observable(userId);
    self.Password = ko.observable();
    self.ConfirmPassword = ko.observable();
    self.Code = ko.observable(code);
}
function showPassword() {

    if ($('#show-pwd-checkbox').hasClass("icon-check-empty")) {

        $('#show-pwd-checkbox').removeClass("icon-check-empty");
        $('#show-pwd-checkbox').addClass("icon-check-sign");
        $('#NewPassword').attr("type", "text");
        
    } else {
        $('#show-pwd-checkbox').removeClass("icon-check-sign");
        $('#show-pwd-checkbox').addClass("icon-check-empty");
        $('#NewPassword').attr("type", "password");
        
    }
}

function AppViewModel() {

    var self = this;
    self.ServerMessages = ko.observable();

    self.URLParams = ko.computed(function () {
        var match,
            pl = /\+/g,  // Regex for replacing addition symbol with a space
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
            query = window.location.search.substring(1);

        var urlParams = {};
        while (match = search.exec(query))
            urlParams[decode(match[1])] = decode(match[2]);
        return urlParams;
    });

    self.ResetPwd = ko.observable(new ResetPwdModel(self.URLParams()["userId"],self.URLParams()["code"]));

    self.resetPassword = function () {
        var data = ko.toJSON(self.ResetPwd);

        var validator = $("#resetpwd-form").validate({
            rules: {
                ConfirmPassword: {
                    equalTo: "#NewPassword"
                }
            },
            messages: {
                ConfirmPassword: {
                    equalTo: "Password and Confirm Password should be the same."
                }
            },
            errorPlacement: function (error, element) {
                if (error.text() == "Password and Confirm Password should be the same.") {
                    error.insertAfter(element);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea")) {
                    $(element).removeClass('error-required');
                }
            },
            highlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea"))
                    $(element).addClass('error-required');
                if ($(element).is("select")) {
                    var button = $(element).multiselect("getButton");
                    $(button).addClass('error-required');
                }
            },
            onkeyup: function (element, event) {
                if ($(element).val() == '') {
                    var id = "#" + element.id + "-error";
                    $(id).remove();
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                    var invalidElement = validator.errorList[0].element;
                    $(invalidElement).addClass('error-required')
                }
            }
        });

        validateResult = validator.form();

        if (validateResult) {

            $('#resetpassword').text("Resetting Password");

            $.ajax({
                type: "POST",
                url: '/api/Account/ResetPassword',
                data: data,
                contentType: 'application/json',
                dataType: 'json'
            }).success(function () {
                self.ServerMessages(null);
                $('#resetpassword').addClass("btn-success");
                $('#resetpassword').text("Password Reset");
                $('#resetpassword').attr("disabled", true);

                setTimeout(function () {
                    window.location = "/Account/Login";
                }, 3000);

            }).fail(function (data) {
                $('#resetpassword').addClass("btn-fail");
                $('#resetpassword').text("Reset Failed");
                $('#resetpassword').attr("disabled", true);
                self.ServerMessages(data.responseJSON.ModelState);
                setTimeout(function () {
                    $('#resetpassword').text("Reset Password");
                    $('#resetpassword').removeClass("btn-fail");
                    $('#resetpassword').attr("disabled", false);
                    self.ServerMessages(null);
                }, 3000);
            });
        }
    }
}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('body-content'));
    $('body').show();
});