﻿ko.bindingHandlers.select = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

ko.bindingHandlers.autosize = {
    init: function (element, valueAccessor) {
        $(element).autosize();
        if ($(document).width() <= 320)
            $(element).css('height', '38px');
        else
            $(element).css('height', '72px');
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

function AppViewModel() {
    var self = this;
    self.OrgSub = ko.observable(new OrgSubModel(initJSONData));
    var url = document.URL;
    var params = url.split("Detail/");
    var id = params[1];

    self.showOrgSub = function (data) {
        return "/OrgSub/Detail/" + id;
    }

    self.showTouchpoint = function () {
        return "/OrgSub/Touchpoint/" + id;
    }

    self.showUsers = function () {
        return "/Users/" + id;
    }

    self.saveOrgSub = function () {
        var data = ko.toJSON(self.OrgSub());

        $.validator.addMethod("phonevalidation",
        function (value, element) {
            return /^[0-9-+# ]+$/.test(value);
        },
        "Please enter a valid Phone Number"
       );

        $.validator.addMethod("emailvalidation",
                function (value, element) {
                    return /\S+@\S+\.\S+/.test(value);
                },
        "Please enter a valid Email"
        );
        
        var validator = $("#orgsub-form").validate({
            rules: {
                Email: {
                    emailvalidation: true,
                },
                PhoneNumber: {
                    phonevalidation: true
                },
            },
            messages: {
                Email: {
                    email: "Please enter a valid Email"
                },
                PhoneNumber: {
                    phonevalidation: "Please enter a valid Phone Number"
                },
            },
            errorPlacement: function (error, element) {
                if (error.text() == "Please enter a valid Email") {
                    error.insertAfter(element);
                }
                else if (error.text() == "Please enter a valid Phone Number") {
                    error.insertAfter(element);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea")) {
                    $(element).removeClass('error-required');
                }
            },
            highlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea"))
                    $(element).addClass('error-required');
                if ($(element).is("select")) {
                    var button = $(element).multiselect("getButton");
                    $(button).addClass('error-required');
                }
            },
            onkeyup: function (element, event) {
                if ($(element).val() == '') {
                    var id = "#" + element.id + "-error";
                    $(id).remove();
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

        if (validator.form()) {
            $('#savebutton').text("Saving");

            $.ajax({
                type: "POST",
                url: '/api/PostOrgSub',
                data: data,
                contentType: 'application/json'
                   
            }).success(function (allData) {
                $('#savebutton').addClass("btn-success");
                $('#savebutton').text("Saved");
                $('#savebutton').attr("disabled", true);

                setTimeout(function () {
                    $('#savebutton').text("Save");
                    $('#savebutton').removeClass("btn-success");
                    $('#savebutton').attr("disabled", false);
                }, 5000);
           
            }).fail(function (allDate) {
                $('#savebutton').addClass("btn-fail");
                $('#savebutton').text("Save Fail");
                $('#savebutton').attr("disabled", true);

                setTimeout(function () {
                    $('#savebutton').text("Save");
                    $('#savebutton').removeClass("btn-fail");
                    $('#savebutton').attr("disabled", false);
                }, 5000);
            });
        }
    }
}

$(document).ready(function () {
    var appViewModel = new AppViewModel();
    ko.applyBindings(appViewModel, document.getElementById('body-content'));
    $('body').show();
});