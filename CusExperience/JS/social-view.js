﻿
ko.bindingHandlers.toggleSurveyResponse = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if ($(element).find('span').hasClass("icon-plus")) {
                var surveyResponseDiv = $(element).parent().find('.surveyResponse');
                var domElement = surveyResponseDiv.get(0);
                ko.cleanNode(domElement);
                var surveyResponse = new ResponseViewModel();
                surveyResponse.loadSurveyResponse(domElement, bindingContext.$data.SurveyResponseID);
                $(element).find('span').removeClass("icon-plus");
                $(element).find('span').addClass("icon-minus");
            } else {
                var surveyResponseDiv = $(element).parent().find('.surveyResponse');
                surveyResponseDiv.hide();
                surveyResponseDiv.empty();
                $(element).find('span').removeClass("icon-minus");
                $(element).find('span').addClass("icon-plus");
            }
        });
    }
}

ko.bindingHandlers.selectOrder = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });


    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

        $(element).on('multiselectclick', function () {
            bindingContext.$root.OrderBy($(element).val());
            bindingContext.$root.loadCustomerData();
        });

        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

ko.bindingHandlers.pagination = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).pagination({
            items: bindingContext.$root.TotalResponses(),
            itemsOnPage: bindingContext.$root.NoofRecords(),
            currentPage: bindingContext.$root.CurrentPage(),
            displayedPages: 3,
            cssStyle: "light-theme",
            onPageClick: bindingContext.$root.loadSurveyResponses
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).pagination({
            items: bindingContext.$root.TotalResponses(),
            itemsOnPage: bindingContext.$root.NoofRecords(),
            currentPage: bindingContext.$root.CurrentPage(),
            displayedPages: 3,
            cssStyle: "light-theme",
            onPageClick: bindingContext.$root.loadSurveyResponses
        });
    }
};

ko.bindingHandlers.activateLinks = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).linkify();
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).linkify();
    }
}

function SocialViewModel() {
    var self = this;
    self.Survey = ko.observable((new SurveyModel(initJSONData, false, false, false)));
    self.AnalyticsTitle = ko.observable(self.Survey().SurveyTitle() + " Analytics");

    self.Tweets = ko.observableArray([]);
    self.CurrentPage = ko.observable(1);
    self.TotalResponses = ko.observable();
    self.Pages = ko.observableArray();
    self.NoofRecords = ko.observable(10);
    self.OrderBy = ko.observable('desc');
    self.OrderByValue = ko.observable('asc');
    self.hashTags = ko.observableArray([ko.observable(""), ko.observable(""), ko.observable("")]);

    var url = document.URL;
    var params = url.split("SocialView/");
    var id = params[1];
    var socialUrlString = "/api/GetTwitterMessages/" + id;

    self.generateSNo = function (index) {
        return ((self.CurrentPage() - 1) * self.NoofRecords()) + index() + 1;
    }

    self.loadIndexView = function () {
        return "/Analytics/Detail/" + id;
    }

    self.loadCustomerView = function () {
        return "/Analytics/CustomerView/" + id;
    }

    self.loadSocialView = function () {
        return "/Analytics/SocialView/" + id;
    }

    self.loadTrackingView = function () {
        return "/Analytics/TrackingView/" + id;
    }

    self.loadSocialData = function () {
        var data = ko.toJSON(self.hashTags);
        $.ajax({
            type: "POST",
            data: data,
            url: socialUrlString,
            contentType: "application/json",
        }).success(function (allData) {
            self.Tweets(allData.tweetsJSON);
        }).fail(function (httpObj, data) {
        });
    }
}

$(document).ready(function () {
    var socialViewModel = new SocialViewModel();
    ko.applyBindings(socialViewModel, document.getElementById('body-content'));
    $('body').show();
});