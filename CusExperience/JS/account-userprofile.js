﻿ko.bindingHandlers.select = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);

        $(element).bind('multiselectclick', function (event,ui) {
            if (ui.value.toLowerCase() === "others") {
                $('.othersdiv').show();
            } else {
                $('.othersdiv').hide();
            }
        });
    }
};

function UserModel(data) {
    var self = this;
    self.UserName = ko.observable(data != null ? data.UserName : null);
    self.Title = ko.observable(data != null ? data.Title : null);
    self.OthersTitle = ko.observable(data != null ? data.OthersTitle : null);
    self.FirstName = ko.observable(data!=null? data.FirstName: null);
    self.LastName = ko.observable(data!=null? data.LastName: null);
    self.Email = ko.observable(data!=null? data.Email: null);
    self.PhoneNumber = ko.observable(data != null ? data.PhoneNumber : null);
    self.Country = ko.observable(data != null ? data.Country : null);
    self.Designation = ko.observable(data != null ? data.Designation : null);
    self.OrgSubName = ko.observable(data != null && data.OrgSub != null ? data.OrgSub.RegisteredName : null);
}

function AppViewModel() {
    var self = this;
    self.User = ko.observable(new UserModel(initJSONData));

    self.saveProfile = function () {
        var data = ko.toJSON(self.User());
        var userFormValidateResult = false;

        $.validator.addMethod("phonevalidation",
                function (value, element) {
                    return /^[0-9-+# ]+$/.test(value);
                },
        "Please enter a valid Phone Number"
        );

        $.validator.addMethod("emailvalidation",
                function (value, element) {
                    return /\S+@\S+\.\S+/.test(value);
                },
        "Please enter a valid Email"
        );

        var validator = $("#userprofile-form").validate({
            rules: {
                Email: {
                    emailvalidation: true,
                },
                PhoneNumber: {
                    phonevalidation: true
                },
            },
            messages: {
                Email: {
                    email: "Please enter a valid Email"
                },
                PhoneNumber: {
                    phonevalidation: "Please enter a valid Phone Number"
                },
            },
            errorPlacement: function (error, element) {
                if (error.text() == "Please enter a valid Email") {
                    error.insertAfter(element);
                }
                else if (error.text() == "Please enter a valid Phone Number") {
                    error.insertAfter(element);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea")) {
                    $(element).removeClass('error-required');
                }
            },
            highlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea"))
                    $(element).addClass('error-required');
                if ($(element).is("select")) {
                    var button = $(element).multiselect("getButton");
                    $(button).addClass('error-required');
                }
            },
            onkeyup: function (element, event) {
                if ($(element).val() == '') {
                    var id = "#" + element.id + "-error";
                    $(id).remove();
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

        userFormValidateResult = validator.form();

        if (userFormValidateResult) {

            $('#savebutton').text("Saving");

            $.ajax({
                type: "POST",
                url: '/api/PostUserProfile',
                data: data,
                contentType: 'application/json'
              
            }).success(function (allData) {

                $('#savebutton').addClass("btn-success");
                $('#savebutton').text("Saved");
                $('#savebutton').attr("disabled", true);

                setTimeout(function () {
                    $('#savebutton').text("Save");
                    $('#savebutton').removeClass("btn-success");
                    $('#savebutton').attr("disabled", false);
                }, 5000);
                
            }).fail(function (allData) {

                $('#savebutton').addClass("btn-fail");
                $('#savebutton').text("Save Failed");
                $('#savebutton').attr("disabled", true);

                setTimeout(function () {
                    $('#savebutton').text("Save");
                    $('#savebutton').removeClass("btn-fail")
                    $('#savebutton').attr("disabled", false);
                }, 5000);
            });
        }
    }

}

$(document).ready(function () {
    var appViewModel = new AppViewModel();
    ko.applyBindings(appViewModel, document.getElementById('body-content'));
    $('body').show();
});