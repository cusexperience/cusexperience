﻿function getCallingCode(country) {
    if (country == "Afghanistan") {
        return "+93";
    } else if (country == "Albania") {
        return "+355"
    } else if (country == "Algeria") {
        return "+213"
    } else if (country == "American Samoa") {
        return "+1 (684)"
    } else if (country == "Andorra") {
        return "+376"
    } else if (country == "Angola") {
        return "+244"
    } else if (country == "Anguilla") {
        return "+1 (264)"
    } else if (country == "Antigua and Barbuda") {
        return "+1 (268)"
    } else if (country == "Argentina") {
        return "+54"
    } else if (country == "Armenia") {
        return "+374"
    } else if (country == "Aruba") {
        return "+297"
    } else if (country == "Ascension") {
        return "+247"
    } else if (country == "Australia") {
        return "+61"
    } else if (country == "Austria") {
        return "+43"
    } else if (country == "Azerbaijan") {
        return "+994"
    } else if (country == "Bahamas") {
        return "+1 (242)"
    } else if (country == "Bahrain") {
        return "+973"
    } else if (country == "Bangladesh") {
        return "+880"
    } else if (country == "Barbados") {
        return "+1 (246)"
    } else if (country == "Belarus") {
        return "+375"
    } else if (country == "Belgium") {
        return "+32"
    } else if (country == "Belize") {
        return "+501"
    } else if (country == "Benin") {
        return "+229"
    } else if (country == "Bermuda") {
        return "+1 (441)"
    } else if (country == "Bhutan") {
        return "+975"
    } else if (country == "Bolivia") {
        return "+591"
    } else if (country == "Bosnia and Herzegowina") {
        return "+387"
    } else if (country == "Botswana") {
        return "+267"
    } else if (country == "Brazil") {
        return "+55"
    } else if (country == "British Indian Ocean Territory") {
        return "+246"
    } else if (country == "Brunei Darussalam") {
        return "+673"
    } else if (country == "Bulgaria") {
        return "+359"
    } else if (country == "Burkina Faso") {
        return "+226"
    } else if (country == "Burma") {
        return "+95"
    } else if (country == "Burundi") {
        return "+257"
    } else if (country == "Cambodia") {
        return "+855"
    } else if (country == "Cameroon") {
        return "+237"
    } else if (country == "Canada") {
        return "+1"
    } else if (country == "Cape Verde") {
        return "+238"
    } else if (country == "Cayman Islands") {
        return "+1 (345)"
    } else if (country == "Central African Republic") {
        return "+236"
    } else if (country == "Chad") {
        return "+235"
    } else if (country == "Chile") {
        return "+56"
    } else if (country == "China") {
        return "+86"
    } else if (country == "Colombia") {
        return "+57"
    } else if (country == "Comoros") {
        return "+269"
    } else if (country == "Christmas Island") {
        return "+61"
    } else if (country == "Cocos (Keeling) Islands") {
        return "+891"
    } else if (country == "Congo") {
        return "+242"
    } else if (country == "Congo, the Democratic Republic of the") {
        return "+243"
    } else if (country == "Cook Islands") {
        return "+682"
    } else if (country == "Costa Rica") {
        return "+506"
    } else if (country == "Cote d'Ivoire") {
        return "+225"
    } else if (country == "Croatia") {
        return "+385"
    } else if (country == "Cuba") {
        return "+53"
    } else if (country == "Cyprus") {
        return "+357"
    } else if (country == "Czech Republic") {
        return "+420"
    } else if (country == "Denmark") {
        return "+45"
    } else if (country == "Djibouti") {
        return "+253"
    } else if (country == "Dominica") {
        return "+1 (767)"
    } else if (country == "Dominican Republic") {
        return "+1 (809/829)"
    } else if (country == "East Timor") {
        return "+670"
    } else if (country == "Ecuador") {
        return "+593"
    } else if (country == "Egypt") {
        return "+20"
    } else if (country == "El Salvador") {
        return "+503"
    } else if (country == "Equatorial Guinea") {
        return "+240"
    } else if (country == "Eritrea") {
        return "+291"
    } else if (country == "Estonia") {
        return "+372"
    } else if (country == "Falkland Islands (Malvinas)") {
        return "+500"
    } else if (country == "Faroe Islands") {
        return "+298"
    } else if (country == "Fiji") {
        return "+679"
    } else if (country == "Finland") {
        return "+358"
    } else if (country == "France") {
        return "+33"
    } else if (country == "France Metropolitan") {
        return "+33"
    } else if (country == "French Guiana") {
        return "+594"
    } else if (country == "French Polynesia") {
        return "+689"
    } else if (country == "French Southern Territories") {
        return "+33"
    } else if (country == "Gabon") {
        return "+241"
    } else if (country == "Gambia") {
        return "+220"
    } else if (country == "Georgia") {
        return "+995"
    } else if (country == "Germany") {
        return "+49"
    } else if (country == "Ghana") {
        return "+233"
    } else if (country == "Gibraltar") {
        return "+350"
    } else if (country == "Greece") {
        return "+30"
    } else if (country == "Greenland") {
        return "+299"
    } else if (country == "Grenada") {
        return "+1 (473)"
    } else if (country == "Guadeloupe") {
        return "+590"
    } else if (country == "Guam") {
        return "+1 (671)"
    } else if (country == "Guatemala") {
        return "+502"
    } else if (country == "Guinea") {
        return "+224"
    } else if (country == "Guinea-Bissau") {
        return "+245"
    } else if (country == "Guyana") {
        return "+592"
    } else if (country == "Haiti") {
        return "+509"
    } else if (country == "Heard and Mc Donald Islands") {
        return "+61"
    } else if (country == "Holy See (Vatican City State)") {
        return "+39"
    } else if (country == "Honduras") {
        return "+504"
    } else if (country == "Hong Kong") {
        return "+852"
    } else if (country == "Hungary") {
        return "+36"
    } else if (country == "Iceland") {
        return "+354"
    } else if (country == "India") {
        return "+91"
    } else if (country == "Indonesia") {
        return "+62"
    } else if (country == "Iran (Islamic Republic of)") {
        return "+98"
    } else if (country == "Iraq") {
        return "+964"
    } else if (country == "Ireland") {
        return "+353"
    } else if (country == "Israel") {
        return "+972"
    } else if (country == "Italy") {
        return "+39"
    } else if (country == "Jamaica") {
        return "+1 (876)"
    } else if (country == "Japan") {
        return "+81"
    } else if (country == "Jordan") {
        return "+962"
    } else if (country == "Kazakhstan") {
        return "+7"
    } else if (country == "Kenya") {
        return "+254"
    } else if (country == "Kiribati") {
        return "+686"
    } else if (country == "Korea, Democratic People's Republic of") {
        return "+850"
    } else if (country == "Korea, Republic of") {
        return "+82"
    } else if (country == "Kuwait") {
        return "+965"
    } else if (country == "Kyrgyzstan") {
        return "+996"
    } else if (country == "Lao, People's Democratic Republic") {
        return "+856"
    } else if (country == "Latvia") {
        return "+371"
    } else if (country == "Lebanon") {
        return "+961"
    } else if (country == "Lesotho") {
        return "+266"
    } else if (country == "Liberia") {
        return "+231"
    } else if (country == "Libyan Arab Jamahiriya") {
        return "+218"
    } else if (country == "Liechtenstein") {
        return "+423"
    } else if (country == "Lithuania") {
        return "+370"
    } else if (country == "Luxembourg") {
        return "+352"
    } else if (country == "Macau") {
        return "+853"
    } else if (country == "Macedonia, The Former Yugoslav Republic of") {
        return "+389"
    } else if (country == "Madagascar") {
        return "+261"
    } else if (country == "Malawi") {
        return "+265"
    } else if (country == "Malaysia") {
        return "+60"
    } else if (country == "Maldives") {
        return "+960"
    } else if (country == "Mali") {
        return "+223"
    } else if (country == "Malta") {
        return "+356"
    } else if (country == "Marshall Islands") {
        return "+692"
    } else if (country == "Martinique") {
        return "+596"
    } else if (country == "Mauritania") {
        return "+222"
    } else if (country == "Mauritius") {
        return "+230"
    } else if (country == "Mayotte") {
        return "+269"
    } else if (country == "Mexico") {
        return "+52"
    } else if (country == "Micronesia, Federated States of") {
        return "+691"
    } else if (country == "Moldova, Republic of") {
        return "+373"
    } else if (country == "Monaco") {
        return "+377"
    } else if (country == "Mongolia") {
        return "+976"
    } else if (country == "Montserrat") {
        return "+1 (664)"
    } else if (country == "Morocco") {
        return "+212"
    } else if (country == "Mozambique") {
        return "+258"
    } else if (country == "Myanmar") {
        return "+95"
    } else if (country == "Namibia") {
        return "+264"
    } else if (country == "Nauru") {
        return "+674"
    } else if (country == "Nepal") {
        return "+977"
    } else if (country == "Netherlands") {
        return "+31"
    } else if (country == "Netherlands Antilles") {
        return "+599"
    } else if (country == "New Caledonia") {
        return "+687"
    } else if (country == "New Zealand") {
        return "+64"
    } else if (country == "Nicaragua") {
        return "+505"
    } else if (country == "Niger") {
        return "+227"
    } else if (country == "Nigeria") {
        return "+234"
    } else if (country == "Niue") {
        return "+683"
    } else if (country == "Norfolk Island") {
        return "+672"
    } else if (country == "Northern Mariana Islands") {
        return "+1"
    } else if (country == "Norway") {
        return "+47"
    } else if (country == "Oman") {
        return "+968"
    } else if (country == "Pakistan") {
        return "+92"
    } else if (country == "Palau") {
        return "+680"
    } else if (country == "Panama") {
        return "+507"
    } else if (country == "Papua New Guinea") {
        return "+675"
    } else if (country == "Paraguay") {
        return "+595"
    } else if (country == "Peru") {
        return "+51"
    } else if (country == "Philippines") {
        return "+63"
    } else if (country == "Pitcairn") {
        return "+64"
    } else if (country == "Poland") {
        return "+48"
    } else if (country == "Portugal") {
        return "+351"
    } else if (country == "Puerto Rico") {
        return "+1 (787/939)"
    } else if (country == "Qatar") {
        return "+974"
    } else if (country == "Reunion") {
        return "+262"
    } else if (country == "Romania") {
        return "+40"
    } else if (country == "Russian Federation") {
        return "+7"
    } else if (country == "Rwanda") {
        return "+250"
    } else if (country == "Saint Kitts and Nevis") {
        return "+1 (869)"
    } else if (country == "Saint Lucia") {
        return "+1 (758)"
    } else if (country == "Saint Vincent and the Grenadines") {
        return "+1 (784)"
    } else if (country == "Samoa") {
        return "+685"
    } else if (country == "San Marino") {
        return "+378"
    } else if (country == "Sao Tome and Principe") {
        return "+239"
    } else if (country == "Saudi Arabia") {
        return "+966"
    } else if (country == "Senegal") {
        return "+221"
    } else if (country == "Seychelles") {
        return "+248"
    } else if (country == "Sierra Leone") {
        return "+232"
    } else if (country == "Singapore") {
        return "+65"
    } else if (country == "Slovakia (Slovak Republic)") {
        return "+421"
    } else if (country == "Slovenia") {
        return "+386"
    } else if (country == "Solomon Islands") {
        return "+677"
    } else if (country == "Somalia") {
        return "+252"
    } else if (country == "South Africa") {
        return "+27"
    } else if (country == "South Georgia and the South Sandwich Islands") {
        return "+500"
    } else if (country == "Spain") {
        return "+34"
    } else if (country == "Sri Lanka") {
        return "+94"
    } else if (country == "St. Helena") {
        return "+290"
    } else if (country == "St. Pierre and Miquelon") {
        return "+508"
    } else if (country == "Sudan") {
        return "+249"
    } else if (country == "Suriname") {
        return "+597"
    } else if (country == "Svalbard and Jan Mayen Islands") {
        return "+47"
    } else if (country == "Swaziland") {
        return "+268"
    } else if (country == "Sweden") {
        return "+46"
    } else if (country == "Switzerland") {
        return "+41"
} else if (country == "Syrian Arab Republic") {
        return "+963"
} else if (country == "Taiwan, Province of China") {
        return "+886"
} else if (country == "Tajikistan") {
        return "+992"
} else if (country == "Tanzania, United Republic of") {
        return "+255"
} else if (country == "Thailand") {
        return "+66"
} else if (country == "Togo") {
        return "+228"
} else if (country == "Tokelau") {
        return "+690"
} else if (country == "Tonga") {
        return "+676"
} else if (country == "Trinidad and Tobago") {
    return "+1 (868)"
} else if (country == "Tunisia") {
        return "+216"
} else if (country == "Turkey") {
        return "+90"
} else if (country == "Turkmenistan") {
        return "+993"
} else if (country == "Turks and Caicos Islands") {
    return "+1 (649)"
} else if (country == "Tuvalu") {
    return "+688"
    } else if (country == "Uganda") {
        return "+256"
    } else if (country == "Ukraine") {
        return "+380"
    } else if (country == "United Arab Emirates") {
        return "+971"
    } else if (country == "United Kingdom") {
        return "+44"
    } else if (country == "United States") {
        return "+1"
    } else if (country == "United States Minor Outlying Islands") {
        return "+1"
    } else if (country == "Uruguay") {
        return "+598"
    } else if (country == "Uzbekistan") {
        return "+998"
    } else if (country == "Vanuatu") {
        return "+678"
    } else if (country == "Venezuela") {
        return "+58"
    } else if (country == "Vietnam") {
        return "+84"
    } else if (country == "Virgin Islands (British)") {
        return "+1"
    } else if (country == "Virgin Islands (U.S.)") {
        return "+1"
    } else if (country == "Wallis and Futuna Islands") {
        return "+681"
    } else if (country == "Western Sahara") {
        return "+212"
    } else if (country == "Yemen") {
        return "+967"
    } else if (country == "Yugoslavia") {
        return "+38"
    } else if (country == "Zambia") {
        return "+260"
    } else if (country == "Zimbabwe") {
        return "+263"
    } 
    //console.log(country);
}