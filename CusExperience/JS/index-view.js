﻿/***************************** Module Addition Bindings **************************************************/

ko.bindingHandlers.countExpertComments = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var found = false;
        for (var i in bindingContext.$root.ExpertCommentCounts()) {
            if (bindingContext.$root.ExpertCommentCounts()[i].key == bindingContext.$data.ModuleID()) {
                bindingContext.$root.ExpertCommentCounts()[i].value = bindingContext.$root.ExpertCommentCounts()[i].value + 1;
            found = true;
        }
      }
        if (!found) bindingContext.$root.ExpertCommentCounts.push({ key: bindingContext.$data.ModuleID(), value: 1 });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        
    }
}


ko.bindingHandlers.addExpertComment = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).keydown(function (e) {
            if (e.keyCode == 13) {
                var comment = $(element).val();
                if (comment !== "")
                {
                    var moduleID = 0;
                    var moduleInfo = null;
                    var surveyID = bindingContext.$root.Survey().SurveyID();
                    if (typeof (bindingContext.$data.ModuleID()) !== undefined) {
                        moduleID = bindingContext.$data.ModuleID();
                        moduleInfo = "Question";
                    }
                    var ecData = {
                        "Comment": comment,
                        "SurveyID": surveyID,
                        "ModuleID": moduleID,
                        "ModuleInfo": moduleInfo
                    };
                    var data = ko.toJSON(ecData);
                    $.ajax({
                        type: "POST",
                        data: data,
                        url: "/api/AddExpertComment",
                        contentType: "application/json",
                        success: function (data) {
                            bindingContext.$root.Survey().ExpertComments($.map(data, function (item) { return new ExpertCommentModel(item) }));
                        },
                        error: function (data) {
                        }
                    });
                    $(element).val("");
                }
                return false;
            }
        });
    }
}

ko.bindingHandlers.toggleExpertComments = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function (e) {
            $(element).closest("div.comment-header-zone").next("div.comment-togglezone").toggle();
        });
    }
}

ko.bindingHandlers.removeExpertComment = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function (e) {
            var comment = $(element).val();
            var moduleID = 0;
            var moduleInfo = null;
            var expertCommentID = bindingContext.$data.ExpertCommentID();
            $.ajax({
                type: "GET",
                url: "/api/DeleteExpertComment/" + expertCommentID,
                contentType: "application/json",
                success: function (data) {
                    bindingContext.$root.Survey().ExpertComments($.map(data, function (item) { return new ExpertCommentModel(item) }));
                },
                error: function (data) {
                }
            });

            $(element).val("");
            return false;
        });
    }
}

ko.bindingHandlers.plotCoordinates = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var mapCanvas = $(element).parent().find('.map-canvas')[0];
        var bounds = new google.maps.LatLngBounds();

        var mapOptions = {

            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions);

        for (var index in bindingContext.$data.AnalyticsData().LocationInfo()) {
            var eachLocation = bindingContext.$data.AnalyticsData().LocationInfo()[index];

            var image = "/Images/Location-Icon-Blue copy.png";

            var position = new google.maps.LatLng(eachLocation.Latitude(), eachLocation.Longitude());
            console.log("lat: " + eachLocation.Latitude());
            console.log("long: " + eachLocation.Longitude());
            bounds.extend(position);
            var marker = new google.maps.Marker({
                position: position,
                map: map,
                icon: image
            });
            map.fitBounds(bounds);
        }

        // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function (event) {
            this.setZoom(15);
            google.maps.event.removeListener(boundsListener);
        });
    }
}

ko.bindingHandlers.toggleSidePanel = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var minusIcon = "/Images/Minus-Icon.png";
        var plusIcon = "/Images/Plus-Icon.png";
        var currIcon = $('#toggle-icon-btn').attr("src");
        $(element).click(function () {
            $('.sidepanel').toggle();
            currIcon = $('#toggle-icon-btn').attr("src");

            if (currIcon == plusIcon) {
                $('#toggle-icon-btn').attr("src", minusIcon);
                
            } else if (currIcon == minusIcon) {
                $('#toggle-icon-btn').attr("src", plusIcon);
            }
        });
    }
}

ko.bindingHandlers.checkBox = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).removeClass("icon-check-empty");
        $(element).addClass("icon-check-sign");
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var observable = valueAccessor();
            if ($(element).hasClass("icon-check-empty")) {
                $(element).removeClass("icon-check-empty");
                $(element).addClass("icon-check-sign");
                observable(true);
            } else if ($(element).hasClass("icon-check-sign")) {
                $(element).removeClass("icon-check-sign");
                $(element).addClass("icon-check-empty");
                observable(false);
            }
        });
    }
}

ko.bindingHandlers.selectTag = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        bindingContext.$parent.SearchData().ToggleSelectedTag(bindingContext.$index());
        $(element).toggleClass('tag-selected');
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$parent.SearchData().ToggleSelectedTag(bindingContext.$index());
            $(element).toggleClass('tag-selected');
        });
    }
}


ko.bindingHandlers.chooseRatingRange = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var ModuleData = bindingContext.$parent;
        $(element).removeClass(ModuleData.QuestionData().RateType());
        $(element).addClass(ModuleData.QuestionData().RateType() + '-Selected');
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var ModuleData = bindingContext.$parent;
            if ((bindingContext.$parent.SearchData().FromRating() == null && bindingContext.$parent.SearchData().ToRating() == null) ||
                (bindingContext.$parent.SearchData().FromRating() != null && bindingContext.$parent.SearchData().ToRating() != null)) {
                bindingContext.$parent.SearchData().FromRating(bindingContext.$data);
                bindingContext.$parent.SearchData().ToRating(null);
                $(element).parent().parent().find('span').removeClass(ModuleData.QuestionData().RateType() + '-Selected');
                $(element).parent().parent().find('span').addClass(ModuleData.QuestionData().RateType());
                $(element).removeClass(ModuleData.QuestionData().RateType());
                $(element).addClass(ModuleData.QuestionData().RateType() + '-Selected');
            }
            else if (bindingContext.$parent.SearchData().FromRating() == bindingContext.$data) {
                bindingContext.$parent.SearchData().FromRating(null);
                bindingContext.$parent.SearchData().ToRating(null);
                $(element).parent().parent().find('span').removeClass(ModuleData.QuestionData().RateType() + '-Selected');
                $(element).parent().parent().find('span').addClass(ModuleData.QuestionData().RateType());
            } else if (bindingContext.$parent.SearchData().FromRating() > bindingContext.$data) {
                bindingContext.$parent.SearchData().FromRating(bindingContext.$data);
                $(element).parent().parent().find('span').removeClass(ModuleData.QuestionData().RateType() + '-Selected');
                $(element).parent().parent().find('span').addClass(ModuleData.QuestionData().RateType());
                $(element).removeClass(ModuleData.QuestionData().RateType());
                $(element).addClass(ModuleData.QuestionData().RateType() + '-Selected');
            } else {
                bindingContext.$parent.SearchData().ToRating(bindingContext.$data);
                $(element).parent().parent().find('span').removeClass(ModuleData.QuestionData().RateType() + '-Selected');
                $(element).parent().parent().find('span').addClass(ModuleData.QuestionData().RateType());
                for (var index = bindingContext.$parent.SearchData().FromRating() ; index < bindingContext.$parent.SearchData().ToRating() + 1 ; index++) {
                    $(element).parent().parent().find("li").eq(index).find('span').removeClass(ModuleData.QuestionData().RateType());
                    $(element).parent().parent().find("li").eq(index).find('span').addClass(ModuleData.QuestionData().RateType() + '-Selected');
                }
            }
        });
    }
}

ko.bindingHandlers.resetSTAFFNames = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        bindingContext.$data.STAFFEntryText(null);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}

ko.bindingHandlers.reset = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {        
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.reset();
        });
    }
}


ko.bindingHandlers.resetRating = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        bindingContext.$data.SearchData().FromRating(0);
        bindingContext.$data.SearchData().ToRating(bindingContext.$data.QuestionData().NumLimit());
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}

ko.bindingHandlers.resetChoice = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        bindingContext.$data.SearchData().ChoosenOptions([]);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}

ko.bindingHandlers.resetTags = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        bindingContext.$data.SearchData().SelectedTags([]);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}


function toggleSearch() {
    $('#search-section').toggle();
    if ($('#mine-expand').hasClass("icon-plus")) {
        $('#mine-analytics').addClass("analytics-border");
        $('#mine-expand').removeClass("icon-plus");
        $('#mine-expand').addClass("icon-minus");
        $('#sidepanel').toggle();
        $('.showhidebutton').toggle();
        $("html, body").animate({ scrollTop: $('#mine-analytics').offset().top - 35 }, 700);
        $('#toggle-icon-btn').attr("src", "/Images/Minus-Icon.png");
    } else if ($('#mine-expand').hasClass("icon-minus")) {
        $('#mine-analytics').removeClass("analytics-border");
        $('#mine-expand').addClass("icon-plus");
        $('#mine-expand').removeClass("icon-minus")
        $('#sidepanel').hide();
        $('.showhidebutton').hide();
    }
}

function IndexViewModel() {
    var self = this;
    self.Comments = ko.observableArray([]);
    self.CommentModalTitle = ko.observable(null);
    self.NoofVisible = ko.observable(10);
    self.AttachmentDetail = new AttachmentDetailModel();
    self.AnalyticsTitle = ko.observable(null);
    self.Questions = ko.observableArray([{ value: "All", key: "All" }]);
    self.SelectedQuestions = ko.observableArray([]);
    self.DGQuestionFields = ko.observableArray();
    self.SelectedDGQuestionFields = ko.observableArray([]);
    self.Touchpoints = ko.observableArray([]);
    self.Subsidiaries = ko.observableArray([]);
    self.PublishedSurvey = ko.observable(new SurveyModel(initPSJSONData, false, false, true));
    self.AnalyticsTitle(self.PublishedSurvey().SurveyTitle() + " Analytics");
    self.Survey = ko.observable(new SurveyModel(initASJSONData, false, true, false));
    self.OrderByOptions = ko.observableArray(['Highest to Lowest', 'Lowest to Highest']);
    self.IsTouchpointViewOpen = ko.observable(false);
    self.IsQuestionViewOpen = ko.observable(false);
    self.IsDemographicViewOpen = ko.observable(false);
    self.CurrentUser = ko.observable(new UserModel(currentUserData));
    self.ExpertCommentCounts = ko.observableArray([]);


    var url = document.URL;
    var params = url.split("Detail/");
    var id = params[1];

    self.Subsidiaries.push({ key: self.PublishedSurvey().OrgSub().RegisteredName(), value: self.PublishedSurvey().OrgSub().OrgSubID });

    for (var index = 0 ; index < self.PublishedSurvey().OrgSub().Touchpoints.length ; index++) {
        self.Touchpoints.push({ key: self.PublishedSurvey().OrgSub().Touchpoints[index].TouchpointName() + " (" + self.PublishedSurvey().OrgSub().RegisteredName() + ")", value: self.PublishedSurvey().OrgSub().Touchpoints[index].TouchpointID, OrgSubID: self.PublishedSurvey().OrgSub().OrgSubID });
    }

    function addSubsidiaries(subsidiaries) {
        if (subsidiaries.length >= 1) {
            for (var index = 0 ; index < subsidiaries.length ; index++) {
                self.Subsidiaries.push({ key: subsidiaries[index].RegisteredName(), value: subsidiaries[index].OrgSubID });

                for (var index3 = 0 ; index3 < subsidiaries[index].Touchpoints.length ; index3++) {
                    self.Touchpoints.push({ key: subsidiaries[index].Touchpoints[index3].TouchpointName() + " [" + subsidiaries[index].RegisteredName() + "]", value: subsidiaries[index].Touchpoints[index3].TouchpointID, OrgSubID: subsidiaries[index].OrgSubID });
                }
                addSubsidiaries(subsidiaries[index].Subsidiaries());
            }
        }
    }
    subsidiaries = self.PublishedSurvey().OrgSub().Subsidiaries();

    for (var index1 = 0 ; index1 < subsidiaries.length ; index1++) {
        self.Subsidiaries.push({ key:  subsidiaries[index1].RegisteredName(), value: subsidiaries[index1].OrgSubID });
        for (var index2 = 0 ; index < subsidiaries[index1].Touchpoints.length ; index2++)
             self.Touchpoints.push({ key: subsidiaries[index1].Touchpoints[index2].TouchpointName() + " [ " + subsidiaries[index1].RegisteredName() + " ] ", value: subsidiaries[index1].Touchpoints[index2].TouchpointID, OrgSubID: subsidiaries[index1].OrgSubID });
        addSubsidiaries(subsidiaries[index1].Subsidiaries());
    }

    for (index = 0 ; index < self.Subsidiaries().length ; index++)
        self.PublishedSurvey().SelectedSubsidiaries.push(self.Subsidiaries()[index].value());

    self.PublishedSurvey().SelectedSubsidiaries().push('All');

    for (index = 0 ; index < self.Touchpoints().length ; index++)
        self.PublishedSurvey().SelectedTouchpoints.push(self.Touchpoints()[index].value());

    self.PublishedSurvey().SelectedTouchpoints.push('All');

    for (var i in self.PublishedSurvey().Modules()) {
        if (self.PublishedSurvey().Modules()[i].ModuleType() === MODULETYPES.QUESTION) {
            if (self.PublishedSurvey().Modules()[i].ModuleData().QuestionType() != QUESTIONTYPES.DEMOGRAPHIC) {
                if (!$.isEmptyObject(self.PublishedSurvey().Modules()[i].ModuleData().QuestionData())) {
                    var div = document.createElement('div');
                    var questionText = $(div).append(self.PublishedSurvey().Modules()[i].ModuleData().QuestionData().QuestionText()).text();
                    self.Questions.push({ value: self.PublishedSurvey().Modules()[i].ModuleID(), key: questionText });
                }
            } else {
                self.DGQuestionFields.push("All");
                var DGQuestionFields = self.PublishedSurvey().Modules()[i].ModuleData().QuestionData().DGQuestionFields();
                for (var i in DGQuestionFields) {
                    if(DGQuestionFields[i].IsAnonymized() == false){
                        var div = document.createElement('div');
                        var questionFieldText = $(div).append(DGQuestionFields[i].FieldName()).text();
                        self.DGQuestionFields.push(questionFieldText);
                    }
                }
            }
        }
    }

    for (var index in self.Questions()) {
        self.SelectedQuestions.push(self.Questions()[index].value);
    }

    for (var index in self.DGQuestionFields()) {
        self.SelectedDGQuestionFields.push(self.DGQuestionFields()[index]);
    }

    var currentdate = new Date();
    self.PublishedSurvey().ToDateTime(pad(currentdate.getDate(), 2) + "/" + pad((currentdate.getMonth() + 1), 2)
    + "/" + currentdate.getFullYear() + " @ "
    + pad(currentdate.getHours(), 2) + ":"
    + pad(currentdate.getMinutes(), 2));

    self.InitFromDate = self.PublishedSurvey().FromDateTime();
    self.InitToDate = self.PublishedSurvey().ToDateTime();


    self.loadIndexView = function () {
        return "/Analytics/Detail/" + id;
    }

    self.loadCustomerView = function () {
        return "/Analytics/CustomerView/" + id;
    }

    self.loadSocialView = function () {
        return "/Analytics/SocialView/" + id;
    }

    self.loadTrackingView = function () {
       return  "/Analytics/TrackingView/" + id;
    }

    self.reset = function () {
        var tempQuestions = self.Questions();
        self.Questions([]);
        self.Questions(tempQuestions);

        var tempQuestionFields = self.DGQuestionFields();
        self.DGQuestionFields([]);
        self.DGQuestionFields(tempQuestionFields);

        for (var index in self.Questions()) {
            self.SelectedQuestions.push(self.Questions()[index].value);
        }

        for (var index in self.DGQuestionFields()) {
            self.SelectedDGQuestionFields.push(self.DGQuestionFields()[index]);
        }

        var tempModules = self.PublishedSurvey().Modules();
        self.PublishedSurvey().Modules([]);
        self.PublishedSurvey().Modules(tempModules);
        self.PublishedSurvey().SelectedTouchpoints([]);
        self.PublishedSurvey().FromDateTime(self.InitFromDate);
        var currentdate = new Date();
        self.PublishedSurvey().ToDateTime(pad(currentdate.getDate(), 2) + "/" + pad((currentdate.getMonth() + 1), 2)
        + "/" + currentdate.getFullYear() + " @ "
        + pad(currentdate.getHours(), 2) + ":"
        + pad(currentdate.getMinutes(), 2));
        self.PublishedSurvey().Keywords(null);
        self.PublishedSurvey().WOKeywords(null);
        for (index = 0 ; index < self.PublishedSurvey().OrgSub().Touchpoints.length ; index++)
            self.PublishedSurvey().SelectedTouchpoints.push(self.PublishedSurvey().OrgSub().Touchpoints[index].TouchpointName());
        self.PublishedSurvey().SelectedTouchpoints.push("All");
        self.Survey(new SurveyModel(initASJSONData, false, true, false));
        $('#reset-analytics-btn').text("Resetted");
        $("html, body").animate({ scrollTop: $('#mine-analytics').offset().top - 35 }, 1000);
        setTimeout(function () {
            $('#reset-analytics-btn').text("Reset");
        }, 5000);
    }

    self.getSearchSurvey = function () {
        var searchSurvey = new SurveyModel(null, false, false, true);
        searchSurvey.SurveyID = self.PublishedSurvey().SurveyID;
        searchSurvey.OrgSub = self.PublishedSurvey().OrgSub();
        searchSurvey.SurveyConfigs = self.PublishedSurvey().SurveyConfigs;
        searchSurvey.IsAllowViewResults = self.PublishedSurvey().IsAllowViewResults;
        searchSurvey.IsAllowViewSocial = self.PublishedSurvey().IsAllowViewSocial;
        searchSurvey.FromDateTime = self.PublishedSurvey().FromDateTime;
        searchSurvey.ToDateTime = self.PublishedSurvey().ToDateTime;
        searchSurvey.Keywords = self.PublishedSurvey().Keywords;
        searchSurvey.WOKeywords = self.PublishedSurvey().WOKeywords;
        searchSurvey.SelectedTouchpoints = self.PublishedSurvey().SelectedTouchpoints;
        searchSurvey.SelectedSubsidiaries = self.PublishedSurvey().SelectedSubsidiaries;

        for (var index in self.PublishedSurvey().Modules()) {
            if (self.PublishedSurvey().Modules()[index].ModuleType() == MODULETYPES.QUESTION) {
                if (self.PublishedSurvey().Modules()[index].ModuleData().QuestionType() !=QUESTIONTYPES.DEMOGRAPHIC) {
                    if ($.inArray(self.PublishedSurvey().Modules()[index].ModuleID(),self.SelectedQuestions()) > -1) {
                        searchSurvey.Modules.push(self.PublishedSurvey().Modules()[index]);
                    }
                }
                else {
                    searchSurvey.Modules.push(self.PublishedSurvey().Modules()[index]);
                    var DGQuestionFields = searchSurvey.Modules()[searchSurvey.Modules().length -1].ModuleData().QuestionData().DGQuestionFields();
                    for (var i in DGQuestionFields) {
                        var div = document.createElement('div');
                        var questionFieldText = $(div).append(DGQuestionFields[i].FieldName()).text();
                        if ($.inArray(questionFieldText, self.SelectedDGQuestionFields()) == -1)
                            DGQuestionFields[i].HideInSearch(true);
                        else DGQuestionFields[i].HideInSearch(false);
                    }
                }
            }
        }
        return searchSurvey;
        
    }

    self.showSearchResults = function (element) {
        $(element).blur();
        $(element).attr("disabled", false);
        $(element).removeClass("search-button-fail");
        $(element).removeClass("search-button-success");
        $(element).removeClass('hover');
        $(element).trigger('mouseup');
        $(element).unbind("mouseenter mouseleave");
        $(element).text("Mining...");
        $(element).trigger('mouseup');

        var searchSurvey = self.getSearchSurvey();
        var data = ko.toJSON(searchSurvey);

        $.ajax({
            type: "POST",
            data: data,
            dataType: 'json',
            url: "/api/GetSearchAnalytics",
            contentType: "application/json"
        }).success(function (allData) {
            self.Survey(new SurveyModel(allData, false, true, false));
            if (self.Survey().NoResultsFound() == false) {
                $(element).addClass("search-button-success");
                $(element).text("Results Found");
                $(element).attr("disabled", true);
                
                $("html, body").animate({ scrollTop: $('#index-view-section').offset().top - 35 }, 1000);
                $(element).delay(7000).queue(function (n) {
                    $(element).text("Mine");
                    $(element).removeClass("search-button-success");
                    $(element).attr("disabled", false);
                    n();
                });
            } else {
                $(element).addClass("search-button-fail");
                $(element).text("No Results Found");
                $(element).attr("disabled", true);
                $(element).delay(7000).queue(function (n) {
                    $(element).text("Search");
                    $(element).removeClass("search-button-fail");
                    $(element).attr("disabled", false);
                    n();
                });
            }
        }).fail(function (data) {
            // alert(ko.toJSON(data));
            $(element).addClass("search-button-fail");
            $(element).text("Search Failed");
            $(element).delay(7000).queue(function (n) {
                $(element).text("Search");
                $(element).removeClass("search-button-fail");
                n();
            });
        });
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    self.showResponseView = function (data) {
        var responseURL = "/Analytics/SurveyResponse/" + data.SurveyResponseID;
        var x = window.open(responseURL, responseURL);
        x.focus();
    }

    self.downloadReport = function (data) {
        // alert(ko.toJSON(self));
        var d = new Date();
        $("#downloadReport").get(0).setAttribute('action', '/Analytics/AnalyticsReport/' + id);
        $("#analyticsData").val(ko.toJSON(self));
        console.log($("#analyticsData").val());
        $("#downloadReport").submit();
    }

    self.showComments = function (moduleID, keyWord) {
        self.Comments([]);
        self.CommentModalTitle(null);
        $("#commentModal").modal('show');
        $("#comments-loading-bar").show();

        var searchSurvey = self.getSearchSurvey();
        var jsonData = { "searchSurvey": ko.toJSON(searchSurvey), "moduleID": moduleID, "keyWord": keyWord };
        var data = ko.toJSON(jsonData);
        $.ajax({
            type: "POST",
            data: data,
            dataType: 'json',
            url: "/api/GetComments",
            contentType: "application/json"
        }).success(function (allData) {
            self.Comments($.map(allData, function (key, value) { return new CommentModel(key, value) }));
            $("#comments-loading-bar").hide();
        }).fail(function (httpObj, data) {
        }); 
    }
    
    self.ExpertCount = function (data) {
        for (var i in self.ExpertCommentCounts()) {
            if (self.ExpertCommentCounts()[i].key == data.ModuleID()) {
                return self.ExpertCommentCounts()[i].value;
            }
        }
    }


    self.showCommentsByTag = function (moduleID, tagName, count) {
        self.Comments([]);
        self.CommentModalTitle(tagName + '(' + count + ')');
        $("#commentModal").modal('show');
        $("#comments-loading-bar").show();

        var searchSurvey = self.getSearchSurvey();
        var jsonData = { "searchSurvey": ko.toJSON(searchSurvey), "moduleID": moduleID, "tagName": tagName };
        var data = ko.toJSON(jsonData);

        $.ajax({
            type: "POST",
            data: data,
            dataType: 'json',
            url: "/api/GetCommentsByTag",
            contentType: "application/json"
        }).success(function (allData) {
            self.Comments($.map(allData, function (key, value) { return new CommentModel(key, value) }));
            $("#comments-loading-bar").hide();
        }).fail(function (httpObj, data) {
        });
    }
}

$(document).ready(function () {
    var indexViewModel = new IndexViewModel();
    ko.applyBindings(indexViewModel, document.getElementById('body-content'));
    $('body').show();
});