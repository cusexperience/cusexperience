﻿
/***************************** Knockout Bindings **************************************************/

ko.bindingHandlers.movePreviousSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index >= 0) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('prev');
            }
        });
        $(element).parent().swiperight(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index >= 0) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('prev');
            }
        });
    }
}

ko.bindingHandlers.moveNextSlide = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index <= bindingContext.$data.QuestionData().QuestionItems().length - 1) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('next');
            }
        });
        $(element).parent().swipeleft(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index <= bindingContext.$data.QuestionData().QuestionItems().length - 1) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('next');
            }
        });
    }
}

ko.bindingHandlers.changeCurrentSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var csIndex = bindingContext.$index();
            $(element).parent().parent().carousel(csIndex);
            bindingContext.$parent.QuestionData().CurrentIndex(csIndex);
        });
    }
}

ko.bindingHandlers.hideSurveyResponse = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.SurveyResponseDiv().hide();
            bindingContext.$root.SurveyResponseDiv().parent().find('span').removeClass("icon-minus");
            bindingContext.$root.SurveyResponseDiv().parent().find('span').addClass("icon-plus");
        });
    }
}

ko.bindingHandlers.autosize = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).autosize();
    }
}

/* Binding Handler to flip an Image */
ko.bindingHandlers.flipImage = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var toggleDiv = $('.file-input-wrapper').find('#flip-toggle');
            toggleDiv.toggleClass('flip');
        });
    }
}

ko.bindingHandlers.flipAppreciation = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var toggleDiv = $(element).closest('.file-input-wrapper').find('#flip-toggle-app');
            toggleDiv.toggleClass('flip');
        });
    }
}

ko.bindingHandlers.choosenRating = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var ModuleData = bindingContext.$parent;
        if (ModuleData.AnswerData().Rating() == bindingContext.$data) {
            if (ModuleData.QuestionData().RateType() == RATETYPES.NUMBER) {
                $(element).removeClass(ModuleData.QuestionData().RateType());
                $(element).addClass(ModuleData.QuestionData().RateType() + '-Selected');
            }
            else if (ModuleData.QuestionData().RateType() == RATETYPES.COMMENT) {
                $(element).addClass('tag-selected');
            }
            else if (ModuleData.QuestionData().RateType() == RATETYPES.STAR || ModuleData.QuestionData().RateType() == RATETYPES.HEART) {
                $(element).parent().prevAll().find('span').removeClass('Star');
                $(element).parent().prevAll().find('span').addClass('Star-Selected');
                $(element).removeClass(ModuleData.QuestionData().RateType());
                $(element).addClass(ModuleData.QuestionData().RateType() + '-Selected');
            }
        }
    }
}

ko.bindingHandlers.choosenMultipleRating = {
    init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var carouselElement = $(element).parent().parent().parent().parent().parent().find('.carousel');
        carouselElement.on('slid.bs.carousel', function () {
            var ModuleData = bindingContext.$parents[1];
            var index = carouselElement.find('div.active').index();
            ModuleData.QuestionData().CurrentIndex(index);
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var ModuleData = bindingContext.$parents[1];
            var cindex = bindingContext.$parentContext.$index();
            var carouselElement = $(element).parent().parent().parent().parent().parent().find('.carousel');
            if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.NUMBER ||
                ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.COMMENT ||
                ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.STAR ||
                ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.HEART) {
                if (ModuleData.AnswerData().AnswerItems()[cindex].Rating == bindingContext.$data) {

                    if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.NUMBER) {
                        $(element).addClass('rate-selected');
                    }
                    else if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.COMMENT) {
                        $(element).addClass('tag-selected');
                    }
                    else if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.STAR) {
                        $(element).parent().prevAll().find('span').removeClass('Star');
                        $(element).parent().prevAll().find('span').addClass('Star-Selected');
                        $(element).removeClass('Star');
                        $(element).addClass('Star-Selected');
                    }
                    else if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.HEART) {
                        $(element).parent().prevAll().find('span').removeClass('Heart');
                        $(element).parent().prevAll().find('span').addClass('Heart-Selected');
                        $(element).removeClass('Heart');
                        $(element).addClass('Heart-Selected');
                    }
                }
            }
      }
}

ko.bindingHandlers.autosize = {
    init: function (element, valueAccessor) {
        $(element).autosize();
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.selectedChoice = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if ($.inArray(bindingContext.$index(), bindingContext.$parent.AnswerData().ChoosenOptions()) > -1)
            $(element).addClass('choice-selected');
    }
}

ko.bindingHandlers.selectedMultipleChoice = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var ModuleData = bindingContext.$parents[1];
        var cindex = bindingContext.$parentContext.$index();
        var carouselElement = $(element).parent().parent().parent().parent().parent().find('.carousel');
        if (ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.SINGLECHOICE||
            ModuleData.QuestionData().MultipleType() == MULTIPLETYPES.MULTIPLECHOICE) {
            if ($.inArray(bindingContext.$index(), ModuleData.AnswerData().AnswerItems()[cindex].ChoosenOptions) > -1)
                $(element).addClass('choice-selected');
        }
    }
}

ko.bindingHandlers.selectedTag = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
       if( $.inArray(bindingContext.$index(), bindingContext.$parent.AnswerData().SelectedTags()) > -1)
            $(element).addClass('tag-selected');
    }
}

ko.bindingHandlers.showToolTip = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = bindingContext.$index();
        var direction = value % 3 == 0 ? 'left' : (value % 3 == 2) ? 'right' : 'top';
        $(element).tooltip({ placement : direction });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}

ko.bindingHandlers.placePosition = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if ($(document).width() <= 639) {
            $(element).css('top', bindingContext.$data.TopPos().replace('px', '') / 2 + 'px');
            $(element).css('left', bindingContext.$data.LeftPos().replace('px', '') / 2 + 'px');
        } else {
            $(element).css('top', bindingContext.$data.TopPos());
            $(element).css('left', bindingContext.$data.LeftPos());
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if ($(document).width() <= 639) {
            $(element).css('top', bindingContext.$data.TopPos().replace('px', '') / 2 + 'px');
            $(element).css('left', bindingContext.$data.LeftPos().replace('px', '') / 2 + 'px');
        } else {
            $(element).css('top', bindingContext.$data.TopPos());
            $(element).css('left', bindingContext.$data.LeftPos());
        }
    }
}



ko.bindingHandlers.select = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

ko.bindingHandlers.selectSingle = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        $(element).bind("multiselectclick", function(event, ui){
            event.preventDefault();
        });
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
        $(element).bind("multiselectclick", function (event, ui) {
            event.preventDefault();
        });
    }
};

ko.bindingHandlers.selectMultiple = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        $(element).bind("multiselectclick", function (event, ui) {
            event.preventDefault();
        });
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
        $(element).bind("multiselectclick", function (event, ui) {
            event.preventDefault();
        });
    }
};

ko.bindingHandlers.addTouchpoints = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        bindingContext.$data.Touchpoints.removeAll();
        var Touchpoints = bindingContext.$root.Survey().OrgSub().Touchpoints;
        for (var index in Touchpoints) {
            bindingContext.$data.Touchpoints().push(new TouchpointModel(null));
            bindingContext.$data.Touchpoints()[index].TouchpointName(bindingContext.$root.Survey().OrgSub().Touchpoints[index].TouchpointName());
            bindingContext.$data.Touchpoints()[index].RegisteredName(bindingContext.$root.Survey().OrgSub().Touchpoints[index].RegisteredName());
            bindingContext.$data.Touchpoints()[index].BrandWebsite(bindingContext.$root.Survey().OrgSub().Touchpoints[index].BrandWebsite());
            bindingContext.$data.Touchpoints()[index].PhoneNumber(bindingContext.$root.Survey().OrgSub().Touchpoints[index].PhoneNumber());
            bindingContext.$data.Touchpoints()[index].EmailAddress(bindingContext.$root.Survey().OrgSub().Touchpoints[index].EmailAddress());
            bindingContext.$data.Touchpoints()[index].Address(bindingContext.$root.Survey().OrgSub().Touchpoints[index].Address());
            bindingContext.$data.Touchpoints()[index].FrontImage(bindingContext.$root.Survey().OrgSub().Touchpoints[index].FrontImage());
            bindingContext.$data.Touchpoints()[index].LogoImage(bindingContext.$root.Survey().OrgSub().Touchpoints[index].LogoImage());
            bindingContext.$data.Touchpoints()[index].LeftPos(bindingContext.$root.Survey().OrgSub().Touchpoints[index].LeftPos());
            bindingContext.$data.Touchpoints()[index].TopPos(bindingContext.$root.Survey().OrgSub().Touchpoints[index].TopPos());
            bindingContext.$data.Touchpoints()[index].SliderValue(bindingContext.$root.Survey().OrgSub().Touchpoints[index].SliderValue());
        }
        if (bindingContext.$root.Survey().SurveyConfigs().length > 0)
            bindingContext.$data.SelectedTouchPoint(bindingContext.$root.Survey().SurveyConfigs()[0].TouchpointName);

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.addSurveyConfig = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (bindingContext.$root.Survey().SurveyConfigs().length == 0) {
            bindingContext.$root.Survey().SurveyConfigs.push(new ConfigDataModel(null));
            bindingContext.$root.Survey().SurveyConfigs()[0].TouchpointName(bindingContext.$root.Survey().OrgSub().Touchpoints[0].TouchpointName());
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
};

ko.bindingHandlers.deleteResponse = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            
            if (confirm("Would you like to delete this survey response " + bindingContext.$root.Survey().SummaryData().SurveyResponseID() + " permanently? If yes, press Ok.\n\nSystem will log the record of who made this deletion.")) {
                var urlString = "/api/DeleteSurveyResponse/" + bindingContext.$root.Survey().SummaryData().SurveyResponseID();
                $.ajax({
                    type: "DELETE",
                    url: urlString,
                    contentType: "application/json"
                }).success(function (allData) {
                    $(element).closest(".list-section").empty();
                }).fail(function (allData) {
                });
            }
        });
    }
}

function ResponseViewModel() {

    var self = this;
    var multiplier = 1;
    self.Survey = ko.observable(null);
    self.SurveyResponseDiv = ko.observable();
    self.SurveyID = ko.observable();
    var url = document.URL;
    self.IsResponseView = ko.observable(url.indexOf("/SurveyResponse") > 0 ? true : false);

    self.Touchpoints = ko.observableArray([]);
    self.AvlblTouchpoints = ko.observableArray([]);
    self.Subsidiaries = ko.observableArray([]);

    self.getAddressLink = function (address) {
        return ko.computed({
            read: function () {
                address = $('<div>').append(address).text()
                return 'http://maps.google.com/?q=' + address.replace(/[#,-]/g, ' ');
            }
        });
    }

    self.loadSurveyResponse = function (element, id) {
        self.SurveyResponseDiv($(element));
        var urlString = "/api/GetSurveyResponse/" + id;
        $.ajax({
            type: "GET",
            url: urlString,
            contentType: "application/json"
        }).success(function (allData) {
            self.Survey(new SurveyModel(allData, true, false, false));
            self.Subsidiaries.push({ key: self.Survey().OrgSub().RegisteredName(), value: self.Survey().OrgSub().OrgSubID });

            for (var index = 0 ; index < self.Survey().OrgSub().Touchpoints.length ; index++) {
                self.Touchpoints.push({ key: self.Survey().OrgSub().Touchpoints[index].TouchpointName(), value: self.Survey().OrgSub().Touchpoints[index].TouchpointID, valueObject: self.Survey().OrgSub().Touchpoints[index], OrgSubID: self.Survey().OrgSub().OrgSubID });
                if (self.Survey().OrgSub().OrgSubID == self.Survey().SelectedSubsidiaries()[0])
                    self.AvlblTouchpoints.push({ key: self.Survey().OrgSub().Touchpoints[index].TouchpointName(), value: self.Survey().OrgSub().Touchpoints[index].TouchpointID, valueObject: self.Survey().OrgSub().Touchpoints[index], OrgSubID: self.Survey().OrgSub().OrgSubID });
            }

            function addSubsidiaries(subsidiaries) {
                if (subsidiaries.length >= 1) {
                    for (var index = 0 ; index < subsidiaries.length ; index++) {
                        self.Subsidiaries.push({ key: subsidiaries[index].RegisteredName(), value: subsidiaries[index].OrgSubID });
                        for (var index3 = 0 ; index3 < subsidiaries[index].Touchpoints.length ; index3++) {
                            self.Touchpoints.push({ key: subsidiaries[index].Touchpoints[index3].TouchpointName(), value: subsidiaries[index].Touchpoints[index3].TouchpointID, valueObject: subsidiaries[index].Touchpoints[index3], OrgSubID: subsidiaries[index].OrgSubID() });
                            if (subsidiaries[index].OrgSubID() == self.Survey().SelectedSubsidiaries()[0]) {
                                self.AvlblTouchpoints.push({ key: subsidiaries[index].Touchpoints[index3].TouchpointName(), value: subsidiaries[index].Touchpoints[index3].TouchpointID, valueObject: subsidiaries[index].Touchpoints[index3], OrgSubID: subsidiaries[index].OrgSubID() });
                            }
                        }
                        addSubsidiaries(subsidiaries[index].Subsidiaries());
                    }
                }
            }
            subsidiaries = self.Survey().OrgSub().Subsidiaries();

            for (var index1 = 0 ; index1 < subsidiaries.length ; index1++) {
                self.Subsidiaries.push({ key: subsidiaries[index1].RegisteredName(), value: subsidiaries[index1].OrgSubID });
                for (var index2 = 0 ; index2 < subsidiaries[index1].Touchpoints.length ; index2++) {
                    self.Touchpoints.push({ key: subsidiaries[index1].Touchpoints[index2].TouchpointName(), value: subsidiaries[index1].Touchpoints[index2].TouchpointID, valueObject: subsidiaries[index1].Touchpoints[index2], OrgSubID: subsidiaries[index1].OrgSubID() });
                    if (subsidiaries[index1].OrgSubID() === self.Survey().SelectedSubsidiaries()[0])
                        self.AvlblTouchpoints.push({ key: subsidiaries[index1].Touchpoints[index2].TouchpointName(), value: subsidiaries[index1].Touchpoints[index2].TouchpointID, valueObject: subsidiaries[index1].Touchpoints[index2], OrgSubID: subsidiaries[index1].OrgSubID() });
                }
                addSubsidiaries(subsidiaries[index1].Subsidiaries());
            }
            $(element).html(document.getElementById("response-template").innerHTML);
            ko.cleanNode(element);
            ko.applyBindings(self, element);
            $(element).show();
        }).fail(function (allData) {
        });
    }  
}