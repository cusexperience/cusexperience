﻿/***************************** Module Addition Bindings **************************************************/
ko.bindingHandlers.toggleSidePanel = {
    init: function (element, valueAccessor) {
        var minusIcon = "/Images/Minus-Icon.png";
        var plusIcon = "/Images/Plus-Icon.png";
        var currIcon = $('#toggle-icon-btn').attr("src");
        setTimeout(function () {
            $('.sidepanel').toggle();
            currIcon = $('#toggle-icon-btn').attr("src");

            if (currIcon == plusIcon) {
                $('#toggle-icon-btn').attr("src", minusIcon);
            } else if (currIcon == minusIcon) {
                $('#toggle-icon-btn').attr("src", plusIcon);
            }
        }, 3000);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var minusIcon ="/Images/Minus-Icon.png";
        var plusIcon ="/Images/Plus-Icon.png";
        var currIcon = $('#toggle-icon-btn').attr("src");
        $(element).click(function () {
            $('.sidepanel').toggle();
            currIcon = $('#toggle-icon-btn').attr("src");
            
            if (currIcon == plusIcon) {
                $('#toggle-icon-btn').attr("src", minusIcon);
            } else if (currIcon == minusIcon) {
                $('#toggle-icon-btn').attr("src", plusIcon);
            }
        });
    }
}


ko.bindingHandlers.toggleSidePanelLeft = {
    init: function (element, valueAccessor) {
        var minusIcon = "/Images/Minus-Icon.png";
        var plusIcon = "/Images/Plus-Icon.png";
        var currIcon = $('#toggle-icon-btn').attr("src");
        setTimeout(function () {
            $('.sidepanel-left').toggle();
            currIcon = $('#toggle-icon-btn-left').attr("src");
            if (currIcon == plusIcon) {
                $('#toggle-icon-btn-left').attr("src", minusIcon);
            } else if (currIcon == minusIcon) {
                $('#toggle-icon-btn-left').attr("src", plusIcon);
            }
        },3000);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var minusIcon = "/Images/Minus-Icon.png";
        var plusIcon = "/Images/Plus-Icon.png";
        var currIcon = $('#toggle-icon-btn').attr("src");
        $(element).click(function () {
            $('.sidepanel-left').toggle();
            currIcon = $('#toggle-icon-btn-left').attr("src");
            if (currIcon == plusIcon) {
                $('#toggle-icon-btn-left').attr("src", minusIcon);
            } else if (currIcon == minusIcon) {
                $('#toggle-icon-btn-left').attr("src", plusIcon);
            }
        });
    }
}

ko.bindingHandlers.toggleDemographicsDropdown = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            $(element).parent().parent().find('.options-panel').toggle();
            down = $(element).hasClass("icon-angle-down");
            if (down) {
                $(element).removeClass("icon-angle-down");
                $(element).addClass("icon-angle-up");
            } else {
                $(element).removeClass("icon-angle-up");
                $(element).addClass("icon-angle-down");
            }
        });
    }
}

/* Binding Handler to add a new Module */
ko.bindingHandlers.addModule = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$data.addModule();
            $.scrollTo($(".survey-sections:last").offset().top - 55, 100);
        });
    }
}

/* Binding Handler to add a new Module */
ko.bindingHandlers.addOptions = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            $(element).parent().find("#optionsModal").modal('show');
        });
    }
}

ko.bindingHandlers.removeSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if (bindingContext.$parent.QuestionData().QuestionItems().length > 1) {
                var moveIndex = bindingContext.$index() >= bindingContext.$parent.QuestionData().QuestionItems().length - 1 ? bindingContext.$index() - 1 :
                    bindingContext.$index();
                var carouselElement = $(element).closest('.carousel');
                bindingContext.$parent.QuestionData().QuestionItemRemoveClick(bindingContext.$data, carouselElement);
                carouselElement.find('.carousel-indicators').find('li').eq(moveIndex).addClass("active");
                carouselElement.find('.carousel-inner').find('.item').eq(moveIndex).addClass("active");
                bindingContext.$parent.QuestionData().CurrentIndex(moveIndex);
            }
        });
    }
}

/* Binding Handler to add a new Module Between */
ko.bindingHandlers.addModuleBetween = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var value = valueAccessor();
            bindingContext.$parent.addModuleBetween(bindingContext.$index(), UNSET);
            //$.scrollTo($(".survey-sections").eq(bindingContext.$index() - 1), 100, function () { return { top: -1000 }; });
            $.scrollTo($(".survey-sections").eq(bindingContext.$index() - 1).offset().top-55, 100);
            
        });
    }
}

ko.bindingHandlers.changeModuleType = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var value = valueAccessor();
            $(element).parent().hide('slide', { direction: 'left' }, 300,
            function () {
                bindingContext.$parents[1].changeModuleType(value, bindingContext.$parentContext.$index());
            });
        });
    }
}

/* Binding Handler to add a Duplicate Question */
ko.bindingHandlers.duplicateModule = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var value = valueAccessor();
            bindingContext.$parent.duplicateModule(bindingContext.$data, bindingContext.$index());
            $.scrollTo($(".survey-sections").eq(bindingContext.$index()), 100, { offset: -100 });
            $(".survey-sections").eq(bindingContext.$index()).addClass('duplicated-module');
            setTimeout(function () {
                $(".survey-sections").eq(bindingContext.$index()).removeClass("duplicated-module");
            }, 5000);
        });
    }
}

/* Binding Handler to change the type of Question */
ko.bindingHandlers.changeQuestionType = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();

        if (value == QUESTIONTYPES.RANK || value == QUESTIONTYPES.SLIDE || value == QUESTIONTYPES.PICTURE 
             || value == QUESTIONTYPES.SEGMENT || value == QUESTIONTYPES.SOCIAL ) {
            $(element).addClass('disabled');
            $(element).find('a').removeClass('blue-font').addClass('disabled-link');
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();

        if (value == QUESTIONTYPES.RANK || value == QUESTIONTYPES.SLIDE || value == QUESTIONTYPES.PICTURE
             || value == QUESTIONTYPES.SEGMENT || value == QUESTIONTYPES.SOCIAL)
            return false;

        $(element).click(function () {
            var value = valueAccessor();
            $(element).parent().hide('slide', { direction: 'left' }, 300,
           function () {
               bindingContext.$parent.changeQuestionType(value, bindingContext.$parent);
           });
        });
    }
}

/* Binding Handler to change the type of Question */
ko.bindingHandlers.unsetModuleType = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var value = valueAccessor();
            if (!bindingContext.$parent.checkIsModuleChanged(bindingContext.$data)) {
                bindingContext.$parent.changeModuleType(UNSET, bindingContext.$index());
            } else {
                if (confirm("Your previous changes on this module would be deleted permanently. are you sure you want to change module type?")) {
                    bindingContext.$parent.changeModuleType(UNSET, bindingContext.$index());
                }
            }
        });
    }
}

/* Binding Handler to change the type of Question */
ko.bindingHandlers.unsetQuestionType = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var value = valueAccessor();
            if (!bindingContext.$parent.checkIsModuleChanged(bindingContext.$data)) {
                bindingContext.$parent.changeModuleType(MODULETYPES.QUESTION, bindingContext.$index());
            } else {
                if (confirm("Your previous changes on this question would be deleted permanently. are you sure you want to change question type?")) {
                    bindingContext.$parent.changeModuleType(MODULETYPES.QUESTION, bindingContext.$index());
                }
            }
        });
    }
}

/* Slider Functionality */
ko.bindingHandlers.slider = {
    init: function (element, valueAccessor, allBindingsAccessor) {
        var options = allBindingsAccessor().sliderOptions || {};
        $(element).slider(options);
        ko.utils.registerEventHandler(element, "slidechange", function (event, ui) {
            var observable = valueAccessor();
            observable(ui.value);
        })
    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        if (isNaN(value)) value = 0;
        $(element).slider("option", allBindingsAccessor().sliderOptions);
        $(element).slider("value", value);
    }
};


ko.bindingHandlers.selectOrgSub = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0); 
        $(element).on('multiselectclick', function (event, ui) {
            bindingContext.$root.AvlblTouchpoints([]);
            for (index = 0 ; index < bindingContext.$root.Touchpoints().length ; index++) {
                var selValues = $(element).multiselect("getChecked").map(function () {
                    return parseInt(this.value);
                }).get();
                if ($.inArray(bindingContext.$root.Touchpoints()[index].OrgSubID(), selValues) != -1) {
                    bindingContext.$root.AvlblTouchpoints.push(bindingContext.$root.Touchpoints()[index]);
                }
            }
            if (bindingContext.$root.AvlblTouchpoints().length > 0) {
                bindingContext.$root.Survey().SelectedTouchpoints = ko.observableArray([]);
                bindingContext.$root.Survey().SelectedTouchpoints().push(bindingContext.$root.AvlblTouchpoints()[0].value);
                setTimeout(function () {
                $(".touchpoint-select").multiselect("widget").find(":radio:first").each(function () {
                    this.click();
                });
                }, 0);
            }   
        });
    }
};

ko.bindingHandlers.select = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

ko.bindingHandlers.selectMultiple = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);

        $(element).bind('multiselectopen', function (event, ui) {
            $(element).multiselect("widget").find(":checkbox").each(function () {
                if (this.checked == true) {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                }
            });
        });

        $(element).on('multiselectclick', function (event, ui) {
            if (ui.value === "All" && ui.checked == true) {
                $(element).multiselect('checkAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                });

            } else if (ui.value === "All" && ui.checked == false) {
                $(element).multiselect('uncheckAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if ($(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').removeClass('multiselect-sel');
                });
            } else {
                var alltrue = true;
                var allElement;
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (this.value === "All") allElement = this;
                    if (this.checked == true) {
                        if (!$(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').addClass('multiselect-sel');
                    } else {
                        if (this.value != "All") alltrue = false;
                        if ($(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').removeClass('multiselect-sel');
                    }
                });
                if (alltrue == true && allElement.checked == false) {
                    bindingContext.$parent.AnswerData().DGAnswerFields()[bindingContext.$index()].FieldValues.push("All");
                    $(allElement).closest('label').addClass('multiselect-sel');
                    $(allElement).prop('checked', true);
                } else if (alltrue == false && allElement.checked == true) {
                    bindingContext.$parent.AnswerData().DGAnswerFields()[bindingContext.$index()].FieldValues.remove("All");
                    $(allElement).closest('label').removeClass('multiselect-sel');
                    $(allElement).prop('checked', false);
                }
            }
        });


        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

ko.bindingHandlers.selectSingle = {
    init: function (element) {
        $(element).multiselect({
            header: false,
            selectedList: 1,
            multiple: false,
            classes: 'demographics-multiselect'
        });
    },
    update: function (element) {
        $(element).multiselect({
            header: false,
            selectedList: 1,
            multiple: false
        });
    }
};

ko.bindingHandlers.enterKey = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).keypress(function (event) {

            var value = $(element).val();
            if (value !== "") {
                $(element).removeClass('error-required');
                bindingContext.$root.PublishIDValidation.IsValid(true);
                bindingContext.$root.PublishIDValidation.Message("");
            }

            var keyCode = (event.which ? event.which : event.keyCode);
            if (keyCode === 13) {
                bindingContext.$root.updatePublishID(element);
            }
            return true;
        });
    }
};

ko.bindingHandlers.updatePublishID = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.updatePublishID($('#PublishID')[0]);
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).val(value);
 
    }
}

ko.bindingHandlers.autosize = {
    init: function (element, valueAccessor) {
        $(element).autosize();
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).autosize();
        if ($(document).width() <= 320)
            $(element).css('height', '38px');
        else
            $(element).css('height', '72px');
    }
}

ko.bindingHandlers.autoComplete = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).autocomplete({
            source: function (request, response) {
                var data = ko.toJSON({
                    OrgSubID : bindingContext.$root.Survey().OrgSub().OrgSubID,
                    TouchpointName: bindingContext.$root.Survey().SurveyConfigs().length > 0 ? bindingContext.$root.Survey().SurveyConfigs()[0].TouchpointName : '',
                    QueryTerm: request.term
                });
                $.ajax({
                    type: "POST",
                    url: "/api/STAFF/UserNames",
                    contentType: "application/json",
                    data: data,
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
            },
            open: function () {
                $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function () {
                $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
    },
    update: function (element) {
    }
};

ko.bindingHandlers.DGFieldAddBetween = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {


    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$parent.QuestionData().DGFieldAddBetweenClick(bindingContext.$index(), UNSET);
            $.scrollTo($(element).parent().find(".demotagdiv").eq(bindingContext.$index() - 1), 100, { offset: -100 });
        });
    }
};

ko.bindingHandlers.DGFieldChangeFieldType = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {


    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
          
            $.scrollTo($(element).parent().parent().parent().find(".demotagdiv").eq(bindingContext.$parentContext.$index()), 100, { offset: -100 });
            bindingContext.$parents[1].QuestionData().DGFieldChangeFieldType(bindingContext.$data, bindingContext.$parentContext.$index());
           
           
        });
    }
};

ko.bindingHandlers.DGFieldAdd = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {


    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$data.QuestionData().DGFieldAdd();
            $.scrollTo($(element).parent().find(".demotagdiv").eq(bindingContext.$data.QuestionData().DGQuestionFields().length - 1), 100, { offset: -100 });
        });
    }
};

ko.bindingHandlers.DGFieldRemoveClick = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {


    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
           
            $.scrollTo($(element).parent().find(".demotagdiv").eq(bindingContext.$index() - 1), 100, { offset: -100 });
            console.log($(element).parent().attr("class"));
            bindingContext.$parent.QuestionData().DGFieldRemoveClick(bindingContext.$data);
                     
        });
    }
};

ko.bindingHandlers.drag = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).css('top', bindingContext.$data.TopPos());
        $(element).css('left', bindingContext.$data.LeftPos());
       var dragElement = $(element);
        dragElement.draggable({
            stop: function () {
                if ($(document).width <= 320) {
                    bindingContext.$data.TopPos($(element).css('top') * 2);
                    bindingContext.$data.LeftPos($(element).css('left') * 2);
                } else {
                    bindingContext.$data.TopPos($(element).css('top'));
                    bindingContext.$data.LeftPos($(element).css('left'));
                }
            }
        });  
    }
};

ko.bindingHandlers.drop = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var dropElement = $(element);
        var dropOptions = {
            drop: function (event, ui) {
            }
        };
        dropElement.droppable(dropOptions);
    }
};

/* Binding Handler for the file upload functionality */
ko.bindingHandlers.fileUpload = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).change(function () {
            var options = ko.utils.unwrapObservable(valueAccessor()),
            property = ko.utils.unwrapObservable(options.property),
            url = ko.utils.unwrapObservable(options.url);
            component = ko.utils.unwrapObservable(options.component);
            if (property && url) {
                if (element.files.length) {
                    var $this = $(this),
                        fileName = $this.val();
                    var formElement = $(element).closest('form');
                    var formData = new FormData(formElement[0]);
                    $.ajax({
                        url: '/api/PostSurveyImage',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            switch (component) {
                                case "FrontImage":
                                    bindingContext.$data.FrontImage(data);
                                    $(element).val('');
                                    bindingContext.$data.SliderValue(50);
                                    bindingContext.$data.LeftPos('0px');
                                    bindingContext.$data.TopPos('0px');
                                    break;
                                case "LogoImage":
                                    bindingContext.$data.LogoImage(data);
                                    $(element).val('');
                                    break;
                                case "FBImage":
                                    bindingContext.$data.FBImage(data);
                                    $(element).val('');
                                    break;
                            }
                        },
                        error: function () {
                            alert('Error in uploading File. Please try again.');
                        }
                    });
                }
            }
        });
    }
};

ko.bindingHandlers.editableText = {
    init: function (element, valueAccessor) {
        $(element).on('blur', function () {
            var observable = valueAccessor();
            
            observable($(this).html());
        });
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        var div = $('<div>').html(value);
        div.find('*').each(function () {
            if ($(this).is('[data-bind]')) {
                $(this).removeAttr('data-bind');
             }
        });
        value = div.html();
        $(element).html(value);
    }
};


/* Binding Handler to flip an Image */
ko.bindingHandlers.flipImage = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var toggleDiv = $(element).parent().parent().find('#flip-toggle');
            toggleDiv.toggleClass('flip');
        });
    }
}

ko.bindingHandlers.flipAppreciation = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var toggleDiv = $('.file-input-wrapper').find('#flip-toggle-app');
            toggleDiv.toggleClass('flip');
        });
    }
}
ko.bindingHandlers.closeModel = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            $("#publishModal").modal('hide');
        });
    }
}

ko.bindingHandlers.movePreviousSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index >= 0) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('prev');
            }
        });
        $(element).parent().swiperight(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index >= 0) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('prev');
            }
        });
    }
}

ko.bindingHandlers.moveNextSlide = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index <= bindingContext.$data.QuestionData().QuestionItems().length - 1) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('next');
            }
        });
        $(element).parent().swipeleft(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index <= bindingContext.$data.QuestionData().QuestionItems().length - 1) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('next');
            } 
        });
    }
}

ko.bindingHandlers.createNewSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$data.QuestionData().QuestionItemAddClick();
            $(element).parent().parent().carousel(bindingContext.$data.QuestionData().QuestionItems().length - 1);
            bindingContext.$data.QuestionData().CurrentIndex(bindingContext.$data.QuestionData().QuestionItems().length - 1);
        });
    }
}

ko.bindingHandlers.changeCurrentSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var csIndex = bindingContext.$index();
            $(element).parent().parent().carousel(csIndex);
            bindingContext.$parent.QuestionData().CurrentIndex(csIndex);
        });
    }
}


ko.bindingHandlers.generateQRCode = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var url = ko.unwrap(valueAccessor());
        new QRCode(element, {
            text: url,
            width:1600,
            height:1600,
            colorDark : "#1B1B1B",
            colorLight : "#FFFFFF",
            correctLevel : QRCode.CorrectLevel.H
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
}

ko.bindingHandlers.enableSorting = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).mousedown(function () {
            bindingContext.$root.isSortable(true);
          //  clearInterval(bindingContext.$root.saveSurvey);
        });
        $(element).mouseleave(function () {
            bindingContext.$root.isSortable(false);
        });
    }
}

ko.bindingHandlers.enableSortingSidepanel = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            $.scrollTo($(".survey-sections").eq(bindingContext.$index()).offset().top - 55, 100);
        });
        $(element).mousedown(function () {
            bindingContext.$root.isSortable(true);
        });
        $(element).mouseleave(function () {
            bindingContext.$root.isSortable(false);
        });
    }
}

ko.bindingHandlers.checkBox = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var observable = ko.utils.unwrapObservable(valueAccessor());
        if (observable) {
            $(element).removeClass("icon-check-empty");
            $(element).addClass("icon-check-sign");
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var observable = valueAccessor();
            if ($(element).hasClass("icon-check-empty")) {
                $(element).removeClass("icon-check-empty");
                $(element).addClass("icon-check-sign");
                observable(true);
            } else if ($(element).hasClass("icon-check-sign")) {
                $(element).removeClass("icon-check-sign");
                $(element).addClass("icon-check-empty");
                observable(false);
            }
        });
    }
}

ko.bindingHandlers.faceBookCheckBox = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var observable = ko.utils.unwrapObservable(valueAccessor());
        if (observable) {
            $(element).removeClass("icon-check-empty");
            $(element).addClass("icon-check-sign");
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var observable = valueAccessor();
            if ($(element).hasClass("icon-check-empty")) {
                $(element).removeClass("icon-check-empty");
                $(element).addClass("icon-check-sign");
                $("html, body").animate({ scrollTop: $(document).height() }, 1000);
                observable(true);
            } else if ($(element).hasClass("icon-check-sign")) {
                $(element).removeClass("icon-check-sign");
                $(element).addClass("icon-check-empty");
                observable(false);
            }
        });
    }
}

ko.bindingHandlers.addTouchpoints = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        bindingContext.$data.Touchpoints.removeAll();
        var Touchpoints = bindingContext.$root.Survey().OrgSub().Touchpoints;
        for (var index in Touchpoints) {
            bindingContext.$data.Touchpoints().push(new TouchpointModel(null));
                if (index == 0) bindingContext.$data.SelectedTouchPoint(bindingContext.$root.Survey().OrgSub().Touchpoints[index].TouchpointName());
                bindingContext.$data.Touchpoints()[index].TouchpointName(bindingContext.$root.Survey().OrgSub().Touchpoints[index].TouchpointName());
                bindingContext.$data.Touchpoints()[index].RegisteredName(bindingContext.$root.Survey().OrgSub().Touchpoints[index].RegisteredName());
                bindingContext.$data.Touchpoints()[index].BrandWebsite(bindingContext.$root.Survey().OrgSub().Touchpoints[index].BrandWebsite());
                bindingContext.$data.Touchpoints()[index].PhoneNumber(bindingContext.$root.Survey().OrgSub().Touchpoints[index].PhoneNumber());
                bindingContext.$data.Touchpoints()[index].EmailAddress(bindingContext.$root.Survey().OrgSub().Touchpoints[index].EmailAddress());
                bindingContext.$data.Touchpoints()[index].Address(bindingContext.$root.Survey().OrgSub().Touchpoints[index].Address());
                bindingContext.$data.Touchpoints()[index].FrontImage(bindingContext.$root.Survey().OrgSub().Touchpoints[index].FrontImage());
                bindingContext.$data.Touchpoints()[index].LeftPos(bindingContext.$root.Survey().OrgSub().Touchpoints[index].LeftPos());
                bindingContext.$data.Touchpoints()[index].TopPos(bindingContext.$root.Survey().OrgSub().Touchpoints[index].TopPos());
                bindingContext.$data.Touchpoints()[index].SliderValue(bindingContext.$root.Survey().OrgSub().Touchpoints[index].SliderValue());
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

function SurveyFormValidationModel(isValid, message) {
    var self = this;
    self.IsValid = ko.observable(isValid);
    self.Message = ko.observable(message);
}

function AppViewModel() {
    var self = this;
    var multiplier = 1;
    var url = document.URL;
    self.HomeURL = url.substring(0, url.indexOf("/Survey"));
    self.HomeURL = self.HomeURL.replace("cusexperience", "CusExperience");
    self.Survey = ko.observable(new SurveyModel(initJSONData));
    self.CurrentLogicModule = ko.observable();
    self.Touchpoints = ko.observableArray([]);
    self.AvlblTouchpoints = ko.observableArray([]);
    self.Subsidiaries = ko.observableArray([]);
    self.PublishIDValidation = new SurveyFormValidationModel(true, "Username already exists");

    self.Subsidiaries.push({ key: self.Survey().OrgSub().RegisteredName(), value: self.Survey().OrgSub().OrgSubID });

    for (var index = 0 ; index < self.Survey().OrgSub().Touchpoints.length ; index++) {
        self.Touchpoints.push({ key: self.Survey().OrgSub().Touchpoints[index].TouchpointName() , value: self.Survey().OrgSub().Touchpoints[index].TouchpointID, valueObject: self.Survey().OrgSub().Touchpoints[index], OrgSubID: self.Survey().OrgSub().OrgSubID });
        self.AvlblTouchpoints.push({ key: self.Survey().OrgSub().Touchpoints[index].TouchpointName() , value: self.Survey().OrgSub().Touchpoints[index].TouchpointID, valueObject: self.Survey().OrgSub().Touchpoints[index] , OrgSubID: self.Survey().OrgSub().OrgSubID });
    }

    if (self.Touchpoints().length > 0)
        self.Survey().SelectedTouchpoints.push(self.AvlblTouchpoints()[0].value);

    function addSubsidiaries(subsidiaries) {
        if (subsidiaries.length >= 1) {
            for (var index = 0 ; index < subsidiaries.length ; index++) {
                self.Subsidiaries.push({ key:  subsidiaries[index].RegisteredName(), value: subsidiaries[index].OrgSubID });
                for (var index3 = 0 ; index3 < subsidiaries[index].Touchpoints.length ; index3++) {
                    self.Touchpoints.push({ key: subsidiaries[index].Touchpoints[index3].TouchpointName(), value: subsidiaries[index].Touchpoints[index3].TouchpointID, valueObject: subsidiaries[index].Touchpoints[index3], OrgSubID: subsidiaries[index].OrgSubID });
                    
                }
                addSubsidiaries(subsidiaries[index].Subsidiaries());
            }
        }
    }
    subsidiaries = self.Survey().OrgSub().Subsidiaries();

    for (var index1 = 0 ; index1 < subsidiaries.length ; index1++) {
        self.Subsidiaries.push({ key: subsidiaries[index1].RegisteredName(), value: subsidiaries[index1].OrgSubID });
        for (var index2 = 0 ; index2 < subsidiaries[index1].Touchpoints.length ; index2++) {
            self.Touchpoints.push({ key: subsidiaries[index1].Touchpoints[index2].TouchpointName(), value: subsidiaries[index1].Touchpoints[index2].TouchpointID, valueObject: subsidiaries[index1].Touchpoints[index2], OrgSubID: subsidiaries[index1].OrgSubID });
            
        }
        addSubsidiaries(subsidiaries[index1].Subsidiaries());
    }

    if(self.Subsidiaries().length > 0)
        self.Survey().SelectedSubsidiaries.push(self.Subsidiaries()[0].value());

    self.SurveyContent = ko.computed(function () {
        return ko.mapping.toJS(self.Survey);
    });

    self.surveyStatusMsg = ko.observable('');
    self.isSortable = ko.observable(false);

    self.startSort = function (event, ui) {
        ui.item.startPos = ui.item.index();
        console.log(ui.item.startPos);
    }

    self.swapModules = function (event,ui) {
        console.log(ui.item.index());
        var temp = self.Survey().swapModules(ui.item.startPos, ui.item.index());
    }

    self.openSurvey = function () {
        $("#publishModal").modal('hide');
        var d = new Date();
        var x = window.open(self.HomeURL + self.Survey().FullPublishedURL(), "Window_" + d.getMilliseconds());
        x.focus();
    }

    self.showContent = function () {
        $(".settingspanel").hide();
        $(".editpanel").show();
    }

    self.showSetting = function () {
        $(".editpanel").toggle();
        $(".settingspanel").toggle();
    }

    self.showPublishSetting = function () {
        $(".publish-settings").show();
        $(".alert-settings").hide();
    }

    self.showAlertSetting = function () {
        $(".publish-settings").hide();
        $(".alert-settings").show();
    }
    

    /* Save the Survey Contents */
    self.saveSurvey = function () {
        for (var i in self.Survey().Modules()) {
            self.Survey().Modules()[i].Position(i);
        }

        $('#sidesavebtn').text("Saving");
        $('#sidesavebtn').attr("disabled", true);
        $('#sidepublishbtn').attr("disabled", true);

        var data = ko.toJSON(self.Survey);
        $.ajax({
            type: "POST",
            data: data,
            url: "/api/PostSurvey",
            contentType: "application/json",
            success: function (data) {
                self.Survey().SurveyID(data.SurveyID);
                self.surveyStatusMsg('Saved.');
                $('#sidesavebtn').addClass("btn-success");
                $('#sidesavebtn').text("Saved");
                setTimeout(function () {
                    if (url.indexOf("/Create") != -1) {
                        window.location = "/Survey/Detail/" + data.SurveyID;
                    } else {
                        $('#sidesavebtn').text("Save");
                        $('#sidesavebtn').removeClass("btn-success");
                        $('#sidesavebtn').attr("disabled", false);
                        $('#sidepublishbtn').attr("disabled", false);
                    }
                }, 5000);
            },
            error: function (data) {
                $('#sidesavebtn').addClass("btn-fail");
                $('#sidesavebtn').text("Save Fail");
                setTimeout(function () {
                    $('#sidesavebtn').text("Save");
                    $('#sidesavebtn').removeClass("btn-fail");
                    $('#sidesavebtn').attr("disabled", false);
                    $('#sidepublishbtn').attr("disabled", false);
                }, 5000);
            }
        });
    }

    self.saveSurvey2 = function () {
        for (var i in self.Survey().Modules()) {
            self.Survey().Modules()[i].Position(i);
        }
        $('#sidesavebtn2').text("Saving");
        $('#sidesavebtn2').attr("disabled", true);
        var data = ko.toJSON(self.Survey);
        $.ajax({
            type: "POST",
            data: data,
            url: "/api/PostSurvey",
            contentType: "application/json",
            success: function (data) {
                self.Survey().SurveyID(data.SurveyID);
                self.surveyStatusMsg('Saved.');
                $('#sidesavebtn2').addClass("btn-success");
                $('#sidesavebtn2').text("Saved");
                setTimeout(function () {
                    if (url.indexOf("/Create") != -1) {
                        window.location = "/Survey/Detail/" + data.SurveyID;
                    } else {
                        $('#sidesavebtn2').text("Save");
                        $('#sidesavebtn2').removeClass("btn-success");
                        $('#sidesavebtn2').attr("disabled", false);
                    }
                }, 5000);
            },
            error: function (data) {
                $('#sidesavebtn2').addClass("btn-fail");
                $('#sidesavebtn2').text("Save Fail");
                setTimeout(function () {
                    $('#sidesavebtn2').text("Save");
                    $('#sidesavebtn2').removeClass("btn-fail");
                    $('#sidesavebtn2').attr("disabled", false);
                }, 5000);
            }
        });
    }

    self.updatePublishID = function (element) {
        var value = self.Survey().PublishedID();
        $('#sidesavebtn3').text("Saving");
        $('#sidesavebtn3').attr("disabled", true);
        if (value === "") {
            $(element).addClass('error-required');
            $(element).focus();
            return false;
        } else if (value.length < 3) {
            $(element).addClass('error-required');
            $(element).focus();
            self.PublishIDValidation.IsValid(false);
            self.PublishIDValidation.Message("Publish ID should contain atleast 3 characters");
            return false;
        }
        $.ajax({
            type: "GET",
            url: "/api/UpdatePublishID/" + self.Survey().SurveyID() + "/" + value,
            contentType: "application/json",
            success: function (data) {
                self.Survey().Status("UNPUBLISHED");
                self.Survey().Status("PUBLISHED");
                self.surveyStatusMsg('Saved.');
                $('#sidesavebtn3').addClass("btn-success");
                $('#sidesavebtn3').text("Saved");
                setTimeout(function () {
                    $('#sidesavebtn3').text("Save");
                    $('#sidesavebtn3').removeClass("btn-success");
                    $('#sidesavebtn3').attr("disabled", false);
                }, 5000);
            },
            error: function (data) {
                if (typeof (data) !== 'undefined' && typeof (data.responseJSON) !== 'undefined' && data.responseJSON.Message == "PublishID Already Exists") {
                    $(element).addClass('error-required');
                    $(element).focus();
                    $('#sidesavebtn3').addClass("btn-fail");
                    $('#sidesavebtn3').text("Save Fail");
                    self.PublishIDValidation.IsValid(false);
                    self.PublishIDValidation.Message("PublishID Already Exists");
                    setTimeout(function () {
                        $('#sidesavebtn3').text("Save");
                        $('#sidesavebtn3').removeClass("btn-fail");
                        $('#sidesavebtn3').attr("disabled", false);
                    }, 5000);
                } else {
                    $('#sidesavebtn3').addClass("btn-fail");
                    $('#sidesavebtn3').text("Save Fail");
                    setTimeout(function () {
                        $('#sidesavebtn3').text("Save");
                        $('#sidesavebtn3').removeClass("btn-fail");
                        $('#sidesavebtn3').attr("disabled", false);
                    }, 5000);
                }
            }
        });
    }

    self.previewSurvey = function () {
        $('#sidepreviewbtn').text("Loading Preview");
        var surveyID;
        for (var i in self.Survey().Modules()) {
            self.Survey().Modules()[i].Position(i);
        }
        var data = ko.toJSON(self.Survey);
        $.ajax({
            type: "POST",
            data: data,
            url: "/api/PostSurvey",
            async: false,
            contentType: "application/json",
            success: function (data) {
                surveyID = data.SurveyID;
                self.Survey().SurveyID(surveyID);
                var x = window.open("/Survey/Preview/" + surveyID, "NewWindow");
                x.focus();
                $('#sidepreviewbtn').text("Preview");
                
            },
            error: function () {
                self.surveyStatusMsg('Oops! Please try again');
            }
        });
    }

    self.publishSurvey = function () {
        for (var i in self.Survey().Modules()) {
            self.Survey().Modules()[i].Position(i);
        }
        var data = ko.toJSON(self.Survey);
        $('#sidesavebtn').text("Saving");
        $('#sidepublishbtn').text("Publishing");
        $('#sidesavebtn').attr("disabled", true);
        $('#sidepublishbtn').attr("disabled", true);
        $.ajax({
            type: "POST",
            data: data,
            url: "/api/PostPublishedSurvey",
            contentType: "application/json",
            success: function (data) {
                $('#sidesavebtn').addClass("btn-success");
                $('#sidesavebtn').text("Saved");
                $('#sidepublishbtn').text("Published");
                $(".editpanel").toggle();
                $(".settingspanel").toggle();
                setTimeout(function () {
                     if (url.indexOf("/Create") != -1) {
                        window.location = "/Survey/Detail/" + data.SurveyID;
                     } else {
                         $('#sidesavebtn').text("Save");
                         $('#sidesavebtn').removeClass("btn-success");
                         $('#sidesavebtn').attr("disabled", false);
                     }
                }, 5000);
                self.Survey().SurveyID(data.SurveyID);
                self.Survey().IsPublished(true);
                self.Survey().Status("PUBLISHED");
                self.Survey().PublishedURL(data.PublishedURL);
                self.Survey().PublishedID(data.PublishedID);
                self.Survey().SurveyConfigs(data.SurveyConfigs);
                $("#publishModal").modal('show');
            },
            error: function (data) {
                self.surveyStatusMsg('Oops! Please try again');
            }
        });
    }

    self.unPublishSurvey = function () {
        var r = confirm("Do you want to Unpublish the Survey?");
        if (r == true) {
            $.ajax({
                type: "GET",
                url: "/api/UnPublishedSurvey/" + self.Survey().SurveyID(),
                contentType: "application/json",
                success: function (data) {
                    self.Survey().Status("UNPUBLISHED");
                },
                error: function (data) {
                    self.surveyStatusMsg('Oops! Please try again');
                }
            });
        }
    }
}

ko.bindingHandlers.loadCaptcha = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).load("/Captcha")
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

$(document).ready(function () {
    var surveyApp = new AppViewModel();
    ko.applyBindings(surveyApp, document.getElementById('body-content'));
    $('body').show();
});
