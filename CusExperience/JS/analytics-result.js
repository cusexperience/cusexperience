﻿ko.bindingHandlers.displayText = {
    init: function (element, valueAccessor) {
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).text($('<div>').append(value).text());
    }
};

ko.bindingHandlers.movePreviousSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index >= 0) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('prev');
            }
        });
        $(element).parent().swiperight(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index >= 0) {
                bindingContext.$data.QuestionData().CurrentIndex(index);
                $(element).parent().carousel('prev');
            }
        });
    }
}

ko.bindingHandlers.moveNextSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index <= bindingContext.$data.QuestionData().QuestionItems().length - 1) {
                $(element).parent().carousel('next');
            }
        });
        $(element).parent().swipeleft(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index <= bindingContext.$data.QuestionData().QuestionItems().length - 1) {
                $(element).parent().carousel('next');
            }
        });
    }
}

ko.bindingHandlers.changeCurrentSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var csIndex = bindingContext.$index();
            $(element).parent().parent().carousel(csIndex);
        });
    }
}

ko.bindingHandlers.showDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var OverallRating = bindingContext.$data.AnalyticsData().OverallRating();
        var dataset = {
            rating: [OverallRating, 100 - OverallRating]
        };
        var margin = { top: 16, right: 16, bottom: 16, left: 16 };
        var width = $(element).width() - 14,
                height = $(element).height(),
                radius = Math.min(width, height) / 2;
        var vBW = 592;
        var vBH = 384;
        var x230 = -122.8;
        var y2 = -169.6;
        var x270 = 122.8;
        var lineFont = "20px";
        var y2Line = y2 - 6;
        var StrokeWidth = 3;
        var x1 = -110.52;
        var y1 = -152.64
        if (width > 300) {

        } else {
            margin = { top: 8, right: 8, bottom: 8, left: 8 };
            vBH = 192
            vBW = 288;
            x230 = x230 / 2;
            y2 = y2 / 2;
            x270 = x270 / 2;
            lineFont = "12px";
            y2Line = y2 - 2;
            StrokeWidth = 2;
            x1 = x1 / 2;
            y1 = y1 / 2;
        }

        var barColor = ["#EA6953", "#e8e8e8"];

        if (OverallRating >= 70) {

            barColor = ["#82CA9C", "#e8e8e8"];

        } else if (OverallRating >= 30) {
            barColor = ["#FDC689", "#e8e8e8"];
        }

        var color = d3.scale.category20().range(barColor);

        var degree = Math.PI / 180; // just to convert the radian-numbers

        var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

        var arc = d3.svg.arc()
            .innerRadius(radius - 3)
            .outerRadius(radius);
        /*
        var svg = d3.select(element).append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
            */
        var svg = d3.select(element).append("svg")
          .attr("width", '100%')
        .attr("height", '100%')
             .attr('viewBox', '0 0 ' + vBW + ' ' + vBH)
        .attr('preserveAspectRatio', 'xMinYMin')

        .append("g")

    .attr("transform", "translate(" + vBW / 2 + "," + vBH / 2 + ")");

        var path = svg.selectAll("path")
            .data(pie(dataset.rating))
            .enter().append("path")
            .attr("fill", function (d, i) {
                return color(i);
            })
            .attr("d", arc);
        var line = svg.append("line")
          .attr("x1", -x1)
           .attr("y1", y1)
           .attr("x2", x270)
           .attr("y2", y2)
            .attr("stroke-width", StrokeWidth)
            //.attr("stroke-dasharray", 5,5)
        .attr("stroke", "#82CA9C");


        var line = svg.append("line")
           .attr("x1", x1)
          .attr("y1", y1)
          .attr("x2", x230)
          .attr("y2", y2).attr("stroke-width", StrokeWidth)
          // .attr("stroke-dasharray", 8, 5)
       .attr("stroke", "#fdc689");

        svg.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "middle")
    .attr("x", x230 + 2)
    .attr("y", y2Line)
   // .text("Number of response(s)")
          .text("30.0")
      .style("font-size", lineFont)
      .style("fill", "#8d8d8d");

        svg.append("text")
  .attr("class", "x label")
  .attr("text-anchor", "middle")
  .attr("x", x270 + 2)
  .attr("y", y2Line)
 // .text("Number of response(s)")
        .text("70.0")
    .style("font-size", lineFont)
    .style("fill", "#8d8d8d");
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}
/*
ko.bindingHandlers.showChoiceDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var choiceWCounts = bindingContext.$data.AnalyticsData().ChoiceWCounts();
        var doughnutData = [];
        $.each(choiceWCounts, function (i, choiceWCount) {
            var doughnutSegment = { value: parseFloat(choiceWCount.Percentage()), color: CHARTCOLOURS[i] }
            doughnutData.push(doughnutSegment);
        });

        var doughnutDataSort = [];
        $.each(choiceWCounts, function (i, choiceWCount) {


            doughnutDataSort.push(parseFloat(choiceWCount.Percentage()));
        });

        var width = $(element).width(),
                height = $(element).height(),
                radius = Math.min(width, height) / 2;

        var dataset = {
            choiceSelection: doughnutDataSort,
        };

        var color = d3.scale.category20().range(CHARTCOLOURS);

        var degree = Math.PI / 180; // just to convert the radian-numbers
        var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

        var arc = d3.svg.arc()
            .innerRadius(radius - 17)
            .outerRadius(radius);

        var svg = d3.select(element).append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var path = svg.selectAll("path")
            .data(pie(dataset.choiceSelection))
          .enter().append("path")
            .attr("fill", function (d, i) { return color(i); })
            .attr("d", arc);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      
    }
}
*/
ko.bindingHandlers.showChoiceDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var choiceWCounts = bindingContext.$data.AnalyticsData().ChoiceWCounts();
        var doughnutData = [];
        $.each(choiceWCounts, function (i, choiceWCount) {
            var doughnutSegment = { value: parseFloat(choiceWCount.Percentage()), color: CHARTCOLOURS[i] }
            doughnutData.push(doughnutSegment);
        });

        var doughnutDataSort = [];
        $.each(choiceWCounts, function (i, choiceWCount) {
            doughnutDataSort.push(parseFloat(choiceWCount.Percentage()));
        });

        var width = $(element).width(),
                height = $(element).height(),
                radius = Math.min(width, height) / 2;

        var vBW = 592;
        var vBH = 384;
        if (width > 300) {

        } else {
            margin = { top: 8, right: 8, bottom: 8, left: 8 };
            vBH = 192
            vBW = 288;
        }

        var dataset = {
            choiceSelection: doughnutDataSort,
        };

        var color = d3.scale.category20().range(CHARTCOLOURS);

        var degree = Math.PI / 180; // just to convert the radian-numbers
        var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

        var arc = d3.svg.arc()
            .innerRadius(radius - 3)
            .outerRadius(radius);

        var svg = d3.select(element).append("svg")
            .attr("width", '100%')
            .attr("height", '100%')
            .attr('viewBox', '0 0 ' + vBW + ' ' + vBH)
            .attr('preserveAspectRatio', 'xMinYMin')
        .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var path = svg.selectAll("path")
            .data(pie(dataset.choiceSelection))
            .enter().append("path")
            .attr("fill", function (d, i) { return color(i); })
            .attr("d", arc);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}
/*
ko.bindingHandlers.showMultipleRatingDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var index = bindingContext.$index();
        if (bindingContext.$parent.AnalyticsData().AnalyticsItems().length > 0 && !$.isEmptyObject(bindingContext.$parent.AnalyticsData().AnalyticsItems()[index])) {
            var OverallRating = bindingContext.$parent.AnalyticsData().AnalyticsItems()[index].OverallRating();

            var dataset = {
                rating: [OverallRating, 100 - OverallRating]
            };

            var width = $(element).width(),
                height = $(element).height(),
                radius = Math.min(width, height) / 2;

            var color = d3.scale.category20().range(RATECHARTCOLOURS);

            var degree = Math.PI / 180; // just to convert the radian-numbers
            var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

            var arc = d3.svg.arc()
                .innerRadius(radius - 17)
                .outerRadius(radius);

            var svg = d3.select(element).append("svg")
                .attr("width", width)
                .attr("height", height)
                .append("g")
                .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

            var path = svg.selectAll("path")
                .data(pie(dataset.rating))
                .enter().append("path")
                .attr("fill", function (d, i) {
                    return color(i);
                })
                .attr("d", arc);
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        
    }
}

*/

ko.bindingHandlers.showMultipleRatingDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var index = bindingContext.$index();
        if (bindingContext.$parent.AnalyticsData().AnalyticsItems().length > 0 && !$.isEmptyObject(bindingContext.$parent.AnalyticsData().AnalyticsItems()[index])) {
            var OverallRating = bindingContext.$parent.AnalyticsData().AnalyticsItems()[index].OverallRating();

            var dataset = {
                rating: [OverallRating, 100 - OverallRating]
            };
            var margin = { top: 16, right: 16, bottom: 16, left: 16 };
            var width = $(element).width() - 14,
                 height = $(element).height(),
                 radius = Math.min(width, height) / 2;
            var vBW = 592;
            var vBH = 384;
            var x230 = -122.8;
            var y2 = -169.6;
            var x270 = 122.8;
            var lineFont = "20px";
            var y2Line = y2 - 6;
            var StrokeWidth = 3;
            var x1 = -110.52;
            var y1 = -152.64
            if (width > 300) {

            } else {
                margin = { top: 8, right: 8, bottom: 8, left: 8 };
                vBH = 192
                vBW = 288;
                x230 = x230 / 2;
                y2 = y2 / 2;
                x270 = x270 / 2;
                lineFont = "12px";
                y2Line = y2 - 2;
                StrokeWidth = 2;
                x1 = x1 / 2;
                y1 = y1 / 2;
            }
            var barColor = ["#EA6953", "#e8e8e8"];
            console.log()
            if (OverallRating >= 70) {

                barColor = ["#82CA9C", "#e8e8e8"];

            } else if (OverallRating >= 30) {
                barColor = ["#FDC689", "#e8e8e8"];
            }
            var color = d3.scale.category20().range(barColor);

            var degree = Math.PI / 180; // just to convert the radian-numbers
            var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

            var arc = d3.svg.arc()
                .innerRadius(radius - 3)
                .outerRadius(radius);

            var svg = d3.select(element).append("svg")
          		.attr("width", '100%')
        		.attr("height", '100%')
             	.attr('viewBox', '0 0 ' + vBW + ' ' + vBH)
        		.attr('preserveAspectRatio', 'xMinYMin')
        	.append("g")
			    .attr("transform", "translate(" + vBW / 2 + "," + vBH / 2 + ")");

            var path = svg.selectAll("path")
                .data(pie(dataset.rating))
                .enter().append("path")
                .attr("fill", function (d, i) {
                    return color(i);
                })
                .attr("d", arc);

            var line = svg.append("line")
          .attr("x1", -x1)
           .attr("y1", y1)
           .attr("x2", x270)
           .attr("y2", y2)
            .attr("stroke-width", StrokeWidth)
            //.attr("stroke-dasharray", 5,5)
        .attr("stroke", "#82CA9C");


            var line = svg.append("line")
               .attr("x1", x1)
              .attr("y1", y1)
              .attr("x2", x230)
              .attr("y2", y2).attr("stroke-width", StrokeWidth)
              // .attr("stroke-dasharray", 8, 5)
           .attr("stroke", "#fdc689");

            svg.append("text")
        .attr("class", "x label")
        .attr("text-anchor", "middle")
        .attr("x", x230 + 2)
        .attr("y", y2Line)
       // .text("Number of response(s)")
              .text("30.0")
          .style("font-size", lineFont)
          .style("fill", "#8d8d8d");

            svg.append("text")
      .attr("class", "x label")
      .attr("text-anchor", "middle")
      .attr("x", x270 + 2)
      .attr("y", y2Line)
     // .text("Number of response(s)")
            .text("70.0")
        .style("font-size", lineFont)
        .style("fill", "#8d8d8d");
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}
/*
ko.bindingHandlers.showMultipleChoiceDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var index = bindingContext.$index();
        if (bindingContext.$parent.AnalyticsData().AnalyticsItems().length > 0) {
            var choiceWCounts = bindingContext.$parent.AnalyticsData().AnalyticsItems()[index].ChoiceWCounts();
            var doughnutData = [];
            $.each(choiceWCounts, function (i, choiceWCount) {

                doughnutData.push(parseFloat(choiceWCount.Percentage()));
            });
            var dataset = {
                rating: doughnutData
            };

            var width = $(element).width(),
                    height = $(element).height(),
                    radius = Math.min(width, height) / 2;
            var color = d3.scale.category20().range(CHARTCOLOURS);

            var degree = Math.PI / 180; // just to convert the radian-numbers
            var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

            var arc = d3.svg.arc()
                .innerRadius(radius - 17)
                .outerRadius(radius);

            var svg = d3.select(element).append("svg")
                .attr("width", width)
                .attr("height", height)
                .append("g")
                .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

            var path = svg.selectAll("path")
                .data(pie(dataset.rating))
                .enter().append("path")
                .attr("fill", function (d, i) {
                    return color(i);
                })
                .attr("d", arc);
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        
    }
}
*/

ko.bindingHandlers.showMultipleChoiceDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var index = bindingContext.$index();
        if (bindingContext.$parent.AnalyticsData().AnalyticsItems().length > 0) {
            var choiceWCounts = bindingContext.$parent.AnalyticsData().AnalyticsItems()[index].ChoiceWCounts();
            var doughnutData = [];
            $.each(choiceWCounts, function (i, choiceWCount) {

                doughnutData.push(parseFloat(choiceWCount.Percentage()));
            });
            var dataset = {
                rating: doughnutData
            };


            var margin = { top: 16, right: 16, bottom: 16, left: 16 };
            var width = $(element).width() - 14,
                    height = $(element).height(),
                    radius = Math.min(width, height) / 2;
            var vBW = 592;
            var vBH = 384;
            if (width > 300) {

            } else {
                margin = { top: 8, right: 8, bottom: 8, left: 8 };
                vBH = 192
                vBW = 288;
            }
            var color = d3.scale.category20().range(CHARTCOLOURS);

            var degree = Math.PI / 180; // just to convert the radian-numbers
            var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

            var arc = d3.svg.arc()
                .innerRadius(radius - 3)
                .outerRadius(radius);

            var svg = d3.select(element).append("svg")
          		.attr("width", '100%')

                .attr('viewBox', '0 0 ' + vBW + ' ' + vBH)
        		.attr('preserveAspectRatio', 'xMinYMin')
        	.append("g")
			    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

            var path = svg.selectAll("path")
                .data(pie(dataset.rating))
                .enter().append("path")
                .attr("fill", function (d, i) {
                    return color(i);
                })
                .attr("d", arc);
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}
ko.bindingHandlers.shareFacebookResultDialog = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var displayTitle = $('<div>').append(bindingContext.$root.Survey().SurveyTitle()).text();
            FB.ui(
            {
                method: 'feed',
                name: displayTitle + ' Results!',
                picture: 'http://cusexperience.com/Images/CusExperienceBox.png',
                link: bindingContext.$root.CurrentURL,
                caption: bindingContext.$root.CurrentURL,
                description: 'Click here to see the ' +  displayTitle + ' results. Take the survey and see how you can influence the results.'
            }, function (response) { });
            if (bindingContext.$root.Survey().IsAllowViewResults)
                bindingContext.$root.loadResultsView();
        });
    },
    update: function (element, valueAccessor) {

    }
};


function ResultViewModel() {
    var self = this;
    self.Survey = ko.observable(new SurveyModel(initJSONData, false, true, false));
    self.CurrentURL = document.URL;
    self.CurrentBaseURL = window.location.protocol + "//" + window.location.host;
}

$(document).ready(function () {
    var resultViewModel = new ResultViewModel();
    ko.applyBindings(resultViewModel, document.getElementById('body-content'));
    $('body').show();
});