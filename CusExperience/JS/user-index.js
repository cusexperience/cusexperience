﻿ko.bindingHandlers.enterKey = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        console.log('hi');
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {

        var originalFunction = valueAccessor();
        $(element).on('input', function (event) {

            var keyCode = (event.which ? event.which : event.keyCode);
            return originalFunction.call(viewModel);
        });
    }
};

ko.bindingHandlers.displayText = {
    init: function (element, valueAccessor) {
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).text($('<div>').append(value).text());
    }
};


ko.bindingHandlers.deleteUser = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.deleteUser(bindingContext.$index(), bindingContext.$data);
        });
    }
}

function AppViewModel() {
    var self = this;
    self.Users = new ListModel(initJSONData);
    var url = document.URL;
    var params = url.split("Users/");
    var id = params[1];

    self.showOrgSub = function (data) {
        return "/OrgSub/Detail/" + id;
    }

    self.showTouchpoint = function () {
        return "/OrgSub/Touchpoint/" + id;
    }

    self.showUsers = function () {
        return "/Users/" + id;
    }

    self.createUser = function (data) {
        return "/User/Create/" + id;
    }

    self.editUser = function (data) {
        return "/User/Detail/" + data.Id;
    }

    self.deleteUser = function (index, data) {
        var r = confirm("Do you want to delete the User?");
        if (r == true) {
            $.ajax({
                url: '/api/DeleteUser/'  + data.Id,
                type: 'DELETE',
                success: function (result) {
                    self.Users.splice(index, 1);
                }, error: function (result) {
                    alert(ko.toJSON(result));
                }
            });
        }
    }
}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('body-content'));
    $('body').show();
});