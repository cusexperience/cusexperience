﻿ko.bindingHandlers.displayText = {
    init: function (element, valueAccessor) {
    },
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).text($('<div>').append(value).text());
    }
};

ko.bindingHandlers.enterKey = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel) {


    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var originalFunction = valueAccessor();
        $(element).on('input', function (event) {
            var keyCode = (event.which ? event.which : event.keyCode);
            return originalFunction.call(viewModel);
        });
    }
};

function AppViewModel() {
    var self = this;
    self.Surveys = new ListModel(initJSONData);
    var item = sessionStorage["authToken"];
    var url = document.URL;
    self.HomeURL = url.substring(0, url.indexOf("/Analytics"));
    self.HomeURL = self.HomeURL.replace("cusexperience", "CusExperience");

    var url = document.URL;
    var params = url.split("Index/");
    var id = params[1];

    self.FullPublishedURL = function (data) {
        console.log(data);
        return ko.computed({
            read: function () {
                var URL = self.HomeURL + "/" + data.PublishedURL;
                if (data.PublishedID !== "") URL = URL + "/" + data.PublishedID;
                return URL;
            }
        })
    }

    self.showSurveyResponses = function (data, event) {
        return "/Analytics/Detail/" + id + "/" + data.SurveyID;
    }

}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('body-content'));
    $('body').show();
});

