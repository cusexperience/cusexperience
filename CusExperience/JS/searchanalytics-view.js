﻿
ko.bindingHandlers.movePreviousSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() - 1;
            if (index <= 0) index = 0;
            $(element).parent().carousel('prev');
        });
    }
}

ko.bindingHandlers.moveNextSlide = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            var index = $(element).parent().find('div.active').index() + 1;
            if (index >= bindingContext.$data.QuestionData().QuestionItems().length) index = 0;
            $(element).parent().carousel('next');
        });
    }
}

ko.bindingHandlers.showDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        
        var OverallRating = bindingContext.$data.AnalyticsData().OverallRating();
        
        //var doughnutData = [{ value: OverallRating, color: "#2f2f2f"},{value: 100 - OverallRating, color: "#DDDDDD" }];
        var dataset = {
            rating: [OverallRating, 100 - OverallRating]
        };
        var width = $(element).width(),
                height = $(element).height(),
                radius = Math.min(width, height) / 2;

        var color = d3.scale.category20().range(RATECHARTCOLOURS);

        var degree = Math.PI / 180; // just to convert the radian-numbers
        var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

        var arc = d3.svg.arc()
            .innerRadius(radius - 17)
            .outerRadius(radius);

        var svg = d3.select(element).append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var path = svg.selectAll("path")
            .data(pie(dataset.rating))
            .enter().append("path")
            .attr("fill", function (d, i) {
                return color(i);
            })
            .attr("d", arc);
       
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
          
    }
}

ko.bindingHandlers.showChoiceDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
      
        var choiceWCounts = bindingContext.$data.AnalyticsData().ChoiceWCounts();
        var doughnutData = [];
        $.each(choiceWCounts, function (i, choiceWCount) {
            var doughnutSegment = { value: parseFloat(choiceWCount.Percentage()), color: CHARTCOLOURS[i] }
            doughnutData.push(doughnutSegment);
        });

        var doughnutDataSort = [];
        $.each(choiceWCounts, function (i, choiceWCount) {
          
            
            doughnutDataSort.push(parseFloat(choiceWCount.Percentage()));
        });
        
        var width = $(element).width(),
                height = $(element).height(),
                radius = Math.min(width, height) / 2;

        var dataset = {
            choiceSelection: doughnutDataSort,
        };
        
        var color = d3.scale.category20().range(CHARTCOLOURS);

        var degree = Math.PI / 180; // just to convert the radian-numbers
        var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

        var arc = d3.svg.arc()
            .innerRadius(radius - 17)
            .outerRadius(radius);

        var svg = d3.select(element).append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var path = svg.selectAll("path")
            .data(pie(dataset.choiceSelection))
          .enter().append("path")
            .attr("fill", function (d, i) { return color(i); })
            .attr("d", arc);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.showMultipleRatingDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        var index = bindingContext.$index();
        if (bindingContext.$parent.AnalyticsData().AnalyticsItems().length > 0 && !$.isEmptyObject(bindingContext.$parent.AnalyticsData().AnalyticsItems()[index])) {
            var OverallRating = bindingContext.$parent.AnalyticsData().AnalyticsItems()[index].OverallRating();

            var dataset = {
                rating: [OverallRating,100-OverallRating]
            };
           
            var width = $(element).width(),
                height = $(element).height(),
                radius = Math.min(width, height) / 2;

            var color = d3.scale.category20().range(RATECHARTCOLOURS);

            var degree = Math.PI / 180; // just to convert the radian-numbers
            var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

            var arc = d3.svg.arc()
                .innerRadius(radius - 17)
                .outerRadius(radius);

            var svg = d3.select(element).append("svg")
                .attr("width", width)
                .attr("height", height)
                .append("g")
                .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

            var path = svg.selectAll("path")
                .data(pie(dataset.rating))
                .enter().append("path")
                .attr("fill", function (d, i) {
                    return color(i);
                })
                .attr("d", arc);
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.showMultipleChoiceDoughnutChart = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        
        var index = bindingContext.$index();
        if (bindingContext.$parent.AnalyticsData().AnalyticsItems().length > 0) {
        var choiceWCounts = bindingContext.$parent.AnalyticsData().AnalyticsItems()[index].ChoiceWCounts();
        var doughnutData = [];
        $.each(choiceWCounts, function (i, choiceWCount) {

            doughnutData.push(parseFloat(choiceWCount.Percentage()));
        }); 
        var dataset = {
            rating: doughnutData
        };

        var width = $(element).width(),
                height = $(element).height(),
                radius = Math.min(width, height) / 2;
        var color = d3.scale.category20().range(CHARTCOLOURS);
        
        var degree = Math.PI / 180; // just to convert the radian-numbers
        var pie = d3.layout.pie().sort(null).startAngle(-90 * degree).endAngle(90 * degree);

        var arc = d3.svg.arc()
            .innerRadius(radius - 17)
            .outerRadius(radius);

        var svg = d3.select(element).append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var path = svg.selectAll("path")
            .data(pie(dataset.rating))
            .enter().append("path")
            .attr("fill", function (d, i) {
                return color(i);
            })
            .attr("d", arc);
        }
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    }
}

ko.bindingHandlers.showComments = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.showComments(bindingContext.$root.Survey().SurveyID(), bindingContext.$parents[1].ModuleID(), bindingContext.$data.Word());
        });
    }
}

ko.bindingHandlers.showSearch = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            $("#searchModal").modal('show');
        });
    }
}

ko.bindingHandlers.addSearchField = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        bindingContext.$root.Search().DGSearch().AddSearchField(bindingContext.$data,bindingContext.$index());
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
};

ko.bindingHandlers.multiSelect = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiSelect);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};


ko.bindingHandlers.multiSelectQuestionType = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiSelect);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);

        $(element).change(function () {
            var selectedQuestionType = $(element).val();
            bindingContext.$root.questionTypeSelected(selectedQuestionType);
        });
    }
};

ko.bindingHandlers.datetimePicker = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var options = ko.utils.unwrapObservable(valueAccessor());
        $(element).datetimepicker({
            format: 'd/m/Y H:i',
            onClose: bindingContext.$root.loadAnalyticsForDateBetween
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    }
};

function AnalyticsViewModel() {

    var self = this;
    self.Survey = ko.observable(new SurveyModel());
    self.PublishedSurvey = ko.observable(new SurveyModel());
    self.Search = ko.observable(new SearchModel());
    self.Comments = ko.observableArray([]);
    var currentdate = new Date();
    self.FromDateTime = "1/1/" + currentdate.getFullYear() + " 00:00";
    var strDatetime = currentdate.getDate() + "/" + (currentdate.getMonth() + 1)
    + "/" + currentdate.getFullYear() + " "
    + currentdate.getHours() + ":"
    + currentdate.getMinutes();
    self.ToDateTime = strDatetime;
    var url = document.URL;
    var params = url.split("SearchAnalyticsView/");
    var id = params[1];
    var urlString = "/api/AnalyticsData/" + id;
    var psUrlString = "/api/PublishedData/" + id;
    var btUrlString = "/api/AnalyticsDataBT/" + id;
    self.selectedQuestionType = ko.observable();
    self.selectedQuestions = ko.observableArray([]);

    self.wordHeight = function (data) {
        var random = Math.floor((Math.random() * 7) + 1);
        if (random == 1) return "grid2-font";
        else if (random == 2) return "grid3-font";
        else if (random == 3) return "grid3-font";
        else if (random == 4) return "grid4-font";
        else if (random == 5) return "grid5-font";
        else if (random == 6) return "grid6-font";
        else "grid7-font";
    };


    self.loadPublishedSurvey = function () {
        $.ajax(psUrlString, {
            type: "GET",
            headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
        }).success(function (allData) {
            self.PublishedSurvey(new SurveyModel(allData));
           // stopSpinner();
        }).fail(function (httpObj, data) {
        });
    }

    self.loadAnalyticsView = function () {
        window.location = "/Report/AnalyticsView/" + id;
    }

    self.loadCustomerView = function () {
        window.location = "/Report/CustomerView/" + id;
    }

    self.loadSearchView = function () {
        window.location = "/Report/SearchAnalyticsView/" + id;
    }

    self.showComments = function (surveyID, moduleID, keyWord) {
        var commentURLString = "/api/CommentData/" + surveyID + "/" + moduleID + "/" + keyWord;
        $.ajax(commentURLString, {
            type: "GET",
            headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
        }).success(function (allData) {
            self.Comments(allData);
            $("#myModal").modal('show');
        }).fail(function (httpObj, data) {
        });
    }

    self.showSearchResults = function () {
        var data = ko.toJSON(self.Search);
        alert(data);
        $.ajax({
            type: "POST",
            data: data,
            url: "/api/SearchAnalyticsData",
            contentType: "application/json"
        }).success(function (allData) {
            self.Survey(new SurveyModel(allData));
        }).fail(function (httpObj, data) {
        }); 
    }

    self.loadAnalytics = function () {
        $.ajax(psUrlString, {
            type: "GET",
            headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
        }).success(function (allData) {
            self.PublishedSurvey(new SurveyModel(allData));
            self.selectedQuestions.removeAll();
            for (var i in self.PublishedSurvey().Modules()) {
                if (self.PublishedSurvey().Modules()[i].ModuleType() === MODULETYPES.QUESTION) {
                    if (self.PublishedSurvey().Modules()[i].ModuleData().QuestionType() != QUESTIONTYPES.DEMOGRAPHIC) {
                        self.selectedQuestions.push({ value: self.PublishedSurvey().Modules()[i].ModuleID , key: self.PublishedSurvey().Modules()[i].ModuleData().QuestionData().QuestionText() }); 
                    }
                }
                self.Search().SurveyID(self.PublishedSurvey().SurveyID());
            }
           
            $.ajax(urlString, {
                type: "GET",
                headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
            }).success(function (allData) {
                self.Survey(new SurveyModel(allData));
            }).fail(function (httpObj, data) {
            });
            //stopSpinner();
        }).fail(function (httpObj, data) {
        });
    }

    function pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    self.loadAnalyticsForToday = function () {

        var fromDateTime = "".concat(pad(currentdate.getDate(), 2), pad((currentdate.getMonth() + 1), 2), currentdate.getFullYear(), "0000");
        var toDateTime = "".concat(pad(currentdate.getDate(), 2), pad((currentdate.getMonth() + 1), 2), currentdate.getFullYear(), pad(currentdate.getHours(), 2), pad(currentdate.getMinutes(), 2));
        self.loadAnalyticsByTime(fromDateTime, toDateTime);
    }

    self.loadAnalyticsForWeek = function () {

        var weekDayBegin = new Date();
        weekDayBegin.setDate(currentdate.getDate() - 7);

        var fromDateTime = "".concat(pad(weekDayBegin.getDate(), 2), pad((weekDayBegin.getMonth() + 1), 2), weekDayBegin.getFullYear(), "0000");
        var toDateTime = "".concat(pad(currentdate.getDate(), 2), pad((currentdate.getMonth() + 1), 2), currentdate.getFullYear(), pad(currentdate.getHours(), 2), pad(currentdate.getMinutes(), 2));
        self.loadAnalyticsByTime(fromDateTime, toDateTime);
    }

    self.loadAnalyticsForMonth = function () {

        var monthDayBegin = new Date();
        monthDayBegin.setDate(currentdate.getMonth() - 1);

        var fromDateTime = "".concat(pad(monthDayBegin.getDate(), 2), pad((monthDayBegin.getMonth() + 1), 2), monthDayBegin.getFullYear() + "0000");
        var toDateTime = "".concat(pad(currentdate.getDate(), 2), pad((currentdate.getMonth() + 1), 2), currentdate.getFullYear(), pad(currentdate.getHours(), 2), pad(currentdate.getMinutes(), 2));
        self.loadAnalyticsByTime(fromDateTime, toDateTime);
    }

    self.loadAnalyticsForYear = function () {

        var yearDayBegin = new Date();
        yearDayBegin.setDate(currentdate.getDate() - 365);

        var fromDateTime = "".concat(pad(yearDayBegin.getDate(), 2), pad((yearDayBegin.getMonth() + 1), 2), yearDayBegin.getFullYear(), "0000");
        var toDateTime = "".concat(pad(currentdate.getDate(), 2), pad((currentdate.getMonth() + 1), 2), currentdate.getFullYear(), pad(currentdate.getHours(), 2), pad(currentdate.getMinutes(), 2));
        self.loadAnalyticsByTime(fromDateTime, toDateTime);
    }

    self.loadAnalyticsForDateBetween = function (dp, $input) {
        var fromDateTimeArray = self.FromDateTime.replace(/[\s:]/g, '').split("/");
        var toDateTimeArray = self.ToDateTime.replace(/[\s:]/g, '').split("/");

        var fromDateTime = "".concat(pad(fromDateTimeArray[0], 2), pad(fromDateTimeArray[1], 2), fromDateTimeArray[2]);
        var toDateTime = "".concat(pad(toDateTimeArray[0], 2), pad(toDateTimeArray[1], 2), toDateTimeArray[2]);
        self.loadAnalyticsByTime(fromDateTime, toDateTime);
    }

    self.loadAnalyticsByTime = function (fromDateTime, toDateTime) {
        var dtUrlString = btUrlString + "/" + fromDateTime + "/" + toDateTime;
        $.ajax(dtUrlString, {
            type: "GET",
            headers: { "Authorization": "Bearer " + sessionStorage.getItem("authToken") },
        }).success(function (allData) {
            self.Survey(new SurveyModel(allData));
        }).fail(function (httpObj, data) {
        });
    }
}

ko.bindingHandlers.toggleDemographicSection = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if ($(element).hasClass("icon-plus")) {
                var surveyResponseDiv = $(element).parent().find('.demographicSection');
                surveyResponseDiv.show('slow');
                $(element).removeClass("icon-plus");
                $(element).addClass("icon-minus");
            } else {
                var surveyResponseDiv = $(element).parent().find('.demographicSection');
                surveyResponseDiv.hide("slow", function () {
                });
                $(element).removeClass("icon-minus");
                $(element).addClass("icon-plus");
            }
        });
    }
}

ko.bindingHandlers.toggleQuestionSection = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if ($(element).hasClass("icon-plus")) {
                var surveyResponseDiv = $(element).parent().find('.questionSection');
                surveyResponseDiv.show('slow');
                $(element).removeClass("icon-plus");
                $(element).addClass("icon-minus");
            } else {
                var surveyResponseDiv = $(element).parent().find('.questionSection');
                surveyResponseDiv.hide("slow", function () {
                });
                $(element).removeClass("icon-minus");
                $(element).addClass("icon-plus");
            }
        });
    }
}

$(document).ready(function () {
    var analyticsModel = new AnalyticsViewModel();
    analyticsModel.loadAnalytics();
    ko.applyBindings(analyticsModel, document.getElementById('analytics-view'));
});
