﻿ko.bindingHandlers.showSearchResults = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            bindingContext.$root.loadFilteredSurveyResponses(element);
        });
    }
};


ko.bindingHandlers.enterKey = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).keypress(function (event) {
            var keyCode = (event.which ? event.which : event.keyCode);
            if (keyCode === 13) {
                bindingContext.$root.loadFilteredSurveyResponses($('#search-analytics-btn')[0]);
            }
            return true;
        });
    }
};

ko.bindingHandlers.reset = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function (event) {
            bindingContext.$root.SearchText('');
            $('#reset-analytics-btn').text("Resetted");
            setTimeout(function () {
                $('#reset-analytics-btn').text("Reset");
            }, 5000);
        });
    }
};

ko.bindingHandlers.toggleSurveyResponse = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if ($(element).find('span').hasClass("icon-plus")) {
                var surveyResponseDiv = $(element).parent().find('.surveyResponse');
                var domElement = surveyResponseDiv.get(0);
                ko.cleanNode(domElement);
                var surveyResponse = new ResponseViewModel();
                surveyResponse.loadSurveyResponse(domElement, bindingContext.$data.SurveyResponseID);
                $(element).find('span').removeClass("icon-plus");
                $(element).find('span').addClass("icon-minus");
            } else {
                var surveyResponseDiv = $(element).parent().find('.surveyResponse');
                surveyResponseDiv.hide();
                surveyResponseDiv.empty();
                $(element).find('span').removeClass("icon-minus");
                $(element).find('span').addClass("icon-plus");
            } 
        });
    }
}

ko.bindingHandlers.selectOrder = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });


    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

        $(element).on('multiselectclick',function(event, ui){ 
            bindingContext.$root.OrderByValue(ui.value);
            bindingContext.$root.loadSurveyResponses();
        });

        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

ko.bindingHandlers.pagination = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).pagination({
            items: bindingContext.$root.Survey().TotalResponses(),
            itemsOnPage: bindingContext.$root.NoofRecords(),
            currentPage: bindingContext.$root.CurrentPage(),
            displayedPages: 3,
            cssStyle: "light-theme",
            onPageClick: bindingContext.$root.loadSurveyResponses
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).pagination({
            items: bindingContext.$root.Survey().TotalResponses(),
            itemsOnPage: bindingContext.$root.NoofRecords(),
            currentPage: bindingContext.$root.CurrentPage(),
            displayedPages: 3,
            cssStyle: "light-theme",
            onPageClick: bindingContext.$root.loadSurveyResponses
        });
    }
};

function CustomerViewModel() {
    var self = this;
    self.Survey = ko.observable(new SurveyModel(initJSONData, false, true, false));
    self.CurrentPage = ko.observable(1);
    self.NoofRecords = ko.observable(10);
    self.OrderByValue = ko.observable('desc');
    self.SearchText = ko.observable('');
    self.AnalyticsTitle = ko.observable(self.Survey().SurveyTitle() + " Analytics");
    var multiplier = 1;
    var url = document.URL;

    self.OrderByOptions = ko.observableArray([
        { key: 'desc', value: 'Latest to Earliest' },
        { key: 'asc', value: 'Earliest to Latest' }
    ]);

    var url = document.URL;
    var params = url.split("CustomerView/");
    var id = params[1];
    var responseUrlString = "/api/GetSurveyResponsesBS/" + id;

    self.downloadSurveyData = function () {
        var d = new Date();
        var SurveyDataURL = "/Analytics/SurveyData/" + id;
        var x = window.open(SurveyDataURL, "Window_" + d.getMilliseconds());
        x.focus();
    }

    self.generateSNo = function (index) {
        return ((self.CurrentPage() - 1) * self.NoofRecords()) + index() + 1;
    }

    self.loadIndexView = function () {
        return "/Analytics/Detail/" + id;
    }

    self.loadCustomerView = function () {
        return "/Analytics/CustomerView/" + id;
    }

    self.loadSocialView = function () {
        return "/Analytics/SocialView/" + id;
    }

    self.loadTrackingView = function () {
        return "/Analytics/TrackingView/" + id;
    }

    self.loadSurveyResponses = function () {
        var currentPage = $("#paginationHolder").pagination('getCurrentPage');
        var queryString = responseUrlString + "?$top=" + self.NoofRecords() + "&$skip=" + ((currentPage - 1) * self.NoofRecords()) + "&$orderby=ResponseDate " + self.OrderByValue();
        if (self.SearchText() != null && self.SearchText() !== "")
            queryString += "&$filter=" + self.SearchText()
        if (cvData == null) {
            $.ajax(queryString, {
                type: "GET",
            }).success(function (allData) {
                self.SurveyResponses(allData);
                self.CurrentPage(currentPage);
                $("#paginationHolder").pagination({
                    items: self.TotalResponses(),
                    itemsOnPage: self.NoofRecords(),
                    currentPage: self.CurrentPage(),
                    displayedPages: 3,
                    cssStyle: "light-theme",
                    onPageClick: self.loadSurveyResponses
                });
            });
        } else {
            $.ajax(queryString, {
                type: "POST",
                data: cvData,
            }).success(function (allData) {
                self.SurveyResponses(allData);
                self.CurrentPage(currentPage);
            });
        }
    }

    self.loadFilteredSurveyResponses = function (element) {

        $(element).attr("disabled", false);
        $(element).removeClass("search-button-fail");
        $(element).removeClass("search-button-success");
        $(element).removeClass('hover');
        $(element).trigger('mouseup');
        $(element).unbind("mouseenter mouseleave");
        $(element).text("Searching...");
        $(element).trigger('mouseup');

        var currentPage = $("#paginationHolder").pagination('getCurrentPage');
        var queryString = responseUrlString + "?$top=" + self.NoofRecords() + "&$skip=" + ((currentPage - 1) * self.NoofRecords()) + "&$orderby=ResponseDate " + self.OrderByValue();
        if (self.SearchText() != null && self.SearchText() !== "")
            queryString += "&$filter=" + self.SearchText()
        if (cvData == null) {
            $.ajax(queryString, {
                type: "GET",
            }).success(function (allData) {
                self.SurveyResponses(allData);
                self.CurrentPage(currentPage);
                if (self.TotalResponses() > 0) {
                    $(element).addClass("search-button-success");
                    $(element).text("Results Found");
                    $(element).attr("disabled", true);

                    $(element).delay(7000).queue(function (n) {
                        $(element).text("Search");
                        $(element).removeClass("search-button-success");
                        $(element).attr("disabled", false);
                        n();
                    });
                } else {
                    $(element).addClass("search-button-fail");
                    $(element).text("No Results Found");
                    $(element).attr("disabled", true);
                    $(element).delay(7000).queue(function (n) {
                        $(element).text("Search");
                        $(element).removeClass("search-button-fail");
                        $(element).attr("disabled", false);
                        n();
                    });
                }
            }).fail(function (data) {
                // alert(ko.toJSON(data));
                $(element).addClass("search-button-fail");
                $(element).text("Search Failed");
                $(element).delay(7000).queue(function (n) {
                    $(element).text("Search");
                    $(element).removeClass("search-button-fail");
                    n();
                });
            });
        } else {
            $.ajax(queryString, {
                type: "POST",
                data: cvData,
            }).success(function (allData) {
                self.SurveyResponses(allData);
                self.CurrentPage(currentPage);
                if (self.TotalResponses() > 0) {
                    $(element).addClass("search-button-success");
                    $(element).text("Results Found");
                    $(element).attr("disabled", true);

                    $(element).delay(7000).queue(function (n) {
                        $(element).text("Search");
                        $(element).removeClass("search-button-success");
                        $(element).attr("disabled", false);
                        n();
                    });
                } else {
                    $(element).addClass("search-button-fail");
                    $(element).text("No Results Found");
                    $(element).attr("disabled", true);
                    $(element).delay(7000).queue(function (n) {
                        $(element).text("Search");
                        $(element).removeClass("search-button-fail");
                        $(element).attr("disabled", false);
                        n();
                    });
                }
            }).fail(function (data) {
                // alert(ko.toJSON(data));
                $(element).addClass("search-button-fail");
                $(element).text("Search Failed");
                $(element).delay(7000).queue(function (n) {
                    $(element).text("Search");
                    $(element).removeClass("search-button-fail");
                    n();
                });
            });
        }
    }
}

$(document).ready(function () {
    var customerViewModel = new CustomerViewModel();
    ko.applyBindings(customerViewModel, document.getElementById('body-content'));
    $('body').show();
});