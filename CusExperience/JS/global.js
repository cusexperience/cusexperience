﻿function logoutSession() {
    var r = confirm("Are you sure you want to log out? All unsaved progress will be lost.");
    if (r == true) {
        $.ajax({
            type: "POST",
            url: '/api/Account/Logout',
            contentType: 'application/json',
            dataType: 'json'
        }).success(function () {
            window.location = "/Account/Login";
        }).fail(function () {
            alert("Logout failed. Please report this issue to us by clicking at the ? tab");
        });
    }
}

function showSupportView() {
    $("#supportModal").modal('toggle');
    if ($('#supportModal').hasClass('in')) {
        document.body.style.overflow = "hidden";
    } else {
        document.body.style.overflow = "";
    }
}