﻿function UserModel(data) {
    var self = this;
    self.Id = ko.observable(data != null ? data.Id : null);
    self.UserName = ko.observable(data != null ? data.UserName : null);
    self.FirstName = ko.observable(data != null ? data.FirstName : null);
    self.LastName = ko.observable(data != null ? data.LastName : null);
    self.Title = ko.observable(data != null ? data.Title : null);
    self.OthersTitle = ko.observable(data != null ? data.OthersTitle : null);
    self.Country = ko.observable(data != null ? data.Country : null);
    self.PhoneNumber = ko.observable(data != null ? data.PhoneNumber : null);
    self.Designation = ko.observable(data != null ? data.Designation : null);
    self.Email = ko.observable(data != null ? data.Email : null);
    self.Address = ko.observable(data != null ? data.Address : null);
    self.OrgSubID = ko.observable(data != null ? data.OrgSub.OrgSubID : null);
    self.OrgSubName = ko.observable(data != null ? data.OrgSub.RegisteredName : null);
    self.AvailableRoles = ko.observableArray(data != null && !$.isEmptyObject(data.AvailableRoles) ? data.AvailableRoles : []);
    self.Role = ko.observable(data != null ? data.Role : null);
    self.SelectedTouchpoints = ko.observableArray(data != null ? data.SelectedTouchpoints : null);
    self.AvailableTouchpoints = ko.observableArray(data != null && !$.isEmptyObject(data.AvailableTouchpoints) ? data.AvailableTouchpoints : []);
}
