﻿/************************************* Global Variables *****************************************/
var multiselectOffset=0;

if ($(window).width()>=1024){
    multiselectOffset = 48;
} else if ($(window).width() >= 320) {
    multiselectOffset = 24;
}
var NUMBERS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
var NUMBERSDESC = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];

var MULTISELECTSPACING = 0;

var UNSET = "Unset";
var CHARTCOLOURS = ["#82CA9C", "#F49AC1", "#fdc689", "#839cCA", "#c4df9b", "#A186be", "#f69679", "#6dcff6", "#a3d39c", "#bd8cbf", "#f9ad81", "#7da7d9", "#75b58c", "#db8aad", "#e3b27b", "#758cb5", "#b0c88b", "#9078ab", "#dd876c", "#62badd", "#92bd8c", "#aa7eab", "#e09b74", "#7096c3", "#68a17c", "#c37b9a", "#ca9e6d", "#687ca1", "#9cb27c", "#806b98", "#c47860", "#57a5c4", "#82a87c", "#977098", "#c78a67", "#6485ad", "#5b8d6d", "#aa6b87", "#b18a5f", "#5b6d8d", "#899c6c", "#705d85", "#ac6954", "#4c90ac", "#72936d", "#846285", "#ae795a", "#577497", "#4e795d", "#925c73", "#977652", "#4e5d79", "#75855d", "#605072", "#935a48", "#417c93", "#617e5d", "#715472", "#95674d", "#4b6482", "#41654e", "#7a4d60", "#7e6344", "#414e65", "#626f4d", "#50435f", "#7b4b3c", "#36677b", "#51694e", "#5e465f", "#7c5640", "#3e536c", "#34503e", "#613d4d", "#654f36", "#343e50", "#4e593e", "#40354c", "#623c30", "#2b5262", "#41543e", "#4b384c", "#634533", "#324256", "#492e39", "#4b3b29", "#272e3c", "#3a422e", "#302839", "#492d24", "#203e49", "#303f2e", "#382a39", "#4a3326", "#253241"];
var RATECHARTCOLOURS = ["#82CA9C", "#e8e8e8"];
var SUBMITTEXT = "Submit";
var MODULETYPES = {
     TOUCHPOINT: "Touchpoint", BRANDING: "Branding", APPRECIATION: "Appreciation", WELCOME: "Welcome", QUESTION: "Question", AUTHENTICATION: "Authentication", SUBMIT: "Submit", THANKS: 'Thanks'
}
var MODULETYPEVALUES = $.map(MODULETYPES, function (value, index) {
    return [value];
});

var DAYS = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
var TIME = ['12:00 AM', '01:00 AM', '02:00 AM', '03:00 AM', '04:00 AM', '05:00 AM', '06:00 AM', '07:00 AM', '08:00 AM', '09:00 AM', '10:00 AM', '11:00 AM', '12:00 PM', '01:00 PM', '02:00 PM', '03:00 PM', '04:00 PM', '05:00 PM', '06:00 PM', '07:00 PM', '08:00 PM', '09:00 PM', '10:00 PM', '11:00 PM'];
var ROLES = ['Administrator', 'Executive', 'CusXP Professional', 'Manager', 'FrontLiner'];
var VALIDATIONTYPES = ['None','Email', 'Number', 'Mobile'];

var STAFFTEXT = "Which Staff did you interact with?";
var HIGHEST = "Highest";
var LOWEST = "Lowest";
var DEMOMSG = "Please fill in the following demographics details for better sensing.";
var THANKSMSG = "Thank you for taking your time to complete this.";
var THANKSDESC = "Your effort is appreciated!";
var QUESTIONTYPES = {
    RATE: "Rate", TAG: "Tag", TEXT: "Text", CHOICE: "Choice", MULTIPLE: "Multiple", ATTACH: "Attach", RANK: "Rank", SLIDE: "Slide", PICTURE: "Picture",
     PSYCHOMETRIC: "Psychometric", LOCATION: "Location", SEGMENT: "Segment",
     STAFF: "Staff", SOCIAL: "Social", DEMOGRAPHIC: "Demographic"
}
var QUESTIONTYPEVALUES = $.map(QUESTIONTYPES, function (value, index) {
    return [value];
});

var RATETYPES = { NUMBER: "Number", HEART: "Heart", STAR: "Star", COMMENT: "Comment", DAISIES: "Daisy" };
var RATETYPEVALUES = $.map(RATETYPES, function (value, index) {
    return [value];
});

var MULTIPLETYPES = { NUMBER: "Number", HEART: "Heart", STAR: "Star", COMMENT: "Comment", SINGLECHOICE: "Single", MULTIPLECHOICE: "Multiple", DAISIES: "Daisy"};
var MULTIPLETYPEVALUES = $.map(MULTIPLETYPES, function (value, index) {
    return [value];
});

var PSYCHOMETRICS = { SINGLECHOICE: 'Single', MULTIPLECHOICE: 'Multiple' };
var PSYCHOMETRICVALUES = $.map(PSYCHOMETRICS, function (value, index) {
    return [value];
});
var PSYCHOMETRICLIST = ["1.<br/>I am the rational, orderly type. I am principled, purposeful, self-controlled and perfectionistic.", "2.<br/>I am the helpful, interpersonal type. I am generous, appreciative and people-pleasing.", "3.<br/>I am the adaptable, ambitious type. I am focused, excelling, driven and image-conscious.", "4.<br/>I am the introspective, artistic type. I like to deliver distinctive products and personalised services with refinement and a sense of style.", "5.<br/>I am the perceptive, curious type. I am innovative, a tireless learner and endless experimenter.", "6.<br/>I am the engaging loyal type. I am likeable, responsible and diligent. I build alliances and partnerships that help get things done.", "7.<br/>I am the upbeat, spontaneous, versatile, impulsive and happy type. I thrive on change, variety and excitement.", "8.<br/>I am the powerful, decisive and self-confident type. I have a clear vision of what I want to accomplish and have the will power to make it happen.", "9.<br/>I am the easygoing, accommodating type. I am receptive, reassuring and agreeable."];
var PSYCHOMETRICQUESTIONTEXT = "Which of the following best describes you? (So we can serve you even better.)";
var PSYCHOMETRICDESCRIPTIONTEXT = "You can only select one option.";

var AUTHENTICATIONDESCRIPTIONTEXT = "Please verify that you are human.";

var LOCATIONDESCRIPTIONTEXT = "Check in.";

var CHOICES = { SINGLECHOICE: 'Single', MULTIPLECHOICE: 'Multiple' };
var CHOICEVALUES = $.map(CHOICES, function (value, index) {
    return [value];
});

var DEMOGRAPHICFIELDTYPES = { TEXTSINGLELINE: 'Single Line Textbox', TEXTMULTILINE: 'Multi Line Textbox', SELECTONE: 'Single Option Dropdown', SELECTMULTIPLE: 'Multiple Options Dropdown'};
var DEMOGRAPHICFIELDVALUES = $.map(DEMOGRAPHICFIELDTYPES, function (value, index) {
    return [value];
});


var INDUSTRIES = ["Accommodation, Food & Beverage", "Agriculture, Forestry & Fishing", "Arts, Entertainment & Recreation", "Construction",
                  "Education", "Energy, Electricity & Gas", "Finance & Insurance", "Information & Communication",
                  "Government & Public Administration", "Healthcare & Social Activities", "Manufacturing", "Mining", "Professional, Scientific & Technical Services",
                  "Real Estate", "Transportation, Logistics & Storage", "Water, Sewerage & Waste Management", "Wholesale & Retail", "Others"];

var COUNTRIES = ["Singapore", "Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda",
    "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", 
    "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory",
    "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands",
    "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo",
    "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic",
    "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia",
    "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia",
    "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe",
    "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", 
    "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", 
    "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", 
    "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", 
    "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", 
    "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", 
    "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island",
    "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn",
    "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia",
    "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone",
    "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", 
    "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden",
    "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau",
    "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", 
    "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", 
    "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"];


var SALUTATIONS = ["Mr.", "Mrs.", "Ms.", "Dr.", "Prof.", "Others"];

var defaultTags = [{
    TagName: "People",
    TagTooltip: "Tag \"People\" if what you said relates to an improvement in the people dimension."
}, {
    TagName: "Product",
    TagTooltip: "Tag \"Product\" if what you said relates to an improvement in the product dimension."
}, {
    TagName: "Process",
    TagTooltip: "Tag \"Process\" If what you said relates to the process dimension."
}, {
    TagName: "Place",
    TagTooltip: "Tag \"Place\" if what you said relates to the place dimension."
}, {
    TagName: "Philosophy",
    TagTooltip: "Tag \"Philosophy\" if what you said relates to the philosophy dimension."
}, {
    TagName: "Others",
    TagTooltip: "Tag \"Other\" if what you said relates to something other than the people, product, process, place and philosophy dimension."
}];

var DEFAULTTERMSCONDITIONS = "1. This complimentary voucher is only valid at (Add organisation's name here).<br/> " +
                             "2. This voucher is given to pre-selected individuals and organization upon full and accurate completion of this survey.<br />" +
                             "3. This voucher is non-transferable, cannot be sold or resold and cannot be redeemed for cash or other equivalents.<br />" +
                             "4. This voucher can only be used with a purchase of (Add the condition here).<br />" +
                             "5. This voucher is for one-time use only, its use need to be indicated to and approved by (Add organisation's name here) before any initiative agreement is drawn up.<br />" +
                             "6. This voucher cannot be used in conjunction with any other vouchers, offers, discounts, promotion, privileges, etc...<br />" +
                             "7. (Add organisation's name here) reserves the right to modify the above Terms & Conditions, reject and discontinues this offer at anytime, without and prior notice.";

/***** Demographic Field Defaults ***********/

var PERSONALDGFIELDS = {
      "Salutations": {
        FieldName: "Salutations",
        FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
        IsRequired: false,
        SelectValues: ["Mr.", "Mrs.", "Ms.", "Dr.", "Prof.", "Others"]
      },
      "Given Names": {
          FieldName: "Given Names",
          FieldType: DEMOGRAPHICFIELDTYPES.TEXTSINGLELINE,
          IsRequired: false,
          SelectValues: null,
          DescriptionText: 'Your Given Names <br/> eg. Amber Lily'
      },
      "Surname": {
          FieldName: "Surname",
          FieldType: DEMOGRAPHICFIELDTYPES.TEXTSINGLELINE,
          IsRequired: false,
          SelectValues: null,
          DescriptionText: 'Your Surname <br/> eg. Tan'
      },
      "Country": {
          FieldName: "Country",
          FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
          Required: true,
          SelectValues: ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"],
          DescriptionText: 'Your Country'
      },
      "Mobile": {
          FieldName: "Mobile",
          FieldType: DEMOGRAPHICFIELDTYPES.TEXTSINGLELINE,
          IsRequired: false,
          SelectValues: null,
          DescriptionText: 'Your Mobile number inclusive of Country and Area Codes <br/> eg. +65 9876 5432'
      },
      "Email": {
          FieldName: "Your Email Address",
          FieldType: DEMOGRAPHICFIELDTYPES.TEXTSINGLELINE,
          IsRequired: false,
          SelectValues: null,
          DescriptionText: 'Your Email address <br/> eg. Amber.Tan@CusExperience.com'
      },
      "Gender" :{
          FieldName: "Gender",
          FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
          IsRequired: false,
          SelectValues: ["Male", "Female"],
          DescriptionText:'Your Gender'
      },
      "Age": {
          FieldName: "Age",
          FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
          IsRequired: false,
          SelectValues: ["15 and below", "16 - 20", "21- 25", "26 - 30", "31 - 35", "36 - 40", "41 - 45", "46 - 50", "51 - 55",
          "56 - 60", "61 - 65", "66 and above."],
          DescriptionText:'Your Age'
      },
      "Race": {
          FieldName: "Race",
          FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
          IsRequired: false,
          SelectValues: ["Chinese", "Malay", "Indian", "Eurasian", "Others"],
          DescriptionText: 'Your Race'
      },
      "Education": {
          FieldName: "Education",
          FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
          IsRequired: false,
          SelectValues: ["None", "Primary", "Secondary", "Vocational Certificate", "Diploma", "Degree", "Masters", "Doctorate", "Others"]
      },
      "Work Status": {
          FieldName: "Work Status",
          FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
          IsRequired: false,
          SelectValues: ["Citizen", "New Citizen", "Permanent Resident", "Employment Pass", "Skills Pass", "Work Pass", "Others"],
          DescriptionText: 'Your Status in the Country'
      },
      "Relationship Status" : {
          FieldName: "Relationship Status",
          FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
          IsRequired: false,
          SelectValues: ["Single","Partner","Married","Separated","Divorced","Widowed"]
      },
      "Number of Children": {
          FieldName: "Number of Children",
          FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
          IsRequired: true,
          SelectValues: ["Not Applicable", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11 and more"],
          DescriptionText: 'Your Number of Children'
      }, 
      "Age Group of Children": {
          FieldName: "Age Group of Children",
          FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
          IsRequired: true,
          SelectValues: ["Not Applicable", "Below 12", "13 - 21", "above 21."],
          DescriptionText: 'Average Age of Children <br/> eg. If you have 3 children and they are 7, 12 and 18 years old each. Then <br/> your average age of children would be ( 7 + 12 + 18 = 37 / 3 = 12.3) <br/> between 13 - 21.'
      }, 
      "Type Of Housing": {
          FieldName: "Type Of Housing",
          FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
          IsRequired: true,
          SelectValues: ["HDB 1-Room", "HDB 2-Room", "HDB 3-Room", "HDB 4-Room", "HDB 5-Room", "HDB Executive Flat", "Condominium / Private Apartment",
                         "Landed Property", "Others"],
          DescriptionText: 'Your Housing type'
      }
};

var PERSONALDGFIELDKEYS = $.map(PERSONALDGFIELDS, function (value, key) {
    return [key];
});

var ORGANISATIONALDGFIELDS = {
    "Industry" : {
        FieldName: "Industry",
        FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
        IsRequired: false,
        SelectValues: ["Accommodation, Food & Beverage", "Agriculture, Forestry & Fishing", "Arts, Entertainment & Recreation", "Construction",
        "Education", "Energy, Electricity & Gas", "Finance & Insurance", "Information & Communication",
        "Government & Public Administration", "Healthcare & Social Activities", "Manufacturing", "Mining", "Professional, Scientific & Technical Services",
        "Real Estate", "Transportation, Logistics & Storage", "Water, Sewerage & Waste Management", "Wholesale & Retail", "Others"],
         DescriptionText: 'Your Industry'
    },
    "Organization Size": {
        FieldName: "Organisation Size",
        FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
        IsRequired: false,
        SelectValues: ["Below 25", "26 - 100", "101 - 500", "501 - 1000", "Above 1001"],
        DescriptionText: 'Your Organisation Size <br/> eg. Approximate number of full and part time staff in your organisation'
    },
    "Name of the Organisation": {
        FieldName: "Name of the Organisation",
        FieldType: DEMOGRAPHICFIELDTYPES.TEXTSINGLELINE,
        IsRequired: false,
        SelectValues: null,
        DescriptionText: 'Your Organisation Name <br/> eg. CusExperience Pte Ltd.'
    },
    "Department": {
        FieldName: "Department",
        FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
        IsRequired: false,
        SelectValues: ["Accounting, Finance & similar.", "Business Development, Marketing, Sales  & similar. Research, Development, Innovation & similar.", "Chairperson, Board of Director, Chief Trustees, Council, Executive Committees, Secretariat & similar.", "Corporate Communication, Investor Relations  & similar.", "Customer Experience, Customer Service, Quality Assurance, Quality Control  & similar.", "Human Resources, Human Capital & similar.", "Operations, Logistics, Warehousing, Purchasing, Procurement  & similar.",
       "Strategy, Governance, Compliance, Audit  & similar.", "Technology, Engineering, Systems & similar."],
        DescriptionText: 'Your Department <br/> Please choose just one option that is most appropriate.'
    },
    "Employment Status": {
        FieldName: "Employment Status",
        FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
        Required: false,
        SelectValues: ["Employee", "Employer", "Self-Employed", "Unemployed", "Military", "Homemaker", "Student",
        "Retiree", "Unable To Work", "Others"]
    },
    "Role": {
        FieldName: "Role",
        FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
        IsRequired: false,
        SelectValues: ["Upper Management", "Middle Management", "Junior Management", "Administrative & Support Staff", "Professional, Technical Expert or Researcher", "Intern or Temporary Staff", "Others"]
    },
    "Annual Income":{
        FieldName: "Annual Income (SGD)",
        FieldType: DEMOGRAPHICFIELDTYPES.SELECTONE,
        IsRequired: false,
        SelectValues: ["Below 12,001", "12,001 - 24,000", "24,001 - 36,000", "36,001 - 48,000", "48,001 - 60,000", "60,001 - 72,000",
                     "72,001 - 84,000", "84,001 - 96,000", "96,001 - 108,000", "108,001 - 120,000", "120,001 - 240,000", "above 240,000."]
    }
}

var ORGANISATIONALDGFIELDKEYS = $.map(ORGANISATIONALDGFIELDS, function (value, key) {
    return [key];
});
