﻿function ListModel(data,options) {
    var self = this;
    self.Items = ko.observableArray(data != null && !$.isEmptyObject(data) ? data : []);
    self.ItemsPerPage = ko.observable(self.Items().length);
    self.CurrentPage = ko.observable(1);
    self.ItemsCount = ko.observable(data != null && !$.isEmptyObject(data) ? data.length : 0);
    self.ItemData = ko.observableArray([]);
    self.SearchText = ko.observable($('#search-input').val());
    self.CurrentItems = ko.observableArray(data != null && !$.isEmptyObject(data) ? self.Items().slice(0, self.ItemsPerPage()) : []);
    self.search = function () {
        if (!self.SearchText()) {
            self.CurrentItems(self.Items());  
        } else {
            self.CurrentItems(ko.utils.arrayFilter(self.Items(), function (item) {
                return item.SurveyTitle.toLowerCase().toLowerCase().indexOf(self.SearchText().toLowerCase()) !== -1;
            }));
        }
    };
    self.searchUsers = function () {
        if (!self.SearchText()) {
            self.CurrentItems(self.Items());
        } else {
            self.CurrentItems(ko.utils.arrayFilter(self.Items(), function (item) {
                return item.UserName.toLowerCase().indexOf(self.SearchText().toLowerCase()) !== -1;
            }));
        }
    };
    self.sortFunction = null;
    self.pageClick = function () {
       var currentPage = $("#paginationHolder").pagination('getCurrentPage');
       self.CurrentPage(currentPage);
       var currentPageStart = (self.CurrentPage() - 1) * self.ItemsPerPage();
       self.CurrentItems(self.Items().slice(currentPageStart, currentPageStart + self.ItemsPerPage()));
    };
}