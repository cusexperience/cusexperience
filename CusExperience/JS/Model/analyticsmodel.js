﻿/**********************************************Analytics Module Models*********************************************/


function WordCloudModel(data) {
    var self = this;
    self.Word = ko.observable(data != null ? data.Word : null);
    self.Frequency = ko.observable(data != null ? data.Frequency : null);
}

function StatisticsModel(data) {
    var self = this;
    self.Text = ko.observable(data != null ? data.Text : null);
    self.Count = ko.observable(data != null ? data.Count : 0);
    self.Score = ko.observable(data != null ? data.Score : 0);
    self.IsSearchKeyword = ko.observable(data != null && typeof (data.IsSearchKeyword) !== 'undefined' ? data.IsSearchKeyword : false);
}

function RateAnalyticsModel(data) {
    var self = this;
    self.FromRating = ko.observable(data != null ? data.FromRating : null);
    self.ToRating = ko.observable(data != null ? data.ToRating : null);
    self.OverallRating = ko.observable(data != null ? data.OverallRating : null);
    self.ResponseCount = ko.observable(data != null ? data.ResponseCount : null);
    self.NotFound = ko.observable(data != null ? data.NotFound : false);
    self.ChoiceWCounts = ko.observableArray(data != null && !$.isEmptyObject(data.ChoiceWCounts) ? $.map(data.ChoiceWCounts, function (item) { return new ChoiceWCountModel(item) }) : []);
    self.WordCloud = ko.observableArray(data != null && !$.isEmptyObject(data.WordCloud) ? $.map(data.WordCloud, function (item) { return new WordCloudModel(item) }) : []);
    self.NoofVisible = ko.observable(data != null && typeof(data.NoofVisible) !== 'undefined' ? data.NoofVisible : 10);
}

function TagCountModel(data) {
    var self = this;
    self.TagName = ko.observable(data != null ? data.TagName : null);
    self.Count = ko.observable(data != null ? data.Count : null);
}

function TagAnalyticsModel(data) {
    var self = this;
    self.WordCloud = ko.observableArray(data != null && !$.isEmptyObject(data.WordCloud) ? $.map(data.WordCloud, function (item) { return new WordCloudModel(item) }) : []);
    self.TagCount = ko.observableArray(data != null && !$.isEmptyObject(data.TagCount) ? $.map(data.TagCount, function (item) { return new TagCountModel(item) }) : []);
    self.NotFound = ko.observable(data != null ? data.NotFound : false);
    self.IsSearchView = ko.observable(data != null ? data.IsSearchView : false);
    self.NoofVisible = ko.observable(data != null && typeof(data.NoofVisible) !== 'undefined' ? data.NoofVisible : 10);
}

function ChoiceWCountModel(data) {
    var self = this;
    self.ChoiceText = ko.observable(data != null ? data.ChoiceText : null);
    self.Count = ko.observable(data != null ? data.Count : null);
    self.Percentage = ko.observable(data != null ? data.Percentage : null);
}

function ChoiceAnalyticsModel(data, choicetype) {
    var self = this;
    self.ChoiceWCounts = ko.observableArray(data != null && !$.isEmptyObject(data.ChoiceWCounts) ? $.map(data.ChoiceWCounts, function (item) { return new ChoiceWCountModel(item) }) : []);
    self.NotFound = ko.observable(data != null ? data.NotFound : false);
}
function PsychometricWCountModel(data) {
    var self = this;
    self.PsychometricText = ko.observable(data != null ? data.PsychometricText : null);
    self.Count = ko.observable(data != null ? data.Count : null);
    self.Percentage = ko.observable(data != null ? data.Percentage : null);
}
function PsychometricAnalyticsModel(data, psychometrictype) {
    var self = this;
    self.PsychometricWCounts = ko.observableArray(data != null && !$.isEmptyObject(data.PsychometricWCounts) ? $.map(data.PsychometricWCounts, function (item) { return new PsychometricWCountModel(item) }) : []);
    self.NotFound = ko.observable(data != null ? data.NotFound : false);
}

function RankAnalyticsModel(data) {
    var self = this;
}

function SlideAnalyticsModel(data) {
    var self = this;
}

function PictureAnalyticsModel(data) {
    var self = this;
}

function STAFFAnalyticsModel(data) {
    var self = this;
    self.Statistics = ko.observableArray(data != null && !$.isEmptyObject(data.Statistics) ? $.map(data.Statistics, function (item) { return new StatisticsModel(item) }) : [])
    self.NotFound = ko.observable(data != null ? data.NotFound : false);
    self.NoofVisibleEV = ko.observable(data != null && typeof (data.NoofVisibleEV) !== 'undefined' ? data.NoofVisibleEV : 10);
    self.NoofVisibleFV = ko.observable(data != null && typeof(data.NoofVisibleFV) !== 'undefined' ? data.NoofVisibleFV : 10);
    self.OrderByValueEV = ko.observable(data != null && typeof(data.OrderByValueEV) !== 'undefined' ? data.OrderByValueEV : 'Highest to Lowest');
    self.OrderByValueFV = ko.observable(data != null && typeof (data.OrderByValueFV) !== 'undefined' ? data.OrderByValueFV : 'Highest to Lowest');
    self.CurrentView = ko.observable(data != null && typeof (data.CurrentView) !== 'undefined' ? data.CurrentView : 'exp');
}

function AttachmentAnalyticsModel(data) {
    var self = this;
    self.NotFound = ko.observable(data != null ? data.NotFound : false);
    self.AllAttachments = ko.observableArray(data != null && !$.isEmptyObject(data.AllAttachments) ? $.map(data.AllAttachments, function (item) { return new AttachmentModel(item) }) : []);
    self.NoofVisible = ko.observable(data != null && typeof (data.NoofVisible) !== 'undefined' ? data.NoofVisible : 12);
}

function TextAnalyticsModel(data) {
    var self = this;
    self.WordCloud = ko.observableArray(data != null && !$.isEmptyObject(data.WordCloud) ? $.map(data.WordCloud, function (item) { return new WordCloudModel(item) }) : []);
    self.NotFound = ko.observable(data != null ? data.NotFound : false);
    self.IsSearchView = ko.observable(data != null ? data.IsSearchView : false);
    self.NoofVisible = ko.observable(data != null && typeof (data.NoofVisible) !== 'undefined' ? data.NoofVisible : 10);
}

function AnalyticsItem(data, rating, comment) {
    var self = this;
    self.FromRating = ko.observable(data != null ? data.FromRating : null);
    self.ToRating = ko.observable(data != null ? data.ToRating : null);
    self.OverallRating = ko.observable(data != null ? data.OverallRating : 0);
    self.ResponseCount = ko.observable(data != null ? data.ResponseCount : 0);
    self.ChoiceWCounts = ko.observableArray(data != null && !$.isEmptyObject(data.ChoiceWCounts) ? $.map(data.ChoiceWCounts, function (item) { return new ChoiceWCountModel(item) }) : []);
    self.NotFound = ko.observable(data != null ? data.NotFound : false);
}

function MultipleAnalyticsModel(data) {
    var self = this;
    self.AnalyticsItems = ko.observableArray(data != null ? $.map(data.AnalyticsItems, function (item) { return new AnalyticsItem(item) }) : []);
}

/*
function PsychometricAnalyticsModel(data) {
    var self = this;

}
*/

function LocationInfoModel( value, key) {
    var self = this;
    self.ResponseID = ko.observable(key != null && typeof (key) !== 'undefined' ? key : null);
    self.Latitude = ko.observable(value != null && typeof (value[0]) !== 'undefined' ? value[0] : 0);
    self.Longitude = ko.observable(value != null && typeof (value[1]) !== 'undefined' ? value[1] : 0);
}

function LocationAnalyticsModel(data) {
    var self = this;
    self.LocationInfo = ko.observableArray(data != null ? $.map(JSON.parse(data.LocationInfo), function (value, key) { return new LocationInfoModel(value, key) }) : []);
    self.NotFound = ko.observable(data != null ? data.NotFound : false);
}

function SegmentAnalyticsModel(data) {
    var self = this;

}

function SocialAnalyticsModel(data) {
    var self = this;
}

function DGStatisticsModel(data) {
    var self = this;
    self.DGQuestionField = ko.observable(data != null && typeof (data.DGQuestionField) !== 'undefined' ? data.DGQuestionField : null);
    self.Statistics = ko.observableArray(data != null && !$.isEmptyObject(data.Statistics) ? $.map(data.Statistics, function (item) { return new StatisticsModel(item) }) : []);
    self.NoofVisibleEV = ko.observable(data != null && typeof (data.NoofVisibleEV) !== 'undefined' ? data.NoofVisibleEV : 10);
    self.NoofVisibleFV = ko.observable(data != null && typeof (data.NoofVisibleFV) !== 'undefined' ? data.NoofVisibleFV : 10);
    self.OrderByValueEV = ko.observable(data != null && typeof (data.OrderByValueEV) !== 'undefined' ? data.OrderByValueEV : 'Highest to Lowest');
    self.OrderByValueFV = ko.observable(data != null && typeof (data.OrderByValueFV) !== 'undefined' ? data.OrderByValueFV : 'Highest to Lowest');
    self.CurrentView = ko.observable(data != null && typeof (data.CurrentView) !== 'undefined' ? data.CurrentView : 'exp');
    self.NotFound = ko.observable(data != null ? data.NotFound : false);
}

function DemographicAnalyticsModel(data) {
    var self = this;
    self.DGStatistics = ko.observableArray(data != null && !$.isEmptyObject(data.DGStatistics) ? $.map(data.DGStatistics, function (item) { return new DGStatisticsModel(item) }) : []);
}

function CommentModel(key,value) {
    var self = this;
    self.SurveyResponseID = value;
    self.CommentText = key;
}

function OrgSubAnalyticsModel(data) {
    var self = this;
    self.Statistics = ko.observableArray(data != null && !$.isEmptyObject(data.Statistics) ? $.map(data.Statistics, function (item) { return new StatisticsModel(item) }) : []);
    self.HStatistics = ko.observableArray(data != null && !$.isEmptyObject(data.HStatistics) ? $.map(data.HStatistics, function (item) { return new StatisticsModel(item) }) : []);
    self.NoofVisibleEV = ko.observable(data != null && typeof (data.NoofVisibleEV) !== 'undefined' ? data.NoofVisibleEV : 10);
    self.NoofVisibleFV = ko.observable(data != null && typeof (data.NoofVisibleFV) !== 'undefined' ? data.NoofVisibleFV : 10);
    self.NoofVisibleHV = ko.observable(data != null && typeof (data.NoofVisibleHV) !== 'undefined' ? data.NoofVisibleHV : 10);
    self.OrderByValueHV = ko.observable(data != null && typeof (data.OrderByValueHV) !== 'undefined' ? data.OrderByValueHV : 'Highest to Lowest');
    self.OrderByValueEV = ko.observable(data != null && typeof (data.OrderByValueEV) !== 'undefined' ? data.OrderByValueEV : 'Highest to Lowest');
    self.OrderByValueFV = ko.observable(data != null && typeof (data.OrderByValueFV) !== 'undefined' ? data.OrderByValueFV : 'Highest to Lowest');
    self.CurrentView = ko.observable(data != null && typeof (data.CurrentView) !== 'undefined' ? data.CurrentView : 'exp');
}

function TouchpointAnalyticsModel(data) {
    var self = this;
    self.Statistics = ko.observableArray(data != null && !$.isEmptyObject(data.Statistics) ? $.map(data.Statistics, function (item) { return new StatisticsModel(item) }) : []);
    self.NoofVisibleEV = ko.observable(data != null && typeof (data.NoofVisibleEV) !== 'undefined' ? data.NoofVisibleEV : 10);
    self.NoofVisibleFV = ko.observable(data != null && typeof (data.NoofVisibleFV) !== 'undefined' ? data.NoofVisibleFV : 10);
    self.OrderByValueEV = ko.observable(data != null && typeof (data.OrderByValueEV) !== 'undefined' ? data.OrderByValueEV : 'Highest to Lowest');
    self.OrderByValueFV = ko.observable(data != null && typeof (data.OrderByValueFV) !== 'undefined' ? data.OrderByValueFV : 'Highest to Lowest');
    self.CurrentView = ko.observable(data != null && typeof (data.CurrentView) !== 'undefined' ? data.CurrentView : 'exp');
}