﻿/**********************************************Question Module Models********************************************/

function RateQuestionModel(data) {
    var self = this;
    self.QuestionText = ko.observable(data != null ? data.QuestionText : null);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
    self.LowestRateText = ko.observable(data != null ? data.LowestRateText : LOWEST);
    self.HighestRateText = ko.observable(data != null ? data.HighestRateText : HIGHEST);
    self.RateType = ko.observable(data != null ? data.RateType : RATETYPES.NUMBER);
    self.NumLimit = ko.observable(data != null ? data.NumLimit : 10);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : false);
    self.IsCommentable = ko.observable(data != null ? data.IsCommentable : false);
    self.NumArray = ko.computed(function () {
        var nArray = [];
        for (num = 0; num <= self.NumLimit() ; num++) {
            nArray.push(num);
        }
        return nArray;
    });
}

function TextQuestionModel(data) {
    var self = this;
    self.QuestionText = ko.observable(data != null ? data.QuestionText : null);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : null);
}

function TagModel(data) {
    var self = this;
    self.TagName = ko.observable(data != null ? data.TagName : null);
    self.TagTooltip = ko.observable(data != null ? data.TagTooltip : null);
}

function TagQuestionModel(data) {
    var self = this;
    self.QuestionText = ko.observable(data != null ? data.QuestionText : null);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : false);
    self.Tags = ko.observableArray(data != null ? $.map(data.Tags, function (item) { return new TagModel(item) }) : $.map(defaultTags, function (item) { return new TagModel(item) }));

    self.TagSelected = ko.observable(new TagModel());

    self.TagAddClick = function () {
        self.Tags.push(new TagModel());
    }

    self.TagRemoveClick = function () {
        self.Tags.remove(this);
    }

    self.selectedTag = function () {
        self.TagSelected(this);
    }
}

function STAFFQuestionModel(data, STAFFentrytype) {
    var self = this;
    self.QuestionText = ko.observable(data != null ? data.QuestionText : STAFFTEXT);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
    self.STAFFType = ko.observable(data != null ? data.STAFFType : STAFFentrytype);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : false);
    self.IsCommentable = ko.observable(data != null ? data.IsCommentable : false);
}

function ChoiceModel(data) {
    var self = this;
    self.ChoiceText = ko.observable(data != null ? data.ChoiceText : null);
}

function ChoiceQuestionModel(data, choicetype) {

    var self = this;
    self.QuestionText = ko.observable(data != null ? data.QuestionText : null);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
    self.Choices = ko.observableArray(data != null ? $.map(data.Choices, function (item) { return new ChoiceModel(item) }) : [new ChoiceModel(null, CHOICES.SINGLECHOICE)]);
    self.ChoiceType = ko.observable(data != null ? data.ChoiceType : choicetype);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : false);
    self.IsCommentable = ko.observable(data != null ? data.IsCommentable : false);

    self.ChoiceAddClick = function () {
        self.Choices.push(new ChoiceModel(null, self.ChoiceType));
    }

    self.ChoiceRemoveClick = function (data) {
        self.Choices.remove(data);
    }
}

function PsychometricModel(data) {
    var self = this;
    self.PsychometricText = ko.observable(data != null ? data.PsychometricText : null);
}

function PsychometricQuestionModel(data, psychometrictype) {

    var self = this;
    self.QuestionText = ko.observable(data != null ? data.QuestionText : PSYCHOMETRICQUESTIONTEXT);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : PSYCHOMETRICDESCRIPTIONTEXT);
    var itemList = [];
    for(var i =0; i< PSYCHOMETRICLIST.length; i++){
        itemList.push(new PsychometricModel({ PsychometricText: PSYCHOMETRICLIST[i] }, PSYCHOMETRICS.SINGLECHOICE));
    }
    self.Psychometrics = ko.observableArray(data != null ? $.map(data.Psychometrics, function (item) { return new PsychometricModel(item) }) : itemList);
    console.log(self.Psychometrics());
    self.PsychometricType = ko.observable(data != null ? data.PsychometricType : psychometrictype);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : false);
    self.IsCommentable = ko.observable(data != null ? data.IsCommentable : false);

    self.PsychometricAddClick = function () {
        self.Psychometrics.push(new PsychometricModel(null, self.PsychometricType));
    }

    self.PsychometricRemoveClick = function (data) {
        self.Psychometrics.remove(data);
    }
}



function RankQuestionModel(data) {
    var self = this;
}

function SlideQuestionModel(data) {
    var self = this;
}

function PictureQuestionModel(data) {
    var self = this;
    self.ImageURL = ko.observable(data != null ? data.ImageURL : null);
    self.IsFlipImage = ko.observable(data.IsFlipImage);
}

function QuestionItemModel(data) {
    var self = this;
    self.QuestionItemText = ko.observable(data != null ? data.QuestionItemText : null);
}

function MultipleQuestionModel(data) {
    var self = this;
    self.QuestionText = ko.observable(data != null ? data.QuestionText : null);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : false);
    self.IsCommentable = ko.observable(data != null ? data.IsCommentable : false);
    self.QuestionItems = ko.observableArray(data != null ? $.map(data.QuestionItems, function (item) { return new QuestionItemModel(item) }) : [new QuestionItemModel(null)]);
    self.LowestRateText = ko.observable(data != null ? data.LowestRateText : LOWEST);
    self.HighestRateText = ko.observable(data != null ? data.HighestRateText : HIGHEST);
    self.MultipleType = ko.observable(data != null ? data.MultipleType : MULTIPLETYPES.NUMBER);
    self.NumLimit = ko.observable(data != null ? data.NumLimit : 10);
    self.CurrentIndex = ko.observable(0);
    self.Choices = ko.observableArray(data != null ? $.map(data.Choices, function (item) { return new ChoiceModel(item) }) : [new ChoiceModel(null, MULTIPLETYPES.SINGLECHOICE)]);
    //self.Psychometrics = ko.observableArray(data != null ? $.map(data.Psychometrics, function (item) { return new PsychometricModel(item) }) : [new PsychometricModel(null, MULTIPLETYPES.SINGLECHOICE)]);

    self.NumArray = ko.computed(function () {
        var nArray = [];
        for (num = 0; num <= self.NumLimit() ; num++) {
            nArray.push(num);
        }
        return nArray;
    });

    self.QuestionItemAddClick = function () {
        self.QuestionItems.push(new QuestionItemModel(null, self.ChoiceType));
    }

    self.QuestionItemRemoveClick = function (data) {
        self.QuestionItems.remove(data);
    }

    self.ChoiceAddClick = function () {
        self.Choices.push(new ChoiceModel(null, self.ChoiceType));
    }

    self.ChoiceRemoveClick = function (data) {
        self.Choices.remove(data);
    }
}

function AttachmentQuestionModel(data) {
    var self = this;  
    self.QuestionText = ko.observable(data != null && typeof (data.QuestionText) !== 'undefined' ? data.QuestionText : 'Would you like to attach anything?');
    self.DescriptionText = ko.observable(data != null && typeof (data.DescriptionText) !== 'undefined' ? data.DescriptionText : null);
    self.IsRequired = ko.observable(data != null && typeof (data.IsRequired) !== 'undefined' ? data.IsRequired : false);
    self.IsImageAllowed = ko.observable(data != null && typeof (data.IsImageAllowed) !== 'undefined' ? data.IsImageAllowed : true);
    self.IsVideoAllowed = ko.observable(data != null && typeof (data.IsVideoAllowed) !== 'undefined' ? data.IsVideoAllowed : true);
    self.IsPdfAllowed = ko.observable(data != null && typeof (data.IsPdfAllowed) !== 'undefined' ? data.IsPdfAllowed : true);
    self.IsOffDocsAllowed = ko.observable(data != null && typeof (data.IsOffDocsAllowed) !== 'undefined' ? data.IsOffDocsAllowed : true);
}
/*
function PsychometricQuestionModel(data) {
    var self = this;
}*/

function LocationQuestionModel(data) {
    var self = this;
    self.QuestionText = ko.observable(data != null ? data.QuestionText : LOCATIONDESCRIPTIONTEXT);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : false);
}

function SegmentQuestionModel(data) {
    var self = this;

}

function SocialQuestionModel(data) {
    var self = this;
    self.QuestionText = ko.observable(data != null ? data.QuestionText : null);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : false);
}

function SelectValueModel(data) {
    var self = this;
    self = ko.observable(data != null ? data : null);
}

function DGQuestionFieldModel(data,fieldtype) {
    var self = this;
    self.FieldName = ko.observable(data != null ? data.FieldName : null);
    self.FieldType = ko.observable(data != null ? data.FieldType : fieldtype);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
    self.DefaultValue = ko.observable(data != null && data.DefaultValue !== 'undefined' ? data.DefaultValue : null);
    self.DefaultValues = ko.observableArray(data != null && !$.isEmptyObject(data.DefaultValues) ? data.DefaultValues : []);
    self.SelectValues = ko.observableArray(data != null && !$.isEmptyObject(data.SelectValues) ? $.map(data.SelectValues, function (item) { return item }) : []);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : false);
    self.Validation = ko.observable(data != null && typeof (data.Validation) !== 'undefined' ? data.Validation : null);
    self.IsCommentable = ko.observable(data != null && typeof (data.IsCommentable) !== 'undefined' ? data.IsCommentable : null);
    self.CommentName = ko.observable(data != null ? data.CommentName : null);
    self.IsAnonymized = ko.observable(data != null && typeof (data.IsAnonymized) !== 'undefined' ? data.IsAnonymized : false);
    self.HideInSearch = ko.observable(data != null && typeof (data.HideInSearch) !== 'undefined' ? data.HideInSearch : false);

    self.SelectValueAddClick = function () {
        self.SelectValues.push(null);
    }

    self.SelectValueRemoveClick = function (data) {
        self.SelectValues.remove(data);
    }
}

function DemographicQuestionModel(data) {
    var self = this;
    self.QuestionText = ko.observable(data != null ? data.QuestionText : DEMOMSG);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
    self.DGQuestionFields = ko.observableArray(data != null ? $.map(data.DGQuestionFields, function (item) { return new DGQuestionFieldModel(item) }) : [new DGQuestionFieldModel(null,UNSET)]);
    self.SelectedQuestionType = ko.observable(DEMOGRAPHICFIELDTYPES.TEXTSINGLELINE);
    self.IsRequired = ko.observable(data != null ? data.IsRequired : null);


    self.DGFieldChangeFieldType = function (fieldtype, index) {
        var questionFieldData = PERSONALDGFIELDS[fieldtype];
        if (questionFieldData == null) questionFieldData = ORGANISATIONALDGFIELDS[fieldtype];
        var changedDGField = questionFieldData != null ? new DGQuestionFieldModel(questionFieldData, fieldtype) : new DGQuestionFieldModel(null, fieldtype);
        self.DGQuestionFields.splice(index, 1, changedDGField);
    }

    self.DGFieldAdd = function () {
        self.DGQuestionFields.push(new DGQuestionFieldModel(null, UNSET));
    }

    self.DGFieldRemoveClick = function (data) {
        self.DGQuestionFields.remove(data);

    }

    /* Adding a new module between the survey */
    self.DGFieldAddBetweenClick = function (index, fieldtype) {
        var questionFieldData = PERSONALDGFIELDS[fieldtype];
        if (questionFieldData == null) questionFieldData = ORGANISATIONALDGFIELDS[fieldtype];
        var addedModule = new DGQuestionFieldModel(questionFieldData, fieldtype);
        self.DGQuestionFields.splice(index, 0, addedModule);
    }.bind(self);
}