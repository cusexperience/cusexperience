﻿/**********************************************Answer Module Models*********************************************/

function RateAnswerModel(data) {
    var self = this;
    self.Rating = ko.observable(data !=null ? data.Rating : null);
    self.CommentText = ko.observable(data != null ? data.CommentText : null);

    self.Validate = function () {
        if (self.Rating() == null)
            return false;
        return true;
    }
}

function TagAnswerModel(data) {
    var self = this;
    self.CommentText = ko.observable(data != null ? data.CommentText : null);
    self.SelectedTags = ko.observableArray(data != null ? data.SelectedTags : []);

    self.ToggleSelectedTag = function (index) {
       if ($.inArray(index, self.SelectedTags()) > -1) {
            self.SelectedTags.remove(index);
        }
        else {
            self.SelectedTags.push(index);
        } 
    }

    self.Validate = function () {
        if (self.CommentText() == null || self.CommentText === "")
            return false;
        return true;
    }
}

function ChoiceAnswerModel(data, choicetype) {
    var self = this;
    self.CommentText = ko.observable(data != null ? data.CommentText : null);
    self.ChoosenOptions = ko.observableArray(data != null ? data.ChoosenOptions : []);
    self.ToggleSelectedOption = function (index) {
        if ($.inArray(index, self.ChoosenOptions()) > -1) {
            self.ChoosenOptions.remove(index);
        }
        else {
            self.ChoosenOptions.push(index);
        }
    }

    self.AddSelectedOption = function (index) {
        self.ChoosenOptions([]);
        self.ChoosenOptions.push(index);
    }

    self.Validate = function () {
        if(self.ChoosenOptions() == null || $.isEmptyObject(self.ChoosenOptions())){
            return false;
        }
        return true;
    }
}

function PsychometricAnswerModel(data, psychometrictype) {
    var self = this;
    self.CommentText = ko.observable(data != null ? data.CommentText : null);
    self.ChoosenOptions = ko.observableArray(data != null ? data.ChoosenOptions : []);
    self.ToggleSelectedOption = function (index) {
        if ($.inArray(index, self.ChoosenOptions()) > -1) {
            self.ChoosenOptions.remove(index);
        }
        else {
            self.ChoosenOptions.push(index);
        }
    }

    self.AddSelectedOption = function (index) {
        self.ChoosenOptions([]);
        self.ChoosenOptions.push(index);
    }

    self.Validate = function () {
        if (self.ChoosenOptions() == null || $.isEmptyObject(self.ChoosenOptions())) {
            return false;
        }
        return true;
    }
}

function RankAnswerModel(data) {
    var self = this;
}

function SlideAnswerModel(data) {
    var self = this;
}

function PictureAnswerModel(data) {
    var self = this;
}

function TextAnswerModel(data) {
    var self = this;
    self.CommentText = ko.observable(data != null ? data.CommentText : null);

    self.Validate = function () {
        if (self.CommentText() == null || self.CommentText() === "")
            return false;
        return true;
    }
}

function STAFFEntryModel(data) {
    var self = this;
    self.STAFFEntryText = ko.observable(data != null ? data.STAFFEntryText : null);
}

function STAFFAnswerModel(data) {
    var self = this;
    self.STAFFEntry = ko.observableArray(data != null ? $.map(data.STAFFEntry, function (item) { return new STAFFEntryModel(item) }) : [new STAFFEntryModel(null), new STAFFEntryModel(null), new STAFFEntryModel(null)]);

    self.STAFFEntryAddClick = function () {
        self.STAFFEntry.push(new STAFFEntryModel(null));
    }

    self.STAFFEntryRemoveClick = function (data) {
        self.STAFFEntry.remove(data);
    }

    self.Validate = function () {
        if (self.STAFFAnswer() == null || self.STAFFAnswer() === "")
            return false;
        return true;
    }

}

function AnswerItem(data, rating, comment) {
    var self = this;
    self.Rating = ko.observable(data != null ? data.Rating : null);
    self.CommentText = ko.observable(data != null ? data.CommentText : null);

    self.ChoosenOptions = ko.observableArray([]);

    self.ToggleSelectedOption = function (index) {
        if ($.inArray(index, self.ChoosenOptions()) > -1) {
            self.ChoosenOptions.remove(index);
        }
        else {
            self.ChoosenOptions.push(index);
        }
    }

    self.AddSelectedOption = function (index) {
        self.ChoosenOptions([]);
        self.ChoosenOptions.push(index);
    }
}

function MultipleAnswerModel(data) {
    var self = this;
    self.AnswerItems = ko.observableArray(data != null ? data.AnswerItems : []);

    self.AddAnswerItem = function (index) {
        self.AnswerItems()[index] = new AnswerItem(null);
    }

    self.SetAnswerItem = function (index, rating) {
        self.AnswerItems()[index].Rating(rating);
    }

    self.Validate = function (type) {
        if (type == MULTIPLETYPES.NUMBER || type == MULTIPLETYPES.HEART || type == MULTIPLETYPES.STAR || type == MULTIPLETYPES.DAISIES) {
            for (var answerItemIndex in self.AnswerItems()) {
                var AnswerItem = self.AnswerItems()[answerItemIndex];
                if (AnswerItem.Rating() == null)
                         return false;
            }
            return true;
        } else if (type == MULTIPLETYPES.SINGLECHOICE || type == MULTIPLETYPES.MULTIPLECHOICE) {
            for (var answerItemIndex in self.AnswerItems()) {    
                var AnswerItem = self.AnswerItems()[answerItemIndex];
                if (AnswerItem.ChoosenOptions() == null || $.isEmptyObject(AnswerItem.ChoosenOptions())) {
                    return false;
                }     
            }
            return true;
        }
    }
}

function AttachmentModel(data) {
    var self = this;
    self.FileName = ko.observable(data != null ? data.FileName : null);
    self.FileType = ko.observable(data != null ? data.FileType : null);
    self.FilePath = ko.observable(data != null ? data.FilePath : null);
    self.CommentText = ko.observable(data != null ? data.CommentText : null);
}

function AttachmentAnswerModel(data) {
    var self = this;
    self.Attachments = ko.observableArray(data != null && !$.isEmptyObject(data.Attachments) ? $.map(data.Attachments, function(item) { return new AttachmentModel(item); }) : []);

    self.addAttachment = function (data) {
        self.Attachments.push(new AttachmentModel(data));
    }

    self.removeAttachment = function (data) {
        self.Attachments.remove(data);
    }
}
/*
function PsychometricAnswerModel(data) {
    var self = this;

}*/

function LocationAnswerModel(data) {
    var self = this;
    self.Latitude = ko.observable(data != null  && !$.isEmptyObject(data.Latitude) ? data.Latitude : null);
    self.Longitude = ko.observable(data != null && !$.isEmptyObject(data.Longitude) ? data.Longitude : null);
}

function SegmentAnswerModel(data) {
    var self = this;

}

function DGAnswerFieldModel(data, fieldtype , defaultvalue, defaultvalues) {
    var self = this;
    self.TextValue = ko.observable(data != null ? data.TextValue : fieldtype == DEMOGRAPHICFIELDTYPES.TEXTSINGLELINE || fieldtype == DEMOGRAPHICFIELDTYPES.TEXTMULTILINE ? defaultvalue : null);
    self.FieldValues = ko.observableArray(data != null && !$.isEmptyObject(data.FieldValues) ? data.FieldValues : fieldtype == DEMOGRAPHICFIELDTYPES.SELECTONE || fieldtype == DEMOGRAPHICFIELDTYPES.SELECTMULTIPLE ? defaultvalues : ["_NOTSELECTED"]);
    self.CommentText = ko.observable(data != null ? data.CommentText : null);
}

function DemographicAnswerModel(data) {
    var self = this;
    self.DGAnswerFields = ko.observableArray(data != null && !$.isEmptyObject(data.DGAnswerFields) ? data.DGAnswerFields : []);

    self.AddAnswerField = function (index, fieldtype, defaultvalue, defaultvalues) {
       self.DGAnswerFields()[index] = new DGAnswerFieldModel(null, fieldtype, defaultvalue, defaultvalues);
    }

    self.Validate = function (questionData) {
        for (var answerIndex in self.DGAnswerFields()) {
            if (questionData.DGQuestionFields()[answerIndex].IsRequired() == true) {
                if (questionData.DGQuestionFields()[answerIndex].FieldType() == DEMOGRAPHICFIELDTYPES.TEXTSINGLELINE || 
                    questionData.DGQuestionFields()[answerIndex].FieldType() == DEMOGRAPHICFIELDTYPES.TEXTMULTILINE) {
                    if (self.DGAnswerFields()[answerIndex].TextValue() == null || self.DGAnswerFields()[answerIndex].TextValue() === "") {
                        return false;
                    }
                }
                if (questionData.DGQuestionFields()[answerIndex].FieldType() == DEMOGRAPHICFIELDTYPES.SELECTONE) {
                    if (self.DGAnswerFields()[answerIndex].FieldValues() == null || $.isEmptyObject(self.DGAnswerFields()[answerIndex].FieldValues())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
