﻿
/**********************************************Search Module Models*********************************************/

function RateSearchModel(data) {
    var self = this;
    self.FromRating = ko.observable(data != null ? data.FromRating : null);
    self.ToRating = ko.observable(data != null ? data.ToRating : null);
    self.KeyWords = ko.observable(data != null ? data.CommentText : null);
}


function TagSearchModel(data) {
    var self = this;
    self.Keywords = ko.observableArray(data != null ? data.Keywords : null);
    self.SelectedTags = ko.observableArray(data != null ? data.SelectedTags : []);
    self.IsNoTagsSelected = ko.observable(data != null ? data.IsNoTagsSelected : true);
    self.ToggleSelectedTag = function (index) {
        if ($.inArray(index, self.SelectedTags()) > -1) {
            self.SelectedTags.remove(index);
        }
        else {
            self.SelectedTags.push(index);
        }
    }
}

function ChoiceSearchModel(data, choicetype) {
    var self = this;
    self.Keywords = ko.observableArray(data != null ? data.CommentText : null);
    self.ChoosenOptions = ko.observableArray(data != null ? data.ChoosenOptions : []);
    self.ToggleSelectedOption = function (index) {
        if ($.inArray(index, self.ChoosenOptions()) > -1) {
            self.ChoosenOptions.remove(index);
        }
        else {
            self.ChoosenOptions.push(index);
        }
    }

    self.AddSelectedOption = function (index) {
        self.ChoosenOptions([]);
        self.ChoosenOptions.push(index);
    }
}

function PsychometricSearchModel(data, psychometrictype) {
    var self = this;
    self.Keywords = ko.observableArray(data != null ? data.CommentText : null);
    self.ChoosenOptions = ko.observableArray(data != null ? data.ChoosenOptions : []);
    self.ToggleSelectedOption = function (index) {
        if ($.inArray(index, self.ChoosenOptions()) > -1) {
            self.ChoosenOptions.remove(index);
        }
        else {
            self.ChoosenOptions.push(index);
        }
    }

    self.AddSelectedOption = function (index) {
        self.ChoosenOptions([]);
        self.ChoosenOptions.push(index);
    }
}

function RankSearchModel(data) {
    var self = this;
}

function SlideSearchModel(data) {
    var self = this;
}

function PictureSearchModel(data) {
    var self = this;
}

function TextSearchModel(data) {
    var self = this;
    self.Keywords = ko.observable(data != null ? data.Keywords : null);
    self.WOKeywords = ko.observable(data != null ? data.WOKeywords : null);
}


function SearchItem(data, rating, comment) {
    var self = this;
    self.Rating = ko.observable(data != null ? data.Rating : null);
    self.CommentText = ko.observable(data != null ? data.CommentText : null);
    self.ChoosenOptions = ko.observableArray([]);
}

function MultipleSearchModel(data) {
    var self = this;

    self.FromRating = ko.observable(data != null ? data.FromRating : null);
    self.ToRating = ko.observable(data != null ? data.ToRating : null);

    self.SearchItems = ko.observableArray(data != null ? data.SearchItems : []);
    self.ChoosenOptions = ko.observableArray(data != null ? data.ChoosenOptions : []);


    self.ToggleSelectedOption = function (index) {
        if ($.inArray(index, self.ChoosenOptions()) > -1) {
            self.ChoosenOptions.remove(index);
        }
        else {
            self.ChoosenOptions.push(index);
        }
    }

    self.AddSelectedOption = function (index) {
        self.ChoosenOptions([]);
        self.ChoosenOptions.push(index);
    }
}

function AttachmentSearchModel(data) {
    var self = this;
    self.Attachments = ko.observableArray(data != null ? data.Attachments : []);
}
/*
function PsychometricSearchModel(data) {
    var self = this;

}*/

function LocationSearchModel(data) {
    var self = this;

}

function SegmentSearchModel(data) {
    var self = this;

}

function SocialSearchModel(data) {
    var self = this;


}

function STAFFSearchModel(data) {
    var self = this;
    self.STAFFEntry = ko.observableArray(data != null ? $.map(data.STAFFEntry, function (item) { return new STAFFEntryModel(item) }) : [new STAFFEntryModel(null), new STAFFEntryModel(null), new STAFFEntryModel(null)]);

    self.STAFFEntryAddClick = function () {
        self.STAFFEntry.push(new STAFFEntryModel(null));
    }

    self.STAFFEntryRemoveClick = function (data) {
        self.STAFFEntry.remove(data);
    }

    self.Validate = function () {
        if (self.STAFFAnswer() == null || self.STAFFAnswer() === "")
            return false;
        return true;
    }

}

function DGSearchFieldModel(data) {
    var self = this;
    self.FieldName = ko.observable(data != null ? data.FieldName : null);
    self.FieldType = ko.observable(data != null ? data.FieldType : DEMOGRAPHICFIELDTYPES.TEXTSINGLELINE);
    self.FieldValues = ko.observableArray(data != null && typeof (data.SelectValues) !== 'undefined' && !$.isEmptyObject(data.SelectValues()) ? $.merge(['All'], data.SelectValues()) : []);
}

function DemographicSearchModel(data) {
    var self = this;
    self.DGSearchFields = ko.observableArray(data != null ? $.map(data.DGSearchFields, function (item) { return new DGSearchFieldModel(item) }) : []);
    self.Keywords = ko.observable(data != null ? data.Keywords : null);
    self.WOKeywords = ko.observable(data != null ? data.WOKeywords : null);

    self.AddSearchField = function (data, index) {
        self.DGSearchFields()[index] = new DGSearchFieldModel(data);
    }
}