﻿

var dateFormat = function () {
    var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d: d,
                dd: pad(d),
                ddd: dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m: m + 1,
                mm: pad(m + 1),
                mmm: dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy: String(y).slice(2),
                yyyy: y,
                h: H % 12 || 12,
                hh: pad(H % 12 || 12),
                H: H,
                HH: pad(H),
                M: M,
                MM: pad(M),
                s: s,
                ss: pad(s),
                l: pad(L, 3),
                L: pad(L > 99 ? Math.round(L / 10) : L),
                t: H < 12 ? "a" : "p",
                tt: H < 12 ? "am" : "pm",
                T: H < 12 ? "A" : "P",
                TT: H < 12 ? "AM" : "PM",
                Z: utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o: (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S: ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Some common format strings
dateFormat.masks = {
    "default": "ddd mmm dd yyyy HH:MM:ss",
    shortDate: "m/d/yy",
    mediumDate: "mmm d, yyyy",
    longDate: "mmmm d, yyyy",
    fullDate: "dddd, mmmm d, yyyy",
    shortTime: "h:MM TT",
    mediumTime: "h:MM:ss TT",
    longTime: "h:MM:ss TT Z",
    isoDate: "yyyy-mm-dd",
    isoTime: "HH:MM:ss",
    isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};


Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};


/**********************************************Module Models********************************************/

function QuestionModuleModel(data, questiontype, answer, analytics, search) {
    var self = this;
    self.QuestionType = ko.observable(data != null ? data.QuestionType : questiontype);

    switch (self.QuestionType()) {
        case QUESTIONTYPES.RATE:
            self.QuestionData = ko.observable(new RateQuestionModel(data != null ? data.QuestionData : null));
            if(answer)    self.AnswerData = ko.observable(new RateAnswerModel(data != null ? data.AnswerData : null));
            if(analytics) self.AnalyticsData = ko.observable(new RateAnalyticsModel(data != null ? data.AnalyticsData : null));
            if(search)    self.SearchData = ko.observable(new RateSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.TAG:
            self.QuestionData = ko.observable(new TagQuestionModel(data != null ? data.QuestionData : null));
            if (answer) self.AnswerData = ko.observable(new TagAnswerModel(data != null ? data.AnswerData : null));
            if(analytics) self.AnalyticsData = ko.observable(new TagAnalyticsModel(data != null ? data.AnalyticsData : null));
            if(search)    self.SearchData = ko.observable(new TagSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.TEXT:
            self.QuestionData = ko.observable(new TextQuestionModel(data != null ? data.QuestionData : null));  
            if(answer)    self.AnswerData = ko.observable(new TextAnswerModel(data != null ? data.AnswerData : null));
            if(analytics) self.AnalyticsData = ko.observable(new TextAnalyticsModel(data != null ? data.AnalyticsData : null));
            if(search)    self.SearchData = ko.observable(new TextSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.CHOICE:
            self.QuestionData = ko.observable(new ChoiceQuestionModel(data != null ? data.QuestionData : null));
            if(answer)     self.AnswerData = ko.observable(new ChoiceAnswerModel(data != null ? data.AnswerData : null));
            if(analytics)  self.AnalyticsData = ko.observable(new ChoiceAnalyticsModel(data != null ? data.AnalyticsData : null));
            if(search)     self.SearchData = ko.observable(new ChoiceSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.PSYCHOMETRIC:
            self.QuestionData = ko.observable(new PsychometricQuestionModel(data != null ? data.QuestionData : null));
            
            if (answer) self.AnswerData = ko.observable(new PsychometricAnswerModel(data != null ? data.AnswerData : null));
            if (analytics) self.AnalyticsData = ko.observable(new PsychometricAnalyticsModel(data != null ? data.AnalyticsData : null));
            if (search) self.SearchData = ko.observable(new PsychometricSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.RANK:
            self.QuestionData = ko.observable(new RankQuestionModel(data != null ? data.QuestionData : null));
            if(answer)    self.AnswerData = ko.observable(new RankAnswerModel(data != null ? data.AnswerData : null));
            if(analytics) self.AnalyticsData = ko.observable(new RankAnalyticsModel(data != null ? data.AnalyticsData : null));
            if(search)    self.SearchData = ko.observable(new RankSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.SLIDE:
            self.QuestionData = ko.observable(new SlideQuestionModel(data != null ? data.QuestionData : null));
            if (answer)    self.AnswerData = ko.observable(new SlideAnswerModel(data != null ? data.AnswerData : null));
            if (analytics) self.AnalyticsData = ko.observable(new SlideAnalyticsModel(data != null ? data.AnalyticsData : null));
            if (search)    self.SearchData = ko.observable(new SlideSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.PICTURE:
            self.QuestionData = ko.observable(new PictureQuestionModel(data != null ? data.QuestionData : null));
            if (answer)    self.AnswerData = ko.observable(new PictureAnswerModel(data != null ? data.AnswerData : null));
            if (analytics) self.AnalyticsData = ko.observable(new PictureAnalyticsModel(data != null ? data.AnalyticsData : null));
            if (search)    self.SearchData = ko.observable(new PictureSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.MULTIPLE:
            self.QuestionData = ko.observable(new MultipleQuestionModel(data != null ? data.QuestionData : null));
            if (answer)    self.AnswerData = ko.observable(new MultipleAnswerModel(data != null ? data.AnswerData : null));
            if (analytics) self.AnalyticsData = ko.observable(new MultipleAnalyticsModel(data != null ? data.AnalyticsData : null));
            if (search)    self.SearchData = ko.observable(new MultipleSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.ATTACH:
            self.QuestionData = ko.observable(new AttachmentQuestionModel(data != null ? data.QuestionData : null));
            if (answer)    self.AnswerData = ko.observable(new AttachmentAnswerModel(data != null ? data.AnswerData : null));
            if (analytics) self.AnalyticsData = ko.observable(new AttachmentAnalyticsModel(data != null ? data.AnalyticsData : null));
            if (search)    self.SearchData = ko.observable(new AttachmentSearchModel(data != null ? data.SearchData : null));
            break;
       
        case QUESTIONTYPES.LOCATION:
            self.QuestionData = ko.observable(new LocationQuestionModel(data != null ? data.QuestionData : null));
            if (answer)    self.AnswerData = ko.observable(new LocationAnswerModel(data != null ? data.AnswerData : null));
            if (analytics) self.AnalyticsData = ko.observable(new LocationAnalyticsModel(data != null ? data.AnalyticsData : null));
            if (search)    self.SearchData = ko.observable(new LocationSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.SEGMENT:
            self.QuestionData = ko.observable(new SegmentQuestionModel(data != null ? data.QuestionData : null));
            if (answer)    self.AnswerData = ko.observable(new SegmentAnswerModel(data != null ? data.AnswerData : null));
            if (analytics) self.AnalyticsData = ko.observable(new SegmentAnalyticsModel(data != null ? data.AnalyticsData : null));
            if (search)    self.SearchData = ko.observable(new SegmentSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.STAFF:
            self.QuestionData = ko.observable(new STAFFQuestionModel(data != null ? data.QuestionData : null));
            if (answer) self.AnswerData = ko.observable(new STAFFAnswerModel(data != null ? data.AnswerData : null));
            if (analytics) self.AnalyticsData = ko.observable(new STAFFAnalyticsModel(data != null ? data.AnalyticsData : null));
            if (search) self.SearchData = ko.observable(new STAFFSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.SOCIAL:
            self.QuestionData = ko.observable(new SocialQuestionModel(data != null ? data.QuestionData : null));
            if (answer)    self.AnswerData = ko.observable(new SocialAnswerModel(data != null ? data.AnswerData : null));
            if (analytics) self.AnalyticsData = ko.observable(new SocialAnalyticsModel(data != null ? data.AnalyticsData : null));
            if (search)    self.SearchData = ko.observable(new SocialSearchModel(data != null ? data.SearchData : null));
            break;
        case QUESTIONTYPES.DEMOGRAPHIC:
            self.QuestionData = ko.observable(new DemographicQuestionModel(data != null ? data.QuestionData : null));
            if (answer)    self.AnswerData = ko.observable(new DemographicAnswerModel(data != null ? data.AnswerData : null));
            if (analytics) self.AnalyticsData = ko.observable(new DemographicAnalyticsModel(data != null ? data.AnalyticsData : null));
            if (search) self.SearchData = ko.observable(new DemographicSearchModel(data != null ? data.SearchData : null));
            break;
    }
}

function WelcomeModuleModel(data) {
    var self = this;
    self.WelcomeText = ko.observable(data != null ? data.WelcomeText : null);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
}

function LanguageModuleModel(data) {
    var self = this;
    if (data != null) {
    } else {

    }
}

function AppreciationModuleModel(data) {
    var self = this;
    var multiplier = 1;
    
    today = new Date();
    today.setMonth(today.getMonth() + 12);
    var dateString = today.format("dd/m/yyyy");
    
    self.AppreciationText = ko.observable(data != null ? data.AppreciationText : "USD ?? / ?? % Discount Voucher");
    self.FrontImage = ko.observable(data != null ? data.FrontImage : null);
    self.LeftPos = ko.observable(data != null ? data.LeftPos : null);
    self.TopPos = ko.observable(data != null ? data.TopPos : null);
    self.SliderValue = ko.observable(data != null ? data.SliderValue : 50);
    self.TermsConditions = ko.observable(data != null ? data.TermsConditions : DEFAULTTERMSCONDITIONS);
    self.Expires = ko.observable(data != null ? data.Expires : dateString);
    self.ImageWidth = ko.computed(function () {
        if ($(window).width() >= 640) {
            multiplier = 12.8;
        } else if ($(window).width() >= 320) {
            multiplier = 6.4;
        }
        return self.SliderValue() * multiplier;
    }, self);

    self.LogoImage = ko.observable(data != null ? data.LogoImage : null);

    self.removeFrontImage = function () {
        self.FrontImage(null);
        self.SliderValue(50);
        self.LeftPos('0px');
        self.TopPos('0px');
    }

    self.removeLogoImage = function () {
        self.LogoImage(null);
    }
}

function BrandingModuleModel(data) {
    var self = this;
    var multiplier = 1;
    self.FrontImage = ko.observable(data != null ? data.FrontImage : null);
    self.LeftPos = ko.observable(data != null ? data.LeftPos : null);
    self.TopPos = ko.observable(data != null ? data.TopPos : null);
    self.SliderValue = ko.observable(data != null ? data.SliderValue : 50);
    self.RegisteredName = ko.observable(data != null && data.RegisteredName !== 'undefined' ? data.RegisteredName : null);
    self.Address = ko.observable(data != null && data.Address !== 'undefined' ? data.Address : null);
    self.BrandWebsite = ko.observable(data != null && data.BrandWebsite !== 'undefined' ? data.BrandWebsite : null);
    self.PhoneNumber = ko.observable(data != null && data.PhoneNumber !== 'undefined' ? data.PhoneNumber : '+1 403 208 8958');
    self.EmailAddress = ko.observable(data != null && data.EmailAddress !== 'undefined' ? data.EmailAddress : 'contact@YourOrganisation.com');

    self.ImageWidth = ko.computed(function () {
        if ($(window).width() >= 640) {
            multiplier = 12.8;
        } else if ($(window).width() >= 320) {
            multiplier = 6.4;
        }
        return self.SliderValue() * multiplier;
    }, self);

    self.LogoImage = ko.observable(data != null ? data.LogoImage : null);

    self.removeFrontImage = function () {
        self.FrontImage(null);
    }

    self.removeLogoImage = function () {
        self.LogoImage(null);
    }
}

function STAFFModuleModel(data) {
    var self = this;
    if (data != null) {
    }
    else {
    }
}

function SubmitModuleModel(data) {
    var self = this;
    self.ThanksText = ko.observable(data != null ? data.ThanksText : THANKSMSG);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : THANKSDESC);
    self.SubmitText = ko.observable(data != null ? data.SubmitText : SUBMITTEXT);
    self.DisclaimerText = ko.observable(data != null ? data.DisclaimerText : null);
}

function AuthenticationModuleModel(data) {
    var self = this;
    self.AuthenticationText = ko.observable(data != null ? data.AuthenticationText : AUTHENTICATIONDESCRIPTIONTEXT);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);
}


function ThanksModuleModel(data) {
    var self = this;
    self.ThanksText = ko.observable(data != null ? data.ThanksText : null);
    self.DescriptionText = ko.observable(data != null ? data.DescriptionText : null);

    self.isShowFacebook = ko.observable(data != null ? data.isShowFacebook : null);
    //Facebook Details
    self.FBImage = ko.observable(data != null ? data.FBImage : null);
    self.FBCaption = ko.observable(data != null ? data.FBCaption : null);
    self.FBDescription = ko.observable(data != null ? data.FBDescription : null);

    self.removeFBImage = function () {
        self.FBImage(null);
    }
}

function TouchpointModuleModel(data) {
    var self = this;
    self.Touchpoints = ko.observableArray([]);
    self.SelectedTouchPoint = ko.observable(null);
}

function ModuleModel(data, moduletype, answer, analytics, search) {
    var self = this;
    self.ModuleID = ko.observable(data != null ? data.ModuleID : 0);
    self.ModuleType = ko.observable(data != null ? data.ModuleType : moduletype);
    self.Position = ko.observable(data != null ? data.Position : 0);

    switch (self.ModuleType()) {
        case MODULETYPES.LANGUAGE:
            self.ModuleData = ko.observable(new LanguageModuleModel(data != null ? data.ModuleData : null));
            break;
        case MODULETYPES.TOUCHPOINT:
            self.ModuleData = ko.observable(new TouchpointModuleModel(data != null ? data.ModuleData : null));
            break;
        case MODULETYPES.WELCOME:
            self.ModuleData = ko.observable(new WelcomeModuleModel(data != null ? data.ModuleData : null));
            break;
        case MODULETYPES.APPRECIATION:
            self.ModuleData = ko.observable(new AppreciationModuleModel(data != null ? data.ModuleData : null));
            break;
        case MODULETYPES.BRANDING:
            self.ModuleData = ko.observable(new BrandingModuleModel(data != null ? data.ModuleData : null));
            break;
        case MODULETYPES.STAFF:
            self.ModuleData = ko.observable(new STAFFModuleModel(data != null ? data.ModuleData : null));
            break;
        case MODULETYPES.QUESTION:
            self.ModuleData = ko.observable(new QuestionModuleModel(data != null ? data.ModuleData : null, data != null ? data.QuestionType : UNSET, answer, analytics, search));
            break;
        case MODULETYPES.AUTHENTICATION:
            self.ModuleData = ko.observable(new AuthenticationModuleModel(data != null ? data.ModuleData : null));
            break;
        case MODULETYPES.SUBMIT:
            self.ModuleData = ko.observable(new SubmitModuleModel(data != null ? data.ModuleData : null));
            break;
        case MODULETYPES.THANKS:
            self.ModuleData = ko.observable(new ThanksModuleModel(data != null ? data.ModuleData : null));
            break;
    }

    /* Changing a question type in the survey */
    self.changeQuestionType = function (type) {
        var changedQuestion = new QuestionModuleModel(null, type);
        self.ModuleData(changedQuestion);
    }
}


function ConfigDataModel(data) {
    var self = this;
    self.SurveyConfigID = ko.observable(data != null ? data.SurveyConfigID : 0);
    self.OrgSubId = ko.observable(data != null ? data.OrgSubId : null);
    self.TouchpointId = ko.observable(data != null ? data.TouchpointId : null);
    self.SurveyURL = ko.observable(data != null ? data.SurveyURL : null);
    self.ConfigURL = ko.observable(data != null ? data.ConfigURL : null);
    self.OrgSub = ko.observable(data != null && typeof (data.OrgSub) !== 'undefined' ? new OrgSubModel(data.OrgSub) : null);
    self.Touchpoint = ko.observable(data != null && typeof (data.Touchpoint) !== 'undefined' ? new TouchpointModel(data.Touchpoint) : null);
}

function PublishSettingModel(data) {
    var self = this;
    self.PublishSettingID = ko.observable(data != null ? data.PublishSettingID : 0);
    self.IsAllowViewResult = ko.observable(data != null ? data.IsAllowViewResult : false);
}


function ResultSettingModel(data) {
    var self = this;
    self.ResultSettingID = ko.observable(data != null ? data.ResultSettingID : 0);
    self.IsAllowViewSocial = ko.observable(data != null ? data.IsAllowViewSocial : false);
}


function SummaryDataModel(data) {
    var self = this;
    //alert(ko.toJSON(data));
    self.SurveyResponseID = ko.observable(data != null && typeof (data.SurveyResponseID) !== 'undefined' ? data.SurveyResponseID : 0);
    self.CusXPIndex = ko.observable(data != null && typeof (data.CusXPIndex) !== 'undefined' ? data.CusXPIndex : 0);
    self.RegisteredName = ko.observable(data != null && typeof (data.RegisteredName) !== 'undefined' ? data.RegisteredName : null);
    self.BrandName = ko.observable(data != null && typeof (data.BrandName) !== 'undefined' ? data.BrandName : null);
    self.Country = ko.observable(data != null && typeof (data.Country) !== 'undefined' ? data.Country : null);
    self.Location = ko.observable(data != null && typeof (data.Location) !== 'undefined' ? data.Location : null);
    self.Date = ko.observable(data != null && typeof (data.Date) !== 'undefined' ? data.Date : null);
    self.Time = ko.observable(data != null && typeof (data.Date) !== 'undefined' ? data.Time : null);
    self.DGFields = ko.observable(data != null && typeof (data.DGFields) !== 'undefined' ? data.DGFields : null);
}


function AlertModel(data) {
    var self = this;
    self.AlertType = ko.observable(data != null && typeof (data.AlertType) !== 'undefined' ? data.AlertType : 'TIMEALERT');
    self.AlertTime = ko.observable(data != null && typeof(data.AlertTime) !== 'undefined' ? data.AlertTime : '09:00');
    self.AlertDay = ko.observable(data != null && typeof (data.AlertDay) !== 'undefined' ? data.AlertDay : 'Monday');
    self.NoofResponses = ko.observable(data != null && typeof (data.NoofResponses) !== 'undefined' ? data.NoofResponses : 100);
    self.SendToRole = ko.observable(data != null && typeof (data.SendToRole) !== 'undefined' ? data.SendToRole : 'CusXP Professional');
}

function AlertSettingsModel(data) {
    var self = this;
    self.Alerts = ko.observableArray(data != null && !$.isEmptyObject(data.Alerts) ? $.map(data.Alerts, function (item) { return new AlertModel(item) }) : []);

    self.AlertAddClick = function () {
        self.Alerts.push(new AlertModel(null));
    }

    self.AlertRemoveClick = function (data) {
        self.Alerts.remove(data);
    }
}

function ExpertCommentModel(data, comment, surveyID, moduleID, moduleInfo) {
    var self = this;
    self.ExpertCommentID = ko.observable(data != null && typeof (data.ExpertCommentID) !== 'undefined' ? data.ExpertCommentID : 0);
    self.Comment = ko.observable(data != null && typeof (data.Comment) !== 'undefined' ? data.Comment : comment);
    self.StringCommentDate = ko.observable(data != null && typeof (data.StringCommentDate) !== 'undefined' ? data.StringCommentDate : null);
    self.SurveyID = ko.observable(data != null && typeof (data.SurveyID) !== 'undefined' ? data.SurveyID : surveyID);
    self.ModuleID = ko.observable(data != null && typeof (data.ModuleID) !== 'undefined' ? data.ModuleID : moduleID);
    self.ModuleInfo = ko.observable(data != null && typeof (data.ModuleInfo) !== 'undefined' ? data.ModuleInfo : moduleInfo);
    self.UserName = ko.observable(data != null && typeof (data.UserName) !== 'undefined' ? data.UserName : null);
}

function SurveyDataModel(data) {
    var self = this;
    self.AlertSettings = ko.observable(data != null && !$.isEmptyObject(data.AlertSettings) ? new AlertSettingsModel(data.AlertSettings): new AlertSettingsModel());
    self.HierarchySettings = ko.observable(data != null && !$.isEmptyObject(data.HierarchySettings) ? new AlertSettingsModel(data.HierarchySettings) : null);
}

function SurveyModel(data, answer, analytics, search) {
    var self = this;
    self.SurveyID = ko.observable(data != null && typeof(data.SurveyID) !== 'undefined' ? data.SurveyID : 0);
    self.SurveyTitle = ko.observable(data != null && typeof(data.SurveyTitle) !== 'undefined' ? data.SurveyTitle : null);
    self.IsPublished = ko.observable(data != null && typeof(data.IsPublished) !== 'undefined' ? data.IsPublished : false);
    self.PublishedURL = ko.observable(data != null && typeof (data.PublishedURL) !== 'undefined' ? data.PublishedURL : false);
    self.PublishPage = ko.observable(data != null && typeof (data.PublishPage) !== 'undefined' ? data.PublishPage : null);
    self.StringCreatedDate = ko.observable(data != null && typeof(data.StringCreatedDate) !== 'undefined' ? data.StringCreatedDate : null);
    self.StringLastUpdatedDate = ko.observable(data != null && typeof(data.StringLastUpdatedDate) !== 'undefined' ? data.StringLastUpdatedDate : null);
    self.StringPublishedDate = ko.observable(data != null && typeof(data.StringPublishedDate) !== 'undefined' ? data.StringPublishedDate : null);
    self.PublishedID = ko.observable(data != null && typeof(data.PublishedID) !== 'undefined' ? data.PublishedID : false);
    self.OrgSub = ko.observable(data != null && typeof(data.OrgSub) !== 'undefined' ? new OrgSubModel(data.OrgSub) : null);
    self.TouchPointName = ko.observable(data != null && typeof(data.TouchPointName) !== 'undefined' ? data.TouchPointName : null);
    self.IsAllowViewResults = ko.observable(data != null && typeof(data.IsAllowViewResults) !== 'undefined' ? data.IsAllowViewResults : null);
    self.IsAllowViewSocial = ko.observable(data != null && typeof(data.IsAllowViewSocial) !== 'undefined' ? data.IsAllowViewSocial : null);
    self.Status = ko.observable(data != null && typeof (data.Status) !== 'undefined' ? data.Status : null);
    self.SelectedTouchpoints = ko.observableArray(data != null && !$.isEmptyObject(data.SelectedTouchpoints) ?$.map(data.SelectedTouchpoints, function (item) { return ko.observable(item) }) : []);
    self.SelectedSubsidiaries = ko.observableArray(data != null && !$.isEmptyObject(data.SelectedSubsidiaries) ? data.SelectedSubsidiaries : []);

    self.isSendAppreciationCode = ko.observable(data != null && typeof (data.isSendAppreciationCode) !== 'undefined' ? data.isSendAppreciationCode : false);
    self.AppreciationCode = ko.observable(data != null && typeof (data.AppreciationCode) !== 'undefined' ? data.AppreciationCode : null);
    self.TotalResponses = ko.observable(data != null && typeof (data.TotalResponses) !== 'undefined' ? data.TotalResponses : 0);
    self.SummaryData = ko.observable(data != null && typeof (data.SummaryData) !== 'undefined' ? new SummaryDataModel(data.SummaryData) : null);
    self.SurveyData = ko.observable(data != null && typeof (data.SurveyData) !== 'undefined' ? new SurveyDataModel(data.SurveyData) : new SurveyDataModel());

    self.FullPublishedURL = ko.computed(function () {
        var URL = "/" + self.PublishedURL();
        if (self.PublishedID() !== "") URL = URL + "/" + self.PublishedID();
        return URL;
    });

    if (search) { self.FromDateTime = ko.observable(data != null ? data.FromDateTime : null); }
    if (search) { self.ToDateTime = ko.observable(data != null ? data.ToDateTime : null); }
    if (search) { self.Keywords = ko.observable(data != null ? data.Keywords : null); }
    if (search) { self.WOKeywords = ko.observable(data != null ? data.WOKeywords : null); }


    if (analytics) {
        self.TouchpointAnalyticsData = ko.observable(data != null ? new TouchpointAnalyticsModel(data.TouchpointAnalyticsData) : null);
        self.OrgSubAnalyticsData = ko.observable(data != null ? new OrgSubAnalyticsModel(data.OrgSubAnalyticsData) : null);
        self.NoResultsFound = ko.observable(data != null ? data.NoResultsFound : false);
        self.ExpertComments = ko.observableArray(data != null && !$.isEmptyObject(data.ExpertComments) ? $.map(data.ExpertComments, function (item) { return new ExpertCommentModel(item) }) : []);
        self.SurveyResponses = ko.observableArray(data != null && !$.isEmptyObject(data.SurveyResponses) ? data.SurveyResponses : []);

        self.addExpertComment = function (comment, surveyID, moduleID, moduleInfo) {
            self.ExpertComments.push(new ExpertCommentModel(null, comment, surveyID, moduleID, moduleInfo));
        }.bind(self);

        self.removeExpertComment = function (data) {
            self.ExpertComments.remove(data);
        }.bind(self);
    }

    self.Modules = ko.observableArray(data != null && !$.isEmptyObject(data.Modules) ? $.map(data.Modules, function (item) { return new ModuleModel(item, UNSET, answer, analytics, search) }) : []);
    self.SurveyConfigs = ko.observableArray(data != null && !$.isEmptyObject(data.SurveyConfigs) ? $.map(data.SurveyConfigs, function (item) { return new ConfigDataModel(item) }) : []);

    if (!search && !analytics && !answer && self.Modules().length <= 0) {
        self.Modules().push(new ModuleModel(null, MODULETYPES.SUBMIT, false, false, false));
        self.Modules().push(new ModuleModel(null, MODULETYPES.THANKS, false, false, false));
    }


   
    /* Adding a new module to the survey */
    self.addModule = function (type) {
        var addedModule = new ModuleModel(null, MODULETYPES.QUESTION);
        self.Modules.push(addedModule);
    }.bind(self);

    /* Adding a new module between the survey */
    self.addModuleBetween = function (index, type) {
        var addedModule = new ModuleModel(null, MODULETYPES.QUESTION);
        self.Modules.splice(index, 0, addedModule);
    }.bind(self);

    self.swapModules = function (sIndex, dIndex) {
        var temp = self.Modules.splice(sIndex, 1);
        self.Modules.splice(dIndex, 0, temp[0]);
    }

    self.checkIsModuleChanged = function (data) {
        var emptyModule = new ModuleModel(null, data.ModuleType(), true, false, false);
        if (ko.toJSON(data) == ko.toJSON(emptyModule)) return false;
        return true;
    }.bind(self);

    /* Remove a survey module */
    self.removeModule = function (data) {
        if (confirm("Question would be deleted permanently. are you sure you want to delete the question? ")) {
            self.Modules.remove(data);
        }
    }.bind(self);

    /* Changing a module type in the survey */
    self.changeModuleType = function (type, index) {
        var changedModule = new ModuleModel(null, type);
        self.Modules.splice(index, 1, changedModule);
    }.bind(self);

    /* Duplicate a survey question */
    self.duplicateModule = function (data,index) {
        var moduleData = ko.mapping.toJS(data);
        var newQuestionModel = new ModuleModel(moduleData);
        newQuestionModel.ModuleID = 0;
        if (index == self.Modules.length-1)
            self.Modules.push(newQuestionModel);
        else
            self.Modules.splice(index, 0, newQuestionModel);
    }.bind(self);
}
