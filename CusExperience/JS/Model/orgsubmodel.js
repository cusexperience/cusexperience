﻿function TouchpointModel(data) {
    var self = this;
    var multiplier = 1;
    self.TouchpointID = ko.observable(data != null ? data.TouchpointID : 0);
    self.TouchpointName = ko.observable(data != null ? data.TouchpointName : null);
    self.TouchpointURL = ko.observable(data != null ? data.TouchpointURL : null);
    self.FrontImage = ko.observable(data != null ? data.FrontImage : null);
    self.LeftPos = ko.observable(data != null ? data.LeftPos : null);
    self.TopPos = ko.observable(data != null ? data.TopPos : null);
    self.SliderValue = ko.observable(data != null ? data.SliderValue : 50);
    self.RegisteredName = ko.observable(data != null ? data.RegisteredName : null);
    self.BrandWebsite = ko.observable(data != null ? data.BrandWebsite : null);
    self.PhoneNumber = ko.observable(data != null ? data.PhoneNumber : null);
    self.EmailAddress = ko.observable(data != null ? data.EmailAddress : null);
    self.Address = ko.observable(data != null ? data.Address : null);
    self.AddressLink = ko.observable(data != null ? data.AddressLink : null);
    self.Position = ko.observable(data != null ? data.Position : 0);
 

    self.ImageWidth = ko.computed(function () {
        if ($(window).width() >= 640) {
            multiplier = 12.8;
            // } else if ($(window).width() >= 540) {
            //     multiplier = 10.8;
        } else if ($(window).width() >= 320) {
            multiplier = 6.4;
        }
        return self.SliderValue() * multiplier;
    }, self);

    self.LogoImage = ko.observable(data != null ? data.LogoImage : null);

    self.removeFrontImage = function () {
        self.FrontImage(null);
        self.SliderValue(50);
        self.LeftPos('0px');
        self.TopPos('0px');
    }

    self.removeLogoImage = function () {
        self.LogoImage(null);
    }
}

function OrgSubModel(data) {
    var self = this;
    self.OrgSubID = ko.observable(data != null ? data.OrgSubID : null);
    self.RegisteredNumber = ko.observable(data != null ? data.RegisteredNumber : null);
    self.RegisteredName = ko.observable(data != null ? data.RegisteredName : null);
    self.BrandName = ko.observable(data != null ? data.BrandName : null);
    self.BrandWebsite = ko.observable(data != null ? data.BrandWebsite : null);
    self.Country = ko.observable(data != null ? data.Country : null);
    self.State = ko.observable(data != null ? data.State : null);
    self.City = ko.observable(data != null ? data.City : null);
    self.PhoneNumber = ko.observable(data != null ? data.PhoneNumber : null);
    self.FrontImage = ko.observable(data != null ? data.FrontImage : null);
    self.LogoImage = ko.observable(data != null ? data.LogoImage : null);
    self.EmailAddress = ko.observable(data != null ? data.EmailAddress : null);
    self.Address = ko.observable(data != null ? data.Address : null);
    self.SurveyURL = ko.observable(data != null ? data.SurveyURL : null);
    self.Industry = ko.observable(data != null ? data.Industry : null);
    self.ParentOrgSubID = ko.observable(data != null ? data.ParentOrgSubID : null);
    self.ParentOrgSubName = ko.observable(data != null ? data.ParentOrgSubName : null);
    self.Touchpoints = (data != null && !$.isEmptyObject(data.Touchpoints) ? $.map(data.Touchpoints, function (item) { return new TouchpointModel(item) }) : []);
    self.Subsidiaries = ko.observableArray(data != null && !$.isEmptyObject(data.Subsidiaries) ? $.map(data.Subsidiaries, function (item) { return new OrgSubModel(item) }) : []);
    self.BaseURL = ko.observable(data != null ? data.BaseURL : null);
    self.HierarchyURL = ko.observable(data != null ? data.HierarchyURL : null);
    self.HierarchyLevel = ko.observable(data != null ? data.HierarchyLevel : null);
    self.SurveyConfigs = ko.observableArray(data != null && !$.isEmptyObject(data.SurveyConfigs) ? $.map(data.SurveyConfigs, function (item) { return new ConfigDataModel(item) }) : []);

    self.OrgSubAddClick = function () {
        self.Subsidiaries.push(new OrgSubModel());
    }

    self.OrgSubRemoveClick = function () {
        self.Subsidiaries.remove(this);
    }
}
