﻿function ChangePwdModel() {
    var self = this;
    self.OldPassword = ko.observable();
    self.NewPassword = ko.observable();
    self.ConfirmPassword = ko.observable();
}

function showPassword() {
    
    if($('#show-pwd-checkbox').hasClass("icon-check-empty")){

        $('#show-pwd-checkbox').removeClass("icon-check-empty");
        $('#show-pwd-checkbox').addClass("icon-check-sign");
        $('#NewPassword').attr("type", "text");
        $('#OldPassword').attr("type", "text");
    } else {
        $('#show-pwd-checkbox').removeClass("icon-check-sign");
        $('#show-pwd-checkbox').addClass("icon-check-empty");
        $('#NewPassword').attr("type", "password");
        $('#OldPassword').attr("type", "password");
    }
}

function AppViewModel() {
    var self = this;
    self.ChangePwd = ko.observable(new ChangePwdModel());
    self.ServerMessages = ko.observable();

    self.changePassword = function () {

        var data = ko.toJSON(self.ChangePwd);

        var validator = $("#chgpwd-form").validate({
            rules: {
                ConfirmPassword: {
                    equalTo: "#NewPassword"
                }
            },
            messages: {
                ConfirmPassword: {
                    equalTo: "Password and Confirm Password should be the same."
                }
            },
            errorPlacement: function (error, element) {
                if (error.text() == "Password and Confirm Password should be the same.") {
                    error.insertAfter(element);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea")) {
                    $(element).removeClass('error-required');
                }
            },
            highlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea"))
                    $(element).addClass('error-required');
                if ($(element).is("select")) {
                    var button = $(element).multiselect("getButton");
                    $(button).addClass('error-required');
                }
            },
            onkeyup: function (element, event) {
                if ($(element).val() == '') {
                    var id = "#" + element.id + "-error";
                    $(id).remove();
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                    var invalidElement = validator.errorList[0].element;
                    $(invalidElement).addClass('error-required')
                }
            }
        });

        validateResult = validator.form();

        if (validateResult) {
            self.ServerMessages(null);
            $('#changepassword').text("Changing");
            $.ajax({
                type: "POST",
                url: '/api/Account/ChangePassword',
                data: data,
                contentType: 'application/json',
                dataType: 'json'
            }).success(function () {
                $('#changepassword').addClass("btn-success");
                $('#changepassword').text("Changed");
                $('#changepassword').attr("disabled", true);
                setTimeout(function () {
                    $('#changepassword').text("Change");
                    $('#changepassword').removeClass("btn-success")
                    $('#changepassword').attr("disabled", false);

                }, 5000);
                self.ChangePwd(new ChangePwdModel());
            }).fail(function (data) {
                self.ServerMessages(data.responseJSON.ModelState);
                $('#changepassword').addClass("btn-fail");
                $('#changepassword').text("Change Fail");
                $('#changepassword').attr("disabled", true);
                setTimeout(function () {
                    $('#changepassword').text("Change");
                    $('#changepassword').removeClass("btn-fail");
                    $('#changepassword').attr("disabled", false);
                }, 5000);
            });
        }
    }
}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('body-content'));
    $('body').show();
});