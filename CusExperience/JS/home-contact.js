﻿
ko.bindingHandlers.autosize = {
    init: function (element, valueAccessor) {
        $(element).autosize();
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).autosize();
        if ($(document).width() <= 320)
            $(element).css('height', '38px');
        else
            $(element).css('height', '72px');
    }
}

ko.bindingHandlers.select = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

ko.bindingHandlers.selectFilter = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

function ContactModel() {
    var self = this;
    self.GivenNames = ko.observable(null);
    self.Surname = ko.observable(null);
    self.Organisation = ko.observable(null);
    self.Country = ko.observable(null);
    self.Mobile = ko.observable(null);
    self.EmailAddress = ko.observable(null);
    self.Comment = ko.observable(null);
}

function AppViewModel() {
    var self = this;
    self.Contact = ko.observable(new ContactModel());

    self.postContactForm = function (element) {
        var data = ko.toJSON(self.Contact());

        $.validator.addMethod("phonevalidation",
       function (value, element) {
           return /^[0-9-+#]+$/.test(value);
       },
       "Please enter a valid Phone Number"
      );


        $.validator.addMethod("emailvalidation",
                function (value, element) {
                    return /\S+@\S+\.\S+/.test(value);
                },
        "Please enter a valid Email"
        );

        var contactValidator = $("#contact-form").validate({
            rules: {
                Email: {
                    emailvalidation: true,
                },
                PhoneNumber: {
                    required: true,
                },
            },
            messages: {
                Email: {
                    email: "Please enter a valid Email"
                },
                PhoneNumber: {
                    phonevalidation: "Please enter a valid Phone Number"
                },
            },
            errorPlacement: function (error, element) {
                if (error.text() == "Please enter a valid Email") {
                    error.insertAfter(element);
                }
                else if (error.text() == "Please enter a valid Phone Number") {
                    error.insertAfter(element);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea")) {
                    $(element).removeClass('error-required');
                }
            },
            highlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea"))
                    $(element).addClass('error-required');
                if ($(element).is("select")) {
                    var button = $(element).multiselect("getButton");
                    $(button).addClass('error-required');
                }
            },
            onkeyup: function (element, event) {
                if ($(element).val() == '') {
                    var id = "#" + element.id + "-error";
                    $(id).remove();
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

        validateResult = contactValidator.form();
        
        if (validateResult == true) {
            $('#submit-btn').text('Submitting');
        $.ajax({
            type: "POST",
            url: '/api/PostContact',
            data: data,
            contentType: 'application/json',
        }).success(function () {
            
            // Success message
            $('#submit-btn').addClass("btn-success");
            $('#submit-btn').text("Submitted");
            setTimeout(function () {
                $('#submit-btn').text("Submit");
                $('#submit-btn').removeClass("btn-success")
            }, 5000);

            $('#contact-form').trigger("reset");
        }).fail(function () {

        });
        }
    }
}

$(document).ready(function () {
    ko.applyBindings(new AppViewModel(), document.getElementById('contact'));
});