﻿ko.bindingHandlers.select = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);

        $(element).bind('multiselectclick', function (event, ui) {
            if (ui.value.toLowerCase() === "others") {
                $('.othersdiv').show();
            } else {
                $('.othersdiv').hide();
            }
        });
    }
};

ko.bindingHandlers.selectMultiple = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);

        $(element).bind('multiselectopen', function (event, ui) {
            $(element).multiselect("widget").find(":checkbox").each(function () {
                if (this.checked == true) {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                }
            });
        });

        $(element).on('multiselectclick', function (event, ui) {
            if (ui.value === "All" && ui.checked == true) {
                $(element).multiselect('checkAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if (!$(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').addClass('multiselect-sel');
                });

            } else if (ui.value === "All" && ui.checked == false) {
                $(element).multiselect('uncheckAll');
                $(element).multiselect("widget").find(":checkbox").each(function () {
                    if ($(this).closest('label').hasClass('multiselect-sel'))
                        $(this).closest('label').removeClass('multiselect-sel');
                });
            } else {

                var alltrue = true;
                var allElement;
                $(element).multiselect("widget").find(":checkbox").each(function () {

                    if (this.value === "All") allElement = this;

                    if (this.checked == true) {
                        if (!$(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').addClass('multiselect-sel');
                    } else {

                        if (this.value != "All") alltrue = false;

                        if ($(this).closest('label').hasClass('multiselect-sel'))
                            $(this).closest('label').removeClass('multiselect-sel');
                    }
                });

                if (alltrue == true && allElement.checked == false) {
                    bindingContext.$data.User().SelectedTouchpoints.push("All");
                    $(allElement).closest('label').addClass('multiselect-sel');
                    $(allElement).prop('checked', true);
                } else if (alltrue == false && allElement.checked == true) {
                    bindingContext.$data.User().SelectedTouchpoints.remove("All");
                    $(allElement).closest('label').removeClass('multiselect-sel');
                    $(allElement).prop('checked', false);
                }
            }
        });

        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);

        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

function UserFormValidationModel(isValid, message) {
    var self = this;
    self.IsValid = ko.observable(isValid);
    self.Message = ko.observable(message);
}

function AppViewModel() {
    var self = this;
    self.User = ko.observable(new UserModel(initJSONData));
    self.UserNameValidation = new UserFormValidationModel(true, "Username already exists");

    self.showDetail = function (data) {
        window.location = "/OrgSub/Detail/" + self.User().OrgSubID();
    }

    self.showTouchpoint = function (data) {
        window.location = "/OrgSub/Touchpoint/" + self.User().OrgSubID();
    }

    self.showUsers = function (data) {
        window.location = "/Users/" + self.User().OrgSubID();
    }

    self.createUser = function () {
        var data = ko.toJSON(self.User());
        var validateResult = false;

        $.validator.addMethod("phonevalidation",
        function (value, element) {
            return /^[0-9-+# ]+$/.test(value);
        }, "Please enter a valid Phone Number");

        $.validator.addMethod("emailvalidation",
                function (value, element) {
                    return /\S+@\S+\.\S+/.test(value);
                },
        "Please enter a valid Email"
        );

        $.validator.addMethod("specialChrs",
            function (value, element) {
            return /^[A-Za-z0-9_]+$/.test(value);
        }, "Special Characters not permitted");

        var validator = $("#userprofile-form").validate({
            rules: {
                UserName: {
                    specialChrs: true
                },
                Email: {
                    emailvalidation: true
                },
                PhoneNumber: {
                    phonevalidation: true
                },
            },
            messages: {
                UserName: {
                    specialChrs: "Please enter only alphabets,numbers and underscore"
                },
                Email: {
                    email: "Please enter a valid Email"
                },
                PhoneNumber: {
                    phonevalidation: "Please enter a valid Phone Number"
                }
            },
            errorPlacement: function (error, element) {
                if (error.text() != "This field is required.") {
                    error.insertAfter(element);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea")) {
                    $(element).removeClass('error-required');
                }
            },
            highlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea"))
                    $(element).addClass('error-required');
                if ($(element).is("select")) {
                    var button = $(element).multiselect("getButton");
                    $(button).addClass('error-required');
                }
            },
            onkeyup: function (element, event) {
                if ($(element).val() == '') {
                    var id = "#" + element.id + "-error";
                    $(id).remove();
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

        validateResult = validator.form();

        if (validateResult) {

            $('#savebutton').text("Saving");

            $.ajax({
                type: "POST",
                url: '/api/CreateUser',
                data: data,
                contentType: 'application/json'

            }).success(function (allData) {

                $('#savebutton').addClass("btn-success");
                $('#savebutton').text("Saved");
                $('#savebutton').attr("disabled", true);

                setTimeout(function () {
                    window.location = "/Users/" + self.User().OrgSubID();
                }, 1000);

            }).fail(function (data) {
                if (data.responseJSON.Message == "UserName already exists") {
                    self.UserNameValidation.IsValid(false);
                    $("#UserName").addClass('error-required');
                    $("#UserName").focus();
                }

                $('#savebutton').addClass("btn-fail");
                $('#savebutton').text("Save Failed");
                $('#savebutton').attr("disabled", true);

                setTimeout(function () {
                    $('#savebutton').text("Save");
                    $('#savebutton').removeClass("btn-fail")
                    $('#savebutton').attr("disabled", false);
                }, 5000);
            });
        }
    }
}

$(document).ready(function () {
    var appViewModel = new AppViewModel();
    ko.applyBindings(appViewModel, document.getElementById('body-content'));
    $('body').show();
});