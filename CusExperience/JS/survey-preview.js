﻿/***************************** Knockout Bindings **************************************************/
function AppViewModel() {
    var self = this;
    self.Survey = ko.observable(new SurveyModel(initJSONData, true, false, false));
    self.CurrentPage = ko.observable(1);
    self.ErrorSections = ko.observableArray([]);

    if (!window.location.origin)
        self.CurrentURL = window.location.protocol + "//" + window.location.host;

    self.getAddressLink = function (address) {
        return ko.computed({
            read: function () {
                address = $('<div>').append(address).text()
                return 'http://maps.google.com/?q=' + address.replace(/[#,-]/g, ' ');
            }
        });
    }

    self.showPosition = function (position) {
        alert("Latitude: " + position.coords.latitude +
        "<br>Longitude: " + position.coords.longitude);
    }

    self.loadResultsView = function () {
        window.location.href = "/Result/" + id;
    }

    self.showError = function (error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("User denied the request for Geolocation.");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("Location information is unavailable.");
                break;
            case error.TIMEOUT:
                alert("The request to get user location timed out.");
                break;
            case error.UNKNOWN_ERROR:
                alert("An unknown error occurred.");
                break;
        }
    }

    self.validate = function () {
        self.ErrorSections([]);

        var validateOverall = true;

        for (var i in self.Survey().Modules()) {
            if (self.Survey().Modules()[i].ModuleType() === MODULETYPES.QUESTION) {
                var moduleData = self.Survey().Modules()[i].ModuleData();
                var questionType = moduleData.QuestionType();
                var validateResult = true;
                switch (questionType) {
                    /* Validation for the rate,tag, text and choice questions */
                    case QUESTIONTYPES.RATE:
                    case QUESTIONTYPES.TEXT:
                    case QUESTIONTYPES.TAG:
                    case QUESTIONTYPES.CHOICE:
                        if (moduleData.QuestionData().IsRequired()) {
                            validateResult = moduleData.AnswerData().Validate();
                            if (validateResult == false) {
                                $(".survey-sections").eq(i).addClass('validation-error');
                                self.ErrorSections.push($(".survey-sections").eq(i));
                            }
                        }
                        break;

                        /* Validation for the multiple type questions */
                    case QUESTIONTYPES.MULTIPLE:
                        if (moduleData.QuestionData().IsRequired()) {
                            validateResult = moduleData.AnswerData().Validate(moduleData.QuestionData().MultipleType());
                            if (validateResult == false) {
                                $(".survey-sections").eq(i).addClass('validation-error');
                                var surveySection = $(".survey-sections").eq(i);
                                self.ErrorSections.push(surveySection);
                                surveySection.find('.carousel-indicators').find("li").each(function (index, value) {
                                    if (!$(value).hasClass("completed")) {
                                        $(value).addClass("not-completed");
                                    }
                                    var carouselElement = surveySection.find('.carousel');
                                    carouselElement.carousel(surveySection.find('.carousel-indicators').find(".not-completed:first").index());
                                });
                            }
                        }
                        break;

                        /* Validation for the demographic questions */
                    case QUESTIONTYPES.DEMOGRAPHIC:
                        if (moduleData.QuestionData().IsRequired()) {

                            var demographicForm = $(".survey-sections").eq(i).find("#demographics-form");

                            var validator = demographicForm.validate({
                                errorPlacement: function (error, element) {

                                },
                                unhighlight: function (element, errorClass, validClass) {
                                    if ($(element).is("input")) {
                                        $(element).removeClass('error-required');
                                        $(element).closest('.survey-sections').removeClass('validation-error');
                                    }
                                },
                                highlight: function (element, errorClass, validClass) {
                                    if ($(element).is("input"))
                                        $(element).addClass('error-required');
                                    if ($(element).is("select")) {
                                        var button = $(element).multiselect("getButton");
                                        $(button).addClass('error-required');
                                    }
                                }
                            });

                            validateResult = validator.form();

                            $("#demographics-form").find('select').each(function (index) {
                                var selectResult = $(this).valid();
                                if (selectResult == false)
                                    validateResult = false;
                            });

                            if (validateResult == false) {
                                var surveySection = $(".survey-sections").eq(i);
                                self.ErrorSections.push(surveySection);
                                surveySection.addClass('validation-error');
                            }
                            break;
                        }
                }
            } else if (self.Survey().Modules()[i].ModuleType() === MODULETYPES.APPRECIATION) {
                var validateResult = true;
                $.ajax({
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    url: "/api/Captcha",
                    contentType: "application/json"
                }).success(function (allData) {
                }).fail(function (httpObj, data) {
                    validateResult = false;
                    var surveySection = $(".survey-sections").eq(i);
                    self.ErrorSections.push(surveySection);
                    surveySection.addClass('validation-error');
                });
            }
            if (validateOverall == true && validateResult == false) validateOverall = false;
        }
        return validateOverall;
    }


    self.submitExperience = function () {
        var validateOverall = self.validate();
        if (validateOverall == false) {
            $.scrollTo(self.ErrorSections()[0], 100, { offset: -100 });
        }

        var d = document.getElementById("share-your-exp");
        if (validateOverall == true) {
            alert("You are in preview mode. Your response is not recorded.")
            d.className = d.className + " share-your-exp-success";
            self.CurrentPage(2);
        }
    }
}

$(document).ready(function () {
    var surveyResponse = new AppViewModel();
    ko.applyBindings(surveyResponse, document.getElementById('body-content'));
    $('body').show();
});