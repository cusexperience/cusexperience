﻿
ko.bindingHandlers.autosizeSupport = {
    init: function (element, valueAccessor) {
        $(element).autosize();
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).autosize();
        if ($(document).width() <= 320)
            $(element).css('height', '38px');
        else
            $(element).css('height', '72px');
    }
}

ko.bindingHandlers.selectProblem = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if (bindingContext.$root.Support().Problem() === true) {
                bindingContext.$root.Support().Problem(false);
                $(element).removeClass('choice-selected');
            }
            else {
                bindingContext.$root.Support().Problem(true);
                $(element).addClass('choice-selected');
            }
        });
    }
}

ko.bindingHandlers.selectSolution = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).click(function () {
            if (bindingContext.$root.Support().Solution() === true) {
                bindingContext.$root.Support().Solution(false);
                $(element).removeClass('choice-selected');
            }
            else {
                bindingContext.$root.Support().Solution(true);
                $(element).addClass('choice-selected');
            }
        });
    }
}

ko.bindingHandlers.selectSupport = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

        $(".modal-dialog").bind('scrollstart', function () {
            $(element).multiselect('close');
        });

        $(".modal-dialog").bind('mousewheel', function () {
            $(element).multiselect('close');
        });

        var multiselectOptions = ko.utils.unwrapObservable(allBindingsAccessor().multiselectOptions) || {};
        if (ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption)) {
            multiselectOptions.noneSelectedText = ko.utils.unwrapObservable(allBindingsAccessor().optionsCaption);
        }
        allBindingsAccessor().optionsCaption = '';
        $(element).multiselect(multiselectOptions);
        $(element).bind('multiselectopen', function () {
            buttonElement = $(element).multiselect("getButton");
            var offset = buttonElement.offset();
            var fromTop = offset.top - $(window).scrollTop();
            if ($(document).width() <= 320) {
                fromTop = fromTop + 24;
            } else {
                fromTop = fromTop + 48;
            }
            $(element).multiselect("widget").css('top', fromTop);
            $(element).multiselect("widget").css('left', offset.left);
            $(element).multiselect("widget").css('position', 'fixed');
            $(element).multiselect("widget").show();
        });
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            $(element).multiselect("destroy");
        });


    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var selectOptions = ko.utils.unwrapObservable(allBindingsAccessor().select);
        allBindingsAccessor().optionsCaption = '';
        ko.bindingHandlers.options.update(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        setTimeout(function () {
            $(element).multiselect("refresh");
        }, 0);
    }
};

function AttachmentModel(data) {
    var self = this;
    self.FileName = ko.observable(data != null ? data.FileName : null);
    self.FileType = ko.observable(data != null ? data.FileType : null);
    self.FilePath = ko.observable(data != null ? data.FilePath : null);
    self.CommentText = ko.observable(data != null ? data.CommentText : null);
}

function SupportModel() {
    var self = this;
    self.Title = ko.observable();
    self.Description = ko.observable();
    self.Attachment = ko.observable();
    self.GivenNames = ko.observable();
    self.Surname = ko.observable();
    self.Country = ko.observable();
    self.Mobile = ko.observable();
    self.EmailAddress = ko.observable();
    self.Problem = ko.observable(false);
    self.Solution = ko.observable(false);
    self.CurrentURL = document.URL;
    self.FilePath = ko.observable();
    self.CaptchaKey = ko.observable();
    self.CaptchaValue = ko.observable();
    self.Attachments = ko.observableArray([]);
    
    self.addAttachment = function (data) {
        self.Attachments.push(new AttachmentModel(data));
    }

    self.removeAttachment = function (data) {
        self.Attachments.remove(data);
    }
}

function ServerValidationModel(valid, message) {
    var self = this;
    self.valid = ko.observable(valid);
    self.message = ko.observable(message);
}

function SupportViewModel() {
    var self = this;
    self.Support = ko.observable(new SupportModel());
    self.ServerValidations = ko.observable();

    self.ServerValidations["Captcha"] = new ServerValidationModel(true, "Captcha verification failed. Please try again.");

    self.requestSupport = function () {
        var validateResult = false;
        self.Support().CaptchaKey($('input[name=sckey]').val());
        self.Support().CaptchaValue($('input[name=scvalue]').val());

        $.validator.addMethod("supportphonevalidation",
        function (value, element) {
            return /^[0-9-+# ]+$/.test(value);
        },
        "Please enter a valid Phone Number"
       );

        $.validator.addMethod("supportemailvalidation",
                function (value, element) {
                    return /\S+@\S+\.\S+/.test(value);
                },
        "Please enter a valid Email"
        );

        var supportValidator = $("#support-form").validate({
            rules: {
                SupportEmail: {
                    supportemailvalidation: true,
                    required: true
                },
                SupportPhoneNumber: {
                    required: true,
                    supportphonevalidation: true
                },
            },
            messages: {
                SupportEmail: {
                    required: "Please enter a valid Email",
                    email: "Please enter a valid Email"
                },
                SupportPhoneNumber: {
                    required: "Please enter a valid Phone Number",
                    supportphonevalidation: "Please enter a valid Phone Number"
                },
            },
            errorPlacement: function (error, element) {
                if (error.text() == "Please enter a valid Email") {
                    error.insertAfter(element);
                }
                else if (error.text() == "Please enter a valid Phone Number") {
                    error.insertAfter(element);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea")) {
                    $(element).removeClass('error-required');
                }
            },
            highlight: function (element, errorClass, validClass) {
                if ($(element).is("input") || $(element).is("textarea"))
                    $(element).addClass('error-required');
                if ($(element).is("select")) {
                    var button = $(element).multiselect("getButton");
                    $(button).addClass('error-required');
                }
            },
            invalidHandler: function (form, validator) {
                var errors = validator.numberOfInvalids();
                if (errors) {
                    validator.errorList[0].element.focus();
                }
            }
        });

        validateResult = supportValidator.form();

        $("#support-form").find('select').each(function (index) {
            var selectResult = $(this).valid();
            if (selectResult == false)
                validateResult = false;
        });

        if (self.Support().CaptchaValue() === "0") {
            $(".captchadiv").addClass('error-required');
            validateResult = false;

            $(".captchadiv").one('mousedown', function () {
                $(".captchadiv").removeClass('error-required');
            });
        }

        if (validateResult == true) {

            var data = ko.toJSON(self.Support);
            $('#support-button').text("Submitting");
            $.ajax({
                type: "POST",
                url: '/api/PostSupport',
                contentType: 'application/json',
                data: data,
                cache: false,
                async: true
            }).success(function (data) {
                $("#supportModal").modal('hide');
                $('#support-button').text("Submit");
                alert('Thank you for submitting your problem or/and solution. We will do our best to act upon it. If you have given us your contact details, we might contact you for more information.');
                $("#supportModal").on('hidden.bs.modal', function () {
                    var supportViewModel = new SupportViewModel();
                    ko.cleanNode(document.getElementById('supportDiv'));
                    ko.applyBindings(supportViewModel, document.getElementById('supportDiv'));

                    $('#supportModal').find('input,textarea,button,div').removeClass('error-required');
                    $('#supportModal').find('div').removeClass('choice-selected');
                });
                

            }).fail(function (data) {
                if (data.responseJSON.Message == "Captcha is Invalid") {
                    self.ServerValidations['Captcha'].valid(false);
                    $(".captchadiv").addClass('error-required');
                }
                    $('#support-button').addClass("btn-fail");
                    $('#support-button').text("Submission failed");
                    $('#support-button').attr("disabled", true);

                    setTimeout(function () {
                        $('#support-button').text("Submit");
                        $('#support-button').removeClass("btn-fail");
                        $('#support-button').attr("disabled", false);
                    }, 2000);  
            });
        }
    }


    self.takeScreenShot = function () {
        $('#supportModal').one('hidden.bs.modal', function (e) {
            html2canvas(document.body, {
                onrendered: function (canvas) {
                    var dataUrl = canvas.toDataURL();
                    dataUrl = dataUrl.replace('data:image/png;base64,', '');
                    $.ajax({
                        type: "POST",
                        url: '/api/PostScreenShot',
                        dataType: 'text',
                        data: dataUrl
                    }).success(function () {
                        $('#supportModal').modal('show');
                    }).fail(function () {

                    });
                }
            });
        })
        $('#supportModal').modal('hide');
    }
}


/* Binding Handler for the file upload functionality */
ko.bindingHandlers.uploadSupportAttachment = {
    init: function (element, valueAccessor) {

    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        $(element).change(function () {
            var options = ko.utils.unwrapObservable(valueAccessor()),
            property = ko.utils.unwrapObservable(options.property);
            if (property) {
                if (element.files.length) {
                    var $this = $(this),
                        fileName = $this.val();
                    var formElement = $(element).closest('form');
                    var formData = new FormData(formElement[0]);
                    $(".progress-bar-success").show();
                    $(".progress-bar-success").css("width", '5%');
                    $.ajax({
                        url: '/api/PostSupportAttachment',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        xhr: function () {
                            var req = $.ajaxSettings.xhr();
                            if (req) {
                                req.upload.addEventListener('progress', function (event) {
                                    if (event.lengthComputable) {
                                        var percent = 100 * event.loaded / event.total;
                                        $(".progress-bar-success").css("width", percent + '%');
                                    }
                                }, false);
                            }
                            return req;
                        },
                        success: function (data) {
                            setTimeout(function () {
                                $(".progress-bar-success").hide();
                            }, 100);
                           if (!window.location.origin)
                                window.location.origin = window.location.protocol + "//" + window.location.host;
                            data.FilePath = window.location.origin + data.FilePath;
                            bindingContext.$root.Support().addAttachment(data);
                        },
                        error: function (data) {
                            alert('Error in uploading File. Please try again.');
                        }
                    });
                }
            }
        });
     }
};

$(document).ready(function () {
    var supportViewModel = new SupportViewModel();
     ko.applyBindings(supportViewModel, document.getElementById('supportDiv'));
});




