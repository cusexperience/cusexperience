﻿using EvoPdf;
using RazorEngine;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace CusExperience.Utilities
{
    public class ReportGenerator
    {
        public byte[] generateReport(string securityCookie, string reportData, string url, string headerHtml, string footerHtml, string frontPageHtml, string baseURL)
        {
            HtmlToPdfConverter pdfConverter = new HtmlToPdfConverter();
            pdfConverter.LicenseKey = "PbOgsqGhsqCqpbKnvKKyoaO8o6C8q6urqw==";
            pdfConverter.JavaScriptEnabled = true;
            pdfConverter.SvgFontsEnabled = true;
            pdfConverter.PdfDocumentOptions.TopSpacing = 50.0f;
            pdfConverter.PdfDocumentOptions.BottomSpacing = 50.0f;

            pdfConverter.PdfDocumentOptions.ShowHeader = true;
            pdfConverter.PdfDocumentOptions.ShowFooter = true;

            AddFrontPage(pdfConverter, frontPageHtml, baseURL);
            AddHeaderElements(pdfConverter, headerHtml, baseURL);
            AddFooterElements(pdfConverter, footerHtml, baseURL);

            pdfConverter.HttpRequestCookies.Add(".AspNet.ApplicationCookie", securityCookie);
            pdfConverter.HttpPostFields.Add("reportData", reportData);

            return pdfConverter.ConvertUrl(url);
        }

        private void AddFrontPage(HtmlToPdfConverter pdfConverter, string frontPageHtml, string baseURL)
        {
            HtmlToPdfConverter frontPageConverter = new HtmlToPdfConverter();
            Document frontPage = frontPageConverter.ConvertHtmlToPdfDocumentObject(frontPageHtml, baseURL);
            pdfConverter.PdfDocumentOptions.AddStartDocument(frontPage);
        }


        private void AddHeaderElements(HtmlToPdfConverter pdfConverter, string headerHtml, string baseURL)
        {
            HtmlToPdfElement headerHtmlElement = new HtmlToPdfElement(headerHtml, baseURL);
            headerHtmlElement.FitHeight = true;
            pdfConverter.PdfHeaderOptions.AddElement(headerHtmlElement);
        }

        private void AddFooterElements(HtmlToPdfConverter pdfConverter, string footerHtml, string baseURL)
        {
            HtmlToPdfVariableElement footerHtmlElement = new HtmlToPdfVariableElement(footerHtml, baseURL);
            footerHtmlElement.FitHeight = true;
            pdfConverter.PdfFooterOptions.AddElement(footerHtmlElement);
        }

    }
}