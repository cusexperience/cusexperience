﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CusExperience.Utilities
{
    class ScreenShotCreator
    {
        public string Url { get; set; }
        public Bitmap ThumbnailImage { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int BrowserWidth { get; set; }
        public int BrowserHeight { get; set; }
        public string DocumentContent { get; set; }

        public ScreenShotCreator(int BrowserWidth, int BrowserHeight, int ThumbnailWidth, int ThumbnailHeight, string DocumentContent)
        {
            this.BrowserWidth = BrowserWidth;
            this.BrowserHeight = BrowserHeight;
            this.Height = ThumbnailHeight;
            this.Width = ThumbnailWidth;
            this.DocumentContent = DocumentContent; 
        }

        public Bitmap GenerateThumbnail()
        {
            Thread thread = new Thread(new ThreadStart(GenerateThumbnailInteral));
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            thread.Join();
            return ThumbnailImage;
        }
        private void GenerateThumbnailInteral()
        {
            WebBrowser webBrowser = new WebBrowser();
            webBrowser.ScrollBarsEnabled = true;
            webBrowser.Navigate("about:blank");
            webBrowser.ScrollBarsEnabled = true;
            webBrowser.Document.Write(DocumentContent);
            webBrowser.ClientSize = new Size(webBrowser.Document.Body.ScrollRectangle.Width, webBrowser.Document.Body.ScrollRectangle.Height);
            this.ThumbnailImage = new Bitmap(webBrowser.Width, webBrowser.Height);
            webBrowser.BringToFront();
            webBrowser.DrawToBitmap(ThumbnailImage, webBrowser.Bounds);
            this.ThumbnailImage = (Bitmap)ThumbnailImage.GetThumbnailImage(webBrowser.Width, webBrowser.Height, null, IntPtr.Zero);
        }

        private void WebBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            WebBrowser webBrowser = (WebBrowser)sender;
            webBrowser.ClientSize = new Size(this.BrowserWidth, this.BrowserHeight);
            webBrowser.ScrollBarsEnabled = true;
            this.ThumbnailImage = new Bitmap(webBrowser.Bounds.Width, webBrowser.Bounds.Height);
            webBrowser.BringToFront();
            webBrowser.DrawToBitmap(ThumbnailImage, webBrowser.Bounds);
            this.ThumbnailImage = (Bitmap)ThumbnailImage.GetThumbnailImage(Width, Height, null, IntPtr.Zero);
        }
    }
}
