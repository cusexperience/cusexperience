﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.Models
{
    public class ReportModel
    {
        public string SurveyTitle { get; set; }
        public string OrgSubName { get; set; }
        public string CurrentDate { get; set; }
        public string CurrentUser { get; set; }
    }
}