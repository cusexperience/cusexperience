﻿using CusExperience.CusExperienceService;
using CusExperience.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.Models
{
    public class RegisterModel
    {
        public OrgSub OrgSub { get; set; }
        public CusExpUser UserProfile { get; set; }
        public string Password { get; set; }

    }
}