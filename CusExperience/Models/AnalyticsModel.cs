﻿using CusExperience.CusExperienceService;
using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.Models
{
    public class AnalyticsModel
    {
        public CusExpUser CurrentUser { get; set; }
        public Survey PublishedSurvey { get; set; }
        public Survey AnalyticsSurvey { get; set; }
    }
}