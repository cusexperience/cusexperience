﻿using System.Web;
using System.Web.Optimization;

namespace CusExperience
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/model").Include(
                 "~/JS/Model/constant.js",
                 "~/JS/Model/questionmodel.js",
                 "~/JS/Model/answermodel.js",
                 "~/JS/Model/analyticsmodel.js",
                 "~/JS/Model/searchmodel.js",
                 "~/JS/Model/surveymodel.js",
                 "~/JS/Model/orgsubmodel.js",
                 "~/JS/Model/searchmodel.js",
                 "~/JS/Model/usermodel.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                "~/Scripts/jquery-2.1.1.min.js",
                "~/Scripts/knockout-3.1.0.js",
                "~/Scripts/jquery-ui-1.10.4.custom.js",
                "~/Scripts/jquery.autosize.min.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/jquery.ui.touch-punch.js",
                "~/Scripts/jquery.multiselect.min.js",
                "~/Scripts/jquery.multiselect.filter.min.js",
                "~/Scripts/jquery.mobile.custom.min.js",
                "~/Scripts/jquery.simplePagination.js",
                "~/Scripts/html2canvas.js",
                "~/JS/global.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/survey-publish").Include(
                "~/Scripts/jquery-2.1.1.min.js",
                "~/Scripts/knockout-3.1.0.js",
                "~/Scripts/jquery-ui-1.10.4.custom.js",
                "~/Scripts/respond.js",
                "~/Scripts/jquery.autosize.min.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/jquery.ui.touch-punch.js",
                "~/Scripts/spin.js",
                "~/Scripts/jquery.multiselect.min.js",
                "~/Scripts/jquery.multiselect.filter.min.js",
                "~/Scripts/jquery.scrollTo.min.js",
                "~/Scripts/socialmedia.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/survey-detail").Include(
                "~/Scripts/knockout-sortable.js",
                "~/Scripts/knockout-mapping.js",
                "~/Scripts/knockout.reactor.js",
                "~/Scripts/jquery.scrollTo.min.js",
                "~/Scripts/jquery.ajaxprogress.js",
                "~/JS/survey-detail.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/scripts-survey-preview").Include(
                "~/JS/common-pp.js",
                "~/JS/survey-preview.js",
                "~/Scripts/jquery.scrollTo.min.js"
            ));

        }
    }
}
