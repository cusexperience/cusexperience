﻿using CusExperience.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CusExperience
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("Content/{*pathInfo}");
            routes.IgnoreRoute("gelrs/{*pathInfo}");
            routes.IgnoreRoute("JS/{*pathInfo}");
            routes.IgnoreRoute("fonts/{*pathInfo}");
            routes.IgnoreRoute("Images/{*pathInfo}");
            routes.IgnoreRoute("Scripts/{*pathInfo}");
            routes.IgnoreRoute("Sitemap/{*pathInfo}");

            /* File Routes */

            routes.MapRoute(
                    name: "Elmah",
                    url: "elmah.axd",
                    defaults: new { controller = "Elmah", action = "Index" }
            );

            routes.MapRoute(
                name: "SurveyImages",
                url: "SurveyImages/{*pathInfo}",
                defaults: new { controller = "File", action = "SurveyImages" }
            );

            routes.MapRoute(
                name: "TouchpointImages",
                url: "TouchpointImages/{*pathInfo}",
                defaults: new { controller = "File", action = "TouchpointImages" }
            );

            routes.MapRoute(
                name: "SurveyAttachment",
                url: "SurveyAttachment/{*pathInfo}",
                defaults: new { controller = "File", action = "SurveyAttachment" }
            );

            routes.MapRoute(
                name: "SupportAttachment",
                url: "SupportAttachment/{*pathInfo}",
                defaults: new { controller = "File", action = "SupportAttachment" }
            );

            /* 
             * 
            routes.IgnoreRoute("SurveyImages/{*pathInfo}");
            routes.IgnoreRoute("TouchpointImages/{*pathInfo}");
            routes.IgnoreRoute("SupportScreenShot/{*pathInfo}");
            routes.IgnoreRoute("SurveyAttachment/{*pathInfo}");
             * 
             */

            routes.RouteExistingFiles = true;

            routes.MapRoute(
                name: "ErrorRouting",
                url: "Error",
                defaults: new { controller = "Home", action = "Error" }
            );

            routes.MapRoute(
               name: "UnAuthorizedRouting",
               url: "UnAuthorized",
               defaults: new { controller = "Home", action = "UnAuthorized" }
           );

            routes.MapRoute(
                 name: "HomeRouting",
                 url: "",
                 defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
           );

            routes.MapRoute(
                 name: "CaptchaRouting",
                 url: "Captcha",
                 defaults: new { controller = "Captcha", action = "Index" }
            );

            routes.MapRoute(
                 name: "SurveyCreateRouting",
                 url: "Survey/Create/{id}/{templateName}",
                 defaults: new { controller = "Survey", action = "Create", id = UrlParameter.Optional, templateName = UrlParameter.Optional }
             );

            routes.MapRoute(
               name: "NewSurveyRouting",
               url: "Survey/{action}/{orgsubid}/{surveyid}",
               defaults: new { controller = "Survey", action = "Detail" }
           );

            routes.MapRoute(
                name: "SurveyRouting",
                url: "Survey/{action}/{id}",
                defaults: new { controller = "Survey", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "PublicRouting",
              url: "Public/{action}/{id}",
              defaults: new { controller = "Public", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "NewAnalyticsRouting",
                url: "Analytics/{action}/{orgsubid}/{surveyid}",
                defaults: new { controller = "Analytics", action = "Detail"}
            );

            routes.MapRoute(
                name: "AnalyticsRouting",
                url: "Analytics/{action}/{id}",
                defaults: new { controller = "Analytics", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "OrgSubRouting",
                url: "OrgSub/{action}/{id}",
                defaults: new { controller = "OrgSub", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "UsersRouting",
               url: "Users/{id}",
               defaults: new { controller = "User", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "UserRouting",
                url: "User/{action}/{id}",
                defaults: new { controller = "User", action = "Detail", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                name: "AdminRouting",
                url: "Admin/{action}/{id}",
                defaults: new { controller = "Admin", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "AccountRouting",
                url: "Account/{action}/{id}",
                defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DashboardRouting",
                url: "Dashboard/{action}/{id}",
                defaults: new { controller = "Dashboard", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ReportRouting",
                url: "Report",
                defaults: new { controller = "Report", action = "Index", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                name: "SupportRouting",
                url: "Support/{id}",
                defaults: new { controller = "Support", action = "Index", id = UrlParameter.Optional }
             );

            routes.MapRoute(
                name: "ResultRouting",
                url: "Result/{*url}",
             defaults: new { controller = "Analytics", action = "Result", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "CatchAll",
                url: "{*url}",
                defaults: new { controller = "Survey", action = "Publish", id = UrlParameter.Optional }
            );
        }
    }
}
