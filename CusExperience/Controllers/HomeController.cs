﻿using CusExperience.CusExperienceService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CusExperience.Controllers
{
    public class HomeController : Controller
    {
        SurveyRepositoryClient sRep = new SurveyRepositoryClient();
        UserRepositoryClient uRep = new UserRepositoryClient();

        // GET: Home
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View("Error");
        }

        public ActionResult UnAuthorized()
        {
            return View("UnAuthorized");
        }

        public ActionResult About()
        {
            return PartialView("About");
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            if (User.Identity.IsAuthenticated) 
            {
                return View();
            }
            else
            {
                return View("ContactBeforeLogin");
            }
        }

        [AllowAnonymous]
        public ActionResult SupportView()
        {
            return PartialView("_SupportView");
        }

        [AllowAnonymous]
        public ActionResult SupportViewPublish()
        {
            return PartialView("_SupportViewPublish");
        }
       
    }
}