﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.CusExperienceService;
using System.IO;
using CusExperience.DataContract;

namespace CusExperience.Controllers
{
    public class FileController : Controller
    {
        FileRepositoryClient fr = new FileRepositoryClient();

        public static readonly List<string> ImageExtensions = new List<string> { ".JPG", ".JPEG", ".BMP", ".GIF", ".PNG" };
        public static readonly List<string> VideoExtensions = new List<string> { ".MPEG", ".MPG", ".MOV", ".OGG", ".FLV", ".MP4" };
        public static readonly string PdfExtension = ".PDF";

        [AllowAnonymous]
        public ActionResult SurveyAttachment(string pathInfo)
        {
            try
            {
                var filePath = "/SurveyAttachment/" + pathInfo;
                FileMetaData downloadFileMetadata = new FileMetaData();
                downloadFileMetadata.RemoteFilePath = filePath;
                byte[] byteArray = new byte[10000];
                fr.DownloadFile(downloadFileMetadata, out byteArray);
                if (ImageExtensions.Contains(Path.GetExtension(pathInfo).ToUpperInvariant()))
                    return File(byteArray, "image/" + Path.GetExtension(pathInfo));
                else if (PdfExtension.Equals("." + Path.GetExtension(pathInfo).ToUpperInvariant()))
                    return File(byteArray, "application/" + Path.GetExtension(pathInfo));
                return File(byteArray, "application/octet-stream");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [AllowAnonymous]
        public ActionResult SupportAttachment(string pathInfo)
        {
            try
            {
                var filePath = "/SupportAttachment/" + pathInfo;
                byte[] byteArray;
                FileMetaData downloadFileMetadata = new FileMetaData();
                downloadFileMetadata.RemoteFilePath = filePath;
                fr.DownloadFile(downloadFileMetadata, out byteArray);
                if (ImageExtensions.Contains("." + Path.GetExtension(pathInfo).ToUpperInvariant()))
                    return File(byteArray, "image/" + Path.GetExtension(pathInfo));
                else if (PdfExtension.Equals("." + Path.GetExtension(pathInfo).ToUpperInvariant()))
                    return File(byteArray, "application/" + Path.GetExtension(pathInfo));
                return File(byteArray, "application/octet-stream");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [AllowAnonymous]
        public ActionResult SurveyImages(string pathInfo)
        {
            try
            {
                var filePath = "/SurveyImages/" + pathInfo;
                byte[] byteArray = new byte[100000];
                FileMetaData downloadFileMetadata = new FileMetaData();
                downloadFileMetadata.RemoteFilePath = filePath;
                fr.DownloadFile(downloadFileMetadata, out byteArray);
                if (ImageExtensions.Contains("." + Path.GetExtension(pathInfo).ToUpperInvariant()))
                    return File(byteArray, "image/" + Path.GetExtension(pathInfo));
                else if (PdfExtension.Equals("." + Path.GetExtension(pathInfo).ToUpperInvariant()))
                    return File(byteArray, "application/" + Path.GetExtension(pathInfo));
                return File(byteArray, "application/octet-stream");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        [AllowAnonymous]
        public ActionResult TouchpointImages(string pathInfo)
        {
            try
            {
                var filePath = "/TouchpointImages/" + pathInfo;
                byte[] byteArray;
                FileMetaData downloadFileMetadata = new FileMetaData();
                downloadFileMetadata.RemoteFilePath = filePath;
                fr.DownloadFile(downloadFileMetadata, out byteArray);
                if (ImageExtensions.Contains("." + Path.GetExtension(pathInfo).ToUpperInvariant()))
                    return File(byteArray, "image/" + Path.GetExtension(pathInfo));
                else if (PdfExtension.Equals("." + Path.GetExtension(pathInfo).ToUpperInvariant()))
                    return File(byteArray, "application/" + Path.GetExtension(pathInfo));
                return File(byteArray, "application/octet-stream");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }
    }
}