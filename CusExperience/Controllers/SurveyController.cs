﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Text.RegularExpressions;
using CusExperience.CusExperienceService;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CusExperience.Entities;

namespace CusExperience.Controllers
{
    //[Authorize(Roles = ROLES.ADMINISTRATOR + "," + ROLES.CUSXPPROFESSIONAL)]
    public class SurveyController : Controller
    {
        SurveyRepositoryClient sRep = new SurveyRepositoryClient();
        OrgSubRepositoryClient oRep = new OrgSubRepositoryClient();
        UserRepositoryClient uRep = new UserRepositoryClient();

        public ActionResult Index(string id)
        {
            long orgSubId = 0;
            if (!long.TryParse(id, out orgSubId))
                return View("Error");

            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()))
            {
                OrgSub orgSub = oRep.GetOrgSub(orgSubId, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName ;
               return View(sRep.GetSurveysByOrgSub(orgSubId, User.Identity.GetUserId()));
            }
            return View("UnAuthorized");
        }

        public ActionResult OrgSub()
        {
             if (uRep.isSuperAdministrator(User.Identity.GetUserId()))
             {
                 return View(oRep.GetOrgSubs(User.Identity.GetUserId()));
             }
             return View("UnAuthorized");
        }

        public ActionResult Create(string id, string templateName)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()))
            {
                long orgSubId = 0;
                if (!long.TryParse(id, out orgSubId))
                    return View("Error");

                OrgSub orgSub = oRep.GetOrgSub(orgSubId, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                Survey survey = sRep.GetTemplateSurveyByOrgSub(orgSubId, templateName, User.Identity.GetUserId());
                if (survey == null)
                    return View("Error");
                return View("Detail", survey);
            }
            return View("UnAuthorized");
        }

        public ActionResult Detail(string orgsubid, string surveyid)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()))
            {
                int surveyID = 0;
                long orgSubID = 0;

                if (!Int32.TryParse(surveyid, out surveyID) || !Int64.TryParse(orgsubid, out orgSubID))
                    return View("Error");

                OrgSub orgSub = oRep.GetOrgSub(orgSubID, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                Survey survey = sRep.GetSurvey(surveyID, User.Identity.GetUserId());

                if (survey == null)
                    return View("Error");
                return View(survey);
            }
            return View("UnAuthorized");
        }

        [AllowAnonymous]
        public ActionResult Publish(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                Survey survey = sRep.GetPublishedSurvey(url);
                if (survey == null)
                    return View("Error");
                string surveyTitle = Regex.Replace(survey.SurveyTitle, @"<[^>]+>|;", string.Empty).Trim();
                string surveyTitleNormalised = Regex.Replace(surveyTitle, @"&nbsp", " ").Trim();
                ViewBag.Title = surveyTitleNormalised + " | CusExperience - Your Customers' Experience Feedback Rating Survey";
                if (!string.IsNullOrEmpty(survey.PublishPage))
                    return View("~/Views/Customized/" + survey.PublishPage + ".cshtml", survey);
                return View(survey);
            }
            return View("Error");
        }

        public ActionResult Preview(string id)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()))
            {
                int surveyID = 0;
                if (!Int32.TryParse(id, out surveyID))
                    return View("Error");
                
                Survey survey = sRep.GetSurvey(surveyID, User.Identity.GetUserId());
                if (survey == null)
                    return View("Error");
                ViewBag.Title = "CusExperience - Your Customer Experience";
                return View(survey);
            }
            return View("UnAuthorized");
        }
    }
}