﻿using CusExperience.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.CusExperienceService;
using CusExperience.Entities;

namespace CusExperience.Controllers
{
    public class OrgSubController : Controller
    {
        OrgSubRepositoryClient oRep = new OrgSubRepositoryClient();
        UserRepositoryClient uRep = new UserRepositoryClient();

        public ActionResult Index()
        {
            try
            {
                if (uRep.isSuperAdministrator(User.Identity.GetUserId()))
                {
                    return View("Index", oRep.GetOrgSubs(User.Identity.GetUserId()));
                }
                else if (uRep.isAdministrator(User.Identity.GetUserId()))
                {
                    return RedirectToAction("Detail", "OrgSub", new { id = uRep.GetCurrentUser(User.Identity.GetUserId()).OrgSub.OrgSubID });
                }
                return View("UnAuthorized");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult Hierarchy(string id)
        {
            try
            {
                long orgSubId = 0;
                if (!long.TryParse(id, out orgSubId))
                    return View("Error");
                if (uRep.isSuperAdministrator(User.Identity.GetUserId()))
                {
                    return View(oRep.GetOrgSub(orgSubId, User.Identity.GetUserId()));
                }
                return View("UnAuthorized");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult Create(string id)
        {
            try
            {
                if (uRep.isSuperAdministrator(User.Identity.GetUserId()))
                {
                    if(string.IsNullOrEmpty(id))
                        return View();
                    else
                    {
                         long parentOrgSubID = 0;
                         if (!long.TryParse(id, out parentOrgSubID))
                             return View("Error");

                         OrgSub parentOrgSub = oRep.GetOrgSub(parentOrgSubID, User.Identity.GetUserId());
                         ViewBag.OrgSubName = !string.IsNullOrEmpty(parentOrgSub.ParentOrgSubName) ? parentOrgSub.ParentOrgSubName + " > " + parentOrgSub.RegisteredName : parentOrgSub.RegisteredName;

                         OrgSub orgSub = oRep.GetNewOrgSub(parentOrgSubID,User.Identity.GetUserId());
                         return View(orgSub);
                    }
                }
                return View("UnAuthorized");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult CreateSub(string id)
        {
            try
            {
                if (uRep.isSuperAdministrator(User.Identity.GetUserId()))
                {
                    if (string.IsNullOrEmpty(id))
                        return View();
                    else
                    {
                        long parentOrgSubID = 0;
                        if (!long.TryParse(id, out parentOrgSubID))
                            return View("Error");

                        OrgSub parentOrgSub = oRep.GetOrgSub(parentOrgSubID, User.Identity.GetUserId());
                        ViewBag.OrgSubName = !string.IsNullOrEmpty(parentOrgSub.ParentOrgSubName) ? parentOrgSub.ParentOrgSubName + " > " + parentOrgSub.RegisteredName : parentOrgSub.RegisteredName;

                        OrgSub orgSub = oRep.GetNewOrgSub(parentOrgSubID, User.Identity.GetUserId());
                        return View(orgSub);
                    }
                }
                return View("UnAuthorized");
            }
            catch (Exception ex)
            {
                return View("Error");
            }
        }

        public ActionResult Detail(string id)
        {
            try
            {
                int orgSubID = Convert.ToInt32(id);
               
                if (uRep.isSuperAdministrator(User.Identity.GetUserId()))
                {
                    OrgSub orgSub = oRep.GetOrgSub(orgSubID, User.Identity.GetUserId());

                    if (orgSub == null)
                        return View("Error");

                    ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;
                    return View("Detail-Admin", orgSub);
                }
                else if (uRep.isAdministrator(User.Identity.GetUserId()))
                {
                    OrgSub orgSub = oRep.GetOrgSub(orgSubID, User.Identity.GetUserId());
                    
                    if(orgSub == null)
                        return View("Error");

                    ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                    return View("Detail", orgSub);
                }
                return View("UnAuthorized");
            }
            catch (Exception ex)
            {
                if (ex is UnauthorizedAccessException)
                    return View("UnAuthorized");
                return View("Error");
            }
        }

        public ActionResult TouchPoint(string id)
        {
            try
            {
                long orgSubId = 0;
                if (!long.TryParse(id, out orgSubId))
                    return View("Error");
                if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()))
                {
                    OrgSub orgSub = oRep.GetOrgSub(orgSubId, User.Identity.GetUserId());
                    ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;
                    return View("Touchpoint-Admin", oRep.GetTouchpoints(orgSubId, User.Identity.GetUserId()));
                }
                return View("UnAuthorized");
            }
            catch (Exception ex)
            {
                if (ex is UnauthorizedAccessException)
                    return View("UnAuthorized");
                return View("Error");
            }
        }
    }
}