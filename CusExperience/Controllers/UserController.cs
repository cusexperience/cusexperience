﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.CusExperienceService;
using CusExperience.Entities;

namespace CusExperience.Controllers
{

    public class UserController : Controller
    {
        UserRepositoryClient uRep = new UserRepositoryClient();
        OrgSubRepositoryClient oRep = new OrgSubRepositoryClient();

        // GET: User
        public ActionResult Index(string id)
        {
            try
            {
                long orgSubId = 0;
                if (!long.TryParse(id, out orgSubId))
                    return View("Error");

                if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()))
                {
                    OrgSub orgSub = oRep.GetOrgSub(orgSubId, User.Identity.GetUserId());
                    ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                    return View(uRep.GetUsersByOrgSub(orgSubId, User.Identity.GetUserId()));
                }
                return View("UnAuthorized");
            }
            catch (Exception ex)
            {
                if (ex is UnauthorizedAccessException)
                    return View("UnAuthorized");
                return View("Error");
            }
        }

        public ActionResult Create(string id)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()))
            {
                long orgSubId = 0;
                if (!long.TryParse(id, out orgSubId))
                    return View("Error");

                OrgSub orgSub = oRep.GetOrgSub(orgSubId, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                return View("Create", uRep.GetNewUser(orgSubId, User.Identity.GetUserId()));
            }
            return View("UnAuthorized");
        }

        public ActionResult Detail(string id)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()))
            {

                CusExpUser user = uRep.GetUser(id, User.Identity.GetUserId());

                OrgSub orgSub = oRep.GetOrgSub(user.OrgSub.OrgSubID, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                return View("Detail-Admin",user);
            }
            return View("UnAuthorized");
        }
    }
}