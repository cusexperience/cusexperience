﻿using CusExperience.CusExperienceService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;

namespace CusExperience.Controllers
{
    public class PublicController : Controller
    {
        SurveyRepositoryClient sRep = new SurveyRepositoryClient();

        [AllowAnonymous]
        public ActionResult PrivacyPolicy()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult IamCXO()
        {
            return View();
        }

         [AllowAnonymous]
        public ActionResult GAAPI()
        {
            return View();
        }

         [AllowAnonymous]
         public ActionResult MapView()
         {
             return View();
         }
       
	}
}