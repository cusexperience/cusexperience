﻿using CusExperience.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using EvoPdf;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using CusExperience.Models;
using System.IO;
using RazorEngine;
using System.Text.RegularExpressions;
using CusExperience.CusExperienceService;
using CusExperience.Entities;

namespace CusExperience.Controllers
{

    public class AnalyticsController : Controller
    {
        SurveyRepositoryClient sRep = new SurveyRepositoryClient();
        ResponseRepositoryClient rRep = new ResponseRepositoryClient();
        UserRepositoryClient uRep = new UserRepositoryClient();
        AnalyticsRepositoryClient aRep = new AnalyticsRepositoryClient();
        OrgSubRepositoryClient oRep = new OrgSubRepositoryClient();
        
        internal static string TIMEZONE = "Singapore Standard Time";
        TimeZoneInfo SingaporeTimeZone = TimeZoneInfo.FindSystemTimeZoneById(TIMEZONE);

        public ActionResult Index(string id)
        {
            long orgSubId = 0;
            if (!long.TryParse(id, out orgSubId))
                return View("Error");

            if (uRep.isSuperAdministrator(User.Identity.GetUserId()))
            {
                OrgSub orgSub = oRep.GetOrgSub(orgSubId, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;
                return View(rRep.GetPbSurveysWithRespCountByOrgSub(orgSubId, User.Identity.GetUserId()));
            }
            else if ( uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                OrgSub orgSub = oRep.GetOrgSub(orgSubId, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;
                return View(rRep.GetPbSurveysWithRespCount(User.Identity.GetUserId()));
            }
            return View("UnAuthorized");
        }

        public ActionResult OrgSub()
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()))
            {
                return View(oRep.GetOrgSubs(User.Identity.GetUserId()));
            }
            return View("UnAuthorized");
        }


        public ActionResult Detail(string orgsubid, string surveyid)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                int surveyID = 0;
                long orgSubID = 0;
                if (!Int32.TryParse(surveyid, out surveyID) || !Int64.TryParse(orgsubid, out orgSubID))
                    return View("Error");

                AnalyticsModel analyticsData = new AnalyticsModel();

                OrgSub orgSub = oRep.GetOrgSub(orgSubID, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                analyticsData.CurrentUser = uRep.GetCurrentUser(User.Identity.GetUserId());
                analyticsData.PublishedSurvey = rRep.GetSurveyForIV(surveyID, orgSubID, User.Identity.GetUserId());
                analyticsData.AnalyticsSurvey = aRep.GetAnalytics(surveyID, orgSubID, User.Identity.GetUserId());

                if (analyticsData.PublishedSurvey != null && analyticsData.AnalyticsSurvey != null)
                    return View("IndexView", analyticsData);
                else
                    return View("Error");
            }
            return View("UnAuthorized");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult AnalyticsReport(string orgsubid, string surveyid, FormCollection formCollection)
        {
            /* Object model = formCollection["analyticsData"];
             return View("ReportView", model);* */

            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                 Object model = formCollection["analyticsData"];
                 ViewBag.URL = "/Analytics/DownloadAR/" + surveyid;
                 return View("ReportProgress", model); 
            }
            return View("UnAuthorized"); 
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult DownloadAR(string id, FormCollection formCollection)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
             {
                 int surveyID = 0;
                 if (!Int32.TryParse(id, out surveyID))
                     return View("Error");
                 Survey survey = sRep.GetSurvey(surveyID, User.Identity.GetUserId());
                 if (survey == null)
                     return View("Error");

                 ReportGenerator rGenerator = new ReportGenerator();

                 var frontPageHtmlRaw = System.IO.File.ReadAllText(Server.MapPath(@"~/Views/Shared/_ReportFrontPage.cshtml"));
                 var headerHtmlRaw = System.IO.File.ReadAllText(Server.MapPath(@"~/Views/Shared/_ReportHeader.cshtml"));
                 var footerHtmlRaw = System.IO.File.ReadAllText(Server.MapPath(@"~/Views/Shared/_ReportFooter.cshtml"));

                 string surveyTitle = Regex.Replace(survey.SurveyTitle, @"<[^>]+>|;", string.Empty).Trim();
                 string surveyTitleNormalised = Regex.Replace(surveyTitle, @"&nbsp", " ").Trim();

                 ReportModel rm = new ReportModel();
                 rm.SurveyTitle = surveyTitleNormalised;
                 rm.OrgSubName = survey.OrgSub.RegisteredName;
                 rm.CurrentDate = TimeZoneInfo.ConvertTime(DateTime.Now, SingaporeTimeZone).ToString("dd/MM/yyyy @ HH:mm tt");
                 CusExpUser curUser = uRep.GetCurrentUser(User.Identity.GetUserId());
                 rm.CurrentUser = curUser.FirstName + ' ' + curUser.LastName.ToUpper();

                 string frontPageHtml = Razor.Parse(frontPageHtmlRaw, rm);
                 string headerHtml = Razor.Parse(headerHtmlRaw, rm);
                 string footerHtml = Razor.Parse(footerHtmlRaw, rm);

                 var pdfBytes = rGenerator.generateReport(Request.Cookies[".AspNet.ApplicationCookie"].Value, formCollection["reportData"], Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~") + "/Analytics/ReportView", headerHtml, footerHtml, frontPageHtml, Request.Url.GetLeftPart(UriPartial.Authority) + Url.Content("~"));

                  // send the PDF document as a response to the browser for download
                  var response = HttpContext.Response;
                  response.Clear();
                  response.AddHeader("Content-Type", "application/pdf");
                  response.AddHeader("Content-Disposition", String.Format("attachment; filename=AnalyticsReport.pdf; size={0}", pdfBytes.Length));
                  response.BinaryWrite(pdfBytes);
                  response.End();
             } 
           return View("UnAuthorized");
        }

        [HttpPost,ValidateInput(false)]
        public ActionResult ReportView(FormCollection formCollection)
        {
            if (formCollection["reportData"] != null)
            {
                Object model = formCollection["reportData"];
                return View("ReportView", model);
            }
          return View("Error");
        }

        public ActionResult CustomerView(string orgsubid, string surveyid)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                int surveyID = 0;
                long orgSubID = 0;
                if (!Int32.TryParse(surveyid, out surveyID) || !Int64.TryParse(orgsubid, out orgSubID))
                    return View("Error");

                OrgSub orgSub = oRep.GetOrgSub(orgSubID, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                Survey survey = rRep.GetResponsesBySurvey(surveyID, orgSubID, User.Identity.GetUserId(), 0, 10, "DESC", "");

                if (survey == null)
                    return View("Error");

                return View(survey);
            }
            return View("UnAuthorized");
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult CustomerView(string orgsubid, string surveyid, FormCollection formCollection)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                int surveyID = 0;
                long orgSubID = 0;
                if (!Int32.TryParse(surveyid, out surveyID) || !Int64.TryParse(orgsubid, out orgSubID))
                    return View("Error");

                OrgSub orgSub = oRep.GetOrgSub(orgSubID, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                string cvData = formCollection["cvData"].ToString();
                JObject cvObj = JObject.Parse(cvData);
                ViewBag.CVData = cvObj;
                Survey survey = aRep.GetMatchedResponses(surveyID, 0, 10, "DESC", "", cvData, User.Identity.GetUserId());
                if (survey == null)
                    return View("Error");
                return View(survey);
            }
            return View("UnAuthorized");
        }

        public ActionResult SocialView(string orgsubid, string surveyid)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                int surveyID = 0;
                long orgSubID = 0;
                if (!Int32.TryParse(surveyid, out surveyID) || !Int64.TryParse(orgsubid, out orgSubID))
                    return View("Error");

                OrgSub orgSub = oRep.GetOrgSub(orgSubID, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                Survey survey = rRep.GetSurveyForSV(surveyID, orgSubID, User.Identity.GetUserId());
                if (survey != null)
                    return View(survey);
                else
                    return View("Error");
            }
            return View("UnAuthorized");
        }

        public ActionResult TrackingView(string orgsubid, string surveyid)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                int surveyID = 0;
                long orgSubID = 0;
                if (!Int32.TryParse(surveyid, out surveyID) || !Int64.TryParse(orgsubid, out orgSubID))
                    return View("Error");

                Survey survey = sRep.GetSurvey(surveyID, User.Identity.GetUserId());

                OrgSub orgSub = oRep.GetOrgSub(orgSubID, User.Identity.GetUserId());
                ViewBag.OrgSubName = !string.IsNullOrEmpty(orgSub.ParentOrgSubName) ? orgSub.ParentOrgSubName + " > " + orgSub.RegisteredName : orgSub.RegisteredName;

                if (survey != null)
                    return View(survey);
                else
                    return View("Error");
            }
            return View("UnAuthorized");
        }

        public ActionResult ResponseView()
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                return PartialView("_ResponseView");
            }
            return View("UnAuthorized");
        }

        public ActionResult SurveyResponse(string id)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                int surveyResponseID = 0;
                if (!Int32.TryParse(id, out surveyResponseID))
                    return View("Error");

                return View(rRep.GetSurveyResponseByID(surveyResponseID, User.Identity.GetUserId()));
            }
            return View("UnAuthorized");
        }

        [AllowAnonymous]
        public ActionResult Result(string url)
        {
            if (!String.IsNullOrEmpty(url))
            {
                Survey survey = aRep.GetQuantitativeAnalytics(url);
                if(survey!=null)
                    return View(survey);
                return View("Error");
            }
            return View("Error");
        }

        public ActionResult SurveyData(string orgsubid, string surveyid)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                ViewBag.URL = "/Analytics/DownloadSD/" + surveyid;
                return View("SDReportProgress");
            }
            return View("UnAuthorized");
        }

        // GET: SurveyData
        public ActionResult DownloadSD(string id)
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()) || uRep.isAdministrator(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()) || uRep.isManager(User.Identity.GetUserId()))
            {
                int surveyID = 0;
                if (!Int32.TryParse(id, out surveyID))
                    return View("Error");

                byte[] excelBytes = rRep.writeResponsetoExcel(surveyID, User.Identity.GetUserId());
                var response = HttpContext.Response;
                response.Clear();
                response.AddHeader("Content-Type", "application/octet-stream");
                response.AddHeader("Content-Disposition", String.Format("attachment; filename=SurveyData.xlsx; size={0}", excelBytes.Length));
                response.BinaryWrite(excelBytes);
                response.End();
            }
            return View("UnAuthorized");
        }
    }
}