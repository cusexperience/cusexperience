﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using CusExperience.Models;
using CusExperience.CusExperienceService;

namespace CusExperience.Controllers
{

    public class AccountController : Controller
    {
        private UserRepositoryClient uRep = new UserRepositoryClient();

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                if(uRep.isSuperAdministrator(User.Identity.GetUserId()))
                    return RedirectToAction("Index", "OrgSub");
                else if (uRep.isAdministrator(User.Identity.GetUserId()))
                    return RedirectToAction("Index", "Survey");
                else if (uRep.isManager(User.Identity.GetUserId()) || uRep.isExecutive(User.Identity.GetUserId()) || uRep.isCusXPProfessional(User.Identity.GetUserId()))
                    return RedirectToAction("Index", "Analytics");
                else if (uRep.isFrontLiner(User.Identity.GetUserId()))
                    return RedirectToAction("UserProfile", "Account");
                else return View("Error");
            }
        }

        [AllowAnonymous]
        public ActionResult ForgetPassword()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult RegisterOrg()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult PRConfirmation()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult PasswordReset()
        {
            return View();
        }

        public ActionResult UserProfile()
        {
            return View(uRep.GetCurrentUser(User.Identity.GetUserId()));
        }
    }
}