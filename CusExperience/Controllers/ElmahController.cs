﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CusExperience.CusExperienceService;
using System.IO;
using CusExperience.DataContract;


namespace CusExperience.Controllers
{
    public class ElmahController : Controller
    {
        UserRepositoryClient uRep = new UserRepositoryClient();

        // GET: Elmah
        public ActionResult Index()
        {
            if (uRep.isSuperAdministrator(User.Identity.GetUserId()))
            {
                return new FilePathResult(HttpContext.Server.MapPath("~/elmah.axd"),"application/html");
            }
            return View("UnAuthorized");
        }
    }
}