﻿using CusExperience.CusExperienceService;
using CusExperience.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace CusExperience.Controllers
{
    public class SupportController : Controller
    {
        SupportRepositoryClient spr = new SupportRepositoryClient();

        [AllowAnonymous]
        // GET: Support
        public ActionResult Index(string id)
        {
            if (string.IsNullOrEmpty(id)) return View("Error");

            Support s = spr.GetSupport(id);
            if (s == null) return View("Error");

            return View("~/Views/Support/Customized/" + s.SupportPage + ".cshtml");
        }
    }
}