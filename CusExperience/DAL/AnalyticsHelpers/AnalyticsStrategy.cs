﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.DAL.AnalyticsHelpers
{
    public abstract class AnalyticsStrategy
    {
        protected long moduleID;
        protected JObject analyticsData = null;
        protected JObject questionData = null;
        protected JObject searchData = null;

        public AnalyticsStrategy(long mID, JObject moduleData)
        {
            moduleID = mID;
            searchData = (JObject)moduleData["SearchData"];
            questionData = (JObject)moduleData["QuestionData"];
            analyticsData = new JObject();
            moduleData["AnalyticsData"] = analyticsData;
        }

        public abstract void collectAnalyticsData(JObject responseData);

        public abstract void getModuleDataWithAnalytics();

        public abstract bool IsFilteredResponse(JObject responseData);

        public void collectSearchAnalyticsData(List<SurveyResponse> surveyResponses)
        {
            foreach (SurveyResponse surveyResponse in surveyResponses)
            {
                ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();

                if (mr != null)
                {
                    JObject responseData = JObject.Parse(mr.ModuleData);
                    if (IsFilteredResponse(responseData)) { 
                         collectAnalyticsData(responseData);
                    }
                }
            }
        }

        public void getModuleDataWithSearchAnalytics()
        {
            getModuleDataWithAnalytics();
        }

    }
}