﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.DAL.AnalyticsHelpers
{
    public class AnalyticsStrategyFactory
    {

        public static AnalyticsStrategy getAnalyticsStrategy(long moduleID, JObject moduleData)
        {

            string questionType = moduleData["QuestionType"].ToString();

            switch (questionType)
            {
                case "Rate":
                    return new RatingAnalyticsStrategy(moduleID, moduleData);

                case "Multiple":
                    return new MultipleAnalyticsStrategy(moduleID, moduleData);

                case "Text":
                    return new TextAnalyticsStrategy(moduleID, moduleData);

                case "Choice":
                    return new ChoiceAnalyticsStrategy(moduleID, moduleData);

                case "Demographic":
                    return new DGAnalyticsStrategy(moduleID, moduleData);

            }
            return null;
        }
    }
}