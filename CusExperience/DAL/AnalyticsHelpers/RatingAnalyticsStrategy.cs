﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.DAL.AnalyticsHelpers
{
    public class RatingAnalyticsStrategy : AnalyticsStrategy
    {
        double rating = 0;
        int responseCount = 0;
        int totalCount = 0;


        /* Search Criteria */
        int fromRating = 0;
        int toRating = 10;

        public RatingAnalyticsStrategy(long mID, JObject moduleData)
            : base(mID, moduleData)
        {
            if (moduleData["SearchData"] != null)
            {
                JObject searchData = (JObject) moduleData["SearchData"];
                var somedata = searchData["FromRating"];
                if(searchData["FromRating"] != null && !string.IsNullOrEmpty(searchData["FromRating"].ToString())) {    
                    fromRating = Int32.Parse(searchData["FromRating"].ToString());

                    if(searchData["ToRating"] != null && !string.IsNullOrEmpty(searchData["ToRating"].ToString()))
                           toRating = Int32.Parse(searchData["ToRating"].ToString());
                    else   toRating = fromRating;
                }
            }
        }

        public override bool IsFilteredResponse(JObject responseData)
        {
            JObject answerData = (JObject)responseData["AnswerData"];

            if (answerData["Rating"] != null && !string.IsNullOrEmpty(answerData["Rating"].ToString()))
            {
                int rating = Int32.Parse(answerData["Rating"].ToString());
                if (rating >= fromRating && rating <= toRating)
                    return true;
                return false;
            }
            return false;
        }

        public override void collectAnalyticsData(JObject responseData)
        {
            JObject answerData = (JObject)responseData["AnswerData"];

            if (answerData == null) return;

            if (answerData["Rating"] != null && !string.IsNullOrEmpty(answerData["Rating"].ToString()))
            {
                rating += Int32.Parse(answerData["Rating"].ToString());
                responseCount++;
            }
            totalCount++;
        }

        public override void getModuleDataWithAnalytics()
        {
            if (responseCount != 0)
            {
                analyticsData["OverallRating"] = ((double)rating * 10) / responseCount;
                analyticsData["ResponseCount"] = responseCount;
                analyticsData["NotFound"] = false;
            }
            else
                analyticsData["NotFound"] = true;
        }
    }
}