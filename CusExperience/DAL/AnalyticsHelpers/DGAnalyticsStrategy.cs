﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.DAL.AnalyticsHelpers
{
    public class DGAnalyticsStrategy: AnalyticsStrategy
    {

        public DGAnalyticsStrategy(long mID, JObject moduleData)
            : base(mID, moduleData)
        {

        }

        internal Dictionary<string, string> BuildSearchDGDictionary(JObject dgSearch)
        {
            Dictionary<string, string> dgDictionary = new Dictionary<string, string>();
            JArray jsonDGSearchFields = (JArray)dgSearch["DGSearchFields"];
            for (int i = 0; i < jsonDGSearchFields.Count; i++)
            {
                string convertedString = jsonDGSearchFields[i].ToString().Trim();
                if (convertedString.Equals("")) continue;

                JObject jsonDGSearchField = (JObject)jsonDGSearchFields[i];
                if (jsonDGSearchField == null) continue;

                if (jsonDGSearchField["FieldName"] == null) continue;

                if (jsonDGSearchField["FieldValues"] == null) continue;

                string strFieldName = jsonDGSearchField["FieldName"].ToString();
                string strFieldValue = jsonDGSearchField["FieldValues"].ToString();
                if (!strFieldValue.Equals("[]"))
                    dgDictionary.Add(strFieldName, strFieldValue);
            }

            string keywords = dgSearch["Keywords"].ToString();
            if (!string.IsNullOrEmpty(keywords))
                dgDictionary.Add("Keywords", keywords);

            string WOKeywords = dgSearch["WOKeywords"].ToString();
            if (!string.IsNullOrEmpty(WOKeywords))
                dgDictionary.Add("WOKeywords", WOKeywords);

            return dgDictionary;
        }

        internal Dictionary<string, string> BuildDGDictionary(JObject moduleData)
        {
            Dictionary<string, string> dgDictionary = new Dictionary<string, string>();

            JObject jsonQuestionData = (JObject)moduleData["QuestionData"];
            if (jsonQuestionData == null) return null;

            JObject jsonAnswerData = (JObject)moduleData["AnswerData"];
            if (jsonAnswerData == null) return null;

            JArray jsonDGQuestionFields = (JArray)jsonQuestionData["DGQuestionFields"];
            JArray jsonDGAnswerFields = (JArray)jsonAnswerData["DGAnswerFields"];

            if (jsonDGQuestionFields.Count != jsonDGAnswerFields.Count) return null;

            int indexCount = 0;

            foreach (JObject jsonDGQuestionField in jsonDGQuestionFields)
            {
                string jsonFieldName = jsonDGQuestionField["FieldName"].ToString();

                if (jsonDGAnswerFields[indexCount].ToString().Equals("")) { indexCount++; continue; }

                if (jsonDGAnswerFields[indexCount]["FieldValue"] == null) { indexCount++; continue; }

                string jsonFieldValue = jsonDGAnswerFields[indexCount]["FieldValue"].ToString();

                if (!string.IsNullOrEmpty(jsonFieldValue))
                    dgDictionary.Add(jsonFieldName, jsonFieldValue);

                indexCount++;
            }
            return dgDictionary;
        }

        public override bool IsFilteredResponse(JObject responseData)
        {
            return true;
        }

        public override void collectAnalyticsData(JObject responseData)
        {
        }

        public override void getModuleDataWithAnalytics()
        {
            
        }

        public void RemoveUnmatchedResponses(List<SurveyResponse> surveyResponses)
        {
            Dictionary<string, string> searchDictionary = BuildSearchDGDictionary(searchData);

            int indexCount = surveyResponses.Count, currentIndex = 0;
            while (currentIndex < indexCount)
            {
                SurveyResponse surveyResponse = surveyResponses.ElementAt(currentIndex);

                ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();

                if (mr != null)
                {
                    JObject responseData = JObject.Parse(mr.ModuleData);
                    Dictionary<string, string> moduleDictionary = BuildDGDictionary(responseData);

                    foreach (KeyValuePair<string, string> searchItem in searchDictionary)
                    {
                        string value = "";
                        if (moduleDictionary.TryGetValue(searchItem.Key, out value))
                        {
                            if (!searchItem.Value.Contains(value))
                            {
                                surveyResponses.RemoveAt(currentIndex);
                                currentIndex = currentIndex - 1;
                                indexCount = indexCount - 1;
                                break;
                            }
                        }
                        if (string.IsNullOrEmpty(value))
                        {
                            surveyResponses.RemoveAt(currentIndex);
                            currentIndex = currentIndex - 1;
                            indexCount = indexCount - 1;
                            break;
                        }
                    }
                }
                currentIndex++;
            }
        }
    }
}