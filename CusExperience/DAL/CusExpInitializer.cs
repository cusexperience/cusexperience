﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CusExperience.Entities;
using CusExperience.Migrations;

namespace CusExperience.DAL
{
    public class CusExpInitializer: System.Data.Entity.CreateDatabaseIfNotExists<CusExpContext>
    {
        protected override void Seed(CusExpContext context)
        {
            Module m1 = new Module
            {
                ModuleData = "{\"WelcomeMessage\": null,\"FrontImage\": null,\"LogoImage\": null}",
                ModuleType = "Welcome",
                Position = 1
            };

            Module m2 = new Module
            {
                ModuleData = "{\"ThanksText\": null,\"DescriptionText\": null,\"SubmitText\": null}",
                ModuleType = "Thanks",
                Position = 2
            };

            var modules = new List<Module> {
                m1,
                m2
            };

            Survey s1 = new Survey { SurveyTitle = "Use Sample Survey", CreatedDate = DateTime.Now, CreatedBy = "CusXPAdmin", IsTemplate = true, IsPredefined = true, TemplateName = "Sample", Modules = modules };

            context.Surveys.Add(s1);
            context.SaveChanges();

            Touchpoint tc1 = new Touchpoint
            {
                TouchpointName = "Customer Service Center",
            };

            Touchpoint tc2 = new Touchpoint
            {
                TouchpointName = "Call Center",
            };

            Touchpoint tc3 = new Touchpoint
            {
                TouchpointName = "Email",
            };

            Touchpoint tc4 = new Touchpoint
            {
                TouchpointName = "Website",
            };

            var touchPoints = new List<Touchpoint> {
                tc1,
                tc2,
                tc3,
                tc4
            };

            OrgSub org1 = new OrgSub
            {
                RegisteredName = "Singapore Kindness Movement",
                RegisteredNumber = "NA",
                BrandName = "SKM",
                BrandWebsite = "http://kindness.sg/",
                City = "Singapore",
                Address = "",
                PhoneNumber = "+65 6837 9954",
                EmailAddress = "kindness@kindness.sg",
                Industry = "NGO",
                Country = "Singapore",
                State = "Singapore",
                TouchPoints = touchPoints
            };

            context.OrgSubs.Add(org1);
            context.SaveChanges();
        }
    }
}