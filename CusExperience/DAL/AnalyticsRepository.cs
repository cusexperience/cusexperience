﻿using CusExperience.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using CusExperience.Utilities;
using CusExperience.DAL.AnalyticsHelpers;

namespace CusExperience.DAL
{
    public class AnalyticsRepository
    {
        CusExpContext dbContext = new CusExpContext();
        internal static string QUESTION = "Question";

        internal Survey GetAnalytics(int id)
        {
            Survey survey = dbContext.Surveys.Include("Modules").Where(s => s.SurveyID == id).FirstOrDefault<Survey>();
            if (survey == null) return null;

            var surveyResponses = dbContext.SurveyResponses.Include("ModuleResponses").Where(s => s.Survey.SurveyID == id).ToList();
            return GetAnalyticsForResponses(survey, surveyResponses);
        }

        internal Survey GetAnalyticsBySC(Survey searchSurvey)
        {

            Survey survey = dbContext.Surveys.Include("Modules").Where(s => s.SurveyID == searchSurvey.SurveyID).FirstOrDefault<Survey>();
            if (survey == null) return null;

            DateTime? fromDateTime = DateTime.ParseExact(searchSurvey.FromDateTime,"dd/MM/yyyy HH:mm",null);
            DateTime? toDateTime = DateTime.ParseExact(searchSurvey.ToDateTime, "dd/MM/yyyy HH:mm", null);

            List<SurveyResponse> surveyResponses = dbContext.SurveyResponses.Include("ModuleResponses").Where(s => s.Survey.SurveyID == searchSurvey.SurveyID  && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime).ToList();

            foreach (Module module in searchSurvey.Modules)
            {
                JObject moduleData = JObject.Parse(module.ModuleJSONData.ToString());
                if (moduleData == null) continue;

                string questionType = moduleData["QuestionType"].ToString();

                if (questionType == "Demographic")
                {
                    DGAnalyticsStrategy dgStrategy = new DGAnalyticsStrategy(module.ModuleID, moduleData);
                    dgStrategy.RemoveUnmatchedResponses(surveyResponses);
                }
            }
            return GetSearchAnalyticsForResponses(searchSurvey, surveyResponses);
        }

        internal Survey GetAnalyticsByTime(int id, DateTime? fromDateTime, DateTime? toDateTime)
        {
            Survey survey = dbContext.Surveys.Include("Modules").Where(s => s.SurveyID == id).FirstOrDefault<Survey>();
            if (survey == null) return null;

            var surveyResponses = dbContext.SurveyResponses.Include("ModuleResponses").Where(s => s.Survey.SurveyID == id && s.ResponseDate >= fromDateTime && s.ResponseDate <= toDateTime).ToList();
            return GetAnalyticsForResponses(survey, surveyResponses);
        }

        /* Helper Method */

        internal Survey GetAnalyticsForResponses(Survey survey, List<SurveyResponse> surveyResponses)
        {

            survey.Modules = survey.Modules.Where(m => m.ModuleType == QUESTION).ToList();

            foreach (Module module in survey.Modules)
            {
                JObject moduleData = JObject.Parse(module.ModuleData);
                if (moduleData == null) continue;

                AnalyticsStrategy strategyObject = AnalyticsStrategyFactory.getAnalyticsStrategy(module.ModuleID, moduleData);
                if (strategyObject == null) continue;

                foreach (SurveyResponse surveyResponse in surveyResponses)
                {
                    ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == module.ModuleID).FirstOrDefault();

                    if (mr != null)
                    {
                        JObject responseData = JObject.Parse(mr.ModuleData);
                        strategyObject.collectAnalyticsData(responseData);
                    }
                }

                strategyObject.getModuleDataWithAnalytics();
                module.ModuleJSONData = moduleData;
            }
            return survey;
        }

        internal Survey GetSearchAnalyticsForResponses(Survey searchSurvey, List<SurveyResponse> surveyResponses)
        {
            foreach (Module module in searchSurvey.Modules)
            {
                JObject moduleData = JObject.Parse(module.ModuleJSONData.ToString());
                if (moduleData == null) continue;

                AnalyticsStrategy strategyObject = AnalyticsStrategyFactory.getAnalyticsStrategy(module.ModuleID, moduleData);
                if (strategyObject == null) continue;

                strategyObject.collectSearchAnalyticsData(surveyResponses);

                strategyObject.getModuleDataWithSearchAnalytics();

                module.ModuleJSONData = moduleData;
            }
            return searchSurvey;
        }

        internal List<string> GetComments(int surveyID, int moduleID, string keyWord)
        {
            Survey survey = dbContext.Surveys.Include("Modules").Where(s => s.SurveyID == surveyID).FirstOrDefault<Survey>();
            var surveyResponses = dbContext.SurveyResponses.Include("ModuleResponses").Where(s => s.Survey.SurveyID == surveyID);
            List<string> Comments = new List<string>();

            foreach (SurveyResponse surveyResponse in surveyResponses)
            {
                ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == moduleID).FirstOrDefault();

                if (mr != null)
                {
                    JObject responseData = JObject.Parse(mr.ModuleData);
                    if (responseData == null) continue;

                    JObject answerData = (JObject)responseData["AnswerData"];
                    if (answerData == null) continue;

                    string comment = answerData["CommentText"].ToString();
                    if (!string.IsNullOrEmpty(comment))
                        Comments.Add(comment);
                }
            }
            List<string> matchedComments = Comments.Where(s => s.Split().Contains(keyWord)).ToList();

            matchedComments = matchedComments.Select(x => x.Replace(keyWord, "<strong>" + keyWord + "</strong>")).ToList();
            return matchedComments;
        }

    }

}