﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CusExperience.DAL
{
    public class CusExpContext : IdentityDbContext<UserProfile>
    {
        public CusExpContext() : base("CusExpContext")
        {
            Configuration.AutoDetectChangesEnabled = false;
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = true;
            Configuration.ValidateOnSaveEnabled = true;
        }

        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<ModuleResponse> ModuleResponses { get; set; }
        public DbSet<SurveyResponse> SurveyResponses { get; set; }
        public DbSet<OrgSub> OrgSubs { get; set; }
        public DbSet<Touchpoint> Touchpoints { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
           base.OnModelCreating(modelBuilder);
 	       modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

}
