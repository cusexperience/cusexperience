﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CusExperience.Entities;
using CusExperience.DAL;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Http.OData;


namespace CusExperience.DAL
{
    public class ResponseRepository
    {
        CusExpContext dbContext = new CusExpContext();
        internal static string QUESTION = "Question";

        internal List<Survey> GetPbSurveysWithRespCount()
        {
            List<Survey> publishedSurveys = dbContext.Surveys.Where(s => s.Status != "DELETED" && s.IsTemplate == false && s.IsPublished == true).ToList();
            foreach (Survey survey in publishedSurveys)
            {
                var counts = dbContext.SurveyResponses.Where(x => x.Survey.SurveyID == survey.SurveyID).GroupBy(x => x.Survey)
                      .Select(g => new { g.Key, Count = g.Count() }).SingleOrDefault();
                if (counts != null)
                    survey.TotalResponses = counts.Count;
            }
            return publishedSurveys;
        }

        internal Survey GetPbSurveyWithRespCount(long id)
        {
            var dbsurvey = from s in dbContext.Surveys.Include(x => x.Modules)
                           where s.SurveyID == id
                           && s.Status != "DELETED"
                           && s.IsPublished == true
                           select s;

            if (dbsurvey.Count() <= 0)
                return null;

            Survey survey = dbsurvey.First<Survey>();

            if (survey.SurveyData != null)
                survey.SurveyJSONData = JToken.Parse(survey.SurveyData);
            else
                survey.SurveyJSONData = JToken.Parse("{}");

            if (survey.OrgSubData != null)
                survey.OrgSubJSONData = JToken.Parse(survey.OrgSubData);
            else
                survey.OrgSubJSONData = JToken.Parse("{}");

            //Sort based on the module position
            survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();

            foreach (Module module in survey.Modules)
            {

                if (module.ModuleData != null)
                    module.ModuleJSONData = JToken.Parse(module.ModuleData);
            }

            var counts = dbContext.SurveyResponses.Where(x => x.Survey.SurveyID == survey.SurveyID).GroupBy(x => x.Survey)
                    .Select(g => new { g.Key, Count = g.Count() }).SingleOrDefault();
            if (counts != null)
                survey.TotalResponses = counts.Count;

            return survey;
           
        }

        internal List<SurveyResponse> GetResponsesBySurvey(long id)
        {
            List<SurveyResponse> surveyResponses = dbContext.SurveyResponses.Include("Survey").Where(x => x.Survey.SurveyID == id).ToList();
            foreach (SurveyResponse sr in surveyResponses)
            {
                sr.SurveyTitle = sr.Survey.SurveyTitle;
                if (sr.Survey.SurveyData != null)
                    sr.SurveyJSONData = JToken.Parse(sr.Survey.SurveyData);
            }
            return surveyResponses;
        }

        internal Survey GetSurveyResponseByID(int surveyResponseID)
        {
            var dbSR = (from sr in dbContext.SurveyResponses.Where(sr => sr.SurveyResponseID == surveyResponseID)
                        select new { sr, sr.ModuleResponses, sr.Survey, sr.Survey.Modules }).FirstOrDefault();

            Survey survey = dbSR.Survey;
            SurveyResponse surveyResponse = dbSR.sr;
            surveyResponse.SurveyTitle = survey.SurveyTitle;

            if (survey.SurveyData != null)
                surveyResponse.SurveyJSONData = JToken.Parse(survey.SurveyData);

            if (surveyResponse.Survey.OrgSubData != null)
                surveyResponse.OrgSubJSONData = JToken.Parse(survey.OrgSubData);

            foreach (Module module in survey.Modules)
            {
                if (module.ModuleType == QUESTION)
                {
                    ModuleResponse mr = surveyResponse.ModuleResponses.Where(m => m.Module.ModuleID == module.ModuleID).FirstOrDefault();
                    if (mr != null)
                        module.ModuleJSONData = JToken.Parse(mr.ModuleData);
                }
                else
                {
                    module.ModuleJSONData = JToken.Parse(module.ModuleData);
                }
            }

            //Sort based on the module position
            survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();

            return survey;
        }

        internal void AddSurveyResponse(Survey survey)
        {
            Survey matchedSurvey = dbContext.Surveys.Find(survey.SurveyID);

            SurveyResponse newResponse = new SurveyResponse
            {
                ResponseDate = DateTime.Now,
                Survey = matchedSurvey
            };

            newResponse.ModuleResponses = new List<ModuleResponse>();

            foreach (Module module in survey.Modules)
            {
                if (module.ModuleType == QUESTION)
                {
                    if (module.ModuleJSONData != null)
                        module.ModuleData = module.ModuleJSONData.ToString();

                    Module matchedModule = dbContext.Modules.Find(module.ModuleID);

                    ModuleResponse answerModule = new ModuleResponse
                    {
                        ModuleJSONData = module.ModuleJSONData,
                        ModuleData = module.ModuleData,
                        ModuleType = module.ModuleType,
                        Page = module.Page,
                        Position = module.Position,
                        Module = matchedModule
                    };

                    newResponse.ModuleResponses.Add(answerModule);
                }
            }

            dbContext.SurveyResponses.Add(newResponse);
            dbContext.SaveChanges();
        }
    }
}