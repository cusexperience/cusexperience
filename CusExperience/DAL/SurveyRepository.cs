﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CusExperience.Entities;
using CusExperience.DAL;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Http.OData;


namespace CusExperience.DAL
{
    public class SurveyRepository
    {
        CusExpContext dbContext = new CusExpContext();
        internal static string QUESTION = "Question";

        internal IQueryable<Survey> GetSurveys()
        {
            return dbContext.Surveys.Where(s => s.Status != "DELETED").OrderBy(s => s.IsTemplate);
        }

        internal Survey GetTemplateSurvey(string templateName)
        {
            Survey survey = null;

            if (templateName != "undefined")
            {
                var dbsurvey = from s in dbContext.Surveys.Include(x => x.Modules)
                               where s.TemplateName == templateName
                               && s.Status != "DELETED"
                               select s;

                if (dbsurvey == null)
                {
                    return null;
                }

                survey = dbsurvey.First<Survey>();
                survey.SurveyID = 0;
                survey.SurveyTitle = "";

                if (survey.SurveyData != null)
                    survey.SurveyJSONData = JToken.Parse(survey.SurveyData);
                else
                    survey.SurveyJSONData = JToken.Parse("{}");

                OrgSub orgSub = GetOrgSub();
                string jsonOrgSub = Newtonsoft.Json.JsonConvert.SerializeObject(orgSub);
                survey.OrgSubJSONData = JToken.Parse(jsonOrgSub);

                //Sort based on the module position
                survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();

                foreach (Module module in survey.Modules)
                {
                    module.ModuleID = 0;
                    if (module.ModuleData != null)
                        module.ModuleJSONData = JToken.Parse(module.ModuleData);
                }
            }
            else
            {
                survey = new Survey();
                survey.SurveyID = 0;
                survey.SurveyTitle = "";
                survey.Modules = new List<Module>();
                OrgSub orgSub = GetOrgSub();
                string jsonOrgSub = Newtonsoft.Json.JsonConvert.SerializeObject(orgSub);
                survey.OrgSubJSONData = JToken.Parse(jsonOrgSub);
            }
            return survey;
        }

        internal Survey GetSurvey(int id)
        {
            var dbsurvey = from s in dbContext.Surveys.Include(x => x.Modules)
                           where s.SurveyID == id
                           && s.Status != "DELETED"
                           select s;

            if (dbsurvey == null) return null;

            Survey survey = dbsurvey.First<Survey>();

            if (survey.SurveyData != null)
                survey.SurveyJSONData = JToken.Parse(survey.SurveyData);
            else
                survey.SurveyJSONData = JToken.Parse("{}");

            if (survey.OrgSubData != null)
                survey.OrgSubJSONData = JToken.Parse(survey.OrgSubData);
            else
                survey.OrgSubJSONData = JToken.Parse("{}");

            //Sort based on the module position
            survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();

            foreach (Module module in survey.Modules)
            {

                if (module.ModuleData != null)
                    module.ModuleJSONData = JToken.Parse(module.ModuleData);
            }
            return survey;
        }

        internal Survey GetPublishedSurvey(int id)
        {
            var dbsurvey = from s in dbContext.Surveys.Include(x => x.Modules)
                           where s.SurveyID == id
                           && s.Status != "DELETED"
                           && s.IsPublished == true
                           select s;

            if (dbsurvey.Count() <= 0)
                return null;

            Survey survey = dbsurvey.First<Survey>();

            if (survey.SurveyData != null)
                survey.SurveyJSONData = JToken.Parse(survey.SurveyData);
            else
                survey.SurveyJSONData = JToken.Parse("{}");

            if (survey.OrgSubData != null)
                survey.OrgSubJSONData = JToken.Parse(survey.OrgSubData);
            else
                survey.OrgSubJSONData = JToken.Parse("{}");

            //Sort based on the module position
            survey.Modules = survey.Modules.OrderBy(q => q.Position).ToList();

            foreach (Module module in survey.Modules)
            {

                if (module.ModuleData != null)
                    module.ModuleJSONData = JToken.Parse(module.ModuleData);
            }
            return survey;
        }

        internal void AddSurvey(Survey survey)
        {
            if (survey.SurveyJSONData != null)
                survey.SurveyData = survey.SurveyJSONData.ToString();

            if (survey.OrgSubJSONData != null)
                survey.OrgSubData = survey.OrgSubJSONData.ToString();

            foreach (Module module in survey.Modules)
            {
                if (module.ModuleJSONData != null)
                    module.ModuleData = module.ModuleJSONData.ToString();
            }

            survey.CreatedDate = DateTime.Now;
            survey = dbContext.Surveys.Add(survey);
            dbContext.SaveChanges();
        }

        internal void UpdateSurvey(Survey survey)
        {

            if (survey.SurveyJSONData != null)
                survey.SurveyData = survey.SurveyJSONData.ToString();

            if (survey.OrgSubJSONData != null)
                survey.OrgSubData = survey.OrgSubJSONData.ToString();

            foreach (Module module in survey.Modules)
            {
                if (module.ModuleJSONData != null)
                    module.ModuleData = module.ModuleJSONData.ToString();
            }

            var dbsurvey = from s in dbContext.Surveys.Include(x => x.Modules)
                           where s.SurveyID == survey.SurveyID
                           select s;

            Survey matchedSurvey = dbsurvey.First<Survey>();

            long[] presentModuleIDs = survey.Modules.Where(q => q.ModuleID != 0).Select(q => q.ModuleID).ToArray();
            var removedModules = matchedSurvey.Modules.Where(m => !presentModuleIDs.Contains(m.ModuleID));
            dbContext.Modules.RemoveRange(removedModules);
            dbContext.SaveChanges();

            using (var context = new CusExpContext())
            {
                foreach (var module in survey.Modules)
                {
                    if (module.ModuleID == 0)
                        context.Set<Module>().Add(module);
                    else
                    {
                        context.Set<Module>().Attach(module);
                        context.Entry(module).State = EntityState.Modified;
                    }
                }
                context.Set<Survey>().Attach(survey);
                context.Entry(survey).State = EntityState.Modified;
                context.SaveChanges();
            }

        }

        internal void DeleteSurvey(Survey survey)
        {
            survey.Status = "DELETED";
            dbContext.Entry(survey).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        internal bool SurveyExists(int id)
        {
            return dbContext.Surveys.Count(e => e.SurveyID == id) > 0;
        }

        internal OrgSub GetOrgSub()
        {
            var orgsub = from o in dbContext.OrgSubs.Include(x => x.TouchPoints)
                         where o.RegisteredName == "Singapore Kindness Movement"
                         select o;
            if (orgsub != null)
                return orgsub.First<OrgSub>();
            else
                return null;
        }

        internal Survey DuplicateSurvey(int surveyID)
        {
            

            var dbsurvey = from s in dbContext.Surveys.Include(x => x.Modules)
                           where s.SurveyID == surveyID
                           && s.Status != "DELETED"
                           select s;

            if (dbsurvey == null)
            {
                return null;
            }
            
            Survey survey = dbsurvey.FirstOrDefault();

            string SurveyTitle = "Copy of " + survey.SurveyTitle;

            int appendCount = 1;

            while (true)
            {
                int checkExists = dbContext.Surveys.Where(s => s.SurveyTitle == SurveyTitle && s.Status != "DELETED").Count();
                if (checkExists <= 0) break;
                SurveyTitle = appendCount == 1 ? SurveyTitle + appendCount : SurveyTitle.Substring(0, SurveyTitle.Length  - appendCount.ToString().Length) + appendCount;
                appendCount++;
            }

            Survey duplicateSurvey = new Survey()
            {
                SurveyTitle = SurveyTitle,
                SurveyData = survey.SurveyData,
                OrgSubData = survey.OrgSubData,
                Status = survey.Status,
                CreatedDate = DateTime.Now,
                Modules = new List<Module>()
            };

            foreach (Module module in survey.Modules)
            {
                Module dupModule = new Module()
                {
                    ModuleType = module.ModuleType,
                    ModuleData = module.ModuleData,
                    Position = module.Position
                };
                duplicateSurvey.Modules.Add(dupModule);
            }

            dbContext.Surveys.Add(duplicateSurvey);
            dbContext.SaveChanges();

            return duplicateSurvey;
        }
    }
}