﻿using CusExperience.ServiceContract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using CusExperience.Repositories;
using CusExperience.DAL;
using System.Data.Entity.Validation;
using System.Diagnostics;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Transactions;


namespace CusExperienceUnitTest.Tests
{
    [TestClass]
    public class UserRepositoryTest
    {
        UserRepository uRep = new UserRepository();
        CusExpContext dbContext = new CusExpContext();

        [TestInitialize]
        public void Initialize()
        {
            OrgSub orgSub = new OrgSub()
            {
                RegisteredName = "Test Organisation",
                RegisteredNumber = "TEST12345",
                BrandName = "Test",
                BrandWebsite = "www.testorg.org",
                Address = "No 10, West Street",
                City = "Clementi",
                Country = "Singapore",
                PhoneNumber = "+65 5555 5555",
                EmailAddress = "test@test.com",
                FrontImage = "",
                LogoImage = "",
                SurveyURL = "test",
                Industry = "Test Industry"
            };
            dbContext.OrgSubs.Add(orgSub);
            dbContext.SaveChanges();
        }


        [TestMethod]
        public void AddUserTest()
        {
            OrgSub addedOrgSub = dbContext.OrgSubs.Where(o => o.RegisteredName == "Test Organisation").FirstOrDefault();
            CusExpUser spAdminUser = dbContext.Users.Where(u => u.UserName == "superadmin").FirstOrDefault();
            CusExpUser newUser = new CusExpUser()
            {
                UserName = "testUser",
                FirstName = "Administrator",
                LastName = "User",
                Email = "hi@cusjo.com",
                PhoneNumber = "+65 5555 5555",
                Country = "Singapore",
                Role = "SuperAdministrator",
                Designation = "Software Engineer",
                Title = "Mr",
                OrgSubID = addedOrgSub.OrgSubID
            };
            uRep.AddUser(newUser,spAdminUser.Id);
            CusExpUser addedUser = dbContext.Users.Where(u => u.UserName == "testUser").FirstOrDefault();
            Assert.IsNotNull(addedUser, "The User is not saved");
            dbContext.Users.Remove(addedUser);
            dbContext.SaveChanges();
        }

        [TestMethod]
        public void DeleteGetUserTest()
        {
            OrgSub addedOrgSub = dbContext.OrgSubs.Where(o => o.RegisteredName == "Test Organisation").FirstOrDefault();
            CusExpUser spAdminUser = dbContext.Users.Where(u => u.UserName == "superadmin").FirstOrDefault();
            CusExpUser newUser = new CusExpUser()
            {
                UserName = "testUser1",
                FirstName = "Administrator",
                LastName = "User",
                Email = "hi@cusjo.com",
                PhoneNumber = "+65 5555 5555",
                Country = "Singapore",
                Role = "SuperAdministrator",
                Designation = "Software Engineer",
                Title = "Mr",
                OrgSubID = addedOrgSub.OrgSubID
            };
            uRep.AddUser(newUser, spAdminUser.Id);
            CusExpUser addedUser = dbContext.Users.Where(u => u.UserName == "testUser1").FirstOrDefault();
            Assert.IsNotNull(addedUser, "The User is not saved");
            CusExpUser retrievedUser = uRep.GetUser(addedUser.Id, spAdminUser.Id);
            uRep.DeleteUser(addedUser.Id, spAdminUser.Id);
            retrievedUser = uRep.GetUser(addedUser.Id, spAdminUser.Id);
            Assert.IsNull(retrievedUser, "User status is not modified");
            dbContext.Users.Remove(addedUser);
            dbContext.SaveChanges();
        }

        public string findCurrentRole(string userId)
        {
            throw new NotImplementedException();
        }

        public CusExpUser GetCurrentUser(string userId)
        {
            throw new NotImplementedException();
        }

        public object GetNewUser(int orgSubID, string userId)
        {
            throw new NotImplementedException();
        }

        public CusExpUser GetUser(string id, string userId)
        {
            throw new NotImplementedException();
        }

        public List<CusExpUser> GetUsersByOrgSub(long orgSubID, string userId)
        {
            throw new NotImplementedException();
        }

        public bool isAdministrator(string userId)
        {
            throw new NotImplementedException();
        }

        public bool isCusXPProfessional(string userId)
        {
            throw new NotImplementedException();
        }

        public bool isExecutive(string userId)
        {
            throw new NotImplementedException();
        }

        public bool isFrontLiner(string userId)
        {
            throw new NotImplementedException();
        }

        public bool isManager(string userId)
        {
            throw new NotImplementedException();
        }

        public bool isSuperAdministrator(string userId)
        {
            throw new NotImplementedException();
        }

        public void SaveUserProfile(string userProfile, string userId)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(CusExperience.Entities.CusExpUser User, string userId)
        {
            throw new NotImplementedException();
        }
    }
}
