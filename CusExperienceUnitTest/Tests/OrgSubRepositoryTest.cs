﻿using CusExperience.ServiceContract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using CusExperience.Repositories;
using CusExperience.DAL;
using System.Data.Entity.Validation;
using System.Diagnostics;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using System.Transactions;

namespace CusExperienceUnitTest.Tests
{
    [TestClass]
    public class OrgSubRepositoryTest
    {
        OrgSubRepository oRep = new OrgSubRepository();
        CusExpContext dbContext = new CusExpContext();

        [TestInitialize]
        public void InitializeTest()
        {
        }

        [TestMethod]
        public void AddorUpdateOrgSubTest()
        {
            try
            {
                CusExpUser addedUser = dbContext.Users.Where(u => u.UserName == "superadmin").FirstOrDefault();
                Assert.IsNotNull(addedUser, "Super Admin User is Null");
                OrgSub orgSub = new OrgSub()
                {
                    RegisteredName = "Test Organisation",
                    RegisteredNumber = "TEST12345",
                    BrandName = "Test",
                    BrandWebsite = "www.testorg.org",
                    Address = "No 10, West Street",
                    City = "Clementi",
                    Country = "Singapore",
                    PhoneNumber = "+65 5555 5555",
                    EmailAddress = "test@test.com",
                    FrontImage = "",
                    LogoImage = "",
                    SurveyURL = "test",
                    Industry = "Test Industry"
                };
                oRep.AddorUpdateOrgSub(orgSub, addedUser.Id);
                OrgSub savedOrgSub = dbContext.OrgSubs.Where(o => o.RegisteredName == "Test Organisation").FirstOrDefault();
                Assert.IsNotNull(savedOrgSub, "Org Sub Not Added");
                Assert.AreNotSame(orgSub, savedOrgSub, "Both objects are not same");
                dbContext.OrgSubs.Remove(savedOrgSub);
                dbContext.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                        validationErrors.Entry.Entity.GetType().FullName,
                        validationError.PropertyName,
                        validationError.ErrorMessage);
                    }
                }
            }
        }

        [TestMethod]
        public void DeleteGetOrgSubTest()
        {
            CusExpUser addedUser = dbContext.Users.Where(u => u.UserName == "superadmin").FirstOrDefault();
            Assert.IsNotNull(addedUser, "Super Admin User is Null");
            OrgSub orgSub = new OrgSub()
            {
                RegisteredName = "Test Organisation",
                RegisteredNumber = "TEST12345",
                BrandName = "Test",
                BrandWebsite = "www.testorg.org",
                Address = "No 10, West Street",
                City = "Clementi",
                Country = "Singapore",
                PhoneNumber = "+65 5555 5555",
                EmailAddress = "test@test.com",
                FrontImage = "",
                LogoImage = "",
                SurveyURL = "test",
                Industry = "Test Industry"
            };
            oRep.AddorUpdateOrgSub(orgSub,addedUser.Id);
            OrgSub savedOrgSub = dbContext.OrgSubs.Where(o => o.RegisteredName == "Test Organisation").FirstOrDefault();
            OrgSub retrieveOrgSub = oRep.GetOrgSub(savedOrgSub.OrgSubID, addedUser.Id);
            Assert.IsNotNull(retrieveOrgSub, "OrgSub is not null");
            oRep.DeleteOrgSub(savedOrgSub.OrgSubID, addedUser.Id);
            retrieveOrgSub = oRep.GetOrgSub(savedOrgSub.OrgSubID, addedUser.Id);
            Assert.IsNull(retrieveOrgSub, "OrgSub status is not modified");
            dbContext.OrgSubs.Remove(savedOrgSub);
            dbContext.SaveChanges();
        }

        public void GetOrgSubByUserTest()
        {
            throw new NotImplementedException();
        }

        public void GetOrgSubsTest()
        {
            throw new NotImplementedException();
        }

        public void GetTouchpointsTest()
        {
            throw new NotImplementedException();
        }

        public void SaveTouchpointsTest()
        {
            throw new NotImplementedException();
        }

        public void TestCleanup()
        {
        }
    }
}
