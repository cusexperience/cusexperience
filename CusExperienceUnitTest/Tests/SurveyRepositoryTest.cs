﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CusExperience.Repositories;
using CusExperience.Entities;
using CusExperience.DAL;

namespace CusExperienceUnitTest
{
    [TestClass]
    public class SurveyRepositoryTest  
    {
        CusExpContext dbContext = new CusExpContext();
        [TestInitialize]
        public void InitializeTest() {

            OrgSub orgSub = new OrgSub()
            {
                RegisteredName = "",
                RegisteredNumber = "",
                BrandName = "",
                BrandWebsite = "",
                Address = "",
                City = "",
                Country = "",
                PhoneNumber = "",
                EmailAddress = "",
                FrontImage = "",
                LogoImage = "",
                SurveyURL = "",
                Industry = ""
            };
            OrgSub savedOrgSub = dbContext.OrgSubs.Add(orgSub);

            CusExpUser anonymousUser = new CusExpUser();
            CusExpUser superAdminUser = new CusExpUser();
            CusExpUser adminUser = new CusExpUser();
            CusExpUser execUser = new CusExpUser();
            CusExpUser cusxpProfUser = new CusExpUser();
            CusExpUser managerUser = new CusExpUser();
        }

        [TestMethod]
        public void TestMethod1()
        {
            Survey survey = new Survey();
            SurveyRepository sr = new SurveyRepository();
            sr.AddorUpdateSurvey(ref survey, "sample user");
        }
    }
}
