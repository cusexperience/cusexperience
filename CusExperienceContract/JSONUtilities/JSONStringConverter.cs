﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace CusExperience.JSONUtilities
{
    public class JSONStringConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(string);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken jToken = JToken.Load(reader);
            string moduleData = JsonConvert.SerializeObject(jToken);
            if (string.IsNullOrEmpty(moduleData)) 
                moduleData = "{}";
            return moduleData;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value.ToString().ToLower().Equals("null")) {  
                writer.WriteRawValue("null");
                writer.Flush();
            }
            else { 
                var item =  JToken.Parse(value.ToString());
                writer.WriteRawValue(item.ToString());
                writer.Flush();
            }
        }
    }
}