﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.FaultContract
{
    [DataContract]
    public class CusExpFault
    {
        public CusExpFault(string reason)
        {
            // TODO: Complete member initialization
            Reason = reason;
        }

        [DataMember]
        public string Reason
        {
            get;
            set;
        }
    }
}
