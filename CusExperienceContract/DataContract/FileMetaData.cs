﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CusExperience.DataContract
{
    [DataContract]
    public class FileMetaData
    {
        [DataMember]
        public string LocalFileName { get; set; }

        [DataMember]
        public string RemoteFilePath { get; set; }

        [DataMember]
        public string FileType { get; set; }
    }
}
