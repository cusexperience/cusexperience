﻿using CusExperience.JSONUtilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.Entities
{
    [DataContract(IsReference = true)]
    [Serializable]
    public partial class Survey
    {
        /************ Unmapped Datamember fields *****************/

        [DataMember(Name = "StringPublishedDate")]
        [NotMapped]
        public string strPublishedDate
        {
            get
            {
                if (PublishedDate != null)
                    return PublishedDate.Value.ToString("dd/MM/yyyy @ hh:mm tt");
                else return "";
            }
            set
            {

            }
        }

        [DataMember(Name = "StringLastUpdatedDate")]
        [NotMapped]
        public string strLastUpdatedDate
        {
            get
            {
                if (LastUpdatedDate != null)
                    return LastUpdatedDate.Value.ToString("dd/MM/yyyy @ hh:mm tt");
                else return "";
            }
            set
            {

            }
        }

        [DataMember(Name = "StringCreatedDate")]
        [NotMapped]
        public string strCreatedDate
        {
            get
            {
                if (CreatedDate != null)
                    return CreatedDate.Value.ToString("dd/MM/yyyy @ hh:mm tt");
                else return "";
            }
            set
            {

            }
        }

        [NotMapped]
        [DataMember]
        public string FromDateTime { get; set; }

        [NotMapped]
        [DataMember]
        public string ToDateTime { get; set; }

        [NotMapped]
        [DataMember]
        public int TotalResponses { get; set; }

        [NotMapped]
        [DataMember]
        public List<SurveyResponse> SurveyResponses { get; set; }

        [NotMapped]
        [DataMember]
        public string Keywords { get; set; }

        [NotMapped]
        [DataMember]
        public string WOKeywords { get; set; }

        [NotMapped]
        [DataMember]
        public string[] SelectedTouchpoints { get; set; }

        [NotMapped]
        [DataMember]
        public string[] SelectedSubsidiaries { get; set; }

        [DataMember]
        [NotMapped]
        [JsonConverter(typeof(JSONStringConverter))]
        public string SummaryData { get; set; }

        [NotMapped]
        [DataMember]
        [JsonConverter(typeof(JSONStringConverter))]
        public string TouchpointAnalyticsData { get; set; }

        [NotMapped]
        [DataMember]
        [JsonConverter(typeof(JSONStringConverter))]
        public string OrgSubAnalyticsData { get; set; }

        [NotMapped]
        [DataMember]
        public string AppreciationCode { get; set; }

        [DataMember]
        [NotMapped]
        public bool NoResultsFound { get; set; }
    }
}
