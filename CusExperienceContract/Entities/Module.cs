﻿using CusExperience.JSONUtilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CusExperience.Entities
{
    [DataContract]
    [Serializable]
    [Table("Module")]
    public class Module
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long ModuleID { get; set; }

        [DataMember]
        [JsonConverter(typeof(JSONStringConverter))]
        public string ModuleData { get; set; }

        [DataMember]
        public string ModuleType { get; set; }

        [DataMember]
        public int Page { get; set; }

        [DataMember]
        public int Position { get; set; }

        public virtual Survey Survey { get; set; }

        public virtual List<ExpertComment> ExpertComments { get; set; }

    }
}