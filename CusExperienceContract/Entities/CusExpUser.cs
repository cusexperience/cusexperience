﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CusExperience.Entities
{
    [Table("Users")]
    public class CusExpUser : IdentityUser
    {

        [Column]
        [StringLength(100, MinimumLength = 1)]
        [Required]
        public string FirstName { get; set; }

        [Column]
        [StringLength(100, MinimumLength = 1)]
        [Required]
        public string LastName { get; set; }

        [Column]
        [Required]
        public string Title { get; set; }

        [Column]
        public string Country { get; set; }

        [Column]
        public string Designation { get; set; }

        [Column]
        public DateTime? DateCreated { get; set; }

        [Column]
        public DateTime? DateModified { get; set; }

        [Column]
        [System.ComponentModel.DefaultValue("NEW")]
        public string Status { get; set; }

        public virtual List<PreviousPassword> PreviousUserPasswords { get; set; }

        public virtual OrgSub OrgSub { get; set; }

        [NotMapped]
        public string FullName { 
            get 
                 { 
                    if (LastName != null) 
                        return FirstName + " " + LastName.ToUpper(); 
                    else return "";
                 } 
        }

        [NotMapped]
        public string Role { get; set; }

        [NotMapped]
        public List<string> AvailableRoles { get; set; }

        [NotMapped]
        public List<string> AvailableTouchpoints { get; set; }

        [NotMapped]
        public List<string> SelectedTouchpoints { get; set; }

        [NotMapped]
        public long OrgSubID { get; set; }

        [NotMapped]
        public string OrgSubName { get; set; }

        public virtual ICollection<CusExpUserTouchpoint> Touchpoints { get; set; }
    }
}