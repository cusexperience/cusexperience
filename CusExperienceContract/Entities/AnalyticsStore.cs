﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CusExperienceBL.Entities
{
    [Table("Analytics")]
    [DataContract(IsReference=true)]
    [Serializable]
    public class AnalyticsStore
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SurveyID { get; set; }

        public string AnalyticsData { get; set; }

        [DataMember(Name = "SurveyData")]
        [NotMapped]
        public JToken AnalyticsJSONData { get; set; }

        [DataMember]
        public DateTime? CollectedTime { get; set; }

        public virtual Survey Survey { get; set; }

    }
}
