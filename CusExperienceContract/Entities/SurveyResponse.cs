﻿using CusExperience.JSONUtilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CusExperience.Entities
{
    [Table("SurveyResponse")]
    [Serializable]
    [DataContract]
    public class SurveyResponse
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SurveyResponseID { get; set; }

        [DataMember]
        public DateTime? ResponseDate { get; set; }

        [DataMember(Name = "StringResponseDate")]
        [NotMapped]
        public string strResponseDate
        {
            get
            {
                return ResponseDate.Value.ToString("dd/MM/yyyy\nhh:mm tt");
            }
            set
            {

            }
        }

        [DataMember]
        [NotMapped]
        [JsonConverter(typeof(JSONStringConverter))]
        public string DemographicData { get; set; }

        [DataMember]
        [NotMapped]
        public double CusXPIndex { get; set; }

        [Required]
        public virtual Survey Survey { get; set; }

        public string Status { get; set; }

        public string AppreciationCode { get; set; }

        public virtual ICollection<ModuleResponse> ModuleResponses { get; set; }

        public OrgSub OrgSub { get; set; }

        public Touchpoint Touchpoint { get; set; }

    }
}