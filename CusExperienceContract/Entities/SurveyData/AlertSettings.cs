﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CusExperienceContract.Entities.SurveyData
{
    [Serializable]
    public class Alert {
        public string AlertType { get; set; }
        public string AlertTime { get; set; }
        public string AlertDay { get; set; }
        public int NoofResponses { get; set; }
        public string SendToRole { get; set; }
    }

    [Serializable]
    public class AlertSettings
    {
        public Alert[] Alerts { get; set; }
    }
}
