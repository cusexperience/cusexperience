﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CusExperienceContract.Entities.SurveyData
{
    public class SurveyData
    {
        public AlertSettings AlertSettings { get; set; }
        public HierarchySettings HeirarchySettings { get; set; }
    }
}
