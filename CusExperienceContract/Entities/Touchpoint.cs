﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CusExperience.Entities
{
    [DataContract(IsReference = true)]
    [JsonObject(IsReference = false)]
    [Serializable]
    public class Touchpoint
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long TouchpointID { get; set; }

        [DataMember]
        public string TouchpointName { get; set; }

        [DataMember]
        public string TouchpointURL { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string BrandWebsite { get; set; }

        [DataMember]
        public string PhoneNumber { get; set; }

        [DataMember]
        public string EmailAddress { get; set; }

        [DataMember]
        public string FrontImage { get; set; }

        [DataMember]
        public string LeftPos { get; set; }

        [DataMember]
        public string TopPos { get; set; }

        [DataMember]
        public string SliderValue { get; set; }

        [DataMember]
        public string LogoImage { get; set; }

        [DataMember]
        public int Position { get; set; }

        public virtual OrgSub OrgSub { get; set; }

        public virtual ICollection<CusExpUserTouchpoint> Users { get; set; }

    }
}