﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace CusExperience.Entities
{
    [DataContract(IsReference = true)]
    [Serializable]
    [Table("OrgSub")]
    public class OrgSub
    {

        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long OrgSubID { get; set; }

        [DataMember]
        [Required]
        public string RegisteredName { get; set; }

        [DataMember]
        [Required]
        public string RegisteredNumber { get; set; }

        [DataMember]
        [Required]
        public string BrandName { get; set; }

        [DataMember]
        [Required]
        public string BrandWebsite { get; set; }

        [DataMember]
        [Required]
        public string Industry { get; set; }

        [DataMember]
        [Required]
        public string Address { get; set; }

        [DataMember]
        [Required]
        public string Country { get; set; }

        [DataMember]
        [Required]
        public string EmailAddress { get; set; }

        [DataMember]
        [Required]
        public string PhoneNumber { get; set; }

        [DataMember]
        [NotMapped]
        public string BaseURL { get; set; }

        [DataMember]
        [NotMapped]
        public string HierarchyURL { get; set; }

        [DataMember]
        public string SurveyURL { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string FrontImage { get; set; }

        [DataMember]
        public string LogoImage { get; set; }

        [DataMember]
        public string LeftPos { get; set; }

        [DataMember]
        public string TopPos { get; set; }

        [DataMember]
        public string SliderValue { get; set; }

        [DataMember]
        public virtual OrgSub ParentOrgSub { get; set; }

        [DataMember]
        public string Status { get; set; }

        [NotMapped]
        [DataMember]
        public long ParentOrgSubID { get; set; }

        [NotMapped]
        [DataMember]
        public string ParentOrgSubName { get; set; }

        [NotMapped]
        [DataMember]
        public int HierarchyLevel { get; set; }

        [DataMember]
        public virtual List<Touchpoint> Touchpoints { get; set; }

        [DataMember]
        public virtual List<OrgSub> Subsidiaries { get; set; }

    }
}