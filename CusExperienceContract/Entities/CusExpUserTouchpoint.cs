﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CusExperience.Entities
{
    [Table("CusExpUserTouchpoint")]
    public class CusExpUserTouchpoint
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public Touchpoint Touchpoint { get; set; }

        public CusExpUser User { get; set; }
    }
}
