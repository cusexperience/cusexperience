﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.Entities
{
    [Table("Support")]
    public class Support
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long SupportID { get; set; }

        [DataMember]
        public string SupportPage { get; set; }

        [DataMember]
        public string SupportURL { get; set; }

        public virtual OrgSub OrgSub{ get; set; }

        public string SupportEmailIDs { get; set; }
    }
}
