﻿using CusExperience.JSONUtilities;
using CusExperienceContract.Entities.SurveyData;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace CusExperience.Entities
{
    [Table("Survey")]
    public partial class Survey
    {
        [DataMember]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SurveyID { get; set; }

        [DataMember]
        public string SurveyTitle { get; set; }

        [DataMember]
        [JsonConverter(typeof(JSONStringConverter))]
        public string SurveyData { get; set; }

        [NotMapped]
        public SurveyData JsonSurveyData
        {
            get
            {
                if(SurveyData != null)
                    return JsonConvert.DeserializeObject<SurveyData>(SurveyData);
                return null;
            }
        }

        [DataMember]
        public DateTime? CreatedDate { get; set; }

        [DataMember]
        public DateTime? LastUpdatedDate { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string TemplateName { get; set; }

        [DataMember]
        public DateTime? PublishedDate { get; set; }

        [DataMember]
        public string PublishedID { get; set; }

        [DataMember]
        public string PublishPage { get; set; }

        [DataMember]
        public virtual OrgSub OrgSub { get; set; }

        [DataMember]
        public virtual List<Module> Modules { get; set; }

        public virtual CusExpUser CreatedBy { get; set; }

        public virtual CusExpUser LastUpdatedBy { get; set; }

        [DataMember]
        public virtual List<ExpertComment> ExpertComments { get; set; }

        /*********** Boolean Valued Fields ********************/

        [DataMember]
        public bool IsTemplate { get; set; }

        [DataMember]
        public bool IsPredefined { get; set; }

        [DataMember]
        public bool IsPublished { get; set; }

        [DataMember]
        public bool IsAllowViewResults { get; set; }

        [DataMember]
        public bool IsAllowViewSocial { get; set; }

        [DataMember]
        public bool isSendAppreciationCode { get; set; }

    }
}
