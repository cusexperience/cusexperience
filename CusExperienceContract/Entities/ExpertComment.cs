﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CusExperience.Entities
{
    [DataContract(IsReference = true)]
    [Serializable]
    [Table("ExpertComment")]
    public class ExpertComment
    {
        [DataMember]
        public long ExpertCommentID { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public DateTime? CommentDate { get; set; }

        [DataMember(Name = "StringCommentDate")]
        [NotMapped]
        public string strPublishedDate
        {
            get
            {
                if (CommentDate != null)
                    return CommentDate.Value.ToString("dd/MM/yyyy @ hh:mm tt");
                else return "";
            }
            set
            {

            }
        }

        [DataMember]
        public string ModuleInfo { get; set; }

        [DataMember]
        public string Status { get; set; }

        public CusExpUser User { get; set; }

        public Module Module { get; set; }

        public Survey Survey { get; set; }

        [DataMember]
        [NotMapped]
        public long ModuleID {  get;set; }

        [DataMember]
        [NotMapped]
        public long SurveyID { get; set; }

        [DataMember]
        [NotMapped]
        public string UserName { get; set; }
    }
}
