﻿using CusExperience.DataContract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.MessageContract
{
    [MessageContract]
    public class FileDownloadReturnMessage
    {
        [MessageHeader(MustUnderstand = true)]
        public FileMetaData DownloadedFileMetadata { get; set; }

        [MessageBodyMember(Order = 1)]
        public byte[] FileByteStream { get; set; }
    }
}
