﻿using CusExperience.Entities;
using CusExperience.Exceptions;
using CusExperience.FaultContract;
using System;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CusExperience.ServiceContract
{
    [ServiceContract]
    public interface IAccountRepository
    {

        [OperationContract]
        [FaultContract(typeof(CusExpFault))]
        void ChangePassword(string changePasswordObj, string userId);

        [OperationContract]
        void ForgotPassword(string forgorPwdObj);

        [OperationContract]
        [FaultContract(typeof(CusExpFault))]
        ClaimsIdentity LoginUser(string loginObj);

        [OperationContract]
        [FaultContract(typeof(CusExpFault))]
        void ResetPassword(string resetPasswordObj);

        [OperationContract]
        Task submitContactForm(string contactFormObj);

        [OperationContract]
        Task submitSupportForm(string supportObj);
    }
}
