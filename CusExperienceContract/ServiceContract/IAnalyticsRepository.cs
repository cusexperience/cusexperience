﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using CusExperience.Entities;


namespace CusExperience.ServiceContract
{
    [ServiceContract]
    public interface IAnalyticsRepository
    {
        [OperationContract]
        Survey GetAnalytics(int surveyId, long orgSubID, string userId);

        [OperationContract]
        Dictionary<long, string> GetComments(string commentsObj, string userId);

        [OperationContract]
        Dictionary<long, string> GetCommentsByTag(string tagsObj, string userId);

        [OperationContract]
        Survey GetMatchedResponses(int surveyID, int skip, int pageSize, string orderByDirection, string filterKeyword, string cvData, string userId);

        [OperationContract]
        Survey GetQuantitativeAnalytics(string url);

        [OperationContract]
        Survey GetSearchAnalytics(Survey searchSurvey, string userId);

        [OperationContract]
        List<ExpertComment> DeleteExpertComment(long ExpertCommentID, string userId);

        [OperationContract]
        List<ExpertComment> AddExpertComment(string expertCommentData, string userId);

    }
}
