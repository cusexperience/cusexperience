﻿using System;
using System.ServiceModel;
using System.Collections.Generic;

namespace CusExperience.ServiceContract
{
    [ServiceContract]
    public interface IStaffRepository
    {
        [OperationContract]
        List<string> GetUserNames(long orgSubID, string touchpointName, string startsWith);
    }
}
