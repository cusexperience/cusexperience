﻿using CusExperience.MessageContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CusExperience.ServiceContract
{
    [ServiceContract]
    public interface IFileRepository
    {
        [OperationContract]
        void UploadFile(FileUploadMessage request);

        [OperationContract]
        FileDownloadReturnMessage DownloadFile(FileDownloadMessage request);
    }
}
