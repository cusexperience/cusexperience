﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using CusExperience.Entities;

namespace CusExperience.ServiceContract
{
    [ServiceContract]
    public interface IUserRepository
    {
        [OperationContract]
        void AddUser(CusExpUser User, string userId);

        [OperationContract]
        void DeleteUser(string id, string userId);

        [OperationContract]
        string findCurrentRole(string userId);

        [OperationContract]
        CusExpUser GetCurrentUser(string userId);

        [OperationContract]
        CusExpUser GetNewUser(long orgSubID, string userId);

        [OperationContract]
        CusExpUser GetUser(string id, string userId);

        [OperationContract]
        List<CusExpUser> GetUsersByOrgSub(long orgSubID, string userId);

        [OperationContract]
        bool isAdministrator(string userId);

        [OperationContract]
        bool isCusXPProfessional(string userId);

        [OperationContract]
        bool isExecutive(string userId);

        [OperationContract]
        bool isFrontLiner(string userId);

        [OperationContract]
        bool isManager(string userId);

        [OperationContract]
        bool isSuperAdministrator(string userId);

        [OperationContract]
        void SaveUserProfile(string userProfile, string userId);

        [OperationContract]
        void UpdateUser(CusExpUser User, string userId);
    }
}
