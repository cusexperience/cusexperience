﻿using System;
using System.ServiceModel;
using System.Threading.Tasks;
using CusExperience.Entities;

namespace CusExperience.ServiceContract
{
    [ServiceContract]
    public interface ISupportRepository
    {
        [OperationContract]
        Support GetSupport(string supportURL);

        [OperationContract]
        Task SendSupport(string id, string supportObj);
    }
}
