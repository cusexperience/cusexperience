﻿using System;
using System.ServiceModel;

namespace CusExperience.ServiceContract
{
    [ServiceContract]
    public interface IQuartzScheduler
    {
        [OperationContract]
        void AddNewEmailAlert();
    }
}
