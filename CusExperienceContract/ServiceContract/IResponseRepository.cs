﻿using System;
using System.ServiceModel;
using System.Collections.Generic;
using CusExperience.Entities;

namespace CusExperience.ServiceContract
{
    [ServiceContract]
    public interface IResponseRepository
    {
        [OperationContract]
        void AddSurveyResponse(Survey survey);

        [OperationContract(Name = "DeleteReponse")]
        void DeleteResponse(int surveyResponseID, string userId);

        [OperationContract]
        Survey GetResponsesByResponseIds(int surveyId, string userId, int skip, int pageSize, string orderByDirection, string filterKeyword, System.Collections.Generic.List<long> surveyResponseIds);

        [OperationContract]
        Survey GetResponsesBySurvey(int surveyId, long orgSubId, string userId, int skip, int pageSize, string orderByDirection, string filterKeyword);

        [OperationContract]
        Survey GetSurveyForIV(long surveyId, long orgSubId, string userId);

        [OperationContract]
        Survey GetSurveyForSV(int surveyId, long orgSubId, string userId);

        [OperationContract]
        Survey GetSurveyResponseByID(long surveyResponseID,  string userId);

        [OperationContract]
        byte[] writeResponsetoExcel(int surveyId, string userId);

        [OperationContract]
        List<Survey> GetPbSurveysWithRespCount(string userId);

        [OperationContract]
        List<Survey> GetPbSurveysWithRespCountByOrgSub(long orgSubId, string userId);
    }
}
