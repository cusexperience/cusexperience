﻿using System;
using System.ServiceModel;
using CusExperience.Entities;
using System.Collections.Generic;


namespace CusExperience.ServiceContract
{
    [ServiceContract]
    public interface IOrgSubRepository
    {
        [OperationContract]
        void AddorUpdateOrgSub(OrgSub OrgSub,string userId);

        [OperationContract]
        void DeleteOrgSub(long orgSubID, string userId);

        [OperationContract]
        OrgSub GetNewOrgSub(long parentOrgSubID, string userId);

        [OperationContract]
        OrgSub GetOrgSub(long orgSubID, string userId);

        [OperationContract]
        List<OrgSub> GetOrgSubs(string userId);

        [OperationContract]
        List<Touchpoint> GetTouchpoints(long orgSubID, string userId);

        [OperationContract]
        void SaveTouchpoints(string userId, long orgSubID, List<Touchpoint> Touchpoints);
    }
}
