﻿using CusExperience.Entities;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace CusExperience.ServiceContract
{
    [ServiceContract]
    public interface ISurveyRepository
    {
        [OperationContract]
        void AddorUpdatePublishedSurvey(ref Survey survey, string userId);

        [OperationContract]
        void AddorUpdateSurvey(ref Survey survey, string userId);

        [OperationContract]
        void DeleteSurvey(Survey survey, string userId);

        [OperationContract]
        Survey DuplicateSurvey(int surveyId, string userId);

        [OperationContract]
        Survey GetPublishedSurvey(string url);

        [OperationContract]
        Survey GetSurvey(int surveyId, string userId);

        [OperationContract]
        List<Survey> GetSurveysByOrgSub(long orgSubId, string userId);

        [OperationContract]
        List<Survey> GetSurveys(string userId);

        [OperationContract]
        Survey GetTemplateSurvey(string templateName, string userId);

        [OperationContract]
        Survey GetTemplateSurveyByOrgSub(long orgSubId, string templateName, string userId);

        [OperationContract]
        bool IsSurveyExists(int surveyId, string userId);

        [OperationContract]
        void UnPublishedSurvey(int surveyId, string userId);

        [OperationContract]
        List<string> GetAttachmentExtensions(int surveyId, long moduleId);

        [OperationContract]
        void UpdatePublishID(int surveyId, string publishID, string userId);
    }
}
