﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CusExperience.Exceptions
{
    [Serializable]
    public class CusExpException: Exception
    {
        public CusExpException(string Message): base(Message)
        {

        }
    }
}