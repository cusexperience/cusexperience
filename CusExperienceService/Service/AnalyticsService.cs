﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using CusExperience.Repositories;
using CusExperience.ServiceContract;
using System.ServiceModel;

namespace CusExperience.Service
{
    public partial class CusExperienceService : IAnalyticsRepository
    {
        AnalyticsRepository aRep = new AnalyticsRepository();

        public Survey GetAnalytics(int surveyID, long orgSubID,  string userId)
        {
            return aRep.GetAnalytics(surveyID, orgSubID, userId);
        }

        public Survey GetMatchedResponses(int surveyID, int skip, int pageSize, string orderByDirection, string filterKeyword, string cvData, string userId)
        {
            return aRep.GetMatchedResponses(surveyID, skip, pageSize, orderByDirection, filterKeyword,  cvData, userId);
        }

        public Survey GetQuantitativeAnalytics(string url)
        {
            return aRep.GetQuantitativeAnalytics(url);
        }

        public Dictionary<long, string> GetComments(string commentsObj, string userId)
        {
            return aRep.GetComments(commentsObj, userId);
        }

        public Dictionary<long, string> GetCommentsByTag(string tagsObj, string userId)
        {
            return aRep.GetCommentsByTag(tagsObj, userId);
        }

        public Survey GetSearchAnalytics(Survey searchSurvey, string userId)
        {
            return aRep.GetSearchAnalytics(searchSurvey, userId);
        }

        public List<ExpertComment> DeleteExpertComment(long ExpertCommentID, string userId)
        {
            return aRep.DeleteExpertComment(ExpertCommentID, userId);
        }

        public List<ExpertComment> AddExpertComment(string expertCommentData, string userId)
        {
            return aRep.SaveExpertComment(expertCommentData, userId);
        }
    }
}
