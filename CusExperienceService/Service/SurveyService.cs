﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using CusExperience.Repositories;
using CusExperience.ServiceContract;

namespace CusExperience.Service
{
    public partial class CusExperienceService : ISurveyRepository
    {
        SurveyRepository sRep = new SurveyRepository();

        public void AddorUpdatePublishedSurvey(ref Survey survey, string userId)
        {
            sRep.AddorUpdatePublishedSurvey(ref survey, userId);
        }

        public void AddorUpdateSurvey(ref Survey survey, string userId)
        {
            sRep.AddorUpdateSurvey(ref survey, userId);
        }

        public void DeleteSurvey(Survey survey, string userId)
        {
            sRep.DeleteSurvey(survey, userId);
        }

        public Survey DuplicateSurvey(int surveyId, string userId)
        {
            return sRep.DuplicateSurvey(surveyId, userId);
        }

        public Survey GetPublishedSurvey(string url)
        {
            return sRep.GetPublishedSurvey(url);
        }

        public Survey GetSurvey(int surveyId, string userId)
        {
            return sRep.GetSurvey(surveyId, userId);
        }

        public List<Survey> GetSurveys(string userId)
        {
            return sRep.GetSurveys(userId);
        }

        public List<Survey> GetSurveysByOrgSub(long orgSubId, string userId)
        {
            return sRep.GetSurveysByOrgSub(orgSubId, userId);
        }

        public Survey GetTemplateSurvey(string templateName, string userId)
        {
            return sRep.GetTemplateSurvey(templateName, userId);
        }

        public Survey GetTemplateSurveyByOrgSub(long orgSubId, string templateName, string userId)
        {
            return sRep.GetTemplateSurveyByOrgSub(orgSubId, templateName, userId);
        }

        public void UnPublishedSurvey(int surveyId, string userId)
        {
            sRep.UnPublishedSurvey(surveyId, userId);
        }

        public bool IsSurveyExists(int surveyId, string userId)
        {
            return sRep.IsSurveyExists(surveyId, userId);
        }

        public List<string> GetAttachmentExtensions(int surveyId, long moduleId)
        {
            return sRep.GetAttachmentExtensions(surveyId, moduleId);
        }

        public void UpdatePublishID(int surveyId, string publishID, string userId)
        {
            sRep.UpdatePublishID(surveyId, publishID, userId);
        }
    }
}
