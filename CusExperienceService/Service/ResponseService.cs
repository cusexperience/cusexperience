﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using CusExperience.Repositories;
using CusExperience.ServiceContract;

namespace CusExperience.Service
{
    public partial class CusExperienceService : IResponseRepository
    {
        ResponseRepository rRep = new ResponseRepository();

        public void AddSurveyResponse(Survey survey)
        {
            rRep.AddSurveyResponse(survey);
        }

        public void DeleteResponse(int surveyResponseID, string userId)
        {
            rRep.DeleteSurveyResponse(surveyResponseID, userId);
        }

        public List<Survey> GetPbSurveysWithRespCount(string userId)
        {
            return rRep.GetPbSurveysWithRespCount(userId);
        }

        public List<Survey> GetPbSurveysWithRespCountByOrgSub(long orgSubId, string userId)
        {
            return rRep.GetPbSurveysWithRespCountByOrgSub(orgSubId,userId);
        }

        public Survey GetResponsesByResponseIds(int surveyId, string userId, int skip, int pageSize, string orderByDirection, string filterKeyword, List<long> surveyResponseIds)
        {
            return rRep.GetResponsesByResponseIds(surveyId, userId, skip, pageSize, orderByDirection, filterKeyword, surveyResponseIds);
        }

        public Survey GetResponsesBySurvey(int surveyId, long orgSubId, string userId, int skip, int pageSize, string orderByDirection, string filterKeyword)
        {
            return rRep.GetResponsesBySurvey(surveyId, orgSubId, userId, skip, pageSize, orderByDirection, filterKeyword);
        }

        public Survey GetSurveyForIV(long surveyId, long orgSubId, string userId)
        {
            return rRep.GetSurveyForIV(surveyId, orgSubId, userId);
        }

        public Survey GetSurveyForSV(int surveyId, long orgSubId, string userId)
        {
            return rRep.GetSurveyForIV(surveyId, orgSubId, userId);
        }

        public Survey GetSurveyResponseByID(long surveyResponseID, string userId)
        {
            return rRep.GetSurveyResponseByID(surveyResponseID, userId);
        }

        public byte[] writeResponsetoExcel(int surveyId, string userId)
        {
            return rRep.writeResponsetoExcel(surveyId, userId);
        }
    }
}
