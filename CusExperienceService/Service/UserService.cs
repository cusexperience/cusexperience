﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using CusExperience.Repositories;
using CusExperience.ServiceContract;

namespace CusExperience.Service
{
    public partial class CusExperienceService : IUserRepository
    {
        UserRepository uRep = new UserRepository();
 
        public void AddUser(CusExpUser User, string userId)
        {
            uRep.AddUser(User, userId);
        }

        public void DeleteUser(string id, string userId)
        {
            uRep.DeleteUser(id, userId);
        }

        public string findCurrentRole(string userId)
        {
            return uRep.findCurrentRole(userId);
        }

        public CusExpUser GetCurrentUser(string userId)
        {
            return uRep.GetCurrentUser(userId);
        }

        public CusExpUser GetNewUser(long orgSubID, string userId)
        {
            return uRep.GetNewUser(orgSubID, userId);
        }

        public CusExpUser GetUser(string id, string userId)
        {
            return uRep.GetUser(id, userId);
        }

        public List<CusExpUser> GetUsersByOrgSub(long orgSubID, string userId)
        {
            return uRep.GetUsersByOrgSub(orgSubID, userId);
        }

        public bool isAdministrator(string userId)
        {
            return uRep.isAdministrator(userId);
        }

        public bool isCusXPProfessional(string userId)
        {
            return uRep.isCusXPProfessional(userId);
        }

        public bool isExecutive(string userId)
        {
            return uRep.isExecutive(userId);
        }

        public bool isFrontLiner(string userId)
        {
            return uRep.isFrontLiner(userId);
        }

        public bool isManager(string userId)
        {
            return uRep.isManager(userId);
        }

        public bool isSuperAdministrator(string userId)
        {
            return uRep.isSuperAdministrator(userId);
        }

        public void SaveUserProfile(string userProfile, string userId)
        {
             uRep.SaveUserProfile(userProfile, userId);
        }

        public void UpdateUser(CusExpUser User, string userId)
        {
            uRep.UpdateUser(User, userId);
        }
    }
}
