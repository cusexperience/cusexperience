﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using CusExperience.Repositories;
using CusExperience.ServiceContract;

namespace CusExperience.Service
{
    public partial class CusExperienceService : ISupportRepository
    {
        SupportRepository spRep = new SupportRepository();

        public Support GetSupport(string supportURL)
        {
            return spRep.GetSupport(supportURL);
        }

        public Task SendSupport(string id, string supportObj)
        {
            return spRep.SendSupport(id, supportObj);
        }
    }
}
