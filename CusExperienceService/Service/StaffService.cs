﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using CusExperience.Repositories;
using CusExperience.ServiceContract;

namespace CusExperience.Service
{
    public partial class CusExperienceService : IStaffRepository
    {
        StaffRepository dRep = new StaffRepository();

        public List<string> GetUserNames(long orgSubID, string touchpointName, string startsWith)
        {
            return dRep.GetUserNames(orgSubID, touchpointName, startsWith);
        }
    }
}
