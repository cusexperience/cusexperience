﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Repositories;
using System.Runtime.Serialization;
using CusExperience.Entities;
using CusExperience.ServiceContract;
using CusExperience.Exceptions;
using System.ServiceModel;
using CusExperience.FaultContract;
using CusExperience.Utilities;


namespace CusExperience.Service
{
    public partial class CusExperienceService: IAccountRepository
    {
        AccountRepository accRep = new AccountRepository();

        public CusExperienceService()
        {
            QuartzScheduler.StartInstance();
        }

        public void ChangePassword(string changePasswordObj, string userId)
        {
            try 
            { 
                accRep.ChangePassword(changePasswordObj, userId);
            }
            catch (Exception ex)
            {
                if (ex is CusExpException)
                {
                    throw new FaultException<CusExpFault>(new CusExpFault(ex.Message));
                }
                throw ex;
            }
        }

        public void ForgotPassword(string forgorPwdObj)
        {
            accRep.ForgotPassword(forgorPwdObj);
        }

        public System.Security.Claims.ClaimsIdentity LoginUser(string loginObj)
        {
            try
            {
                return accRep.LoginUser(loginObj);
            }
            catch (Exception ex)
            {
                if (ex is CusExpException)
                {
                    throw new FaultException<CusExpFault>(new CusExpFault(ex.Message));
                }
                throw ex;
            }
        }

        public void ResetPassword(string resetPasswordObj)
        {
            try 
            {
                accRep.ResetPassword(resetPasswordObj);
            }
            catch (Exception ex)
            {
                if (ex is CusExpException)
                {
                    throw new FaultException<CusExpFault>(new CusExpFault(ex.Message));
                }
                throw ex;
            }
        }

        public Task submitContactForm(string contactFormObj)
        {
            return accRep.SubmitContactForm(contactFormObj);
        }

        public Task submitSupportForm(string supportObj)
        {
            return accRep.SubmitSupportForm(supportObj);
        }

    }
}
