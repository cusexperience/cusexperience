﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.Entities;
using CusExperience.Repositories;
using CusExperience.ServiceContract;

namespace CusExperience.Service
{
    public partial class CusExperienceService : IOrgSubRepository
    {
        OrgSubRepository oRep = new OrgSubRepository();

        public void AddorUpdateOrgSub( OrgSub OrgSub, string userId)
        {
            oRep.AddorUpdateOrgSub(OrgSub, userId);
        }

        public void DeleteOrgSub(long orgSubID, string userId)
        {
            oRep.DeleteOrgSub(orgSubID, userId);
        }

        public OrgSub GetNewOrgSub(long parentOrgSubID, string userId)
        {
            return oRep.GetNewOrgSub(parentOrgSubID, userId);
        }

        public OrgSub GetOrgSub(long orgSubID, string userId)
        {
            return oRep.GetOrgSub(orgSubID, userId);
        }

        public List<OrgSub> GetOrgSubs(string userId)
        {
            return oRep.GetOrgSubs(userId);
        }

        public List<Touchpoint> GetTouchpoints(long orgSubID, string userId)
        {
            return oRep.GetTouchpoints(orgSubID, userId);
        }

        public void SaveTouchpoints(string userId, long orgSubID, List<Touchpoint> Touchpoints)
        {
            oRep.SaveTouchpoints(userId, orgSubID, Touchpoints);
        }
    }
}
