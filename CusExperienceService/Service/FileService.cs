﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CusExperience.ServiceContract;
using CusExperience.MessageContract;
using CusExperience.DataContract;
using CusExperience.Repositories;

namespace CusExperience.Service
{
    public partial class CusExperienceService: IFileRepository
    {
        FileRepository fr = new FileRepository();

        public void UploadFile(FileUploadMessage request)
        {
            fr.UploadFile(request);
        }

        public FileDownloadReturnMessage DownloadFile(FileDownloadMessage request)
        {
            return fr.DownloadFile(request);
        }
    }
}
