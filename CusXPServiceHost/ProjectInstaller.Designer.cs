﻿namespace CusXPServiceHost
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CusXPServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.CusXPServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // CusXPServiceProcessInstaller
            // 
            this.CusXPServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.CusXPServiceProcessInstaller.Password = null;
            this.CusXPServiceProcessInstaller.Username = null;
            // 
            // CusXPServiceInstaller
            // 
            this.CusXPServiceInstaller.ServiceName = "CusXPServiceHost";
            this.CusXPServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.CusXPServiceProcessInstaller,
            this.CusXPServiceInstaller});

        }

        #endregion

        public System.ServiceProcess.ServiceProcessInstaller CusXPServiceProcessInstaller;
        public System.ServiceProcess.ServiceInstaller CusXPServiceInstaller;

    }
}